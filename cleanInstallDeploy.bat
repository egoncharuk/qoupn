call mvn clean install
echo Exit Code = %ERRORLEVEL%
if not "%ERRORLEVEL%" == "0" exit /b

call mvn jetty:run-war
echo Exit Code = %ERRORLEVEL%
if not "%ERRORLEVEL%" == "0" exit /b