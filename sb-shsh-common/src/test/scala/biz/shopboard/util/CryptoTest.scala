package biz.shopboard.util

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CryptoTest extends FunSuite {
   test("Test for null/empty values") {
     assert(Crypto.encryptAES(null).equals(""), "decrypt returns non-empty value")
     assert(Crypto.decryptAES(null).equals(""), "decrypt returns non-empty value")
     assert(Crypto.encryptAES("").equals(""), "encrypt returns non-empty value")
     assert(Crypto.decryptAES("").equals(""), "encrypt returns non-empty value")
   }

   test("Encodning/Decoding test") {
     val testValue: String = "testtesttest"
     val encryption: String = Crypto.encryptAES(testValue)
     assert(Crypto.decryptAES(encryption).equals(testValue), "test value is not matching descrypted value")
   }
}
