package biz.shopboard.rest

import com.fasterxml.jackson.module.scala.{JacksonModule, DefaultScalaModule}
import unfiltered.response.{ ResponseString, ContentType, ComposeResponse }
import com.fasterxml.jackson.databind.{ SerializationFeature, ObjectMapper }
import com.fasterxml.jackson.annotation.{JsonProperty, JsonInclude}
import com.fasterxml.jackson.datatype.joda.JodaModule
import com.fasterxml.jackson.databind.introspect.{AnnotatedMember, NopAnnotationIntrospector}

trait JsonHelper {

  val mapper = new ObjectMapper()
    .enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
    .enable(SerializationFeature.INDENT_OUTPUT)
    .registerModule(new DefaultScalaModule())
    .registerModule(new RequiredPropertiesSchemaModule {})
    .registerModule(new JodaModule())

  def toJson(o: Any): String = {
    mapper.writeValueAsString(o)
  }

  implicit def toOption[String](x: String) : Option[String] = Option(x)

  case class ResponseJson(a: AppResponse) extends ComposeResponse(
    ContentType("application/json") ~> ResponseString(mapper.writeValueAsString(a)))

}

object DefaultRequiredAnnotationIntrospector extends NopAnnotationIntrospector {

  private val OPTION = classOf[Option[_]]

  private def isOptionType(cls: Class[_]) = OPTION.isAssignableFrom(cls)

  override def hasRequiredMarker(m: AnnotatedMember) = boolean2Boolean(
    Option(m.getAnnotation(classOf[JsonProperty])).map(_.required).getOrElse(!isOptionType(m.getRawType))
  )

}

trait RequiredPropertiesSchemaModule extends JacksonModule {
  this += { _.insertAnnotationIntrospector(DefaultRequiredAnnotationIntrospector) }
}
