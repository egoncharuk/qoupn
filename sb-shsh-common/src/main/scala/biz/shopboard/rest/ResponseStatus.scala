package biz.shopboard.rest

object ResponseStatus {
  val OK = "ok"
  val ERROR = "error"
}
