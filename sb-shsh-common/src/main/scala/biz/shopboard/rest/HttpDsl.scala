package biz.shopboard.rest

import com.sun.jersey.api.client.{ Client, ClientResponse }
import com.sun.jersey.api.client.config.{ DefaultClientConfig, ClientConfig }
import javax.ws.rs.HttpMethod
import javax.ws.rs.core.{ NewCookie, MediaType }
import net.liftweb.http.rest.RestHelper
import net.liftweb.json.Implicits
import org.scalatest.matchers.{ ShouldMatchers, MustMatchers }
import scala.collection.JavaConversions._
import org.slf4j.LoggerFactory

trait HttpDsl extends Implicits with JsonHelper {

  self: ShouldMatchers with MustMatchers =>

  def checkError(json: String, error: ErrorInformation) {
    val resp: AppResponse = mapper.readValue(json, classOf[AppResponse])
    resp.errorinfo must equal(error)
    resp.status must equal(ResponseStatus.ERROR)
    resp.data must equal(null)
  }

  def checkNoError(json: String) {
    val resp: AppResponse = mapper.readValue(json, classOf[AppResponse])
    resp.status must equal(ResponseStatus.OK)
    resp.errorinfo must equal(null)
  }

  def checkResponseCode(response: ClientResponse, expectedStatus: Int) {
    response.getStatus must equal(expectedStatus)
  }

  def get(url: String): RichHttp = new RichHttp(url, HttpMethod.GET, this)

  def put(url: String): RichHttp = new RichHttp(url, HttpMethod.PUT, this)

  def post(url: String): RichHttp = new RichHttp(url, HttpMethod.POST, this)
}

class RichHttp(url: String, httpMethod: String, httpDsl: HttpDsl) extends RestHelper with Implicits with JsonHelper {

  val log = LoggerFactory.getLogger("biz.shopboard.rest.RichHttp")

  var requestParameters: Option[AnyRef] = None
  var requestCookies: List[NewCookie] = List()
  private val bodyFormat = MediaType.APPLICATION_JSON
  var response: ClientResponse = _
  var bodyString: String = _

  def responseCookies() = {
    response.getCookies.toList
  }

  def withJsonData(parameters: Any) = {
    if (parameters.isInstanceOf[String]) {
      requestParameters = Some(parameters.asInstanceOf[String])
    } else {
      requestParameters = Some(toJson(parameters))
    }
    this
  }

  def withCookie(cookie: NewCookie) = {
    requestCookies = cookie :: requestCookies
    this
  }

  def withCookiesFrom(other: RichHttp) = {
    requestCookies = other.response.getCookies.toList ::: requestCookies
    this
  }

  def withCookies(cookies: List[NewCookie]) = {
    requestCookies = cookies
    this
  }

  def unary_~ = {
    val clientConfig: ClientConfig = new DefaultClientConfig
    clientConfig.getFeatures.put("com.sun.jersey.api.json.POJOMappingFeature", true)

    val webResource = Client.create(clientConfig).resource(url)
    val builder = requestCookies.foldRight(webResource.`type`(bodyFormat)) {
      (c, b) => b.cookie(c)
    }

    log.info("Request URL: %s" format url)
    if (requestParameters.isDefined) {
      log.info("\nSent json data:\n %s" format requestParameters.get)
    }

    response = httpMethod match {
      case HttpMethod.GET => {
        builder.get(classOf[ClientResponse])
      }
      case HttpMethod.POST => {
        builder.post(classOf[ClientResponse], requestParameters.get)
      }
      case HttpMethod.PUT => {
        builder.put(classOf[ClientResponse], requestParameters.get)
      }
    }
    bodyString = this.response.getEntity(classOf[String])
    log.info("Server response:\n %s" format bodyString)
    this
  }

  def hasResponseCode(code: Int) {
    if (response == null) ~this
    httpDsl.checkResponseCode(response, code)
  }

  def hasError(error: ErrorInformation) {
    if (response == null) ~this
    httpDsl.checkError(bodyString, error)
  }

  def hasNoError {
    if (response == null) ~this
    httpDsl.checkNoError(bodyString)
  }

}
