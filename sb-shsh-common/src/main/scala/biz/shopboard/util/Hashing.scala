package biz.shopboard.util

import org.apache.commons.codec.digest.DigestUtils
import org.joda.time.DateTime
import util.Random
import java.security.SecureRandom
import java.math.BigInteger

object Hashing {

  val secureRandom = new SecureRandom

  def generate(id: String): String = String.valueOf(DigestUtils.sha256(id))

  def generateActionHash(): String = String.valueOf(DigestUtils.md5Hex(Random.nextInt(Integer.MAX_VALUE).toString()))

  def generateQuasiRandom = String.valueOf(new DateTime().getMillis << (Random.nextInt(10) + 1))

  def generateRandomSecurePassword = new BigInteger(130, secureRandom).toString(32)

}
