package biz.shopboard.util

import javax.crypto._
import javax.crypto.spec.SecretKeySpec
import biz.shopboard.ConfigContext
import org.apache.commons.codec.binary.{StringUtils, Base64}

/**
 * Cryptographic utilities.
 *
 * These utilities are intended as a convenience, however it is important to read each methods documentation and
 * understand the concepts behind encryption to use this class properly. Safe encryption is hard, and there is no
 * substitute for an adequate understanding of cryptography. These methods will not be suitable for all encryption
 * needs.
 *
 * For more information about cryptography, we recommend reading the OWASP Cryptographic Storage Cheatsheet:
 *
 * https://www.owasp.org/index.php/Cryptographic_Storage_Cheat_Sheet
 */
object Crypto {

  private val secret: String = ConfigContext.getApplicationSecret
  private val transformation: String = "AES"


  /**
   * Encrypt a String with the AES encryption standard using the application's secret key.
   *
   * The provider used is by default this uses the platform default JSSE provider. This can be overridden by defining
   * `application.crypto.provider` in `application.conf`.
   *
   * The transformation algorithm used is the provider specific implementation of the `AES` name. On Oracles JDK,
   * this is `AES/ECB/PKCS5Padding`. This algorithm is suitable for small amounts of data, typically less than 32
   * bytes, hence is useful for encrypting credit card numbers, passwords etc. For larger blocks of data, this
   * algorithm may expose patterns and be vulnerable to repeat attacks.
   *
   * The transformation algorithm can be configured by defining `application.crypto.aes.transformation` in
   * `application.conf`. Although any cipher transformation algorithm can be selected here, the secret key spec used
   * is always AES, so only AES transformation algorithms will work.
   *
   * @param value The String to encrypt.
   * @return A base64 encrypted string.
   */
  def encryptAES(value: String): String = encryptAES(value, secret.substring(0, 16))


  /**
   * Encrypt a String with the AES encryption standard and the supplied private key.
   *
   * The private key must have a length of 16 bytes.
   *
   * The provider used is by default this uses the platform default JSSE provider. This can be overridden by defining
   * `application.crypto.provider` in `application.conf`.
   *
   * The transformation algorithm used is the provider specific implementation of the `AES` name. On Oracles JDK,
   * this is `AES/ECB/PKCS5Padding`. This algorithm is suitable for small amounts of data, typically less than 32
   * bytes, hence is useful for encrypting credit card numbers, passwords etc. For larger blocks of data, this
   * algorithm may expose patterns and be vulnerable to repeat attacks.
   *
   * The transformation algorithm can be configured by defining `application.crypto.aes.transformation` in
   * `application.conf`. Although any cipher transformation algorithm can be selected here, the secret key spec used
   * is always AES, so only AES transformation algorithms will work.
   *
   * @param value The String to encrypt.
   * @param privateKey The key used to encrypt.
   * @return A base64 encrypted string.
   */
  def encryptAES(value: String, privateKey: String): String = {
    if (!org.apache.commons.lang.StringUtils.isBlank(value)) {
      val raw = StringUtils.getBytesUtf8(privateKey)
      val skeySpec = new SecretKeySpec(raw, "AES")
      val cipher = Cipher.getInstance(transformation)
      cipher.init(Cipher.ENCRYPT_MODE, skeySpec)
      Base64.encodeBase64String(cipher.doFinal(StringUtils.getBytesUtf8(value)))
    } else org.apache.commons.lang.StringUtils.EMPTY
  }

  /**
   * Decrypt a String with the AES encryption standard using the application's secret key.
   *
   * The provider used is by default this uses the platform default JSSE provider. This can be overridden by defining
   * `application.crypto.provider` in `application.conf`.
   *
   * The transformation used is by default `AES/ECB/PKCS5Padding`. It can be configured by defining
   * `application.crypto.aes.transformation` in `application.conf`. Although any cipher transformation algorithm can
   * be selected here, the secret key spec used is always AES, so only AES transformation algorithms will work.
   *
   * @param value A base64 encrypted string.
   * @return The decrypted String.
   */
  def decryptAES(value: String): String = decryptAES(value, secret.substring(0, 16))

  /**
   * Decrypt a String with the AES encryption standard.
   *
   * The private key must have a length of 16 bytes.
   *
   * The provider used is by default this uses the platform default JSSE provider. This can be overridden by defining
   * `application.crypto.provider` in `application.conf`.
   *
   * The transformation used is by default `AES/ECB/PKCS5Padding`. It can be configured by defining
   * `application.crypto.aes.transformation` in `application.conf`. Although any cipher transformation algorithm can
   * be selected here, the secret key spec used is always AES, so only AES transformation algorithms will work.
   *
   * @param value A base64 encrypted string.
   * @param privateKey The key used to encrypt.
   * @return The decrypted String.
   */
  def decryptAES(value: String, privateKey: String): String = {
    if (!org.apache.commons.lang.StringUtils.isBlank(value)) {
      val raw = StringUtils.getBytesUtf8(privateKey)
      val skeySpec = new SecretKeySpec(raw, "AES")
      val cipher = Cipher.getInstance(transformation)
      cipher.init(Cipher.DECRYPT_MODE, skeySpec)
      StringUtils.newStringUtf8(cipher.doFinal(Base64.decodeBase64(value)))
    } else org.apache.commons.lang.StringUtils.EMPTY
  }

}
