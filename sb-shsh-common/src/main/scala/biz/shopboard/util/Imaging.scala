package biz.shopboard.util

import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream

import org.apache.avalon.framework.configuration.DefaultConfiguration
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider
import org.krysalis.barcode4j.BarcodeUtil

object Imaging {

  def generate(id: String): ByteArrayOutputStream = {
    val outputStream = new ByteArrayOutputStream()
    val gen = BarcodeUtil.getInstance().createBarcodeGenerator(buildCfg("code128"))
    val canvas = new BitmapCanvasProvider(outputStream, "image/jpeg", 200, BufferedImage.TYPE_BYTE_BINARY, false, 90)

    gen.generateBarcode(canvas, id)
    canvas.finish()
    outputStream
  }

  def buildCfg(t: String): DefaultConfiguration = {
    val cfg = new DefaultConfiguration("barcode")
    val child = new DefaultConfiguration(t)
    cfg.addChild(child)

    //Human readable text position
    val attr = new DefaultConfiguration("human-readable")
    val subAttr = new DefaultConfiguration("placement")
    subAttr.setValue("bottom")
    attr.addChild(subAttr)
    child.addChild(attr)
    cfg
  }
}
