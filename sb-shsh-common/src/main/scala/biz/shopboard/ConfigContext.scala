package biz.shopboard

import com.typesafe.config._

class ConfigSettings(config: Config) {

  // checkValid(), just as in the plain SimpleLibContext
  config.checkValid(ConfigFactory.defaultReference(), "sb-shsh-common")

  // note that these fields are NOT lazy, because if we're going to
  // get any exceptions, we want to get them on startup.
  val db_name_postfix: String = config.getString("sb-shsh-common.db_name_postfix")   //"_train_DB"//
  val email_action_url: String = config.getString("sb-shsh-common.email_action_url")  //"localhost:8080"//
  val rest_service_port: String = config.getString("sb-shsh-common.rest_service_port") //"9090"//
  val application_secret: String = config.getString("sb-shsh-common.application_secret")

  def getDbNamePostfix = db_name_postfix
  def getEmailActionUrl = email_action_url
  def getApplicationSecret = application_secret
  def getRestServicePort = rest_service_port.toInt
}

// Here we are using the
// ConfigSettings class to encapsulate and validate our
// settings on startup
object ConfigContext {
  val settings = new ConfigSettings(ConfigFactory.load())

  def getDbNamePostfix = settings.getDbNamePostfix
  def getEmailActionUrl = settings.getEmailActionUrl
  def getRestServicePort = settings.getRestServicePort
  def getApplicationSecret = settings.getApplicationSecret
}

object I18nContext {
  def getCurrencyTitle = " EUR"
}
