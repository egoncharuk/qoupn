package biz.shopboard

import math.BigDecimal.RoundingMode

object Constants {
  val login_success = "Successfully logged in!"
  val logout_success = "Successfully logged out!"
  val signup_success = "Successfully signed you up!"
  val saved_success = "Successfully saved!"
  val redeemed_success = "Successfully redeemed coupons!"
  val subscribed_success = "Successfully subscribed!"
  val unsubscribed_success = "Successfully unsubscribed!"

  val json_validation_fail = "Json validation failed!"
  val invalid_user = "Username/password combination is invalid!"
  val signup_fail = "Please retry after 5 seconds!"
  val user_not_found = "User not found with id [%s]!"
  val no_user_relogin = "Can't get user. Please relogin."
  val problems_subscribing = "Can't subscribe to this merchant!"
  val problems_deal_linking = "Can't (un)link shopping list or item and the deal!"
  val problems_coupon_linking = "Can't (un)link shopping list or item and the coupon!"

  val zero = BigDecimal.valueOf(0.0D)
  val mode = RoundingMode.HALF_UP
  val id_delimiter = "-"
}
