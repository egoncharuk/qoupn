package biz.shopboard.posemulator

import javax.swing._

import com.jgoodies.forms.builder.PanelBuilder
import com.jgoodies.forms.layout.CellConstraints
import com.jgoodies.forms.layout.FormLayout
import com.jgoodies.common.collect.ArrayListModel
import java.awt.{ Component, Color }
import java.awt.event.ActionEvent
import biz.shopboard.domain.model.merchant.{ DealDetails, DraftCoupon, CouponDetails, ActiveCoupon }
import biz.shopboard.domain.model.common.DiscountInfo
import org.joda.time.DateTime

/**
 * Quickly introduces the most important features of the FormLayout:
 * create and configure a layout, create a builder, add components.<p>
 *
 * Note that this class is not a JPanel subclass; it just <em>uses</em>
 * a JPanel as layout container that will be returned by
 * <code>#buildPanel()</code>.
 */
object PosEmulator {

  def main(args: Array[String]) {
    val posEmulator: PosEmulator = new PosEmulator
    TutorialApplication.launch(posEmulator.getClass, args);
  }

}

class PosEmulator extends TutorialApplication {

  var urlLabel: JLabel = null
  var urlField: JTextField = null
  var loginLabel: JLabel = null
  var loginField: JTextField = null
  var passwordLabel: JLabel = null
  var passwordField: JPasswordField = null
  var loginButton: JButton = null
  var loginStatusLabel: JLabel = null

  var shopperIdLabel: JLabel = null
  var shopperOrCheckoutIdField: JTextField = null
  var merchantIdLabel: JLabel = null
  var merchantIdField: JTextField = null
  var getCouponsButton: JButton = null
  var getCouponsStatusLabel: JLabel = null

  var couponsListSelectionModel: DefaultListSelectionModel = null
  var couponsListLabel: JLabel = null
  var couponsListField: JList = null
  var couponsScrollListField: JScrollPane = null
  var redeemButton: JButton = null
  var redeemStatusLabel: JLabel = null

  var service: ShopBoardRestService = null

  // Launching **************************************************************

  def startup(args: Array[String]) {
    val frame: JFrame = createFrame("Shopboard :: POS-Emulator");
    frame.getContentPane().add(buildPanel);
    attachListeners
    packAndShowOnScreenCenter(frame);
  }

  // Component Creation and Initialization **********************************

  /**
   * Creates, intializes and configures the UI components.
   * Real applications may further bind the components to underlying models.
   */
  def initComponents {

    urlLabel = new JLabel("URL:port")
    urlField = new JTextField("http://46.102.246.226:9090")//"http://46.102.246.226:9090")
    loginLabel = new JLabel("Login")
    loginField = new JTextField("POSTERMINAL")
    passwordLabel = new JLabel("Password")
    passwordField = new JPasswordField("ebef55f2644c3b7ac8cf3af6c5b56d10")
    loginButton = new JButton("Login")
    loginStatusLabel = new JLabel("Not authenticated yet")
    loginStatusLabel.setForeground(Color.RED)

    shopperIdLabel = new JLabel("Shopper/Checkout Id")
    shopperOrCheckoutIdField = new JTextField("707801124992512")//11059308566840")//") //707801124992512
    merchantIdLabel = new JLabel("Merchant Id")
    merchantIdField = new JTextField("352156863636480-cenuklubs.adm")//pid-1")//43777620964032-rimi_id") //
    getCouponsButton = new JButton("Get Coupons")
    getCouponsStatusLabel = new JLabel("No coupons retrieved yet")
    getCouponsStatusLabel.setForeground(Color.RED)

    couponsListSelectionModel = new DefaultListSelectionModel()
    couponsListLabel = new JLabel("Coupons")
    couponsListField = new JList(new ArrayListModel[ActiveCoupon])
    couponsListField.setSelectionModel(couponsListSelectionModel)
    couponsListField.setCellRenderer(new DefaultListCellRenderer {
      override def getListCellRendererComponent(list: javax.swing.JList, value: Any, index: Int, isSelected: Boolean, cellHasFocus: Boolean): Component = {
        val activeCoupon: ActiveCoupon = value.asInstanceOf[ActiveCoupon]
        val couponName: String = activeCoupon.deal.details.name
        val discountPercent: Double = activeCoupon.deal.details.discount.discountPercent
        val label: String = couponName + " | " + discountPercent + "% OFF"
        super.getListCellRendererComponent(list, label, index, isSelected, cellHasFocus)
      }
    })
    couponsScrollListField = new JScrollPane(couponsListField)

    redeemButton = new JButton("Redeem")
    redeemStatusLabel = new JLabel("No coupons redeemed yet")
    redeemStatusLabel.setForeground(Color.RED)
  }

  // Building *************************************************************

  /**
   * Builds the panel. Initializes and configures components first,
   * then creates a FormLayout, configures the layout, creates a builder,
   * sets a border, and finally adds the components.
   *
   * @return the built panel
   */
  def buildPanel: JComponent = {
    // Separating the component initialization and configuration
    // from the layout code makes both parts easier to read.
    initComponents

    // Create a FormLayout instance on the given column and row specs.
    // For almost all forms you specify the columns; sometimes rows are
    // created dynamically. In this case the labels are right aligned.
    val layout: FormLayout = new FormLayout(
      "right:pref, 3dlu, 135dlu:grow", // cols
      "p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 9dlu, " + // Authentication section
        "p, 3dlu, p, 3dlu, p, 3dlu, p, 9dlu, " + // Coupons acquisition section
        "p, 3dlu, top:80dlu, 3dlu, p"); // rows            Coupons redeem section

    //    // Specify that columns 1 & 5 as well as 3 & 7 have equal widths.
    //    val columnGroups: Array[Array[Int]] = Array.ofDim[Int](2, 2)
    //    //new Int[][]{{1, 5}, {3, 7}}
    //    columnGroups(0)(0) = 1;
    //    columnGroups(0)(1) = 5;
    //    columnGroups(1)(0) = 3;
    //    columnGroups(1)(1) = 7;
    //    layout.setColumnGroups(columnGroups);

    // Create a builder that assists in adding components to the container.
    // Wrap the panel with a standardized border.
    val builder: PanelBuilder = new PanelBuilder(layout);
    builder.setDefaultDialogBorder();

    // Obtain a reusable constraints object to place components in the grid.
    val cc: CellConstraints = new CellConstraints();

    // Fill the grid with components; the builder offers to create
    // frequently used components, e.g. separators and labels.

    // Add a titled separator to cell (1, 1) that spans 7 columns.
    builder.addSeparator("1 - Authentication", cc.xyw(1, 1, 3));
    builder.add(urlLabel, cc.xy(1, 3)); builder.add(urlField, cc.xy(3, 3));
    builder.add(loginLabel, cc.xy(1, 5)); builder.add(loginField, cc.xy(3, 5));
    builder.add(passwordLabel, cc.xy(1, 7)); builder.add(passwordField, cc.xy(3, 7));
    builder.add(loginButton, cc.xy(1, 9)); builder.add(loginStatusLabel, cc.xy(3, 9));

    builder.addSeparator("2 - Coupons acquisition", cc.xyw(1, 11, 3));
    builder.add(shopperIdLabel, cc.xy(1, 13)); builder.add(shopperOrCheckoutIdField, cc.xy(3, 13));
    builder.add(merchantIdLabel, cc.xy(1, 15)); builder.add(merchantIdField, cc.xy(3, 15));
    builder.add(getCouponsButton, cc.xy(1, 17)); builder.add(getCouponsStatusLabel, cc.xy(3, 17))

    builder.addSeparator("3 - Coupons redeem", cc.xyw(1, 19, 3));
    builder.add(couponsListLabel, cc.xy(1, 21)); builder.add(couponsScrollListField, cc.xy(3, 21));
    builder.add(redeemButton, cc.xy(1, 23)); builder.add(redeemStatusLabel, cc.xy(3, 23));

    // The builder holds the layout container that we now return.
    return builder.getPanel();
  }

  def doLogin {
    service = new ShopBoardRestService(urlField.getText)
    val userName: String = loginField.getText
    val password: String = passwordField.getPassword.mkString
    val success: Boolean = service.login(userName, password)
    if (success) {
      loginStatusLabel.setText("Successfully authenticated")
      loginStatusLabel.setForeground(Color.BLUE)
      setEnabledCouponsSection(true)
      setEnabledRedeemSection(false)
    } else {
      loginStatusLabel.setText("Not authenticated!")
      loginStatusLabel.setForeground(Color.RED)
      setEnabledCouponsSection(false)
      setEnabledRedeemSection(false)
    }
    // clear previous session coupons
    couponsListField.setModel(new ArrayListModel[ActiveCoupon])
  }

  def doGetCoupons {
    try {
      //createMockCoupons //
      var coupons: List[ActiveCoupon] = service.getAllCoupons(getShopperOrCheckoutId, getMerchantId)
      if (coupons isEmpty)
        coupons = service.getCheckoutCoupons(getShopperOrCheckoutId, getMerchantId)

      val couponsListModel = new ArrayListModel[ActiveCoupon]
      for (coupon <- coupons) {
        couponsListModel.add(coupon)
      }
      couponsListField.setModel(couponsListModel)

      getCouponsStatusLabel.setText(coupons.length + " coupons retrieved")
      getCouponsStatusLabel.setForeground(Color.BLUE)

      setEnabledRedeemSection(true)
    } catch {
      case e: IllegalStateException => {
        getCouponsStatusLabel.setText("Error while retrieving coupons!")
        getCouponsStatusLabel.setForeground(Color.RED)
        setEnabledRedeemSection(false)
      }
    }
  }

  def createMockCoupons(): List[ActiveCoupon] = {
    val coupon1 = createActiveCoupon("id-1", "Coupon1 name", "Coupon1 Description", 100.0d, 25.0d, 75.0d)
    val coupon2 = createActiveCoupon("id-2", "Coupon2 name", "Coupon2 Description", 1000.0d, 15.0d, 850.0d)
    val coupon3 = createActiveCoupon("id-3", "Coupon3 name", "Coupon3 Description", 100.0d, 5.0d, 95.0d)

    List(coupon1, coupon2, coupon3)
  }

  def createActiveCoupon(id: String, name: String, description: String, origPrice: Double, discountPct: Double, discountPrice: Double) = {
    val discountInfo = new DiscountInfo(origPrice, discountPct, discountPrice)
    val validFrom: org.joda.time.DateTime = new DateTime()
    val validTo: org.joda.time.DateTime = new DateTime()
    val innerDetails = new DealDetails(name, description, discountInfo, validFrom, validTo, productInfo = List(), filePath = null, fileBase = null)
    val couponDetails = new CouponDetails(innerDetails, 1L)
    val couponDraft = new DraftCoupon(id, 1L, couponDetails, List())
    new ActiveCoupon(couponDraft, 1L, 1L);
  }

  def doRedeemCoupons {
    val selectedRows: Array[AnyRef] = couponsListField.getSelectedValues
    var selectedCoupons: List[ActiveCoupon] = Nil
    for (anyRef <- selectedRows) {
      val activeCoupon: ActiveCoupon = anyRef.asInstanceOf[ActiveCoupon]
      selectedCoupons = append(List(activeCoupon), selectedCoupons)
    }

    val success: Boolean = service.redeemCoupons(getShopperOrCheckoutId, getMerchantId, selectedCoupons)
    if (success) {
      redeemStatusLabel.setText(selectedCoupons.size + " coupon(-s) redeemed")
      redeemStatusLabel.setForeground(Color.BLUE)
    } else {
      redeemStatusLabel.setText("Redeem failed!")
      redeemStatusLabel.setForeground(Color.RED)
    }
  }

  def getShopperOrCheckoutId: String = {
    shopperOrCheckoutIdField.getText
  }

  def getMerchantId: String = {
    merchantIdField.getText
  }

  def append[T](xs: List[T], ys: List[T]): List[T] =
    xs match {
      case List() => ys
      case x :: xs1 => x :: append(xs1, ys)
    }

  def attachListeners = {

    loginButton.addActionListener(new AbstractAction() {
      def actionPerformed(p1: ActionEvent) {
        doLogin
      }
    })

    getCouponsButton.addActionListener(new AbstractAction() {
      def actionPerformed(p1: ActionEvent) {
        doGetCoupons
      }
    })

    redeemButton.addActionListener(new AbstractAction() {
      def actionPerformed(p1: ActionEvent) {
        doRedeemCoupons
      }
    })

    setEnabledCouponsSection(false)
    setEnabledRedeemSection(false)
  }

  def setEnabledCouponsSection(enabled: Boolean) {
    shopperIdLabel.setEnabled(enabled)
    shopperOrCheckoutIdField.setEnabled(enabled)
    merchantIdLabel.setEnabled(enabled)
    merchantIdField.setEnabled(enabled)
    getCouponsButton.setEnabled(enabled)
    getCouponsStatusLabel.setEnabled(enabled)
  }

  def setEnabledRedeemSection(enabled: Boolean) {
    couponsListLabel.setEnabled(enabled)
    couponsScrollListField.setEnabled(enabled)
    redeemButton.setEnabled(enabled)
    redeemStatusLabel.setEnabled(enabled)
  }
}