package biz.shopboard.posemulator

import reflect.BeanProperty
import biz.shopboard.Constants

case class ErrorInformationExt() {
  @BeanProperty var errorcode: String = ""
  @BeanProperty var errordesc: String = ""
}

case class ErrorInformation(errorcode: String, errordesc: String)

object ErrorInformation {

  val INVALID_USER: ErrorInformation = new ErrorInformation("1", Constants.invalid_user)
  val SIGNUP_FAIL: ErrorInformation = new ErrorInformation("2", Constants.signup_fail)
  val USER_NOT_FOUND: ErrorInformation = new ErrorInformation("3", Constants.user_not_found)
  val NO_USER_RELOGIN: ErrorInformation = new ErrorInformation("4", Constants.no_user_relogin)

}
