package biz.shopboard.posemulator

import scala.Predef._

/**
 * Created with IntelliJ IDEA.
 * User: andrey
 * Date: 1/6/13
 * Time: 8:31 PM
 * To change this template use File | Settings | File Templates.
 */
case class PassInfo(username: String, password: String)

object EmptyPassInfo extends PassInfo("", "")
