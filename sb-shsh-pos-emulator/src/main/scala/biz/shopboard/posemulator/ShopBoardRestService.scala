package biz.shopboard.posemulator

import javax.ws.rs.core.NewCookie
import com.fasterxml.jackson.core.`type`.TypeReference
import biz.shopboard.rest._
import biz.shopboard.domain.model.merchant.ActiveCoupon
import scala._
import biz.shopboard.domain.model.merchant.ActiveCoupons
import org.slf4j.LoggerFactory

/**
 * Class emulates POS-emulator workflow:
 * 1) Authenticate (Login)
 * 2) Get coupons for redeem for given UserID & MerchantID
 * 3) Redeem coupons
 */
object HeadlessWorkflow {

  def main(args: Array[String]) {
    val services: ShopBoardRestService = new ShopBoardRestService("http://localhost:9090")

    services.login("name", "pass")
    val coupons: List[ActiveCoupon] = services.getAllCoupons("", "")
    services.redeemCoupons("name", "password", coupons)
    services.logout()
  }

}

class ShopBoardRestService(val host: String) extends HttpDslClient with JsonHelper {

  val log = LoggerFactory.getLogger("biz.shopboard.posemulator.ShopBoardRestService")

  //  val port = 8080
  //  val host = "http://localhost:" + port.toString
  val styleName: String = "StyleName"
  var cookies: List[NewCookie] = List()

  def login(userName: String, password: String): Boolean = {
    val req = post(host + "/login") withJsonData PassInfo(userName, password)
    val loginCall = ~req
    //loginCall 200
    if (loginCall.response.getStatus != 200 && "{\n  \"status\" : \"ok\"\n}" != loginCall.bodyString) {
      //throw new IllegalStateException();
      return false
    }
    cookies = req.responseCookies()
    true
  }

  case class CouponsRequest(userid: String, merchantid: String = null)

  def getAllCoupons(shopperId: String, merchantId: String): List[ActiveCoupon] = {
//    val jsonData: CheckoutCouponsRequest = CheckoutCouponsRequest(shopperId, merchantId)
    val req = get(host + "/subscribedcoupons") withCookies cookies //withJsonData jsonData
    try {
      val reqCall = ~req
      //loginCall 200
      if (reqCall.response.getStatus != 200) {
        throw new IllegalStateException()
      }
      deserializeCoupons(reqCall.bodyString).coupons
    } catch {
      case e: Exception => {
        e.printStackTrace()
      }
    }
    List[ActiveCoupon]()
  }

  case class CheckoutRequest(checkoutid: String, merchantid: String = null)

  def getCheckoutCoupons(checkoutId: String, merchantId: String): List[ActiveCoupon] = {
    val jsonData: CheckoutRequest = CheckoutRequest(checkoutId, merchantId)
    val req = post(host + "/checkoutcoupons") withCookies cookies withJsonData jsonData
    val reqCall = ~req
    //loginCall 200
    if (reqCall.response.getStatus != 200) {
      log.error("req response status != 200. req=$s" format (req))
      throw new IllegalStateException()
    }
    try {
      deserializeCoupons(reqCall.bodyString).coupons
    } catch {
      case e: Exception => {
        log.error(e.getMessage)
        List[ActiveCoupon]()
      }
    }
  }

  def deserializeCoupons(body: String): ActiveCoupons = {
    val genType: TypeReference[AppResponseGeneric[ActiveCoupons]] = new TypeReference[AppResponseGeneric[ActiveCoupons]]() {}
    val resp: AppResponseGeneric[ActiveCoupons] = mapper.readValue(body, genType)
    resp.data
  }

  def redeemCoupons(userId: String, merchantId: String, couponsList: List[ActiveCoupon]): Boolean = {
    val couponIds: List[String] = couponsList.map(coupon => coupon.id)
    val jsonData: RedeemCouponRequest = RedeemCouponRequest(userId, merchantId, couponIds);
    val req = post(host + "/redeem") withCookies cookies withJsonData jsonData
    val call = ~req
    if (call.response.getStatus != 200) {
      //throw new IllegalStateException();
      return false
    }
    return true
  }

  def logout() {
    get(host + "/logout") withCookies cookies
  }

  case class RedeemCouponRequest(userid: String, merchantid: String, couponids: List[String])

}
