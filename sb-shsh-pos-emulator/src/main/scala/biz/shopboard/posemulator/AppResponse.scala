package biz.shopboard.posemulator

case class AppResponseGeneric[T](status: String, errorinfo: ErrorInformation, data: T)

case class AppResponse(status: String, errorinfo: ErrorInformation, data: Any)
