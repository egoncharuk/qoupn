package biz.shopboard.posemulator

object ResponseStatus {
  val OK = "ok"
  val ERROR = "error"
}
