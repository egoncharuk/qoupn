package biz.shopboard.posemulator

import java.awt.Component
import java.awt.Dimension
import java.awt.EventQueue
import java.util.logging.Level
import java.util.logging.Logger

import javax.swing.JFrame
import javax.swing.UIManager
import javax.swing.WindowConstants

/**
 * A base class for tutorial applications. It provides a light version
 * of the startup behavior from the JSR 296 "Swing Application Framework".
 *
 * @author  Karsten Lentzsch
 * @version $Revision: 1.2 $
 */
object TutorialApplication {

  val LOGGER: Logger = Logger.getLogger(TutorialApplication.getClass.getName)

  /**
   * Instantiates the given TutorialApplication class, then invokes
   * {@code #startup} with the given arguments. Typically this method
   * is called from an application's #main method.
   *
   * //@param appClass the class of the application to launch
   * //@param args optional launch arguments, often the main method's arguments.
   */
  //synchronized              // Class[_ <: TutorialApplication]
  def launch(appClass: Class[_ <: TutorialApplication], args: Array[String]) = {

    val runnable: Runnable = new Runnable() {
      def run() = {
        var application: TutorialApplication = null
        try {
          application = appClass.newInstance()
        } catch {
          case e: InstantiationException => {
            e.printStackTrace()
            LOGGER.log(Level.SEVERE, "Can't instantiate " + appClass, e)
            //return Unit
          }
          case e: IllegalAccessException => {
            e.printStackTrace()
            LOGGER.log(Level.SEVERE, "Illegal Access during launch of " + appClass, e)
            //return
          }
        }
        try {
          application.initializeLookAndFeel()
          application.startup(args)
        } catch {
          case e: Exception => {
            val message: String = "Failed to launch " + appClass
            //val message: String  = String.format("Failed to launch %s ", appClass)
            LOGGER.log(Level.SEVERE, message, e)
            throw new Error(message, e)
          }
        }
      }
    }
    if (EventQueue.isDispatchThread()) {
      runnable.run()
    } else {
      EventQueue.invokeLater(runnable)
    }
  }
}

abstract class TutorialApplication {

  // Instance Creation ******************************************************

  //  protected TutorialApplication() {
  //    // Just set the constructor visibility.
  //  }

  // Life Cycle *************************************************************

  /**
   * Starts this application when the application is launched.
   * A typical application creates and shows the GUI in this method.
   * This method runs on the event dispatching thread.<p>
   *
   * Called by the static {@code launch} method.
   *
   * @param args optional launch arguments, often the main method's arguments.
   *
   * @see #launch(Class, String[])
   */
  def startup(args: Array[String])

  // Look & Feel ************************************************************
  //protected
  def initializeLookAndFeel() {
    try {
      val osName: String = System.getProperty("os.name")
      if (osName.startsWith("Windows")) {
        UIManager.setLookAndFeel("com.jgoodies.looks.windows.WindowsLookAndFeel")
      } else if (osName.startsWith("Mac")) {
        // do nothing, use the Mac Aqua L&f
      } else {
        UIManager.setLookAndFeel("com.jgoodies.looks.plastic.PlasticXPLookAndFeel")
      }
    } catch {
      case e: Exception => {
        // Likely the Looks library is not in the class path; ignore.
      }
    }
  }

  // Default Frame Configuration ********************************************
  //protected
  def createFrame(title: String): JFrame = {
    val frame: JFrame = new JFrame(title)
    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    return frame
  }

  // Standard Open Behavior *************************************************
  //protected final
  def packAndShowOnScreenCenter(frame: JFrame) {
    frame.pack();
    locateOnOpticalScreenCenter(frame);
    frame.setVisible(true);
  }

  // Screen Position ********************************************************

  /**
   * Locates the given component on the screen's center.
   *
   * @param component   the component to be centered
   */
  //protected final
  def locateOnOpticalScreenCenter(component: Component) {
    val paneSize: Dimension = component.getSize();
    val screenSize: Dimension = component.getToolkit().getScreenSize();

    val x = (screenSize.width - paneSize.width) / 2;
    val y = ((screenSize.height - paneSize.height) * 0.45).toInt

    component.setLocation(x, y);
  }

}