package biz.shopboard.posemulator

import com.fasterxml.jackson.module.scala.DefaultScalaModule
import unfiltered.response.{ ResponseString, ContentType, ComposeResponse }
import com.fasterxml.jackson.databind.{ SerializationFeature, ObjectMapper }
import com.fasterxml.jackson.annotation.JsonInclude

trait JsonHelper {

  val mapper = new ObjectMapper()
    .enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
    .enable(SerializationFeature.INDENT_OUTPUT)
    .registerModule(new DefaultScalaModule())
  //.registerModule(new JodaModule())

  def toJson(o: Any): String = {
    mapper.writeValueAsString(o)
  }

  case class ResponseJson(a: AppResponse) extends ComposeResponse(
    ContentType("application/json") ~> ResponseString(mapper.writeValueAsString(a)))

}
