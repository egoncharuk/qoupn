package biz.shopboard.posemulator

import swing._

object OldPosEmulator extends SimpleSwingApplication {

  import TabbedPane._

  case class Coupon(label: String, info: String)

  val items = List(Coupon("MAXIMA COUPON 1", "For items x1"),
    Coupon("MAXIMA COUPON 2", "For items x2"),
    Coupon("MAXIMA COUPON 3", "For items x3"),
    Coupon("MAXIMA COUPON 4", "For items x4"),
    Coupon("MAXIMA COUPON 5", "For items x5"))

  lazy val tabs = new TabbedPane {
    pages += new Page("Coupons Redeem", new GridBagPanel {
      grid =>

      import GridBagPanel._

      val shopperIdText = new TextField("Shopper ID here...")

      val c = new Constraints
      c.fill = Fill.Horizontal
      c.grid = (1, 1)
      layout(new Label("Shopper ID:")) = c

      c.grid = (2, 1)
      layout(shopperIdText) = c

      c.grid = (1, 2)
      layout(new Button(Action("Retrieve Coupons") {
        //chooser.showOpenDialog(grid)
      })) = c

      import ListView._
      c.grid = (2, 2)
      layout(new ScrollPane(new ListView(items) {
        renderer = Renderer(_.label)
      })) = c

      c.grid = (1, 3)
      layout(new Button(Action("Redeem Coupons") {
        //chooser.showDialog(grid, buttonText.text)
      })) = c

      c.grid = (2, 3)
      layout(new Label(" Nothing redeemed yet... ")) = c

      border = Swing.EmptyBorder(5, 5, 5, 5)
    })
  }

  lazy val ui: Panel = new BorderPanel {
    layout(tabs) = BorderPanel.Position.Center
  }

  lazy val top = new MainFrame {
    title = "POS Emulator"
    contents = ui
  }
}
