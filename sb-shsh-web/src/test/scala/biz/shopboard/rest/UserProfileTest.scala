package biz.shopboard.rest

import org.scalatest.WordSpec
import org.scalatest.matchers.{ ShouldMatchers, MustMatchers }
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import biz.shopboard.domain.model.user._
import javax.ws.rs.core.NewCookie
import biz.shopboard.server.JettyRunnable
import biz.shopboard.infrastructure.FacadeProvided
import org.junit.Assert
import biz.shopboard.domain.model.PassInfo
import scalaz.Failure
import biz.shopboard.domain.model.user.UserDetails
import scalaz.Success
import com.fasterxml.jackson.core.`type`.TypeReference

@RunWith(classOf[JUnitRunner])
class UserProfileTest extends WordSpec with ShouldMatchers with MustMatchers
with JettyRunnable with HttpDsl with JsonHelper with FacadeProvided {

  val defaultVersion = 0L

  val userDetails = UserDetails("name", "pass", "Orson", "Welles", "orson@mars.com")
  val userInfo = User("id-1", defaultVersion, userDetails, List())
  val correctLoginInfo = PassInfo("name", "pass")
  val wrongLoginInfo = PassInfo("name", "wrong-pass")

  "respond with user not found error if no user exist in the system" in {
    val req = get(host + "/user/1")
    val reqCall: RichHttp = ~req
    reqCall hasResponseCode 200
    reqCall hasError ErrorInformation.USER_NOT_FOUND
  }

  "successfully signup new user" in {
    post(host + "/signup") withJsonData userInfo hasResponseCode 200
    Thread.sleep(2000)
    val hashKey: String = userService.getActionHashes.keySet.iterator.next()
    hashKey must not equal (null)
  }

  "not return profile data if not authenticated" in {
    val req = get(host + "/profile")
    val reqCall: RichHttp = ~req
    reqCall hasResponseCode 200
    reqCall hasError ErrorInformation.NO_USER_RELOGIN
  }

  "When working with profiles" should {
    var cookies: List[NewCookie] = List()
    "do not login until user is activated" in {
      val req = post(host + "/login") withJsonData correctLoginInfo
      val loginCall = ~req
      loginCall hasResponseCode 200
      loginCall hasError ErrorInformation.INVALID_USER
    }
    "successfully activate user" in {
      userService.activeUserExists(correctLoginInfo.username, correctLoginInfo.password) must equal(false)
      val hashKey: String = userService.getActionHashes.keySet.iterator.next()
      execute(userService.activateUser(hashKey)) match {
        case Success(_) => userService.activeUserExists(correctLoginInfo.username, correctLoginInfo.password) must equal(true)
        case Failure(e) => Assert.assertTrue("Failure when activating user.", false)
      }
    }
    "unsuccessfully login with wrong login data after user is activated" in {
      val req = post(host + "/login") withJsonData wrongLoginInfo
      val loginCall = ~req
      loginCall hasResponseCode 200
      loginCall hasError ErrorInformation.INVALID_USER
    }
    "successfully login with correct login data" in {
      val req = post(host + "/login") withJsonData correctLoginInfo
      val loginCall: RichHttp = ~req
      loginCall hasResponseCode 200
      loginCall.hasNoError
      cookies = req.responseCookies()
    }
    "successfully get profile with cookies from login" in {
      val req = get(host + "/profile") withCookies cookies
      val profileCall: RichHttp = ~req
      profileCall hasResponseCode 200

      val user: User = retrieveProfile(profileCall.bodyString)
      val details = userInfo.details.copy(status = UserStatus.ACTIVE, password = "")
      user.details must equal(details)
      user.version must equal(1L)
    }
    "successfully logout" in {
      get(host + "/logout") withCookies cookies hasResponseCode 200
    }
    "not get profile with cookies from login if already logged out" in {
      val req = get(host + "/profile") withCookies cookies
      val profileCall: RichHttp = ~req
      profileCall hasResponseCode 200
      profileCall hasError ErrorInformation.NO_USER_RELOGIN
    }

  }

  def retrieveProfile(body: String): User = {
    val genType: TypeReference[AppResponseGeneric[User]] = new TypeReference[AppResponseGeneric[User]]() {}
    val resp: AppResponseGeneric[User] = mapper.readValue(body, genType)
    resp.data
  }

}
