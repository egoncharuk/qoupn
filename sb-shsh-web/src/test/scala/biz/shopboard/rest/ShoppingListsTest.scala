package biz.shopboard.rest

import javax.ws.rs.core.NewCookie
import biz.shopboard.domain.model.user._
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.WordSpec
import org.scalatest.matchers.{ MustMatchers, ShouldMatchers }
import biz.shopboard.server.JettyRunnable
import biz.shopboard.domain.model.PassInfo
import biz.shopboard.domain.model.user.ShoppingListItem
import biz.shopboard.domain.model.user.UserDetails
import com.fasterxml.jackson.core.`type`.TypeReference
import scalaz.{ Failure, Success }
import org.junit.Assert
import biz.shopboard.infrastructure.FacadeProvided

@RunWith(classOf[JUnitRunner])
class ShoppingListsTest extends WordSpec with ShouldMatchers with MustMatchers
    with JettyRunnable with HttpDsl with JsonHelper with FacadeProvided {

  val defaultVersion = 0L

  val userDetails = UserDetails("name", "pass", "Orson", "Welles", "orson@mars.com")
  val userInfo = User("id-1", defaultVersion, userDetails, List())
  val loginInfo = PassInfo("name", "pass")

  val shoppingListId = "slid-1"
  val shoppingListDetails = ShoppingListDetails("My shopping list", List("bakery", "fruits"))
  val createShoppingList = ShoppingListChange(ChangeType.create, shoppingListId, defaultVersion, shoppingListDetails, null, null)
  val shoppingListDetails2 = ShoppingListDetails("My shopping list 2", List("bakery 2", "fruits 2"))
  val updateShoppingListDetails = ShoppingListChange(ChangeType.updateDetails, shoppingListId, defaultVersion, shoppingListDetails2, null, null)
  val shoppingListChanges = ShoppingListChanges(List(createShoppingList, updateShoppingListDetails))

  val firstSLItemId: String = "sliid-1"
  val firstShoppingListItem = ShoppingListItem(firstSLItemId, "Sprite 2.0L", "Drink", 1, 2.32)
  val addFirstShoppingListItem = ShoppingListChange(ChangeType.addItem, shoppingListId, defaultVersion, null, null, firstShoppingListItem)

  val secondSLItemId = "sliid-2"
  val secondShoppingListItem = ShoppingListItem(secondSLItemId, "Karums Piens 1.5L 2.5%", "Piens", 2, 1.25)
  val addSecondShoppingListItem = ShoppingListChange(ChangeType.addItem, shoppingListId, defaultVersion, null, null, secondShoppingListItem)

  val thirdSLItemId = "sliid-3"
  val thirdShoppingListItem = ShoppingListItem(thirdSLItemId, "Maize Hanzas balta", "Maize", 3, 0.98)
  val addThirdShoppingListItem = ShoppingListChange(ChangeType.addItem, shoppingListId, defaultVersion, null, null, thirdShoppingListItem)

  val firstShoppingListItemChanged = ShoppingListItem(firstSLItemId, "Sprite 0.5L", "Drink", 2, 1.05)
  val changeFirstShoppingListItem = ShoppingListChange(ChangeType.updateItem, shoppingListId, defaultVersion, null, null, firstShoppingListItemChanged)

  val removeSecondShoppingListItem = ShoppingListChange(ChangeType.removeItem, shoppingListId, defaultVersion, null, secondSLItemId, null)

  val shoppingListItemsChanges = ShoppingListChanges(List(addFirstShoppingListItem, addSecondShoppingListItem, addThirdShoppingListItem,
    changeFirstShoppingListItem, removeSecondShoppingListItem))

  val removeShoppingList = ShoppingListChange(ChangeType.remove, shoppingListId, defaultVersion, null, null, null)

  val shoppingListRemoveChanges = ShoppingListChanges(List(removeShoppingList))

  "Should successfully signup and activate new user" in {
    post(host + "/signup") withJsonData userInfo hasResponseCode 200
    Thread.sleep(2000)
    val hashKey: String = userService.getActionHashes.keySet.iterator.next()
    execute(userService.activateUser(hashKey)) match {
      case Success(_) => userService.activeUserExists(loginInfo.username, loginInfo.password) must equal(true)
      case Failure(e) => Assert.assertTrue("Failure when activating user.", false)
    }
  }

  "When working with shopping lists, client" should {
    var cookies: List[NewCookie] = List()

    "successfully login with correct login data" in {
      val req = post(host + "/login") withJsonData loginInfo
      val loginCall = ~req
      loginCall hasResponseCode 200
      loginCall.hasNoError
      cookies = req.responseCookies()
    }

    "successfully process shopping list changes batch" in {
      val req = post(host + "/shoppinglistchanges") withCookies cookies withJsonData shoppingListChanges
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "successfully get one shopping list" in {
      val req = get(host + "/shoppinglists") withCookies cookies
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError

      val shoppingLists: List[ShoppingList] = retrieveShoppingLists(putCall.bodyString).shoppinglists
      shoppingLists must have size (1)

      shoppingLists(0).details must equal(shoppingListDetails2)
    }

    "successfully process shopping list items changes batch" in {
      val req = post(host + "/shoppinglistchanges") withCookies cookies withJsonData shoppingListItemsChanges
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "successfully get one shopping list with three items in it" in {
      val req = get(host + "/shoppinglists") withCookies cookies
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError

      val shoppingLists: List[ShoppingList] = retrieveShoppingLists(putCall.bodyString).shoppinglists
      shoppingLists must have size (1)
      val items = shoppingLists(0).items
      items must have size (2)

      val firstItem: Option[ShoppingListItem] = items.get(firstSLItemId)
      firstItem.get.name must equal("Sprite 0.5L")
      firstItem.get.desc must equal("Drink")
      firstItem.get.quantity must equal(2)
      firstItem.get.price must equal(1.05)

      val secondItem: Option[ShoppingListItem] = items.get(thirdSLItemId)
      secondItem.get.name must equal("Maize Hanzas balta")
      secondItem.get.desc must equal("Maize")
      secondItem.get.quantity must equal(3)
      secondItem.get.price must equal(0.98)
    }

    "successfully process shopping list remove changes batch" in {
      val req = post(host + "/shoppinglistchanges") withCookies cookies withJsonData shoppingListRemoveChanges
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "successfully get empty shopping lists" in {
      val req = get(host + "/shoppinglists") withCookies cookies
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError

      val shoppingLists: List[ShoppingList] = retrieveShoppingLists(putCall.bodyString).shoppinglists
      shoppingLists must have size (0)
    }

  }

  def retrieveShoppingLists(body: String): ShoppingLists = {
    val genType: TypeReference[AppResponseGeneric[ShoppingLists]] = new TypeReference[AppResponseGeneric[ShoppingLists]]() {}
    val resp: AppResponseGeneric[ShoppingLists] = mapper.readValue(body, genType)
    resp.data
  }

}
