package biz.shopboard.rest

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.WordSpec
import org.scalatest.matchers.{ MustMatchers, ShouldMatchers }
import biz.shopboard.server.JettyRunnable
import biz.shopboard.domain.model.user.{ CheckoutId, ShoppingListDetails, User, UserDetails }
import javax.ws.rs.core.NewCookie
import org.apache.commons.io.IOUtils
import org.apache.commons.codec.binary.Base64
import org.joda.time.DateTime
import biz.shopboard.domain.model.merchant._
import biz.shopboard.domain.model.PassInfo
import biz.shopboard.domain.model.merchant.CampaignDetails
import scala.Some
import biz.shopboard.domain.model.common.ProductInfo
import biz.shopboard.domain.model.merchant.ActiveCampaign
import biz.shopboard.domain.model.merchant.MerchantDetails
import biz.shopboard.domain.model.merchant.DealDetails
import biz.shopboard.domain.model.common.DiscountInfo
import biz.shopboard.domain.model.common.Address
import biz.shopboard.infrastructure.FacadeProvided
import com.fasterxml.jackson.core.`type`.TypeReference
import scalaz.{ Failure, Success }
import org.junit.Assert

@RunWith(classOf[JUnitRunner])
class DealsCheckoutTest extends WordSpec with ShouldMatchers with MustMatchers
    with JettyRunnable with HttpDsl with JsonHelper with FacadeProvided {

  val defaultVersion = 0L

  val userDetails = UserDetails("name", "pass", "Orson", "Welles", "orson@mars.com")
  val userInfo = User("id-1", defaultVersion, userDetails, List())
  val loginInfo = PassInfo("name", "pass")

  val picturePath: String = "target/apple.jpg"
  val pictureBase64Encoded: String = {
    val stream = getClass.getResourceAsStream("/images/apple.jpg")
    val byteArray = IOUtils.toByteArray(stream)
    Base64.encodeBase64String(byteArray)
  }

  val validFrom: DateTime = new DateTime("2012-10-28T00:00")
  val validTo: DateTime = new DateTime("2025-03-15T00:00")

  val firstDealDetails = DealDetails("Deal 1", "Deal 1 description",
    DiscountInfo(10.0, 12.2), validFrom, validTo,
    List(ProductInfo("Apples", "Apples from New Zealand", "fruits")), picturePath, pictureBase64Encoded)
  val firstDealId: String = "did-1"
  val firstDeal = DraftDeal(firstDealId, defaultVersion, firstDealDetails)

  val campaignDetails = CampaignDetails("First campaign", "First Rimi campaign", validFrom, validTo)
  val campaign = ActiveCampaign(DraftCampaign("cid-1", defaultVersion, campaignDetails, null, List(firstDealId)))
  val merchantDetails = MerchantDetails("Rimi", Address("Ulbrokas street 25", "Riga", "Rigas", "Latvia"))
  val merchantId = "pid-1"
  val merchant = Merchant(merchantId, defaultVersion, "aid-1", merchantDetails, Nil)

  val shoppingListId = "slid-1"
  val shoppingListDetails = ShoppingListDetails("My shopping list", List("bakery", "fruits"))
  val createShoppingList = ShoppingListChange(ChangeType.create, shoppingListId, defaultVersion, shoppingListDetails, null, null)

  "Should successfully signup new user" in {
    post(host + "/signup") withJsonData userInfo hasResponseCode 200
    Thread.sleep(1000)
    val hashKey: String = userService.getActionHashes.keySet.iterator.next()
    execute(userService.activateUser(hashKey)) match {
      case Success(_) => userService.activeUserExists(loginInfo.username, loginInfo.password) must equal(true)
      case Failure(e) => Assert.assertTrue("Failure when activating user.", false)
    }
  }

  "When client" should {

    var cookies: List[NewCookie] = List()
    var userId: String = ""
    var checkoutId: String = ""

    "successfully login with correct login data and retrieve profile" in {
      val req = post(host + "/login") withJsonData loginInfo
      val loginCall = ~req
      loginCall hasResponseCode 200
      cookies = req.responseCookies()

      val req2 = get(host + "/profile") withCookies cookies
      val loginCall2 = ~req2
      loginCall2 hasResponseCode 200
      userId = retrieveUser(loginCall2.bodyString).id
    }

    "successfully subscribe for deals retrieval" in {
      val subscribeData = SubscribeRequest(merchant.id)
      execute(merchantService.createMerchant(merchant))

      val req = post(host + "/subscribe") withCookies cookies withJsonData subscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "successfully link deal to the shopping list while campaign is not activated" in {
      execute(merchantService.createCampaign(merchant.id, Some(defaultVersion), campaign.id, campaignDetails))
      execute(merchantService.createDeal(merchant.id, campaign.id, Some(defaultVersion), firstDealId, firstDealDetails))
      execute(userService.createShoppingList(userId, Some(defaultVersion), shoppingListId, shoppingListDetails))

      val jsonData = DealShoppingListRequest(firstDealId, shoppingListId, null)
      val req = post(host + "/linkdealshoppinglist") withCookies cookies withJsonData (jsonData)
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError
    }

    "unsuccessfully link deal to the shopping list for non-existent shopping list item" in {
      val jsonData = DealShoppingListRequest(firstDealId, shoppingListId, "non-existent-id")
      val req = post(host + "/linkdealshoppinglist") withCookies cookies withJsonData (jsonData)
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasError(ErrorInformation.DEAL_LINK_ERROR)
    }

    "successfully get checkout id for provided shopping list id" in {
      val jsonData = CheckoutShoppingListRequest(shoppingListId)
      val req = post(host + "/checkoutshoppinglist") withCookies cookies withJsonData (jsonData)
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val checkout = retrieveCheckoutId(reqCall.bodyString)
      checkoutId = checkout.checkoutid
      checkout.checkoutid must not be null
      checkout.barcodebase must not be null
      Base64.isBase64(checkout.barcodebase).toString must equal("true")
    }

    "not retrieve any checkout deals for provided shopping list and merchant if campaign is not activated" in {
      val jsonData = CheckoutDealsRequest(checkoutId, merchantId)
      val req = post(host + "/checkoutdeals") withCookies cookies withJsonData (jsonData)
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val deals: List[ActiveDeal] = retrieveDeals(reqCall.bodyString).deals
      deals must have size (0)
    }

    "successfully retrieve checkout deals for provided shopping list and merchant if campaign is activated" in {
      execute(merchantService.activateCampaign(merchantId, campaign.id, Some(defaultVersion)))

      val jsonData = CheckoutDealsRequest(checkoutId, merchantId)
      val req = post(host + "/checkoutdeals") withCookies cookies withJsonData (jsonData)
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val deals: List[ActiveDeal] = retrieveDeals(reqCall.bodyString).deals
      deals must have size (1)
    }

    "successfully unlink deal from shopping list while campaign is activated" in {
      val jsonData = DealShoppingListRequest(firstDealId, shoppingListId, null)
      val req = post(host + "/unlinkdealshoppinglist") withCookies cookies withJsonData (jsonData)
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError
    }

    "not retrieve any checkout deals for provided shopping list and merchant if unlinked" in {
      val jsonData = CheckoutDealsRequest(firstDealId, merchantId)
      val req = post(host + "/checkoutdeals") withCookies cookies withJsonData (jsonData)
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val deals: List[ActiveDeal] = retrieveDeals(reqCall.bodyString).deals
      deals must have size (0)
    }

    def retrieveDeals(body: String): ActiveDeals = {
      val genType: TypeReference[AppResponseGeneric[ActiveDeals]] = new TypeReference[AppResponseGeneric[ActiveDeals]]() {}
      val resp: AppResponseGeneric[ActiveDeals] = mapper.readValue(body, genType)
      resp.data
    }

    def retrieveUser(body: String): User = {
      val genType: TypeReference[AppResponseGeneric[User]] = new TypeReference[AppResponseGeneric[User]]() {}
      val resp: AppResponseGeneric[User] = mapper.readValue(body, genType)
      resp.data
    }

    def retrieveCheckoutId(body: String): CheckoutId = {
      val genType: TypeReference[AppResponseGeneric[CheckoutId]] = new TypeReference[AppResponseGeneric[CheckoutId]]() {}
      val resp: AppResponseGeneric[CheckoutId] = mapper.readValue(body, genType)
      resp.data
    }

  }

}
