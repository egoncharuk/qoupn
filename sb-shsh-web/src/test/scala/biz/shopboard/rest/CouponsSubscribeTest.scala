package biz.shopboard.rest

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.WordSpec
import org.scalatest.matchers.{ MustMatchers, ShouldMatchers }
import biz.shopboard.server.JettyRunnable
import biz.shopboard.domain.model.user.User
import javax.ws.rs.core.NewCookie
import org.apache.commons.io.IOUtils
import org.apache.commons.codec.binary.Base64
import org.joda.time.DateTime
import biz.shopboard.domain.model.merchant._
import biz.shopboard.domain.model.PassInfo
import biz.shopboard.domain.model.merchant.CampaignDetails
import scala.Some
import biz.shopboard.domain.model.user.UserDetails
import biz.shopboard.domain.model.common.ProductInfo
import biz.shopboard.domain.model.merchant.ActiveCampaign
import biz.shopboard.domain.model.merchant.MerchantDetails
import biz.shopboard.domain.model.merchant.DealDetails
import biz.shopboard.domain.model.common.DiscountInfo
import biz.shopboard.domain.model.common.Address
import biz.shopboard.infrastructure.FacadeProvided
import com.fasterxml.jackson.core.`type`.TypeReference
import scalaz.{ Failure, Success }
import org.junit.Assert

@RunWith(classOf[JUnitRunner])
class CouponsSubscribeTest extends WordSpec with ShouldMatchers with MustMatchers
    with JettyRunnable with HttpDsl with JsonHelper with FacadeProvided {

  val defaultVersion = 0L

  val userDetails = UserDetails("name", "pass", "Orson", "Welles", "orson@mars.com")
  val userInfo = User("id-1", defaultVersion, userDetails, List())
  val loginInfo = PassInfo("name", "pass")

  val picturePath: String = "target/apple.jpg"
  val pictureBase64Encoded: String = {
    val stream = getClass.getResourceAsStream("/images/apple.jpg")
    val byteArray = IOUtils.toByteArray(stream)
    Base64.encodeBase64String(byteArray)
  }

  val validFrom: DateTime = new DateTime("2012-10-28T00:00")
  val validTo: DateTime = new DateTime("2025-03-15T00:00")

  val firstCouponDetails = DealDetails("Coupon 1", "Coupon 1 description", DiscountInfo(100.0, 10.0, 90.0),
    validFrom, validTo, List(ProductInfo("Apples", "Apples from New Zealand", "fruits")),
    picturePath, pictureBase64Encoded)

  val firstCouponId: String = "cpid-1"

  val campaignDetails = CampaignDetails("First campaign", "First Rimi campaign", validFrom, validTo)
  val campaign = ActiveCampaign(DraftCampaign("cid-1", defaultVersion, campaignDetails, null, List(firstCouponId)))
  val merchantDetails = MerchantDetails("Rimi", Address("Ulbrokas street 25", "Riga", "Rigas", "Latvia"))
  val merchant = Merchant("pid-1", defaultVersion, "aid-1", merchantDetails, Nil)

  "Should successfully signup new user" in {
    post(host + "/signup") withJsonData userInfo hasResponseCode 200
    Thread.sleep(1000)
    val hashKey: String = userService.getActionHashes.keySet.iterator.next()
    execute(userService.activateUser(hashKey)) match {
      case Success(_) => userService.activeUserExists(loginInfo.username, loginInfo.password) must equal(true)
      case Failure(e) => Assert.assertTrue("Failure when activating user.", false)
    }
  }

  "When client" should {

    var cookies: List[NewCookie] = List()
    var userId: String = ""

    "successfully login with correct login data and retrieve profile" in {
      val req = post(host + "/login") withJsonData loginInfo
      val loginCall = ~req
      loginCall hasResponseCode 200
      cookies = req.responseCookies()

      val req2 = get(host + "/profile") withCookies cookies
      val loginCall2 = ~req2
      loginCall2 hasResponseCode 200
      userId = retrieveUser(loginCall2.bodyString).id
    }

    "fail when subscribing for not existing merchant" in {
      val merchantId = "1"
      val subscribeData = SubscribeRequest(merchantId)

      val req = post(host + "/subscribe") withCookies cookies withJsonData subscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasError(ErrorInformation("0", "List(Merchant 1 does not exist)"))
    }

    "successfully subscribe for first merchant coupons retrieval " in {
      execute(merchantService.createMerchant(merchant))
      execute(merchantService.createCampaign(merchant.id, Some(defaultVersion), campaign.id, campaignDetails))
      execute(merchantService.createCoupon(merchant.id, campaign.id, Some(defaultVersion), firstCouponId, firstCouponDetails, 1000))

      val subscribeData = SubscribeRequest(merchant.id)
      val req = post(host + "/subscribe") withCookies cookies withJsonData subscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "not retrieve any coupons from first merchant while campaign is not activated (/subscribedcoupons returns coupons only if subscribed)" in {
      val req = get(host + "/subscribedcoupons") withCookies cookies
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveCoupon] = retrieveCoupons(reqCall.bodyString).coupons
      coupons must have size (0)
    }

    "successfully retrieve coupons from first merchant when campaign is activated (/subscribedcoupons returns coupons only if subscribed)" in {
      execute(merchantService.activateCampaign(merchant.id, campaign.id, Some(1L)))
      val req = get(host + "/subscribedcoupons") withCookies cookies

      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveCoupon] = retrieveCoupons(reqCall.bodyString).coupons
      coupons must have size (1)
    }

    "successfully unsubscribe from first merchant coupons retrieval" in {
      val unsubscribeData = UnsubscribeRequest(merchant.id)
      val req = post(host + "/unsubscribe") withCookies cookies withJsonData unsubscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "not retrieve any coupons from first merchant when unsubscribed (/subscribedcoupons returns coupons only if subscribed)" in {
      val req = get(host + "/subscribedcoupons") withCookies cookies
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveCoupon] = retrieveCoupons(reqCall.bodyString).coupons
      coupons must have size (0)
    }

    "successfully retrieve coupons from first merchant even if not subscribed (/allcoupons returns coupons even if not subscribed)" in {
      val req = get(host + "/allcoupons") withCookies cookies
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveCoupon] = retrieveCoupons(reqCall.bodyString).coupons
      coupons must have size (1)
    }

    "successfully subscribe second time for first merchant coupons retrieval" in {
      val subscribeData = SubscribeRequest(merchant.id)
      val req = post(host + "/subscribe") withCookies cookies withJsonData subscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "successfully retrieve coupons from first merchant after subscribed second time (/subscribedcoupons returns coupons only if subscribed)" in {
      val req = get(host + "/subscribedcoupons") withCookies cookies

      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveCoupon] = retrieveCoupons(reqCall.bodyString).coupons
      coupons must have size (1)
    }

    def retrieveCoupons(body: String): ActiveCoupons = {
      val genType: TypeReference[AppResponseGeneric[ActiveCoupons]] = new TypeReference[AppResponseGeneric[ActiveCoupons]]() {}
      val resp: AppResponseGeneric[ActiveCoupons] = mapper.readValue(body, genType)
      resp.data
    }

    def retrieveUser(body: String): User = {
      val genType: TypeReference[AppResponseGeneric[User]] = new TypeReference[AppResponseGeneric[User]]() {}
      val resp: AppResponseGeneric[User] = mapper.readValue(body, genType)
      resp.data
    }

  }

}
