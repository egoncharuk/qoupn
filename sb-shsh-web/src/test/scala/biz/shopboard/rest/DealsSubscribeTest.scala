package biz.shopboard.rest

import org.scalatest.WordSpec
import org.scalatest.matchers.{ ShouldMatchers, MustMatchers }
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import biz.shopboard.domain.model.user.{ UserDetails, User }
import biz.shopboard.server.JettyRunnable
import biz.shopboard.infrastructure.FacadeProvided
import biz.shopboard.domain.model.merchant._
import org.joda.time.DateTime
import biz.shopboard.domain.model.merchant.ActiveCampaign
import biz.shopboard.domain.model.merchant.MerchantDetails
import biz.shopboard.domain.model.merchant.CampaignDetails
import biz.shopboard.domain.model.common.{ Address, ProductInfo, DiscountInfo }
import org.apache.commons.codec.binary.Base64
import org.apache.commons.io.IOUtils
import javax.ws.rs.core.NewCookie
import biz.shopboard.domain.model.PassInfo
import scalaz.{ Failure, Success }
import org.junit.Assert
import com.fasterxml.jackson.core.`type`.TypeReference

@RunWith(classOf[JUnitRunner])
class DealsSubscribeTest extends WordSpec with ShouldMatchers with MustMatchers
with JettyRunnable with HttpDsl with JsonHelper with FacadeProvided {

  val defaultVersion = 0L

  val userDetails = UserDetails("name", "pass", "Orson", "Welles", "orson@mars.com")
  val userInfo = toJson(User("id-1", defaultVersion, userDetails))
  val loginInfo = PassInfo("name", "pass")

  val picturePath: String = "target/apple.jpg"
  val pictureBase64Encoded: String = {
    val stream = getClass.getResourceAsStream("/images/apple.jpg")
    val byteArray = IOUtils.toByteArray(stream)
    Base64.encodeBase64String(byteArray)
  }

  val validFrom: DateTime = new DateTime("2012-10-28T00:00")
  val validTo: DateTime = new DateTime("2025-03-15T00:00")

  val firstDealDetails = DealDetails("Deal 1", "Deal 1 description",
    DiscountInfo(10.0, 12.2), validFrom, validTo,
    List(ProductInfo("Apples", "Apples from New Zealand", "fruits")), picturePath, pictureBase64Encoded)
  val firstDealId: String = "did-1"
  val firstDeal = DraftDeal(firstDealId, defaultVersion, firstDealDetails)
  val firstCampaignDetails = CampaignDetails("First campaign", "First Rimi campaign", validFrom, validTo)
  val firstCampaign = ActiveCampaign(DraftCampaign("cid-1", defaultVersion, firstCampaignDetails, List(firstDealId), null))
  val firstMerchantDetails = MerchantDetails("Rimi", Address("Ulbrokas street 25", "Riga", "Rigas", "Latvia"))
  val firstMerchant = Merchant("pid-1", defaultVersion, "aid-1", firstMerchantDetails, Nil)

  val secondDealDetails = DealDetails("Deal 2", "Deal 2 description",
    DiscountInfo(10.0, 14.0, 8.6), validFrom, validTo,
    List(ProductInfo("Grape", "Grape from Italy", "fruits")), picturePath, pictureBase64Encoded)
  val secondDealId: String = "did-2"
  val secondDeal = DraftDeal(secondDealId, defaultVersion, secondDealDetails)
  val secondCampaignDetails = CampaignDetails("Second campaign", "Second Rimi campaign", validFrom, validTo)
  val secondCampaign = ActiveCampaign(DraftCampaign("cid-2", defaultVersion, secondCampaignDetails, List(secondDealId), null))
  val secondMerchantDetails = MerchantDetails("Rimi", Address("Ulbrokas street 25", "Riga", "Rigas", "Latvia"))
  val secondMerchant = Merchant("pid-2", defaultVersion, "aid-2", secondMerchantDetails, Nil)

  "When working with deals" should {

    var cookies: List[NewCookie] = List()

    "successfully sign up new user if correct data is passed" in {
      post(host + "/signup") withJsonData userInfo hasResponseCode 200
      Thread.sleep(1000)
      val hashKey: String = userService.getActionHashes.keySet.iterator.next()
      execute(userService.activateUser(hashKey)) match {
        case Success(_) => userService.activeUserExists(loginInfo.username, loginInfo.password) must equal(true)
        case Failure(e) => Assert.assertTrue("Failure when activating user.", false)
      }
    }

    "successfully retrieve all active deals even if not subscribed (/alldeals returns deals even if not subscribed)" in {
      execute(merchantService.createMerchant(firstMerchant))
      execute(merchantService.createCampaign(firstMerchant.id, Some(defaultVersion), firstCampaign.id, firstCampaignDetails))
      execute(merchantService.createDeal(firstMerchant.id, firstCampaign.id, Some(defaultVersion), firstDealId, firstDealDetails))

      execute(merchantService.createMerchant(secondMerchant))
      execute(merchantService.createCampaign(secondMerchant.id, Some(defaultVersion), secondCampaign.id, secondCampaignDetails))
      execute(merchantService.createDeal(secondMerchant.id, secondCampaign.id, Some(defaultVersion), secondDealId, secondDealDetails))

      execute(merchantService.activateCampaign(firstMerchant.id, firstCampaign.id, Some(defaultVersion)))
      execute(merchantService.activateCampaign(secondMerchant.id, secondCampaign.id, Some(defaultVersion)))

      val req = get(host + "/alldeals")
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val deals: List[ActiveDeal] = retrieveDeals(reqCall.bodyString).deals
      deals must have size (2)
    }

    "successfully login with correct login data" in {
      val req = post(host + "/login") withJsonData loginInfo
      val loginCall = ~req
      loginCall hasResponseCode 200
      cookies = req.responseCookies()
    }

    "successfully retrieve all merchants" in {
      val req = get(host + "/merchants") withCookies cookies
      val merchantsCall: RichHttp = ~req
      merchantsCall hasResponseCode 200

      val merchants: MarchantAndCampaigns = retrieveMerchants(merchantsCall.bodyString)
      merchants.merchants must have size (2)
    }

    "return profile data with no merchants subscribed" in {
      val req = get(host + "/profile") withCookies cookies
      val profileCall: RichHttp = ~req
      profileCall hasResponseCode 200
      val user: User = retrieveProfile(profileCall.bodyString)
      user.subscribedMerchantIds must have size 0
    }

    "successfully subscribe for first merchant coupons/deals retrieval" in {
      val subscribeData = SubscribeRequest(firstMerchant.id)

      val req = post(host + "/subscribe") withCookies cookies withJsonData subscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "successfully retrieve one deal from first merchant (/subscribeddeals returns deals only if subscribed)" in {
      val req = get(host + "/subscribeddeals") withCookies cookies
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveDeal] = retrieveDeals(reqCall.bodyString).deals
      coupons must have size 1
    }

    "return profile data with one merchant subscribed" in {
      val req = get(host + "/profile") withCookies cookies
      val profileCall: RichHttp = ~req
      profileCall hasResponseCode 200
      val user: User = retrieveProfile(profileCall.bodyString)
      user.subscribedMerchantIds must have size 1
    }

    "successfully subscribe for second merchant coupons retrieval" in {
      val subscribeData = SubscribeRequest(secondMerchant.id)

      val req = post(host + "/subscribe") withCookies cookies withJsonData subscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "return profile data with two merchants subscribed" in {
      val req = get(host + "/profile") withCookies cookies
      val profileCall: RichHttp = ~req
      profileCall hasResponseCode 200
      val user: User = retrieveProfile(profileCall.bodyString)
      user.subscribedMerchantIds must have size 2
    }

    "successfully retrieve two deals from first and second merchants (/subscribeddeals returns deals only if subscribed)" in {
      execute(merchantService.activateCampaign(secondMerchant.id, secondCampaign.id, Some(defaultVersion)))

      val req = get(host + "/subscribeddeals") withCookies cookies
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveDeal] = retrieveDeals(reqCall.bodyString).deals
      coupons must have size (2)
    }

    "successfully unsubscribe from first merchant coupons retrieval" in {
      val unsubscribeData = UnsubscribeRequest(firstMerchant.id)
      val req = post(host + "/unsubscribe") withCookies cookies withJsonData unsubscribeData
      val putCall: RichHttp = ~req
      putCall hasResponseCode 200
      putCall.hasNoError
    }

    "return profile data with second merchant only subscribed" in {
      val req = get(host + "/profile") withCookies cookies
      val profileCall: RichHttp = ~req
      profileCall hasResponseCode 200
      val user: User = retrieveProfile(profileCall.bodyString)
      user.subscribedMerchantIds must have size 1
    }

    "successfully retrieve one deal from second merchant (/subscribeddeals returns deals only if subscribed)" in {
      val req = get(host + "/subscribeddeals") withCookies cookies
      val reqCall: RichHttp = ~req
      reqCall hasResponseCode 200
      reqCall.hasNoError

      val coupons: List[ActiveDeal] = retrieveDeals(reqCall.bodyString).deals
      coupons must have size 1
    }

    def retrieveDeals(body: String): ActiveDeals = {
      val genType: TypeReference[AppResponseGeneric[ActiveDeals]] = new TypeReference[AppResponseGeneric[ActiveDeals]]() {}
      val resp: AppResponseGeneric[ActiveDeals] = mapper.readValue(body, genType)
      resp.data
    }

    def retrieveMerchants(body: String): MarchantAndCampaigns = {
      val genType: TypeReference[AppResponseGeneric[MarchantAndCampaigns]] = new TypeReference[AppResponseGeneric[MarchantAndCampaigns]]() {}
      val resp: AppResponseGeneric[MarchantAndCampaigns] = mapper.readValue(body, genType)
      resp.data
    }

    def retrieveProfile(body: String): User = {
      val genType: TypeReference[AppResponseGeneric[User]] = new TypeReference[AppResponseGeneric[User]]() {}
      val resp: AppResponseGeneric[User] = mapper.readValue(body, genType)
      resp.data
    }

  }

}
