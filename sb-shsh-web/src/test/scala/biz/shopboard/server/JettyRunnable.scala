package biz.shopboard.server

import org.scalatest.{ Suite, BeforeAndAfterAll }
import biz.shopboard.infrastructure.ShopBoardContext
import biz.shopboard.rest.RestService
import biz.shopboard.service.MailServiceMock
import org.slf4j.LoggerFactory

trait JettyRunnable extends Suite with BeforeAndAfterAll {
  val port = JettyServer.port
  val host = JettyServer.host

  val log = LoggerFactory.getLogger("biz.shopboard.server.JettyRunnable")

  override def beforeAll() {
    JettyServer.start
    log.info("Cleaning up Journal DB")
    ShopBoardContext.cleanup()
    log.info("Starting Akka")
    ShopBoardContext.mailService = new MailServiceMock
    ShopBoardContext.start()
  }

  override def afterAll() {
    log.info("Stopping Akka")
    ShopBoardContext.stop()
    System.gc()
  }

}

object JettyServer {

  val log = LoggerFactory.getLogger(JettyServer.getClass)

  val port = 9999
  val host = "http://localhost:" + port.toString

  private val _unfiltered_server = unfiltered.jetty.Http.local(port)

  private val _server = _unfiltered_server.underlying
  log.info("JettyTestServer prepared on " + host)

  lazy val start = {
    _unfiltered_server.filter(RestService)
    _server.start()
  }
}

