import java.io.FileInputStream
import net.liftweb.common.Full
import net.liftweb.util.Props
import org.mortbay.jetty.handler.{ DefaultHandler, HandlerCollection }
import org.mortbay.jetty.security.{ UserRealm, HashUserRealm }
import org.mortbay.jetty.{ Handler, Server }
import org.mortbay.jetty.webapp.WebAppContext

object LiftConsole extends App {
  // Set locations
  val warPath: String = System.getProperty("user.dir") + "/src/main/webapp"
  val filename: String = System.getProperty("user.dir") + "/src/main/resources/default.props"
  Props.whereToLook = () => List(("default", () => Full(new FileInputStream(filename))))

  val server = new Server(8080)
  val webappcontext = new WebAppContext(server, warPath, "/") //magic spice

  val handlers: HandlerCollection = new HandlerCollection()
  handlers.setHandlers(Array[Handler](webappcontext, new DefaultHandler()))

  server.setHandler(handlers)
  val myrealm: HashUserRealm = new HashUserRealm("MyRealm", warPath + "/etc/realm.properties")
  server.setUserRealms(Array[UserRealm](myrealm))

  server.start()
}
