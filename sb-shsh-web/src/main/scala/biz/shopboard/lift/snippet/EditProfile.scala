package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.user.User
import biz.shopboard.domain.model.common._
import biz.shopboard.lift._
import support.{ IntRequestVar, JsExecutable, Authenticated }
import net.liftweb._
import common.Full
import http._
import util.Helpers._
import js._
import JsCmds._
import scala.Some
import biz.shopboard.domain.model.user.UserDetails
import biz.shopboard.domain.model.common.Address

class EditProfile extends Authenticated with JsExecutable[User] {
  object email extends TransientRequestVar[String]("")
  object firstName extends TransientRequestVar[String]("")
  object lastName extends TransientRequestVar[String]("")
  object name extends TransientRequestVar[String]("")
  object country extends TransientRequestVar[String]("")
  object street extends TransientRequestVar[String]("")
  object state extends TransientRequestVar[String]("")
  object town extends TransientRequestVar[String]("")
  object sex extends TransientRequestVar[Sex](Sex.MALE)
  object age extends IntRequestVar(1, 100)

  def render(xhtml: NodeSeq): NodeSeq = {
    if (UserGUID.isDefined) userService.getUser(UserGUID) match {
      case Some(user) => {
        def edit(): JsCmd = {
          val cmd: JsCmd = markVars(email)
          if (!checkVarsMandatory(email)) {
            S.error(fields_are_empty); cmd
          } else if (!age.errors.isEmpty) {
            age.errors.map(S.error(_)); cmd
          } else {
            val address: Address = new Address(street.is, town.is, state.is, country.is)
            val details: UserDetails = user.details.copy(user.details.login, user.details.password, firstName.is, lastName.is, email.is, age.is, sex.is, address)//UserDetails.copy(user.details.login, user.details.password, firstName.is, lastName.is, email.is, age.is, sex.is, address)
            executeWith(redirectBack, cmd) { userService.updateUserDetails(user.id, user.versionOption, details) }
          }
        }

        bind("edit-profile", xhtml,
          "userName" -> SHtml.text(user.details.login, _.toString, "disabled" -> ""),
          "email" -> SHtml.text(user.details.email, email(_), "placeholder" -> "Email*", bindId(email)),
          "sex" -> SHtml.selectObj[Sex](Sex.SEXES.map(s => (s, s.name)), Full(user.details.sex), sex(_), "data-role" -> "slider", bindId(sex)),
          "age" -> SHtml.range(user.details.age, age(_), 1, 100, "data-highlight" -> "true", bindId(age)),
          "firstname" -> SHtml.text(user.details.firstName, firstName(_), "placeholder" -> "First Name"),
          "lastname" -> SHtml.text(user.details.lastName, lastName(_), "placeholder" -> "Last Name"),
          "country" -> SHtml.text(user.details.address.country, country(_), "placeholder" -> "Country"),
          "street" -> SHtml.text(user.details.address.street, street(_), "placeholder" -> "Street"),
          "state" -> SHtml.text(user.details.address.state, state(_), "placeholder" -> "State"),
          "town" -> SHtml.text(user.details.address.town, town(_), "placeholder" -> "Town"),
          "submit" -> SHtml.ajaxSubmit("Edit", edit, "data-theme" -> "b"))
      }
      case None => S.error(no_user_found); NodeSeq.Empty
    }
    else NodeSeq.Empty
  }
}
