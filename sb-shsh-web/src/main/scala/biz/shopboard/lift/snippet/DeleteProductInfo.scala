package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.merchant.{ DraftCoupon, DraftDeal, Deal }
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._

class DeleteProductInfo extends JsExecutable[Deal] {
  def render(xhtml: NodeSeq): NodeSeq =
    (S.param("product-info-id"), DealGUID.isDefined ? merchantService.getDeal(DealGUID) | merchantService.getCoupon(CouponGUID)) match {
      case (Full(id), Some(deal)) => {
        def delete(): JsCmd = {
          deal match {
            case d: DraftDeal => executeWith(redirectTo("product-info.html")) {
              val split = d.details.productInfo.splitAt(id.toInt)
              merchantService.updateDealProductInfo(MerchantGUID, d.id, d.versionOption, split._1 ::: split._2.tail)
            }
            case c: DraftCoupon => executeWith(redirectTo("product-info.html")) {
              val split = c.details.productInfo.splitAt(id.toInt)
              merchantService.updateCouponProductInfo(MerchantGUID, c.id, c.versionOption, split._1 ::: split._2.tail)
            }
            case _ => redirectBack
          }
        }
        bind("delete-product-info", xhtml, "submit" -> SHtml.ajaxSubmit("Delete Selected", delete, "data-theme" -> "b"))
      }
      case _ => NodeSeq.Empty
    }
}
