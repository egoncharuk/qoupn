package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.domain.model.merchant.Campaign

class DeleteDeal extends JsExecutable[Campaign] {

  def render(xhtml: NodeSeq): NodeSeq = S.param("deal-id") match {
    case Full(did) => merchantService.getDeal(did) match {
      case Some(deal) => {
        def delete(): JsCmd = (merchantService.getMerchant(MerchantGUID), merchantService.getCampaign(CampaignGUID)) match {
          case (Some(p), Some(c)) => executeWith(redirectBack) { merchantService.removeDeal(p.id, c.id, c.versionOption, deal.id, deal.versionOption) }
          case _ => S.error(no_merchant_or_campaign_found); Noop
        }
        bind("delete-deal", xhtml, "submit" -> SHtml.ajaxSubmit("Delete", delete, "data-theme" -> "b"))
      }
      case None => S.error(no_campaign_found); NodeSeq.Empty
    }
    case _ => S.error(parameters_are_incorrect); NodeSeq.Empty
  }

}
