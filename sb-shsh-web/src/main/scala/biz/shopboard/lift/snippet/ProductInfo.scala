package biz.shopboard.lift.snippet

import net.liftweb.util.BindHelpers._
import biz.shopboard.domain.model.merchant._
import biz.shopboard.lift.support.Authenticated
import biz.shopboard.lift._
import net.liftweb.util.CssSel
import org.joda.time.DateTime
import net.liftweb.http.S
import net.liftweb.common.{ Empty, Full }
import scala.Some
import xml.NodeSeq

class ProductInfo extends Authenticated {
  def render = {
    (S.param("deal-id"), S.param("coupon-id")) match {
      case (Full(did), Empty) => merchantService.getDeal(did) match {
        case Some(d: DraftDeal) =>
          CouponGUID.set(Empty); DealGUID.set(did); compose(d.details.productInfo, "")
        case Some(d: ActiveDeal) =>
          CouponGUID.set(Empty); DealGUID.set(did); compose(d.details.productInfo, hideCheckbox)
        case _ => NodeSeq.Empty
      }
      case (Empty, Full(cid)) => merchantService.getCoupon(cid) match {
        case Some(c: DraftCoupon) =>
          DealGUID.set(Empty); CouponGUID.set(cid); compose(c.details.productInfo, "")
        case Some(c: ActiveCoupon) =>
          DealGUID.set(Empty); CouponGUID.set(cid); compose(c.details.productInfo, hideCheckbox)
        case _ => NodeSeq.Empty
      }
      case _ if CouponGUID.isDefined => composeWithCoupon
      case _ if DealGUID.isDefined => composeWithDeal
      case _ => NodeSeq.Empty
    }
  }

  def composeWithDeal = merchantService.getDeal(DealGUID) match {
    case Some(d: ActiveDeal) => compose(d.details.productInfo, hideCheckbox)
    case Some(d: DraftDeal) => compose(d.details.productInfo, "")
    case _ => NodeSeq.Empty
  }

  def composeWithCoupon = merchantService.getCoupon(CouponGUID) match {
    case Some(c: ActiveCoupon) => compose(c.details.productInfo, hideCheckbox)
    case Some(c: DraftCoupon) => compose(c.details.productInfo, "")
    case _ => NodeSeq.Empty
  }

  def hideCheckbox(): String = "display:none !important;"

  def compose(products: List[biz.shopboard.domain.model.common.ProductInfo], visibilityCheckBox: String): CssSel = {
    val visibility = if (!products.isEmpty) "" else "display:none;"
    val visibilityDivSel: CssSel = "#divider-item [style]" #> visibility
    val visibilitySel: CssSel = "#product-info-item [style]" #> visibility
    val countSel: CssSel = "#divider-item" #> { "#cnt *" #> products.size }
    val timeSel: CssSel = "#divider-item" #> { "#time *" #> printDate(new DateTime()) }
    val bindInfo: (biz.shopboard.domain.model.common.ProductInfo) => CssSel = info => {
      "h3 *+" #> info.name & "#delete [href+]" #> products.lastIndexOf(info) &
        "div .ui-li-aside [style]" #> visibilityCheckBox &
        "p" #> { "strong *" #> info.category } & "p .description *" #> info.description
    }

    timeSel & countSel & visibilitySel & visibilityDivSel & "#product-info-item *" #> products.map(bindInfo)
  }
}
