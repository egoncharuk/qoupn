package biz.shopboard.lift.snippet

import biz.shopboard.domain.model.merchant._
import _root_.scala.xml.NodeSeq
import biz.shopboard.lift._
import support.{ JsExecutable, Authenticated }
import net.liftweb._
import common.Full
import http._
import util.Helpers._
import js._
import JsCmds._
import scala.Some
import net.liftweb.http.SHtml.ElemAttr

class EditCampaign extends Authenticated with JsExecutable[Campaign] {

  object id extends TransientRequestVar[String]("")
  object name extends TransientRequestVar[String]("")
  object desc extends TransientRequestVar[String]("")
  object validFrom extends TransientRequestVar[String]("")
  object validTo extends TransientRequestVar[String]("")

  val dateIdAttribute: ElemAttr = "id" -> "datefield"

  def render(xhtml: NodeSeq): NodeSeq = {
    S.param("campaign-id") match {
      case Full(cid) => merchantService.getCampaign(cid) match {
        case Some(campaign) => {
          def edit(): JsCmd = campaign match {
            case c: DraftCampaign => {
              val cmd: JsCmd = markVars(name, desc, validTo, validFrom)
              if (!checkVarsMandatory(name, desc, validTo, validFrom)) {
                S.error(fields_are_empty); cmd
              } else if (!checkValidDate(validFrom)) {
                S.error(valid_from_invalid); cmd
              } else if (!checkValidDate(validTo)) {
                S.error(valid_to_invalid); cmd
              } else merchantService.getMerchant(MerchantGUID) match {
                case None =>
                  S.error(no_merchant_found); cmd
                case Some(merchant) => {
                  val details = new CampaignDetails(name.is, desc.is, parseDate(validFrom), parseDate(validTo))
                  executeWith(redirectTo("campaigns.html"), cmd) { merchantService.updateCampaignDetails(merchant.id, campaign.id, campaign.versionOption, details) }
                }
              }
            }
            case c: InactiveCampaign => {
              val cmd: JsCmd = markVars(validTo)
              if (!checkVarsMandatory(validTo)) {
                S.error(fields_are_empty); cmd
              } else if (!checkValidDate(validTo)) {
                S.error(valid_to_invalid); cmd
              } else merchantService.getMerchant(MerchantGUID) match {
                case None =>
                  S.error(no_merchant_found); cmd
                case Some(merchant) => {
                  val details = new CampaignDetails(campaign.details.name, campaign.details.description, campaign.details.valid_from, parseDate(validTo))
                  executeWith(redirectTo("campaigns.html"), cmd) { merchantService.updateCampaignDetails(merchant.id, campaign.id, campaign.versionOption, details) }
                }
              }
            }
            case _ => redirectTo("campaigns.html")
          }

          id(campaign.id)
          name(campaign.details.name)
          desc(campaign.details.description)
          validTo(printDateFull(campaign.details.valid_to))
          validFrom(printDateFull(campaign.details.valid_from))

          val firstModifier = campaign match {
            case c: DraftCampaign => "enabled" -> ""
            case _ => "disabled" -> ""
          }
          val secondModifier = campaign match {
            case c: DraftCampaign => "enabled" -> ""
            case c: InactiveCampaign => "enabled" -> ""
            case _ => "disabled" -> ""
          }

          bind("edit-campaign", xhtml,
            "id" -> SHtml.text(id.is, id(_), "disabled" -> ""),
            "name" -> SHtml.text(name.is, name(_), "placeholder" -> "Name*", bindId(name)) % firstModifier,
            "desc" -> SHtml.text(desc.is, desc(_), "placeholder" -> "Description*", bindId(desc)) % firstModifier,
            "valid-to" -> SHtml.text(validTo.is, validTo(_), "placeholder" -> ("Valid To* [" + dateFormatFull.toUpperCase + "]"), dateIdAttribute) % secondModifier,
            "valid-from" -> SHtml.text(validFrom.is, validFrom(_), "placeholder" -> ("Valid From* [" + dateFormatFull.toUpperCase + "]"), dateIdAttribute) % firstModifier,
            "submit" -> SHtml.ajaxSubmit("Edit", edit, "data-theme" -> "b") % secondModifier)
        }
        case _ => S.error(no_campaign_found); NodeSeq.Empty
      }
      case _ => S.error(no_campaign_found); NodeSeq.Empty
    }
  }

}
