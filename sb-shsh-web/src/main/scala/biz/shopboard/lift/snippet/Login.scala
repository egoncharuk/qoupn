package biz.shopboard.lift.snippet

import scala.xml.NodeSeq
import biz.shopboard.lift.support.Authenticated
import biz.shopboard.lift._
import net.liftweb._
import http._
import util.Helpers._
import js._
import JsCmds._

class Login extends Authenticated {
  object user extends RequestVar[String]("")
  def render(xhtml: NodeSeq): NodeSeq = {
    var pass = ""
    def auth(): JsCmd = {
      if (userService.activeUserExists(user.is, pass)) {
        UserGUID.set(userService.findUserByNamePwd(user.is, pass).id)
        if (isAdmin) setContextPath & redirectTo("merchants.html")
        else if (isMerchant) setContextPath & redirectTo("campaigns.html")
        else if (isUser) setContextPath & redirectTo("my-shopping-lists.html")
        else Noop
      } else {
        S.error(invalid_user); Noop
      }
    }
    bind("login", xhtml,
      "username" -> SHtml.text(user.is, user(_), "placeholder" -> "Username*"),
      "password" -> SHtml.password(pass, pass = _, "placeholder" -> "Password*"),
      "submit" -> SHtml.ajaxSubmit("Login", auth, "data-theme" -> "b"))
  }
}
