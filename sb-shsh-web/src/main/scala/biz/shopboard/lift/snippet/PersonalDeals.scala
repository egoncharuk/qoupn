package biz.shopboard.lift.snippet

import biz.shopboard.lift._
import support.{ Authenticated, DealDetailsComposable }
import net.liftweb.util.CssSel
import scala.Some
import xml.NodeSeq
import biz.shopboard.domain.model.merchant.{Merchant, Deal}

class PersonalDeals extends Authenticated with DealDetailsComposable {
  def render = {
    if (UserGUID.isDefined) dealSubscriptionService.collectMerchants(UserGUID) match {
      case Some(merchants) => compose(merchants)
      case _ => compose(Iterable())
    }
    else NodeSeq.Empty
  }

  def compose(merchantIds: Iterable[String]): CssSel = {
    val deals = merchantIds.flatMap {
      id => retrieveDealsForMerchant(id)
    }
    var seenDeals: Set[String] = Set()
    val dealDetails = deals.collect {
      case (merchant, Some(deal))
        if !seenDeals.contains(deal.id) => seenDeals = seenDeals + deal.id;(deal.id, deal.details, merchant)
    }
    compose(dealDetails, "#deal-item")
  }

  def retrieveDealsForMerchant(id: String): List[(Option[Merchant], Option[Deal])] = {
    val merchant = merchantService.getMerchant(id)
    dealSubscriptionService.collectDeals(UserGUID, merchant) match {
      case Some(dealIds) => dealIds.map(did => (merchant, merchantService.getDeal(did)))
      case None => List()
    }
  }
}
