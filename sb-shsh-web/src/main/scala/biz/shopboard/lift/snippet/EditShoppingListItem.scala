package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.user.{ ShoppingListItem, ShoppingList }
import biz.shopboard.lift._
import support.{ DoubleRequestVar, IntRequestVar, JsExecutable }
import net.liftweb._
import common.Full
import http._
import util.Helpers._
import js._
import JsCmds._
import scala.Some
import biz.shopboard.Constants._

class EditShoppingListItem extends JsExecutable[ShoppingList] {
  object name extends TransientRequestVar[String]("")
  object desc extends TransientRequestVar[String]("")
  object quantity extends IntRequestVar(0, Int.MaxValue)
  object price extends DoubleRequestVar(0, Double.MaxValue)
  object done extends TransientRequestVar[Boolean](false)

  def render(xhtml: NodeSeq): NodeSeq = {
    (S.param("shopping-list-item-id"), S.param("shopping-list-id")) match {
      case (Full(slid), Full(sid)) => {
        userService.getShoppingList(sid) match {
          case Some(shoppingList) => shoppingList.items.get(slid) match {
            case Some(item) => {
              def edit(): JsCmd = {
                val cmd: JsCmd = markVars(name)
                if (!checkVarsMandatory(name)) {
                  S.error(fields_are_empty); cmd
                } else {
                  val newItem: ShoppingListItem = ShoppingListItem(item.id, name.is, desc.is, quantity.is, price.is, done.is)
                  executeWith(redirectBack, cmd) { userService.updateShoppingListItem(UserGUID, shoppingList.id, shoppingList.versionOption, newItem) }
                }
              }

              def value(n: Number) = if (n == 0) "value" -> "" else "value-exists" -> ""

              bind("edit-shopping-list-item", xhtml,
                "done" -> SHtml.checkbox(item.done, done(_), bindId(done)),
                "name" -> SHtml.text(item.name, name(_), "placeholder" -> "Name*", bindId(name)),
                "quantity" -> SHtml.number(item.quantity, quantity(_), 0, Int.MaxValue, "placeholder" -> "Quantity", bindId(quantity)) % value(item.quantity),
                "price" -> SHtml.number(item.price, price(_), 0.0D, Double.MaxValue, 0.01D, "placeholder" -> "Price", bindId(price)) % value(item.price),
                "desc" -> SHtml.text(item.desc, desc(_), "placeholder" -> "Description"),
                "submit" -> SHtml.ajaxSubmit("Edit", edit, "data-theme" -> "b"))
            }
            case None => S.error(no_shopping_list_item_found); NodeSeq.Empty
          }
          case None => S.error(no_shopping_list_found); NodeSeq.Empty
        }
      }
      case _ => S.error(parameters_are_incorrect); NodeSeq.Empty
    }
  }
}
