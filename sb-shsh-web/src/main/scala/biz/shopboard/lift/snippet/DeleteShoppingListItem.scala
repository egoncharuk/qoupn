package biz.shopboard.lift.snippet

import xml.NodeSeq
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.domain.model.user.ShoppingList
import biz.shopboard.Constants._
import scala.Some

class DeleteShoppingListItem extends JsExecutable[ShoppingList] {

  def render(xhtml: NodeSeq): NodeSeq = {
    (S.param("shopping-list-item-id"), S.param("shopping-list-id")) match {
      case (Full(slid), Full(sid)) => {
        userService.getShoppingList(sid) match {
          case Some(shoppingList) => {
            def delete(): JsCmd = executeWith(redirectBack, Noop) { userService.removeShoppingListItem(UserGUID, shoppingList.id, shoppingList.versionOption, slid) }
            bind("delete-shopping-list-item", xhtml, "submit" -> SHtml.ajaxSubmit("Delete", delete, "data-theme" -> "b"))
          }
          case None => S.error(no_shopping_list_found); NodeSeq.Empty
        }
      }
      case _ => S.error(parameters_are_incorrect); NodeSeq.Empty
    }
  }

}
