package biz.shopboard.lift.snippet

import biz.shopboard.lift._
import support.{ DoubleRequestVar, JsExecutable, Authenticated }
import net.liftweb._
import common.{ Empty, Box, Full }
import http._
import util.Helpers._
import js._
import JsCmds._
import scala.xml._
import biz.shopboard.domain.model.merchant._
import org.joda.time.DateTime
import org.apache.commons.codec.binary.Base64
import net.liftweb.http.SHtml.ElemAttr
import scala.Some
import biz.shopboard.domain.model.merchant.DealDetails
import biz.shopboard.domain.model.common.DiscountInfo

class EditDeal extends Authenticated with JsExecutable[Deal] {

  object id extends TransientRequestVar[String]("")
  object name extends TransientRequestVar[String]("")
  object desc extends TransientRequestVar[String]("")
  object validFrom extends TransientRequestVar[String]("")
  object validTo extends TransientRequestVar[String]("")
  object originalPrice extends DoubleRequestVar(0D, Double.MaxValue)
  object discountPercent extends DoubleRequestVar(1D, 100D)
  object discountedPrice extends DoubleRequestVar(0D, Double.MaxValue)
  object picture extends TransientRequestVar[Box[FileParamHolder]](Empty)

  val dateIdAttribute: ElemAttr = "id" -> "datefield"

  def render(xhtml: NodeSeq): NodeSeq = {
    S.param("deal-id") match {
      case Full(did) => merchantService.getDeal(did) match {
        case Some(deal) => {
          def edit(): JsCmd = deal match {
            case c: DraftDeal => {
              val cmd: JsCmd = markVars(id, name, desc, validTo, validFrom) & originalPrice & discountedPrice & discountPercent
              if (!checkVarsMandatory(name, desc, validTo, validFrom)) {
                S.error(fields_are_empty); cmd
              } else if (!checkValidDate(validFrom)) {
                S.error(valid_from_invalid); cmd
              } else if (!checkValidDate(validTo)) {
                S.error(valid_to_invalid); cmd
              } else if (!originalPrice.errors.isEmpty) {
                originalPrice.errors.map(S.error(_)); cmd
              } else if (!discountedPrice.errors.isEmpty) {
                discountedPrice.errors.map(S.error(_)); cmd
              } else if (!discountPercent.errors.isEmpty) {
                discountPercent.errors.map(S.error(_)); cmd
              } else if (!checkImage(picture)) {
                S.error(image_invalid); cmd
              } else executeWith(reload) {
                val to: DateTime = parseDate(validTo)
                val from: DateTime = parseDate(validFrom)
                val path: String = deal.details.filePath
                val discount: DiscountInfo = DiscountInfo(originalPrice.is, discountPercent.is, discountedPrice.is)
                val fileContent: String = picture.is.isEmpty ? deal.details.fileBase | Base64.encodeBase64String(picture.is.file)
                merchantService.updateDealDetails(MerchantGUID, CampaignGUID, deal.id, deal.versionOption, new DealDetails(name.is, desc.is, discount, from, to, deal.details.productInfo, path, fileContent))
              }
            }
            case c: InactiveDeal => {
              val cmd: JsCmd = markVars(validTo)
              if (!checkVarsMandatory(validTo)) {
                S.error(fields_are_empty); cmd
              } else if (!checkValidDate(validTo)) {
                S.error(valid_to_invalid); cmd
              } else executeWith(reload) {
                val to: DateTime = parseDate(validTo)
                val inner: DealDetails = c.details.copy(validTo = to)
                merchantService.updateDealDetails(MerchantGUID, CampaignGUID, deal.id, deal.versionOption, inner)
              }
            }
            case _ => reload
          }

          id(deal.id)
          name(deal.details.name)
          desc(deal.details.description)
          validTo(printDateFull(deal.details.validTo))
          validFrom(printDateFull(deal.details.validFrom))
          originalPrice(deal.details.discount.originalPrice)
          discountedPrice(deal.details.discount.discountedPrice)
          discountPercent(deal.details.discount.discountPercent)

          val firstModifier = deal match {
            case c: DraftDeal => "enabled" -> ""
            case _ => "disabled" -> ""
          }
          val secondModifier = deal match {
            case c: DraftDeal => "enabled" -> ""
            case c: InactiveDeal => "enabled" -> ""
            case _ => "disabled" -> ""
          }

          bind("edit-deal", xhtml,
            "id" -> SHtml.text(id.is, id(_), "disabled" -> ""),
            "name" -> SHtml.text(name.is, name(_), "placeholder" -> "Name*", bindId(name)) % firstModifier,
            "desc" -> SHtml.text(desc.is, desc(_), "placeholder" -> "Description*", bindId(desc)) % firstModifier,
            "original-price" -> SHtml.number(originalPrice.is, originalPrice(_), 0.0D, Double.MaxValue, 0.01D, "placeholder" -> "Original Price", bindId(originalPrice)) % firstModifier,
            "discount-price" -> SHtml.number(discountedPrice.is, discountedPrice(_), 0.0D, Double.MaxValue, 0.01D, "placeholder" -> "Discounted Price", bindId(discountedPrice)) % firstModifier,
            "discount-percent" -> SHtml.number(discountPercent.is, discountPercent(_), 1D, 100D, 0.01D, "placeholder" -> "Discount Percent", bindId(discountPercent)) % firstModifier,
            "valid-to" -> SHtml.text(validTo.is, validTo(_), "placeholder" -> ("Valid To* [" + dateFormatFull.toUpperCase + "]"), dateIdAttribute) % secondModifier,
            "valid-from" -> SHtml.text(validFrom.is, validFrom(_), "placeholder" -> ("Valid From* [" + dateFormatFull.toUpperCase + "]"), dateIdAttribute) % firstModifier,
            "image" -> SHtml.fileUpload(param => picture(Full(param)), "id" -> "uploadImage") % firstModifier,
            "submit" -> SHtml.ajaxSubmit("Edit", edit, "data-theme" -> "b") % secondModifier)
        }
        case _ => S.error(no_deal_found); NodeSeq.Empty
      }
      case _ => S.error(no_deal_found); NodeSeq.Empty
    }
  }
}
