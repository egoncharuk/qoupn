package biz.shopboard.lift.snippet

import xml.NodeSeq
import biz.shopboard.lift._
import net.liftweb._
import common.{Empty, Box, Full}
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.domain.model.merchant.{ Merchant, MerchantDetails }
import biz.shopboard.domain.model.common.Address
import org.apache.commons.codec.binary.Base64
import biz.shopboard.util.Hashing
import biz.shopboard.Constants._

class EditMerchant extends JsExecutable[Merchant] {

  object id extends TransientRequestVar[String]("")
  object userId extends TransientRequestVar[String]("")
  object name extends TransientRequestVar[String]("")
  object street extends TransientRequestVar[String]("")
  object state extends TransientRequestVar[String]("")
  object town extends TransientRequestVar[String]("")
  object country extends TransientRequestVar[String]("")
  object picture extends TransientRequestVar[Box[FileParamHolder]](Empty)

  def render(xhtml: NodeSeq): NodeSeq = {
    S.param("merchant-id") match {
      case Full(pid) => merchantService.getMerchant(pid) match {
        case Some(merchant) => {
          def edit(): JsCmd = {
            val cmd: JsCmd = markVars(name, street, town, country)
            if (!checkVarsMandatory(name, street, town, country)) {
              S.error(fields_are_empty); cmd
            } else if (!checkImage(picture)) {
              S.error(image_invalid); Noop
            } else {
              val hash: String = Hashing.generate(merchant.adminId + id_delimiter + id.is)
              val pid = "merchant_" + hash.getBytes("UTF-8").foldRight("") { String.valueOf(_) + _ }
              val path = directories.appDir + imgPath + pid + "." + picture.is.mimeType.split("/").last
              val filePath: String = (merchant.details.filePath == null) ? path | merchant.details.filePath
              val fileBase: String = picture.is.isEmpty ? merchant.details.fileBase | Base64.encodeBase64String(picture.is.file)
              val details = new MerchantDetails(name.is, Address(street.is, town.is, state.is, country.is), filePath, fileBase)
              executeWith(redirectTo("merchants.html"), cmd) { merchantService.updateMerchantDetails(merchant.id, merchant.versionOption, details) }
            }
          }
          id(merchant.id)
          userId(merchant.adminId)
          name(merchant.details.name)
          street(merchant.details.address.street)
          town(merchant.details.address.town)
          state(merchant.details.address.state)
          country(merchant.details.address.country)

          bind("edit-merchant", xhtml,
            "id" -> SHtml.text(id.is, id(_), "disabled" -> ""),
            "userid" -> SHtml.select(userService.getMerchants.map(u => (u.id, u.details.login)).toSeq, Full(userId.is), value => userId(value), "disabled" -> ""),
            "name" -> SHtml.text(name.is, name(_), "placeholder" -> "Name*", bindId(name)),
            "street" -> SHtml.text(street.is, street(_), "placeholder" -> "Street*", bindId(street)),
            "town" -> SHtml.text(town.is, town(_), "placeholder" -> "Town*", bindId(town)),
            "state" -> SHtml.text(state.is, state(_), "placeholder" -> "State", bindId(state)),
            "country" -> SHtml.text(country.is, country(_), "placeholder" -> "Country*", bindId(country)),
            "image" -> SHtml.fileUpload(param => picture(Full(param)), "id" -> "uploadImage"),
            "submit" -> SHtml.ajaxSubmit("Edit Merchant", edit, "data-theme" -> "b"))
        }
        case None => S.error(no_merchant_found); NodeSeq.Empty
      }
      case _ => S.error(no_merchant_found); NodeSeq.Empty
    }
  }

}
