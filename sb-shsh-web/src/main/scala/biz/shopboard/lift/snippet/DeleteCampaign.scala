package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.domain.model.merchant.Campaign

class DeleteCampaign extends JsExecutable[Campaign] {

  def render(xhtml: NodeSeq): NodeSeq = S.param("campaign-id") match {
    case Full(cid) => merchantService.getCampaign(cid) match {
      case Some(campaign) => {
        def delete(): JsCmd = merchantService.getMerchant(MerchantGUID) match {
          case Some(merchant) => executeWith(redirectTo("campaigns.html")) { merchantService.removeCampaign(merchant.id, campaign.id, campaign.versionOption) }
          case None => S.error(no_merchant_found); Noop
        }
        bind("delete-campaign", xhtml, "submit" -> SHtml.ajaxSubmit("Delete", delete, "data-theme" -> "b"))
      }
      case None => S.error(no_campaign_found); NodeSeq.Empty
    }
    case _ => S.error(parameters_are_incorrect); NodeSeq.Empty
  }

}
