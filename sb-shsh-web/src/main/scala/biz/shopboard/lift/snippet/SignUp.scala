package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.user.{ User, UserDetails }
import biz.shopboard.lift._
import support.Authenticated
import net.liftweb._
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.domain.model.common.UserRole
import biz.shopboard.lift.support.JsExtendedCmds.AlertWithTitle

class SignUp extends Authenticated with JsExecutable[User] {
  object username extends TransientRequestVar[String]("")
  object email extends TransientRequestVar[String]("")
  object firstName extends TransientRequestVar[String]("")
  object lastName extends TransientRequestVar[String]("")
  object honeypot extends TransientRequestVar[String]("")

  def render(xhtml: NodeSeq): NodeSeq = {
    var password = ""
    var password2 = ""
    def signup(): JsCmd = {
      val cmd1: JsCmd = markVars(username, email)
      val cmd2: JsCmd = markMandatory(password -> "pass1", password2 -> "pass2") & cmd1
      if (!checkVarsMandatory(username, email) || password == "" || password2 == "") {
        S.error(fields_are_empty); cmd2
      } else if (!honeypot.is.isEmpty) {
        S.error(bots_are_ignored); cmd2
      } else if (password != password2) {
        S.error(passwords_dont_match); cmd2
      } else {
        val details: UserDetails = UserDetails(username.is, password, firstName.is, lastName.is, email.is)
        executeWith(AlertWithTitle(signup_success) & redirectTo("index.html"), cmd2) { userService.createUser("", details, List(UserRole)) }
      }
    }
    bind("signup", xhtml,
      "username" -> SHtml.text(username.is, username(_), "placeholder" -> "Username*", bindId(username)),
      "password" -> SHtml.password(password, password = _, "placeholder" -> "Password*", "id" -> "pass1"),
      "password2" -> SHtml.password(password2, password2 = _, "placeholder" -> "Password confirmation*", "id" -> "pass2"),
      "email" -> SHtml.text(email.is, email(_), "placeholder" -> "Email*", bindId(email)),
      "firstname" -> SHtml.text(firstName.is, firstName(_), "placeholder" -> "First Name", bindId(firstName)),
      "lastname" -> SHtml.text(lastName.is, lastName(_), "placeholder" -> "Last Name", bindId(lastName)),
      "honeypot" -> SHtml.text(honeypot.is, honeypot(_), "class" -> "pot", "data-role" -> "none"),
      "submit" -> SHtml.ajaxSubmit("Signup", signup, "data-theme" -> "b"))
  }
}
