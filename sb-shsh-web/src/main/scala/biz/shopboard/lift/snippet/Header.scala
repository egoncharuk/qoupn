package biz.shopboard.lift.snippet

import xml.NodeSeq
import biz.shopboard.lift.UserGUID

class Header {
  def render(xhtml: NodeSeq): NodeSeq = {
    if (UserGUID.isDefined) {
      <a id="logout" class="center-button" data-role="button" data-icon="custom" data-theme="a" data-inline="true" onclick="logout();">Logout</a>
    } else {
      <a id="signup" class="center-button" href="signup.html" data-role="button" data-icon="custom" data-theme="a" data-inline="true">Signup</a>
    }
  }
}
