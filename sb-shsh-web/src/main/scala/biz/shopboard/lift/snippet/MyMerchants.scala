package biz.shopboard.lift.snippet

import biz.shopboard.domain.model.user.User
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import js.JE.{Call, JsArray, JsRaw, Str}
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.lift.support.JsExtendedCmds.AlertWithTitle
import biz.shopboard.domain.model.merchant.MerchantDetails

class MyMerchants extends JsExecutable[User] {

  def render = {
    var merchants: Map[String, Boolean] = Map()
    val selected: List[String] = S.param("type") match {
      case Full("subscribe") => couponSubscriptionService.collectSubscribeOnlyMerchants(UserGUID).getOrElse(Iterable()).toList
      case _ => List() //TODO adjust according to session storage values passed in param
    }

    val allMerchants: List[(String, MerchantDetails)] = merchantService.getMerchants.map(u => (u.id, u.details)).toList

    def subscribe(): JsCmd = {
      val filtered: Map[String, Boolean] = merchants.filter(m => m._2 && !selected.contains(m._2))
      executeWithSeq(Noop, Noop) { filtered.map(m => userService.subscribeToMerchant(UserGUID, m._1)).toList }
      executeWithSeq(if (S.errors.isEmpty) AlertWithTitle(save_merchants_success) & redirectBack else Noop, Noop) {
        merchants.filterNot(_._2).map(m => userService.unsubscribeFromMerchant(UserGUID, m._1)).toList
      }
    }

    def search(): JsCmd = {
      val filtered: Map[String, Boolean] = merchants.filter(m => m._2 && !selected.contains(m._2))
      val filteredPaths = JsArray(allMerchants.filter(m => filtered.contains(m._1)).map(m => {
        Str(!m._2.isWritten ? imgStaticStorePath | m._2.filePath.substring(m._2.filePath.indexOf(imgPath)))
      }))
      SetExp(JsRaw("sessionStorage.stores"), Call("JSON.stringify", JsArray(filtered.keys.toList.map(Str(_))))) &
        SetExp(JsRaw("sessionStorage.paths"), Call("JSON.stringify", filteredPaths)) & redirectBack
    }

    val f: () => JsCmd = S.param("type") match {
      case Full("subscribe") => subscribe
      case Full("search") => search
      case _ => () => redirectBack
    }

    ".btn" #> SHtml.ajaxSubmit("Save", f, "data-theme" -> "b") &
      ".merchant-lbl" #> allMerchants.map(m => <label for={ m._1 }>{ m._2.name }</label>) &
      ".merchant-id" #> allMerchants.map(m => SHtml.checkbox_id(selected.contains(m._1), value => { merchants = merchants.updated(m._1, value) }, Full(m._1)))
  }
}
