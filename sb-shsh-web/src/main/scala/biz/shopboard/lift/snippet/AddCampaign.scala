package biz.shopboard.lift.snippet

import biz.shopboard.domain.model.merchant.{ Campaign, CampaignDetails }
import scala.xml.NodeSeq
import biz.shopboard.lift._
import biz.shopboard.Constants._
import support.{ JsExecutable, Authenticated }
import net.liftweb._
import http._
import util.Helpers._
import js._
import JsCmds._
import net.liftweb.http.SHtml.ElemAttr

class AddCampaign extends Authenticated with JsExecutable[Campaign] {

  object id extends TransientRequestVar[String]("")
  object name extends TransientRequestVar[String]("")
  object desc extends TransientRequestVar[String]("")
  object validFrom extends TransientRequestVar[String]("")
  object validTo extends TransientRequestVar[String]("")

  val dateIdAttribute: ElemAttr = "id" -> "datefield"

  def render(xhtml: NodeSeq): NodeSeq = {
    def add(): JsCmd = {
      val cmd: JsCmd = markVars(id, name, desc, validTo, validFrom)
      if (!checkVarsMandatory(id, name, desc, validTo, validFrom)) {
        S.error(fields_are_empty); cmd
      } else if (!checkValidDate(validFrom)) {
        S.error(valid_from_invalid); cmd
      } else if (!checkValidDate(validTo)) {
        S.error(valid_to_invalid); cmd
      } else merchantService.getMerchant(MerchantGUID) match {
        case None =>
          S.error(no_merchant_found); cmd
        case Some(merchant) => {
          val details = new CampaignDetails(name.is, desc.is, parseDate(validFrom), parseDate(validTo))
          executeWith(redirectTo("campaigns.html"), cmd) { merchantService.createCampaign(merchant.id, merchant.versionOption, merchant.id + id_delimiter + id.is, details) }
        }
      }
    }
    bind("add-campaign", xhtml,
      "id" -> SHtml.text(id.is, id(_), "placeholder" -> "Campaign ID*", bindId(id)),
      "name" -> SHtml.text(name.is, name(_), "placeholder" -> "Name*", bindId(name)),
      "desc" -> SHtml.text(desc.is, desc(_), "placeholder" -> "Description*", bindId(desc)),
      "valid-to" -> SHtml.text(validTo.is, validTo(_), "placeholder" -> ("Valid To* [" + dateFormatFull.toUpperCase + "]"),
        dateIdAttribute),
      "valid-from" -> SHtml.text(validFrom.is, validFrom(_), "placeholder" -> ("Valid From* [" + dateFormatFull.toUpperCase + "]"),
        dateIdAttribute),
      "submit" -> SHtml.ajaxSubmit("Add Campaign", add, "data-theme" -> "b"))
  }
}
