package biz.shopboard.lift.snippet

import net.liftweb.util.BindHelpers._
import biz.shopboard.domain.model.merchant._
import biz.shopboard.lift.support.Authenticated
import biz.shopboard.lift._
import biz.shopboard.lift.UserGUID
import net.liftweb.util.CssSel
import org.joda.time.DateTime
import net.liftweb.http.S
import net.liftweb.common.Full
import scala.Some
import xml.NodeSeq

class Campaigns extends Authenticated {
  def render = {
    if (isAdmin) {
      S.param("merchant-id") match {
        case Full(pid) => merchantService.getMerchant(pid) match {
          case Some(merchant) =>
            MerchantGUID.set(pid); compose(merchant)
          case _ => NodeSeq.Empty
        }
        case _ if MerchantGUID.isDefined => composeWithMerchant
        case _ => NodeSeq.Empty
      }
    } else if (MerchantGUID.isDefined) {
      composeWithMerchant
    } else merchantService.findMerchantByAdmin(UserGUID) match {
      case Some(merchant) =>
        MerchantGUID.set(merchant.id); compose(merchant)
      case _ => NodeSeq.Empty
    }
  }

  def composeWithMerchant = merchantService.getMerchant(MerchantGUID) match {
    case Some(merchant) => compose(merchant)
    case _ => NodeSeq.Empty
  }

  def compose(merchant: Merchant): CssSel = {
    val campaignIds: List[String] = merchant.campaignIds
    val campaigns = campaignIds.map(merchantService.getCampaign(_))
    val draftDetails = campaigns collect {
      case Some(c: DraftCampaign) => (c.id, c.details)
    }
    val activeDetails = campaigns collect {
      case Some(c: ActiveCampaign) => (c.id, c.details)
    }
    val inactiveDetails = campaigns collect {
      case Some(c: InactiveCampaign) => (c.id, c.details)
    }

    val sel1 = compose(draftDetails, "#draft-campaign-item", "#divider-item-1", "#cnt-1", "#time-1")
    val sel2 = compose(activeDetails, "#active-campaign-item", "#divider-item-2", "#cnt-2", "#time-2")
    val sel3 = compose(inactiveDetails, "#inactive-campaign-item", "#divider-item-3", "#cnt-3", "#time-3")

    val bindListMenu: ((String, CampaignDetails)) => CssSel = d => "data-role=popup [id]" #> strip(d._1) &
      "#edit-campaign [href+]" #> d._1 & "#delete-campaign [href+]" #> d._1 &
      "#activate-campaign [href+]" #> d._1 & "#deactivate-campaign [href+]" #> d._1

    sel1 & sel2 & sel3 & ".popup-container *" #> (draftDetails ::: activeDetails ::: inactiveDetails).map(bindListMenu)
  }

  def compose(details: List[(String, CampaignDetails)], itemId: String, dividerId: String, cntId: String, timeId: String): CssSel = {
    val visibility = if (!details.isEmpty) "" else "display:none;"
    val visibilitySel: CssSel = (itemId + " [style]") #> visibility
    val visibilityDivSel: CssSel = (dividerId + " [style]") #> visibility
    val countSel: CssSel = dividerId #> {
      (cntId + " *") #> details.size
    }
    val timeSel: CssSel = dividerId #> {
      (timeId + " *") #> printDate(new DateTime())
    }
    val bindDetails: ((String, CampaignDetails)) => CssSel = d => "h3 *" #> d._2.name & "p .ui-li-aside" #> {
      "strong *" #> printDate(d._2.valid_to)
    } & "p" #> {
      "strong *" #> d._2.description
    } & ".popupLink [href+]" #> strip(d._1) & ".go-to-deals-ignore [href+]" #> d._1

    timeSel & countSel & visibilitySel & visibilityDivSel & (itemId + " *") #> details.map(bindDetails)
  }
}
