package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.lift._
import net.liftweb._
import common.{Full, Empty, Box}
import http._
import biz.shopboard.lift.support.{ Authenticated, JsExecutable }
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.Constants._
import biz.shopboard.domain.model.merchant.Merchant
import biz.shopboard.domain.model.user.{ User, UserDetails }
import biz.shopboard.domain.model.merchant.MerchantDetails
import biz.shopboard.domain.model.common.{ MerchantRole, Address }
import scalaz.{ Failure, Success }
import concurrent.Future
import biz.shopboard.util.Hashing
import org.apache.commons.codec.binary.Base64

class AddMerchant extends Authenticated with JsExecutable[Merchant] {

  object id extends TransientRequestVar[String]("")
  object userName extends TransientRequestVar[String]("")
  object email extends TransientRequestVar[String]("")
  object firstName extends TransientRequestVar[String]("")
  object lastName extends TransientRequestVar[String]("")
  object companyName extends TransientRequestVar[String]("")
  object street extends TransientRequestVar[String]("")
  object town extends TransientRequestVar[String]("")
  object country extends TransientRequestVar[String]("")
  object state extends TransientRequestVar[String]("")
  object picture extends TransientRequestVar[Box[FileParamHolder]](Empty)

  def render(xhtml: NodeSeq): NodeSeq = {
    var password = ""
    var password2 = ""
    def add(): JsCmd = {
      val cmd1: JsCmd = markVars(userName, email)
      val cmd2: JsCmd = markMandatory(password -> "pass1", password2 -> "pass2") & cmd1
      val cmd3: JsCmd = markVars(id, userName, email, companyName, street, town, country)
      if (!checkVarsMandatory(userName, email) || password == "" || password2 == "") {
        S.error(fields_are_empty); cmd2
      } else if (password != password2) {
        S.error(passwords_dont_match); cmd2
      } else if (!checkVarsMandatory(id, userName, email, companyName, street, town, country)) {
        S.error(fields_are_empty); cmd3
      } else if (picture.is.isEmpty) {
        S.error(image_field_empty); Noop
      } else if (!checkImage(picture)) {
        S.error(image_invalid); Noop
      } else {
        addUserMerchant(cmd2)
      }
    }
    def addUserMerchant(cmd: JsCmd): JsCmd = {
      val userDetails: UserDetails = new UserDetails(userName.is, password, firstName.is, lastName.is, email.is)
      executeWith(redirectTo("merchants.html"), cmd) {
        execute(userService.createUser("", userDetails, List(MerchantRole))) match {
          case Success(user) => {
            val hash: String = Hashing.generate(user.id + id_delimiter + id.is)
            val pid = "merchant_" + hash.getBytes("UTF-8").foldRight("") { String.valueOf(_) + _ }
            val path = directories.appDir + imgPath + pid + "." + picture.is.mimeType.split("/").last
            val merchantDetails = new MerchantDetails(companyName.is, Address(street.is, town.is, state.is, country.is), path, Base64.encodeBase64String(picture.is.file))
            merchantService.createMerchant(user.id, user.id + id_delimiter + id.is, merchantDetails)
          }
          case Failure(e) => Future.failed(new RuntimeException(e.head))
        }
      }
    }
    bind("add-merchant", xhtml,
      "id" -> SHtml.text(id.is, id(_), "placeholder" -> "Merchant ID*", bindId(id)),
      "username" -> SHtml.text(userName.is, userName(_), "placeholder" -> "Username*", bindId(userName)),
      "password" -> SHtml.password(password, password = _, "placeholder" -> "Password*", "id" -> "pass1"),
      "password2" -> SHtml.password(password2, password2 = _, "placeholder" -> "Password confirmation*", "id" -> "pass2"),
      "firstname" -> SHtml.text(firstName.is, firstName(_), "placeholder" -> "First Name", bindId(firstName)),
      "lastname" -> SHtml.text(lastName.is, lastName(_), "placeholder" -> "Last Name", bindId(lastName)),
      "email" -> SHtml.text(email.is, email(_), "placeholder" -> "Email*", bindId(email)),
      "companyname" -> SHtml.text(companyName.is, companyName(_), "placeholder" -> "Company name*", bindId(companyName)),
      "street" -> SHtml.text(street.is, street(_), "placeholder" -> "Street*", bindId(street)),
      "town" -> SHtml.text(town.is, town(_), "placeholder" -> "Town*", bindId(town)),
      "country" -> SHtml.text(country.is, country(_), "placeholder" -> "Country*", bindId(country)),
      "state" -> SHtml.text(state.is, state(_), "placeholder" -> "State", bindId(state)),
      "image" -> SHtml.fileUpload(param => picture(Full(param)), "id" -> "uploadImage"),
      "submit" -> SHtml.ajaxSubmit("Add Merchant", add, "data-theme" -> "b"))
  }
}
