package biz.shopboard.lift.snippet

import biz.shopboard.lift._
import support.{ Authenticated, DealDetailsComposable }
import net.liftweb.util.CssSel
import scala.Some
import xml.NodeSeq

class PersonalCoupons extends Authenticated with DealDetailsComposable {
  def render = {
    if (UserGUID.isDefined) couponSubscriptionService.collectMerchants(UserGUID) match {
      case Some(merchants) => compose(merchants)
      case _ => compose(Iterable())
    }
    else NodeSeq.Empty
  }

  def compose(merchantIds: Iterable[String]): CssSel = {
    val coupons = merchantIds.flatMap {
      id =>
        val merchant = merchantService.getMerchant(id)
        couponSubscriptionService.collectCoupons(UserGUID, merchant) match {
          case Some(couponIds) => couponIds.map(cid => (merchant, merchantService.getCoupon(cid)))
          case None => List()
        }
    }
    var seenCoupons: Set[String] = Set()
    val dealDetails = coupons collect {
      case (merchant, Some(deal)) if !seenCoupons.contains(deal.id) => seenCoupons = seenCoupons + deal.id; (deal.id, deal.details, merchant)
    }

    compose(dealDetails, "#coupon-item")
  }

}
