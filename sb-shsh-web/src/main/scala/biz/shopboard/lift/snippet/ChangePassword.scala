package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.lift._
import net.liftweb._
import http._
import biz.shopboard.lift.support.JsExecutable
import util.Helpers._
import js._
import biz.shopboard.domain.model.user.{ ActionHashType, User }
import scalaz.{ Failure, Success }

class ChangePassword extends JsExecutable[User] {

  def render(xhtml: NodeSeq): NodeSeq = {

    var password = ""
    var password2 = ""
    var key = ""

    def change(): JsCmd = {
      val cmd: JsCmd = markMandatory(password -> "pass1", password2 -> "pass2")
      if (password == "" || password2 == "") {
        S.error(fields_are_empty); cmd
      } else if (password != password2) {
        S.error(passwords_dont_match); cmd
      } else {
        execute(userService.changeUserPassword(key, password)) match {
          case Success(_) => S.redirectTo("login.html", () => S.notice(change_password_success))
          case Failure(e) => e.map(S.error(_)); cmd
        }
      }
    }

    val actionHashKey: String = S.param("key").getOrElse("")
    execute(userService.actionHashIsValid(actionHashKey, ActionHashType.RESET_PASSWORD)) match {
      case Success(_) => {
        bind("change-password", xhtml,
          "password" -> SHtml.password(password, password = _, "placeholder" -> "Password*", "id" -> "pass1"),
          "password2" -> SHtml.password(password2, password2 = _, "placeholder" -> "Password confirmation*", "id" -> "pass2"),
          "key" -> SHtml.hidden(key = _, actionHashKey),
          "submit" -> SHtml.ajaxSubmit("Submit", change, "data-theme" -> "b"))
      }
      case Failure(e) => S.redirectTo("login.html", () => S.warning("Incorrect link!"))
    }

  }
}

