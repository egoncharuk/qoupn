package biz.shopboard.lift.snippet

import net.liftweb.util.BindHelpers._
import biz.shopboard.domain.model.user.{ ShoppingListDetails, User, ShoppingList }
import biz.shopboard.Constants._
import org.joda.time.DateTime
import net.liftweb.util.CssSel
import biz.shopboard.lift._
import support.JsExecutable
import xml.NodeSeq
import net.liftweb.http.js.{ JsCmds, JsCmd }
import net.liftweb.http.SHtml
import biz.shopboard.util.Hashing
import JsCmds._

class MyShoppingLists extends JsExecutable[ShoppingList] {
  def render = userService.getUser(UserGUID) match {
    case Some(user) => {
      val shoppingLists = user.shoppingListsIds.map { userService.getShoppingList(_).get }
      val visibility = if (!shoppingLists.isEmpty) "" else "display:none;"
      val visibilitySel: CssSel = "#shopping-list [style]" #> visibility
      val visibilityDivSel: CssSel = "#divider-item [style]" #> visibility
      val countSel: CssSel = "#divider-item" #> { "#cnt *" #> shoppingLists.size }
      val timeSel: CssSel = "#divider-item *+" #> printDate(new DateTime())
      val bindList: (ShoppingList) => CssSel = sl => "h3 *" #> sl.details.name & "p -*" #> sl.items.filterNot(_._2.done).size &
        //"#tags *" #> sl.details.tags.foldLeft(new StringBuilder())((sb: StringBuilder, t: String) => sb.append(t).append(" ")).toString &
        "#popupLink [href+]" #> sl.id & "#go-to-shopping-list [href+]" #> sl.id
      val bindListMenu: (ShoppingList) => CssSel = sl => "data-role=popup [id]" #> sl.id & "#edit-shopping-list [href+]" #> sl.id & "#delete-shopping-list [href+]" #> sl.id
      timeSel & countSel & visibilitySel & visibilityDivSel & "#shopping-list *" #> shoppingLists.map(bindList) & ".container *" #> shoppingLists.map(bindListMenu)
    }
    case None => NodeSeq.Empty
  }

  def renderPopup = {
    var name = "Shopping List"

    def add(): JsCmd = {
      val details = new ShoppingListDetails(name)
      val user: Option[User] = userService.getUser(UserGUID)
      executeDeferred(sl => redirectTo("shopping-list-items.html", "shopping-list-id" -> sl.id)) {
        userService.createShoppingList(UserGUID, user.map(_.version), UserGUID + id_delimiter + Hashing.generateQuasiRandom, details)
      }
    }

    "#name" #> SHtml.text("", name = _, "placeholder" -> "Name Your New List", "data-theme" -> "a") &
      "#btn" #> SHtml.ajaxSubmit("Add", add, "data-iconpos" -> "notext", "data-icon" -> "arrow-r", "data-theme" -> "a")
  }
}
