package biz.shopboard.lift.snippet

import biz.shopboard.infrastructure.FacadeProvided
import biz.shopboard.domain.model.merchant.Merchant
import org.joda.time.DateTime
import net.liftweb.util.CssSel
import biz.shopboard.lift._
import net.liftweb.util
import util.Helpers._

class Merchants extends FacadeProvided {
  def render = {
    val current: DateTime = new DateTime()
    val merchants = merchantService.getMerchants
    val visibility = if (!merchants.isEmpty) "" else "display:none;"
    val visibilitySel: CssSel = "#merchant-item [style]" #> visibility
    val visibilityDivSel: CssSel = "#divider-item [style]" #> visibility
    val countSel: CssSel = "#divider-item" #> { "#cnt *" #> merchants.size }
    val timeSel: CssSel = "#divider-item -*" #> (new DateTime() + "")
    val bindMerchant: (Merchant) => CssSel = p => {
      val picturePath: String = !p.details.isWritten ? imgStaticMerchantPath | p.details.filePath.substring(p.details.filePath.indexOf(imgPath))
      "h3 *" #> p.details.name & "img [src+]" #> (picturePath + "?" + current.getMillis) & "p" #> { "strong *" #> p.details.address.toString } &
        "#edit-merchant [href+]" #> p.id & "#go-to-campaigns [href+]" #> p.id
    }
    timeSel & countSel & visibilitySel & visibilityDivSel & "#merchant-item *" #> merchants.map(bindMerchant)
  }
}
