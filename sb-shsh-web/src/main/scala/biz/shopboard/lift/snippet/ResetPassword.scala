package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.lift._
import net.liftweb._
import http._
import biz.shopboard.lift.support.JsExecutable
import util.Helpers._
import js._
import biz.shopboard.domain.model.user.User
import scalaz.{ Failure, Success }

class ResetPassword extends JsExecutable[User] {

  object email extends TransientRequestVar[String]("")

  def render(xhtml: NodeSeq): NodeSeq = {
    def reset(): JsCmd = {
      val cmd: JsCmd = markVars(email)
      if (!checkVarsMandatory(email)) {
        S.error(fields_are_empty)
        cmd
      } else {
        execute(userService.sendChangeUserPasswordLink(email.is)) match {
          case Success(_s) => S.redirectTo("login.html", () => S.notice(send_reset_password_link_success))
          case Failure(e) => e.map(S.error(_)); cmd
        }
      }
    }
    bind("reset-password", xhtml,
      "email" -> SHtml.text(email.is, email(_), "placeholder" -> "Email*", bindId(email)),
      "submit" -> SHtml.ajaxSubmit("Reset", reset, "data-theme" -> "b"))
  }
}
