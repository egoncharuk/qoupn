package biz.shopboard.lift.snippet

import biz.shopboard.lift._
import net.liftweb.util.BindHelpers._
import biz.shopboard.lift.support.Authenticated

class Index extends Authenticated {
  def render = if (!UserGUID.isDefined) "a [href+]" #> "login.html"
  else if (isAdmin) "a [href+]" #> "merchants.html"
  else if (isMerchant) "a [href+]" #> "campaigns.html"
  else "a [href+]" #> "my-shopping-lists.html"
}
