package biz.shopboard.lift.snippet

import xml.NodeSeq
import biz.shopboard.lift.CampaignGUID
import biz.shopboard.infrastructure.FacadeProvided
import net.liftweb.http.S
import net.liftweb.common.Full
import biz.shopboard.domain.model.merchant.DraftCampaign

class CampaignHeader extends FacadeProvided {
  def render(xhtml: NodeSeq): NodeSeq = {
    S.param("campaign-id") match {
      case Full(cid) => merchantService.getCampaign(cid) match {
        case Some(c: DraftCampaign) => html
        case _ => NodeSeq.Empty
      }
      case _ if CampaignGUID.isDefined => merchantService.getCampaign(CampaignGUID) match {
        case Some(c: DraftCampaign) => html
        case _ => NodeSeq.Empty
      }
      case _ => NodeSeq.Empty
    }
  }

  private def html: NodeSeq = {
    <li data-icon="custom" id="add-deal"><a href="add-deal.html" data-theme="e">Add Deal</a></li>
    <li data-icon="custom" id="add-coupon"><a href="add-coupon.html" data-theme="e">Add Coupon</a></li>
  }
}
