package biz.shopboard.lift.snippet

import biz.shopboard.Constants._
import biz.shopboard.lift._
import net.liftweb._
import http._
import js.JE.{ Str, Call }
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.domain.model.user._
import biz.shopboard.util.Hashing

class AddShoppingList extends JsExecutable[ShoppingList] {

  object name extends TransientRequestVar[String]("")

  def render = {
    var items: Array[String] = Array("")

    def save(): JsCmd = {
      val cmd: JsCmd = markVars(name)
      if (!checkVarsMandatory(name)) {
        S.error(fields_are_empty); cmd
      } else {
        val details = new ShoppingListDetails(name.is)
        val user: Option[User] = userService.getUser(UserGUID)
        executeDeferred(sl => redirectTo("shopping-list-items.html", "shopping-list-id" -> sl.id) & executeDeferredWithSeq(_ => Noop) {
          var cnt = -1; items.filterNot(_ == "").map { i =>
            val newItem = ShoppingListItem(sl.id + id_delimiter + Hashing.generateQuasiRandom, i)
            cnt += 1; userService.addShoppingListItem(UserGUID, sl.id, Some(sl.version + cnt), newItem)
          }
        }) {
          userService.createShoppingList(UserGUID, user.map(_.version), UserGUID + id_delimiter + Hashing.generateQuasiRandom, details)
        }
      }
    }
    def create(i: Int) = {
      items = items :+ ""
      <div class="ui-grid-a" id={ "grid" + i }>
        <div class="ui-block-a" style="width:85%;"> { SHtml.text("", s => items = items.updated(i, s), "placeholder" -> "Add an Item", "id" -> ("item" + i)) } </div>
        <div class="ui-block-b" style="width:15%;"> { SHtml.ajaxButton("Delete", () => delete(i), "data-iconpos" -> "notext", "data-icon" -> "delete") } </div>
      </div>
      <div id={ "nextGrid" + (i + 1) }></div>
    }
    def add(): JsCmd = {
      val size = items.size; Replace("nextGrid" + size, create(size)) &
        Call("$('#grid" + size + "').trigger", Str("create")).cmd & Focus("item" + size)
    }
    def delete(i: Int): JsCmd = {
      items = items.updated(i, ""); Replace("grid" + i, <div></div>)
    }

    "#btn" #> SHtml.ajaxSubmit("Save", save, "data-theme" -> "b") &
      "#add" #> SHtml.ajaxButton("Add", () => add(), "data-iconpos" -> "notext", "data-icon" -> "plus") &
      "#name" #> SHtml.text(name.is, name(_), "placeholder" -> "Name Your New List", bindId(name)) &
      ".container *" #> items.indices.map { i =>
        ".ui-grid-a [id]" #> ("grid" + i) &
          ".ui-block-a *" #> SHtml.text(items(i), s => items = items.updated(i, s), "placeholder" -> "Add an Item", "id" -> ("item" + i)) &
          ".ui-block-b *" #> SHtml.ajaxButton("Delete", () => delete(i), "data-iconpos" -> "notext", "data-icon" -> "delete")
      }
  }
}
