package biz.shopboard.lift.snippet

import biz.shopboard.domain.model.user.{ ShoppingListItem, ShoppingList }
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import xml.NodeSeq
import biz.shopboard.Constants._
import biz.shopboard.lift.support.JsExtendedCmds.AlertWithTitle
import biz.shopboard.domain.model.merchant.Deal
import biz.shopboard.util.Hashing

class LinkDeal extends JsExecutable[ShoppingList] {

  def render = S.param("deal-id") match {
    case Full(did) => {
      var shoppingLists: Map[String, Boolean] = Map()
      val existing = userService.getUser(UserGUID).map(u => u.shoppingListsIds.flatMap(id => userService.getShoppingList(id).toList)).toList.flatten
      val selected = existing.map(_.id).filter(id => userService.getLinkedDeals(id).exists(!_.isEmpty)).toList
      val all = existing.map(sl => (sl.id, sl.details.name))

      def save(): JsCmd = {
        //===================== CREATE ITEM =====================
        val deal: Option[Deal] = merchantService.getDeal(did)
        deal.foreach { d =>
          existing.foreach { sl =>
            if (shoppingLists.get(sl.id).getOrElse(false) && !sl.itemExists(d.details.name)) {
              val item = new ShoppingListItem(sl.id + id_delimiter + Hashing.generateQuasiRandom, d.details.name, quantity = 1, price = d.details.discount.discountedPrice)
              userService.addShoppingListItem(UserGUID, sl.id, sl.versionOption, item)
            }
          }
        }
        //===================== LINK SHOPPING LISTS =============
        executeWithSeq(Noop, Noop) { shoppingLists.filter(m => m._2 && !selected.contains(m._2)).map(m => userService.linkShoppingListAndDeal(UserGUID, m._1, did)).toList }
        //===================== UNLINK SHOPPING LISTS ===========
        executeWithSeq(if (S.errors.isEmpty) AlertWithTitle(deal_linkage_success) & redirectBack else Noop, Noop) {
          shoppingLists.filterNot(_._2).map(m => userService.unlinkShoppingListAndDeal(UserGUID, m._1, did)).toList
        }
      }

      ".btn" #> SHtml.ajaxSubmit("Save", save, "data-theme" -> "b") &
        ".shopping-list-lbl" #> all.map(m => <label for={ m._1 }>{ m._2 }</label>) &
        ".shopping-list-id" #> all.map(m => SHtml.checkbox_id(selected.contains(m._1), value => { shoppingLists = shoppingLists.updated(m._1, value) }, Full(m._1)))
    }
    case _ => S.error(parameters_are_incorrect); NodeSeq.Empty
  }
}
