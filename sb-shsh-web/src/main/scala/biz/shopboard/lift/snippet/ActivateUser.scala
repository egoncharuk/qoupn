package biz.shopboard.lift.snippet

import net.liftweb._
import http._
import biz.shopboard.lift.support.JsExecutable
import biz.shopboard.domain.model.user.User
import net.liftweb.common.Box
import scalaz.{ Failure, Success }

object ActivateUser extends JsExecutable[User] {
  def apply(): Box[LiftResponse] = {
    val actionHashKey = S.param("key").getOrElse("")
    execute(userService.activateUser(actionHashKey)) match {
      case Success(_) => S.redirectTo("login.html", () => S.warning("Your account was activated successfully. Please login!"))
      case Failure(e) => S.redirectTo("login.html", () => S.warning("Incorrect link!"))
    }
  }
}
