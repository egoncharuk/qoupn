package biz.shopboard.lift.snippet

import net.liftweb.util.BindHelpers._
import biz.shopboard.domain.model.merchant._
import biz.shopboard.lift.support.Authenticated
import biz.shopboard.lift._
import net.liftweb.util.CssSel
import org.joda.time.DateTime
import net.liftweb.http.S
import net.liftweb.common.Full
import scala.Some
import xml.NodeSeq

class Deals extends Authenticated {
  def render = {
    S.param("campaign-id") match {
      case Full(cid) => merchantService.getCampaign(cid) match {
        case Some(c) =>
          CampaignGUID.set(cid); compose(c.dealIds, c.couponIds)
        case _ => NodeSeq.Empty
      }
      case _ if CampaignGUID.isDefined => composeWithCampaign
      case _ => NodeSeq.Empty
    }
  }

  def composeWithCampaign = merchantService.getCampaign(CampaignGUID) match {
    case Some(c) => compose(c.dealIds, c.couponIds)
    case _ => NodeSeq.Empty
  }

  def compareDeals(option1: Option[Deal], option2: Option[Deal]) = {
    (option1, option2) match {
      case (Some(d1), Some(d2)) => d1.details.name.compareTo(d2.details.name) < 0
      case (_, _) => false
    }
  }

  def compose(dealIds: List[String], couponIds: List[String]): CssSel = {
    val unsortedDeals = dealIds.map(merchantService.getDeal(_))
    val unsortedCoupons = couponIds.map(merchantService.getCoupon(_))

    val deals = unsortedDeals.sortWith(compareDeals)
    val coupons = unsortedCoupons.sortWith(compareDeals)

    val dealDetails = deals collect {
      case Some(deal) => (deal.id, deal.details)
    }
    val couponDetails = coupons collect {
      case Some(coupon) => (coupon.id, coupon.details)
    }

    val bindDealMenu: ((String, DealDetails)) => CssSel = d =>
      "data-role=popup [id]" #> strip(d._1) & "#edit-deal [href+]" #> d._1 & "#delete-deal [href+]" #> d._1
    val bindCouponMenu: ((String, DealDetails)) => CssSel = d =>
      "data-role=popup [id]" #> strip(d._1) & "#edit-coupon [href+]" #> d._1 & "#delete-coupon [href+]" #> d._1

    val sel1 = compose(dealDetails, "#deal-item", "#divider-item-1", "#cnt-1", "#time-1")
    val sel2 = compose(couponDetails, "#coupon-item", "#divider-item-2", "#cnt-2", "#time-2")
    sel1 & sel2 & ".popup-container-1 *" #> dealDetails.map(bindDealMenu) & ".popup-container-2 *" #> couponDetails.map(bindCouponMenu)
  }

  def compose(details: List[(String, DealDetails)], itemId: String, dividerId: String, cntId: String, timeId: String): CssSel = {
    val visibility = if (!details.isEmpty) "" else "display:none;"
    val visibilitySel: CssSel = (itemId + " [style]") #> visibility
    val visibilityDivSel: CssSel = (dividerId + " [style]") #> visibility
    val countSel: CssSel = dividerId #> {
      (cntId + " *") #> details.size
    }
    val current: DateTime = new DateTime()
    val timeSel: CssSel = dividerId #> {
      (timeId + " *") #> printDate(current)
    }
    val bindDetails: ((String, DealDetails)) => CssSel = d => {
      val picturePath: String = d._2.filePath.substring(d._2.filePath.indexOf(imgPath))
      "h3 *" #> d._2.name & "img [src+]" #> (picturePath + "?" + current.getMillis) & "p .ui-li-aside" #> {
        "strong *" #> printDate(d._2.validTo)
      } & "p" #> {
        "strong *" #> (d._2.description + " @ " + d._2.discount)
      } & ".popupLink [href+]" #> strip(d._1) & ".go-to-prod-info-ignore [href+]" #> d._1
    }

    timeSel & countSel & visibilitySel & visibilityDivSel & (itemId + " *") #> details.map(bindDetails)
  }
}
