package biz.shopboard.lift.snippet

import biz.shopboard.domain.model.user.ShoppingList
import biz.shopboard.Constants._
import net.liftweb.http._
import biz.shopboard.lift._
import js.JsCmds
import net.liftweb.common.Box
import support.JsExecutable
import net.liftweb.http.js.JE.Call
import biz.shopboard.domain.model.user.ShoppingListItem
import net.liftweb.common.Full
import scala.Some
import JsCmds._
import biz.shopboard.I18nContext

object MarkItem extends JsExecutable[ShoppingList] {
  def apply(r: Req): Box[LiftResponse] = {
    (r.param("id"), r.param("sid")) match {
      case (Full(id), Full(sid)) => {
        userService.getShoppingList(sid) match {
          case Some(shoppingList) => shoppingList.items.get(id) match {
            case Some(item) => {
              val newItem: ShoppingListItem = item.copy(done = !item.done)
              def getRunningSum(sli: (String, ShoppingListItem)): BigDecimal =
                if (sli._2.done) zero
                else sli._2.price * sli._2.quantity
              val newTotal: ShoppingList => String = sl => String.valueOf(
                sl.items.foldLeft(zero) {
                  (sum, sli) => sum + getRunningSum(sli)
                }.setScale(2, mode) + I18nContext.getCurrencyTitle)
              JavaScriptResponse(executeDeferred(sl =>
                Call("$('#cnt').html", sl.items.filterNot(_._2.done).size)
                  &
                  Call("$('#remaining-total').html", "Remaining: " + newTotal(sl))) {
                userService.updateShoppingListItem(UserGUID, shoppingList.id, shoppingList.versionOption, newItem)
              })
            }
            case None => Full(NotFoundResponse(no_shopping_list_item_found))
          }
          case None => Full(NotFoundResponse(no_shopping_list_found))
        }
      }
      case _ => Full(NotFoundResponse(parameters_are_incorrect))
    }
  }
}
