package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.merchant.Deal
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._

class AddActiveCoupon extends JsExecutable[Deal] {
  def render(xhtml: NodeSeq): NodeSeq = {
    (S.param("coupon-id"), S.param("merchant-id")) match {
      case (Full(did), Full(mid)) => {
        def add(): JsCmd = executeWith(redirectBack) { merchantService.distributeCoupon(mid, UserGUID, did) }
        bind("add-active-coupon", xhtml, "submit" -> SHtml.ajaxSubmit("Add", add, "data-theme" -> "b"))
      }
      case _ => NodeSeq.Empty
    }
  }
}
