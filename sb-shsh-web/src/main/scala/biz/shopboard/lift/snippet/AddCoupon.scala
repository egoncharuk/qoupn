package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.lift._
import support._
import net.liftweb._
import common.{ Full, Empty, Box }
import http._
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.Constants._
import biz.shopboard.domain.model.merchant.{ DraftCampaign, Deal, DealDetails }
import biz.shopboard.domain.model.common.DiscountInfo
import org.apache.commons.codec.binary.Base64
import scala.Some
import biz.shopboard.util.Hashing
import net.liftweb.http.SHtml.ElemAttr
import org.joda.time.format.{ DateTimeFormat, DateTimeFormatter }

class AddCoupon extends Authenticated with JsExecutable[Deal] {

  object id extends TransientRequestVar[String]("")
  object name extends TransientRequestVar[String]("")
  object desc extends TransientRequestVar[String]("")
  object validFrom extends TransientRequestVar[String]("")
  object validTo extends TransientRequestVar[String]("")
  object originalPrice extends DoubleRequestVar(0D, Double.MaxValue)
  object discountPercent extends DoubleRequestVar(1D, 100D)
  object discountedPrice extends DoubleRequestVar(0D, Double.MaxValue)
  object picture extends TransientRequestVar[Box[FileParamHolder]](Empty)
  object count extends IntRequestVar(1, Integer.MAX_VALUE)

  val dateIdAttribute: ElemAttr = "id" -> "datefield"
  val fmt: DateTimeFormatter = DateTimeFormat.forPattern("dd.MM.yyyy")

  def render(xhtml: NodeSeq): NodeSeq = {
    merchantService.getCampaign(CampaignGUID) match {
      case Some(c: DraftCampaign) => {
        def add(): JsCmd = {
          val cmd = markVars(id, name, desc, validTo, validFrom) & originalPrice & discountedPrice & discountPercent & count

          if (!checkVarsMandatory(id, name, desc, validTo, validFrom)) {
            S.error(fields_are_empty); cmd
          } else if (!checkValidDate(validFrom)) {
            S.error(valid_from_invalid); cmd
          } else if (!checkValidDate(validTo)) {
            S.error(valid_to_invalid); cmd
          } else if (!originalPrice.errors.isEmpty) {
            originalPrice.errors.map(S.error(_)); cmd
          } else if (!discountedPrice.errors.isEmpty) {
            discountedPrice.errors.map(S.error(_)); cmd
          } else if (!discountPercent.errors.isEmpty) {
            discountPercent.errors.map(S.error(_)); cmd
          } else if (!count.errors.isEmpty) {
            count.errors.map(S.error(_)); cmd
          } else if (picture.is.isEmpty) {
            S.error(image_field_empty); cmd
          } else if (!checkImage(picture)) {
            S.error(image_invalid); cmd
          } else merchantService.getCampaign(CampaignGUID) match {
            case None =>
              S.error(no_campaign_found); cmd
            case Some(campaign) => {
              val hash: String = Hashing.generate(campaign.id + id_delimiter + id.is)
              val pid = "coupon_" + hash.getBytes("UTF-8").foldRight("") { String.valueOf(_) + _ }
              val path = directories.appDir + imgPath + pid + "." + picture.is.mimeType.split("/").last
              val details = new DealDetails(name.is, desc.is, DiscountInfo(
                originalPrice.is, discountPercent.is, discountedPrice.is),
                parseDate(validFrom), parseDate(validTo), List(), path, Base64.encodeBase64String(picture.is.file))
              executeWith(reload) {
                merchantService.createCoupon(MerchantGUID, campaign.id, campaign.versionOption,
                  campaign.id + id_delimiter + id.is, details, count.is)
              }
            }
          }
        }
        bind("add-coupon", xhtml,
          "id" -> SHtml.text(id.is, id(_), "placeholder" -> "Coupon ID*", bindId(id)),
          "name" -> SHtml.text(name.is, name(_), "placeholder" -> "Name*", bindId(name)),
          "desc" -> SHtml.text(desc.is, desc(_), "placeholder" -> "Description*", bindId(desc)),
          "original-price" -> SHtml.number(0.0D, originalPrice(_), 0.0D, Double.MaxValue, 0.01D, "placeholder" -> "Original Price", bindId(originalPrice)) % ("value" -> ""),
          "discount-price" -> SHtml.number(0.0D, discountedPrice(_), 0.0D, Double.MaxValue, 0.01D, "placeholder" -> "Discounted Price", bindId(discountedPrice)) % ("value" -> ""),
          "discount-percent" -> SHtml.number(0.0D, discountPercent(_), 1D, 100D, 0.01D, "placeholder" -> "Discount Percent", bindId(discountPercent)) % ("value" -> ""),
          "valid-to" -> SHtml.text(c.details.valid_to.toString(fmt), validTo(_), "placeholder" -> ("Valid To* [" + dateFormatFull.toUpperCase + "]"), dateIdAttribute),
          "valid-from" -> SHtml.text(c.details.valid_from.toString(fmt), validFrom(_), "placeholder" -> ("Valid From* [" + dateFormatFull.toUpperCase + "]"), dateIdAttribute),
          "count" -> SHtml.number(1, count(_), 1, Integer.MAX_VALUE, "placeholder" -> "Maximum Count", bindId(count)) % ("value" -> ""),
          "image" -> SHtml.fileUpload(param => picture(Full(param)), "id" -> "uploadImage"),
          "submit" -> SHtml.ajaxSubmit("Add Coupon", add, "data-theme" -> "b"))
      }
      case _ => NodeSeq.Empty
    }
  }
}
