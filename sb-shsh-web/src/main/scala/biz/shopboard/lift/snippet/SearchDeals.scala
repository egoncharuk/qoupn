package biz.shopboard.lift.snippet

import net.liftweb.util.BindHelpers._
import biz.shopboard.domain.model.merchant._
import biz.shopboard.lift.support.Authenticated
import biz.shopboard.lift._
import net.liftweb.util.CssSel
import org.joda.time.DateTime
import net.liftweb.http.S
import net.liftweb.common.{Empty, Full}
import xml.NodeSeq
import biz.shopboard.I18nContext
import util.parsing.json.JSON

class SearchDeals extends Authenticated {
  def render = {
    (S.param("q"), S.param("stores")) match {
      case (Full(q), Full(s)) => JSON.parseFull(s) match {
        case Some(stores: List[String]) => {
          val info = productSearchService.searchProducts(q, q, q)
          val coupons = merchantService.searchCoupons(q, info, Some(stores))
          val deals = merchantService.searchDeals(q, info, Some(stores))
          compose(deals, coupons)
        }
        case None => simpleSearch(q)
      }
      case (Full(q), Empty) => simpleSearch(q)
      case _ => NodeSeq.Empty
    }
  }


  def simpleSearch(q: String): CssSel = {
    val info = productSearchService.searchProducts(q, q, q)
    val coupons = merchantService.searchCoupons(q, info)
    val deals = merchantService.searchDeals(q, info)
    compose(deals, coupons)
  }

  def compose(deals: Iterable[(Merchant, Deal)], coupons: Iterable[(Merchant, Deal)]): CssSel = {
    val dealDetails = deals map { d => (d._1, d._2.id, d._2.details) }
    val couponDetails = coupons map { c => (c._1, c._2.id, c._2.details) }

    val sel1 = compose(dealDetails, "#deal-item", "#divider-item-1", "#cnt-1", "#time-1")
    val sel2 = compose(couponDetails, "#coupon-item", "#divider-item-2", "#cnt-2", "#time-2")
    sel1 & sel2
  }

  def compose(details: Iterable[(Merchant, String, DealDetails)], itemId: String, dividerId: String, cntId: String, timeId: String): CssSel = {
    val visibility = if (!details.isEmpty) "" else "display:none;"
    val visibilitySel: CssSel = (itemId + " [style]") #> visibility
    val visibilityDivSel: CssSel = (dividerId + " [style]") #> visibility
    val countSel: CssSel = dividerId #> {
      (cntId + " *") #> details.size
    }
    val current: DateTime = new DateTime()
    val timeSel: CssSel = dividerId #> {
      (timeId + " *") #> printDate(current)
    }
    val bindItems: ((Merchant, String, DealDetails)) => CssSel = d => {
      val picturePath: String = d._3.filePath.substring(d._3.filePath.indexOf(imgPath))
      "h2 *" #> d._3.name & "img [src+]" #> picturePath & "p .ui-li-aside" #> {
        "strong *" #> printDate(d._3.validTo)
      } & "p .ul-li-in" #> {
        "strong" #> (d._1.details.name + " @ " + d._3.discount)
      } & ".add-deal-ignore [href+]" #> (d._2 + "&merchant-id=" + d._1.id) &
        ".show-deal-ignore [href+]" #> ("details-popup-" + d._2)
    }
    def bindDetails: ((Merchant, String, DealDetails)) => CssSel = d => {
      ".name *+" #> d._3.name & ".desc *+" #> d._3.description &
        ".price *+" #> (d._3.discount.discountedPrice + I18nContext.getCurrencyTitle) &
        ".discount *+" #> (d._3.discount.discountPercent + "%") &
        ".from *+" #> printDate(d._3.validFrom) & ".to *+" #> printDate(d._3.validTo) &
        "data-role=popup [id]" #> ("details-popup-" + d._2)
    }

    timeSel & countSel & visibilitySel & visibilityDivSel &
      ("." + itemId.tail + "-details-container *") #> details.map(bindDetails) &
      (itemId + " *") #> details.map(bindItems)
  }
}
