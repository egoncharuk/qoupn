package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.merchant.{ DraftCoupon, DraftDeal, Deal }
import biz.shopboard.lift._
import net.liftweb._
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._

class AddProductInfo extends JsExecutable[Deal] {

  object name extends TransientRequestVar[String]("")
  object description extends TransientRequestVar[String]("")
  object category extends TransientRequestVar[String]("")

  def render(xhtml: NodeSeq): NodeSeq =
    DealGUID.isDefined ? merchantService.getDeal(DealGUID) | merchantService.getCoupon(CouponGUID) match {
      case Some(deal) => {
        def add(): JsCmd = {
          val cmd: JsCmd = markVars(name, category, description)
          if (!checkVarsMandatory(name, category, description)) {
            S.error(fields_are_empty); cmd
          } else {
            val info = new biz.shopboard.domain.model.common.ProductInfo(name.is, description.is, category.is)
            deal match {
              case d: DraftDeal => executeWith(redirectTo("product-info.html"), cmd) { merchantService.addDealProductInfo(MerchantGUID, d.id, d.versionOption, info) }
              case c: DraftCoupon => executeWith(redirectTo("product-info.html"), cmd) { merchantService.addCouponProductInfo(MerchantGUID, c.id, c.versionOption, info) }
              case _ => S.error(cannot_update_non_draft_campaign); cmd
            }
          }
        }
        bind("add-product-info", xhtml,
          "name" -> SHtml.text(name.is, name(_), "placeholder" -> "Name*", bindId(name)),
          "category" -> SHtml.text(category.is, category(_), "placeholder" -> "Category*", bindId(category)),
          "description" -> SHtml.textarea(description.is, description(_), "placeholder" -> "Description*", bindId(description)),
          "submit" -> SHtml.ajaxSubmit("Add Product Info", add, "data-theme" -> "b"))
      }
      case _ => NodeSeq.Empty
    }
}
