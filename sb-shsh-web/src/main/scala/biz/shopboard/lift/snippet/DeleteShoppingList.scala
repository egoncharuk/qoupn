package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.user.ShoppingList
import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._

class DeleteShoppingList extends JsExecutable[ShoppingList] {

  def render(xhtml: NodeSeq): NodeSeq = S.param("shopping-list-id") match {
    case Full(sid) => userService.getShoppingList(sid) match {
      case Some(shoppingList) => {
        def delete(): JsCmd = executeWith(redirectTo("my-shopping-lists.html")) {
          userService.removeShoppingList(UserGUID, shoppingList.id, shoppingList.versionOption)
        }
        bind("delete-shopping-list", xhtml, "submit" -> SHtml.ajaxSubmit("Delete", delete, "data-theme" -> "b"))
      }
      case None => S.error(no_shopping_list_found); NodeSeq.Empty
    }
    case _ => S.error(parameters_are_incorrect); NodeSeq.Empty
  }

}
