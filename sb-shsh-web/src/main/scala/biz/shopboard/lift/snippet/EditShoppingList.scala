package biz.shopboard.lift.snippet

import biz.shopboard.lift._
import net.liftweb._
import common.Full
import http._
import support.JsExecutable
import util.Helpers._
import js._
import JsCmds._
import biz.shopboard.domain.model.user.{ ShoppingListDetails, ShoppingList }
import xml.NodeSeq

class EditShoppingList extends JsExecutable[ShoppingList] {

  object name extends TransientRequestVar[String]("")

  def render = S.param("shopping-list-id") match {
    case Full(sid) => userService.getShoppingList(sid) match {
      case Some(shoppingList) => {
        def save(): JsCmd = {
          val cmd: JsCmd = markVars(name)
          if (!checkVarsMandatory(name)) {
            S.error(fields_are_empty); cmd
          } else {
            val details = new ShoppingListDetails(name.is)
            executeWith(redirectTo("my-shopping-lists.html"), cmd) { userService.updateShoppingListDetails(UserGUID, sid, shoppingList.versionOption, details) }
          }
        }

        "#btn" #> SHtml.ajaxSubmit("Save", save, "data-theme" -> "b") &
          "#name" #> SHtml.text(shoppingList.details.name, name(_), "placeholder" -> "Name*", bindId(name))
      }
      case None => S.error(no_shopping_list_found); NodeSeq.Empty
    }
    case _ => S.error(no_shopping_list_found); NodeSeq.Empty
  }
}
