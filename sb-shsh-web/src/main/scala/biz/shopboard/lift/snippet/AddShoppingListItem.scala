package biz.shopboard.lift.snippet

import _root_.scala.xml.NodeSeq
import biz.shopboard.domain.model.user.{ ShoppingListItem, ShoppingList }
import biz.shopboard.lift._
import support.{ DoubleRequestVar, IntRequestVar, JsExecutable }
import net.liftweb._
import common.Full
import http._
import util.Helpers._
import js._
import JsCmds._
import scala.Some
import biz.shopboard.Constants._
import biz.shopboard.util.Hashing

class AddShoppingListItem extends JsExecutable[ShoppingList] {
  object name extends TransientRequestVar[String]("")
  object desc extends TransientRequestVar[String]("")
  object quantity extends IntRequestVar(0, Int.MaxValue)
  object price extends DoubleRequestVar(0, Double.MaxValue)
  object done extends TransientRequestVar[Boolean](false)

  def render(xhtml: NodeSeq): NodeSeq = {
    S.param("shopping-list-id") match {
      case Full(sid) => {
        userService.getShoppingList(sid) match {
          case Some(shoppingList) => {
            def add(): JsCmd = {
              val cmd: JsCmd = markVars(name)
              if (!checkVarsMandatory(name)) {
                S.error(fields_are_empty); cmd
              } else {
                val newItem: ShoppingListItem = ShoppingListItem(shoppingList.id + id_delimiter + Hashing.generateQuasiRandom, name.is, desc.is, quantity.is, price.is, done.is)
                executeWith(redirectBack, cmd) { userService.addShoppingListItem(UserGUID, shoppingList.id, shoppingList.versionOption, newItem) }
              }
            }

            bind("add-shopping-list-item", xhtml,
              "done" -> SHtml.checkbox(false, done(_), bindId(done)),
              "name" -> SHtml.text("", name(_), "placeholder" -> "Name*", bindId(name)),
              "quantity" -> SHtml.number(0, quantity(_), 0, Int.MaxValue, "placeholder" -> "Quantity", bindId(quantity)) % ("value" -> ""),
              "price" -> SHtml.number(0.0D, price(_), 0.0D, Double.MaxValue, 0.01D, "placeholder" -> "Price", bindId(price)) % ("value" -> ""),
              "desc" -> SHtml.text("", desc(_), "placeholder" -> "Description"),
              "submit" -> SHtml.ajaxSubmit("Add", add, "data-theme" -> "b"))
          }
          case None => S.error(no_shopping_list_found); NodeSeq.Empty
        }
      }
      case _ => S.error(parameters_are_incorrect); NodeSeq.Empty
    }
  }
}
