package biz.shopboard.lift.snippet

import biz.shopboard.domain.model.user.ShoppingList
import net.liftweb.http._
import biz.shopboard.lift._
import net.liftweb.common.Box
import support.JsExecutable
import net.liftweb.http.js.{ JsCmd, JsCmds }
import biz.shopboard.domain.model.merchant.Deal
import JsCmds._
import net.liftweb.common.Full
import scala.Some
import biz.shopboard.lift.support.JsExtendedCmds.AlertWithTitle

object LinkItem extends JsExecutable[ShoppingList] {
  def apply(r: Req): Box[LiftResponse] = {
    (r.param("item-id"), r.param("merchant-id"), r.param("shopping-list-id"), r.param("is-coupon")) match {
      case (Full(did), Full(mid), Full(sid), Full("false")) => merchantService.getDeal(did) match {
        case Some(deal) => {
          val first: JsCmd = executeWithTypeProvided[Deal](Noop) { merchantService.distributeDeal(mid, UserGUID, did) }
          JavaScriptResponse(if (first.eq(Noop)) executeWith(AlertWithTitle(deal_addition_success) & redirectBack) { userService.linkShoppingListAndDeal(UserGUID, sid, did) } else first)
        }
        case None => Full(NotFoundResponse(no_deal_found))
      }
      case (Full(cid), Full(mid), Full(sid), Full("true")) => merchantService.getCoupon(cid) match {
        case Some(coupon) => {
          val first: JsCmd = executeWithTypeProvided[Deal](Noop) { merchantService.distributeCoupon(mid, UserGUID, cid) }
          JavaScriptResponse(if (first.eq(Noop)) executeWith(AlertWithTitle(coupon_addition_success) & redirectBack) { userService.linkShoppingListAndCoupon(UserGUID, sid, cid) } else first)
        }
        case None => Full(NotFoundResponse(no_deal_found))
      }
      case _ => Full(NotFoundResponse(parameters_are_incorrect))
    }
  }
}
