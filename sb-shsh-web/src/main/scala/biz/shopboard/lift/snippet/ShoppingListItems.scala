package biz.shopboard.lift.snippet

import biz.shopboard.lift._
import net.liftweb.util.BindHelpers._
import biz.shopboard.domain.model.user.ShoppingList
import support.{ DealDetailsComposable, JsExecutable }
import biz.shopboard.Constants._
import org.joda.time.DateTime
import net.liftweb.util.CssSel
import xml.NodeSeq
import net.liftweb.http.js.{ JsCmds, JsCmd }
import net.liftweb.http.{ S, SHtml }
import biz.shopboard.util.Hashing
import JsCmds._
import biz.shopboard.domain.model.merchant._
import biz.shopboard.domain.model.user.ShoppingListItem
import net.liftweb.common.Full
import scala.Some
import biz.shopboard.domain.model.merchant.DealDetails
import biz.shopboard.I18nContext

class ShoppingListItems extends JsExecutable[ShoppingList] with DealDetailsComposable {
  def render = S.param("shopping-list-id") match {
    case Full(sid) => userService.getShoppingList(sid) match {
      case Some(shoppingList) => {
        ShoppingListGUID.set(shoppingList.id)
        val shoppingListItems = shoppingList.items.values.toList.sortBy(_.name)
        val visibility = if (!shoppingListItems.isEmpty) "" else "display:none;"
        val visibilitySel: CssSel = "#shopping-list-item [style]" #> visibility
        val visibilityDivSel: CssSel = "#divider-item [style]" #> visibility
        val timeSel: CssSel = "#divider-item *+" #> printDate(new DateTime())
        val countSel: CssSel = "#divider-item" #> { "#cnt *" #> shoppingListItems.filterNot(_.done).size }
        def findDeals(sli: ShoppingListItem): (Iterable[(Merchant, Deal)], Iterable[(Merchant, Deal)]) = {
          val info = productSearchService.searchProducts(sli.name, sli.name, sli.name)
          val coupons = merchantService.searchCoupons(sli.name, info)
          val deals = merchantService.searchDeals(sli.name, info)
          (deals, coupons)
        }
        val bindList: (ShoppingListItem) => CssSel = sli => {
          val deals = findDeals(sli)
          val done: String = if (sli.done) "item-removed" else ""
          val dealsNotFound: Boolean = deals._1.isEmpty && deals._2.isEmpty
          "h3 *" #> sli.name & "h3 [class+]" #> done &
            ".withId [with-id]" #> sli.id & ".withId [class+]" #> done &
            ".withId [with-sl-id]" #> shoppingList.id &
            "span" #> (
              if (dealsNotFound) <img src="/static/images/icons/check-mark.png"/>
              else <img src="/static/images/icons/sale.png"/>) &
              "p -*" #> String.valueOf("Qty " + sli.quantity) &
              "p *+" #> String.valueOf(sli.price + I18nContext.getCurrencyTitle) &
              "#desc *" #> sli.desc & "#popupLink [href+]" #> sli.id
        }
        val bindLinkedItems: (((String, DealDetails, Option[Merchant]), Boolean)) => CssSel = d => {
          val imgPathInd: Int = d._1._2.filePath.indexOf(imgPath)
          val picturePath: String = if (imgPathInd >= 0) d._1._2.filePath.substring(imgPathInd) else ""
          "a [shopping-list-id]" #> sid & "a [merchant-id]" #> d._1._3.get.id &
            "a [item-id]" #> d._1._1 & "a [is-coupon]" #> d._2 &
            "h3 *" #> d._1._2.name & "img [src+]" #> picturePath &
            "p .ui-li-aside" #> { "strong *" #> printDate(d._1._2.validTo) } & //TODO: REMOVE DUPLICATE ONCE RESOLVED
            "p" #> { "strong *" #> addDescription(d._1) }
        }
        val bindProposals: (ShoppingListItem) => CssSel = sli => {
          val r = findDeals(sli)
          val deals: Iterable[(Merchant, Deal)] = r._1
          val coupons: Iterable[(Merchant, Deal)] = r._2
          val dealDetails = deals map { d => (d._2.id, d._2.details, Some(d._1)) }
          val couponDetails = coupons map { c => (c._2.id, c._2.details, Some(c._1)) }
          val allItems = dealDetails.map((_, false)) ++ couponDetails.map((_, true))
          val panelId: String = if (allItems.isEmpty) "null" else sli.id
          "data-role=panel [id]" #> ("panel-" + panelId) & "#linked-item *" #> allItems.map(bindLinkedItems)
        }
        val bindListMenu: (ShoppingListItem) => CssSel = sli => "data-role=popup [id]" #> sli.id &
          "#edit-shopping-list-item [href+]" #> (sli.id + "&shopping-list-id=" + shoppingList.id) &
          "#delete-shopping-list-item [href+]" #> (sli.id + "&shopping-list-id=" + shoppingList.id)


        val linkedCoupons: Iterable[Option[Deal]] = userService.getLinkedCoupons(shoppingList.id) match {
          case Some(list: List[String]) => list.map(id => merchantService.getCoupon(id))
          case None => None
        }
        val couponDetails = linkedCoupons collect { case Some(coupon: Deal) => (coupon.id, coupon.details, None) }

        val linkedDeals: Iterable[Option[Deal]] = userService.getLinkedDeals(shoppingList.id) match {
          case Some(list: List[String]) => list.map(id => merchantService.getDeal(id))
          case None => None
        }
        val dealDetails = linkedDeals collect { case Some(deal: Deal) => (deal.id, deal.details, None) }

        val visibility2 = if (!dealDetails.isEmpty) "" else "display:none;"
        val visibility3 = if (!couponDetails.isEmpty) "" else "display:none;"
        val visibilityDivSel2: CssSel = "#divider-deal-item [style]" #> visibility2
        val visibilityDivSel3: CssSel = "#divider-coupon-item [style]" #> visibility3

        var name = "New Item..."

        def add(): JsCmd = userService.getShoppingList(sid) match {
          case None =>
            S.error(no_shopping_list_found); Noop
          case Some(sl) => {
            val item: ShoppingListItem = new ShoppingListItem(shoppingList.id + id_delimiter + Hashing.generateQuasiRandom, name)
            executeWith(redirectTo("#shopping-list-items-page")) { userService.addShoppingListItem(UserGUID, sid, sl.versionOption, item) }
          }
        }

        "#btn" #> SHtml.ajaxSubmit("Add", add, "data-iconpos" -> "notext", "data-icon" -> "arrow-r", "data-theme" -> "a") &
          "#name" #> SHtml.text("", name = _, "placeholder" -> "Add new item...", "data-theme" -> "a") &
          "#list-name *" #> shoppingList.details.name &
          timeSel & countSel & visibilitySel & visibilityDivSel & visibilityDivSel2 & visibilityDivSel3 &
          "#add-link [href+]" #> shoppingList.id &
          "#shopping-list-item *" #> shoppingListItems.map(bindList) &
          ".popup-container *" #> shoppingListItems.map(bindListMenu) &
          ".deal-container *" #> shoppingListItems.map(bindProposals) &
          compose(dealDetails, "#deal-item") & compose(couponDetails,
            "#coupon-item") & "#remaining-total *+" #> String.valueOf(
              shoppingListItems.foldLeft(zero) {
                (sum, sli) =>
                  sum + (
                    if (sli.done) zero
                    else sli.price * sli.quantity)
              }.setScale(2, mode) + I18nContext.getCurrencyTitle) &
              "#total-saved *+" #> String.valueOf(
                (couponDetails ++ dealDetails).foldLeft(zero) {
                  (sum, d) => sum + d._2.discount.saved
                }.setScale(2, mode) + I18nContext.getCurrencyTitle) &
                "#total *+" #> String.valueOf(
                  shoppingListItems.foldLeft(zero) {
                    (sum, sli) => sum + sli.price * sli.quantity
                  }.setScale(2, mode) + I18nContext.getCurrencyTitle)
      }
      case None => NodeSeq.Empty
    }
    case _ => NodeSeq.Empty
  }

  override protected def addDescription(d: (String, DealDetails, Option[Merchant])) =
    if (d._3.isEmpty)
      super.addDescription(d)
    else d._3.get.details.name + " @ " + I18nContext.getCurrencyTitle + d._2.discount
}
