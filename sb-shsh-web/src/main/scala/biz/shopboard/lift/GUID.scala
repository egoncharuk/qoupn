package biz.shopboard.lift

import net.liftweb.common.{ Box, Empty }
import net.liftweb.http.SessionVar

class GUID extends SessionVar[Box[String]](Empty) {
  def isDefined = get.isDefined
}

object UserGUID extends GUID
object DealGUID extends GUID
object CouponGUID extends GUID
object MerchantGUID extends GUID
object CampaignGUID extends GUID
object ShoppingListGUID extends GUID

