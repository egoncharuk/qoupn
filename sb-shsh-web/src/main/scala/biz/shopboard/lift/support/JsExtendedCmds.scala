package biz.shopboard.lift.support

import net.liftweb.http.js.JsCmd

object JsExtendedCmds {

  case class AlertWithTitle(text: String) extends JsCmd {
    def toJsCmd = "$.mobile.alert(\"" + text + "\", \"title\");"
  }

}
