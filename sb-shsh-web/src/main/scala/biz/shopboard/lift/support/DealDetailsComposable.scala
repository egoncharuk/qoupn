package biz.shopboard.lift.support

import biz.shopboard.lift._
import biz.shopboard.domain.model.merchant.{ Merchant, DealDetails }
import net.liftweb.util.BindHelpers._
import net.liftweb.util.CssSel
import biz.shopboard.I18nContext

trait DealDetailsComposable {
  def compose(details: Iterable[(String, DealDetails, Option[Merchant])], itemId: String): CssSel = {
    val visibility = if (!details.isEmpty) "" else "display:none;"
    val visibilitySel: CssSel = (itemId + " [style]") #> visibility
    val bindItem: ((String, DealDetails, Option[Merchant])) => CssSel = d => {
      val imgPathInd: Int = d._2.filePath.indexOf(imgPath)
      val picturePath: String = if (imgPathInd >= 0) d._2.filePath.substring(imgPathInd) else ""
      "h3 *" #> d._2.name & "img [src+]" #> picturePath &
        "p .ui-li-aside" #> { "strong *" #> printDate(d._2.validTo) } &
        "p" #> { "strong *" #> addDescription(d) } & addLink(d) &
        ".show-deal-ignore [href+]" #> ("details-popup-" + strip(d._1)) &
        "h3 [merchant-id]" #> d._3.map(_.details.name).getOrElse("")
    }
    def bindDetails: ((String, DealDetails, Option[Merchant])) => CssSel = d => {
      ".name *+" #> d._2.name & ".desc *+" #> d._2.description &
        ".price *+" #> (d._2.discount.discountedPrice + I18nContext.getCurrencyTitle) &
        ".discount *+" #> (d._2.discount.discountPercent + "%") &
        ".from *+" #> printDate(d._2.validFrom) & ".to *+" #> printDate(d._2.validTo) &
        "data-role=popup [id]" #> ("details-popup-" + strip(d._1))
    }

    visibilitySel & (itemId + " *") #> details.map(bindItem) & ("." + itemId.tail + "-details-container *") #> details.map(bindDetails)
  }

  protected def addLink(d: (String, DealDetails, Option[Merchant])): CssSel = {
    ".link-deal-ignore [href+]" #> d._1
  }

  protected def addDescription(d: (String, DealDetails, Option[Merchant])): String = d._2.discount.toString
}
