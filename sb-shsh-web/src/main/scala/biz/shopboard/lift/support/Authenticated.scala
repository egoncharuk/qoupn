package biz.shopboard.lift.support

import biz.shopboard.infrastructure.FacadeProvided
import biz.shopboard.domain.model.common.{ UserRole, AdminRole, MerchantRole, ApplicationRole }
import net.liftweb.common.{ Empty, Box, Full }
import net.liftweb.http.{ JsonResponse, LiftResponse }
import net.liftweb.json.JsonAST.JString
import biz.shopboard.lift._

trait Authenticated extends FacadeProvided {
  def getRoles(id: String): List[ApplicationRole] = userService.getUser(id).get.roles

  def isAdmin = UserGUID.isDefined && (UserGUID.get match {
    case Full(id) => getRoles(id).contains(AdminRole)
    case _ => false
  })

  def isMerchant = UserGUID.isDefined && (UserGUID.get match {
    case Full(id) => getRoles(id).contains(MerchantRole)
    case _ => false
  })

  def isUser = UserGUID.isDefined && (UserGUID.get match {
    case Full(id) => getRoles(id).contains(UserRole)
    case _ => false
  })

  def logout: Box[LiftResponse] = {
    UserGUID.set(Empty); MerchantGUID.set(Empty); CampaignGUID.set(Empty)
    Full(JsonResponse(JString(logout_success)))
  }
}
