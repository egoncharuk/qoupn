package biz.shopboard.lift.support

import biz.shopboard.infrastructure.FacadeProvided
import scala.concurrent.Future
import biz.shopboard.domain._
import net.liftweb.http.js._
import JsCmds._
import net.liftweb.http.S
import scalaz.{ Failure, Success }
import net.liftweb.http.js.JE.Str

trait JsExecutable[T] extends FacadeProvided {
  def executeWith(successCmd: JsCmd, failureCmd: JsCmd = Noop)(f: Future[DomainValidation[T]]): JsCmd = execute[T](f) match {
    case Success(_) => successCmd
    case Failure(e) => e.map(S.error(_)); failureCmd
  }
  def executeDeferred(successCmd: T => JsCmd, failureCmd: DomainError => JsCmd = err => err.foldLeft(Noop) { (cmd, s) => cmd & Str(s) })(f: Future[DomainValidation[T]]): JsCmd = execute[T](f) match {
    case Success(t) => successCmd(t)
    case Failure(e) => failureCmd(e)
  }
  def executeDeferredWithSeq(successCmd: T => JsCmd, failureCmd: DomainError => JsCmd = err => err.foldLeft(Noop) { (cmd, s) => cmd & Str(s) })(fs: Seq[Future[DomainValidation[T]]]): JsCmd = {
    if (fs.isEmpty) Noop else fs.tail.foldLeft(execute[T](fs.head)) {
      (dv, f) => dv && execute[T](f)
    } match {
      case Success(t) => successCmd(t)
      case Failure(e) => failureCmd(e)
    }
  }
  def executeWithSeq(successCmd: JsCmd, failureCmd: JsCmd = Noop)(fs: Seq[Future[DomainValidation[T]]]): JsCmd = {
    if (fs.isEmpty) successCmd else fs.tail.foldLeft(execute[T](fs.head)) {
      (dv, f) => dv && execute[T](f)
    } match {
      case Success(_) => successCmd
      case Failure(e) => e.map(S.error(_)); failureCmd
    }
  }
  def executeWithTypeProvided[S](successCmd: JsCmd, failureCmd: JsCmd = Noop)(f: Future[DomainValidation[S]]): JsCmd = execute[S](f) match {
    case Success(_) => successCmd
    case Failure(e) => e.map(S.error(_)); failureCmd
  }
}
