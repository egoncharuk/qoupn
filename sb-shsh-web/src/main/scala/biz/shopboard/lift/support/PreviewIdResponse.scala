package biz.shopboard.lift.support

import net.liftweb.http.{ NotFoundResponse, LiftResponse, InMemoryResponse }
import net.liftweb.common.{ Box, Full }
import biz.shopboard.infrastructure.FacadeProvided
import biz.shopboard.util.Imaging
import biz.shopboard.lift._

object PreviewIdResponse extends FacadeProvided {
  def apply(): Box[LiftResponse] = userService.getUser(UserGUID) match {
    case Some(user) => Full(InMemoryResponse(Imaging.generate(user.id).toByteArray, List("Content-Type" -> "image/jpeg"), Nil, 200))
    case None => Full(NotFoundResponse(no_user_found))
  }
}
