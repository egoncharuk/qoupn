package biz.shopboard.lift.support

import net.liftweb.http.TransientRequestVar
import collection.mutable.ListBuffer
import net.liftweb.http.js.{ JsCmds, JsCmd }
import biz.shopboard.lift._
import JsCmds._
import net.liftweb.http.js.JE.{ Str, Call }

sealed abstract class NumberRequestVar[T](min: T, max: T) extends TransientRequestVar[T](min) with JsCmd {
  val errors: ListBuffer[String] = new ListBuffer[String]()
  private val cmds: ListBuffer[JsCmd] = new ListBuffer[JsCmd]()

  override def toJsCmd = cmds.foldRight(Noop) { _ & _ }
  override def apply(what: T) = { clearAndCheck(what); super.apply(what) }

  protected def isApplicable(what: T): Boolean
  private def clearAndCheck(what: T) = { errors.clear(); cmds.clear(); checkRange(what); }
  private def checkRange(what: T) = if (!isApplicable(what)) {
    errors.append(invalid_range format (min, max)); mark()
  } else if (errors.isEmpty) unmark()

  private def unmark() = cmds.append(Call(
    "$('#" + this.getClass.getSimpleName.replace("$", "") + "').parent('div').removeClass", Str("mandatory")).cmd)
  private def mark() = cmds.append(Call(
    "$('#" + this.getClass.getSimpleName.replace("$", "") + "').parent('div').addClass", Str("mandatory")).cmd)
}

abstract class DoubleRequestVar(min: Double, max: Double) extends NumberRequestVar[Double](min, max) {
  protected def isApplicable(what: Double) = what >= min && what <= max
}
abstract class IntRequestVar(min: Int, max: Int) extends NumberRequestVar[Int](min, max) {
  protected def isApplicable(what: Int) = what >= min && what <= max
}
abstract class LongRequestVar(min: Long, max: Long) extends NumberRequestVar[Long](min, max) {
  protected def isApplicable(what: Int) = what >= min && what <= max
}
