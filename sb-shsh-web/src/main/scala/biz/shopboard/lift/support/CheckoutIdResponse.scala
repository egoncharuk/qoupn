package biz.shopboard.lift.support

import net.liftweb.http.{ BadResponse, NotFoundResponse, LiftResponse, InMemoryResponse }
import net.liftweb.common.{ Box, Full }
import biz.shopboard.infrastructure.FacadeProvided
import biz.shopboard.util.Imaging
import biz.shopboard.lift._

object CheckoutIdResponse extends FacadeProvided {
  def apply(): Box[LiftResponse] = (userService.getUser(UserGUID), userService.getShoppingList(ShoppingListGUID)) match {
    case (Some(user), Some(shoppingList)) => {
      val checkoutId: Option[String] = checkoutService.checkoutShoppingList(user.id, shoppingList.id)
      if (checkoutId.isDefined) Full(InMemoryResponse(Imaging.generate(checkoutId.get).toByteArray, List("Content-Type" -> "image/jpeg"), Nil, 200)) else Full(BadResponse())
    }
    case (None, Some(shoppingList)) => Full(NotFoundResponse(no_user_found))
    case (Some(user), None) => Full(NotFoundResponse(no_shopping_list_found))
    case _ => Full(NotFoundResponse())
  }
}
