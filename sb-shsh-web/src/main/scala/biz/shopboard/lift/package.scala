package biz.shopboard

import net.liftweb.http._
import js.JE.JsRaw
import js.JE.{JsRaw, Str, Call}
import net.liftweb.common.{ Box, Full }
import net.liftweb.http.SHtml.ElemAttr
import net.liftweb.http.js.{ JsCmds, JsCmd }
import JsCmds._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import net.liftweb.util.{ ValueCell, Helpers }

package object lift {
  val login_success = "Successfully logged in!"
  val logout_success = "Successfully logged out!"
  val signup_success = "Successfully signed you up!<br>Please check your email to activate account!"
  val change_password_success = "Successfully changed password!<br>Please login!"
  val send_reset_password_link_success = "Successfully sent reset password link.<br>Please check you email!"
  val deal_addition_success = "Your deal was added successfully!"
  val coupon_addition_success = "Your coupon was added successfully!"
  val deal_linkage_success = "Your deal was successfully linked/unlinked!"
  val coupon_linkage_success = "Your coupon was successfully linked/unlinked!"
  val save_merchants_success = "Your merchants are saved successfully!"
  val general_system_fail = "Please retry after 5 seconds!"
  val parameters_are_incorrect = "Passed parameters are incorrect!"
  val invalid_user = "Username/password combination is invalid!"
  val merchant_exists = "Merchant already exists in the system!"
  val campaign_exists = "Campaign already exists in the system!"
  val valid_from_invalid = "Valid From field is invalid!"
  val valid_to_invalid = "Valid To field is invalid!"
  val valid_to_can_be_only_extended = "Valid To field can be only extended!"
  val image_field_empty = "Please select image to upload!"
  val image_invalid = "Uploaded file type is invalid!"
  val passwords_dont_match = "Passwords don't match!"
  val invalid_range = "Please specify number in the range [%s..%s]"
  val invalid_number = "Please specify valid number"
  val fields_are_empty = "Please fill in mandatory information!"
  val cannot_update_non_draft_campaign = "Cannot update non-draft campaign!"
  val no_merchant_or_campaign_found = "No Merchant or Campaign found!"
  val no_shopping_list_item_found = "No Shopping List item found!"
  val no_shopping_list_found = "No Shopping List found!"
  val no_merchant_selected = "No Merchant selected!"
  val no_campaign_found = "No Campaign found!"
  val no_merchant_found = "No Merchant found!"
  val no_deal_found = "No Deal found!"
  val no_user_found = "No User found!"
  val cannot_activate = "Cannot activate user!"
  val bots_are_ignored = "Thank you bot!"

  val dateFormat: String = "MMMM d"
  val dateFormatFull: String = "dd.MM.yyyy"
  val imgStaticMerchantPath: String = "static/images/icons/merchant.png"
  val imgStaticStorePath: String = "static/images/icons/store.png"
  val imgPath: String = "dynamic/images/"
  val directories = ValueCell(Directories())


  def setContextPath() = SetExp(JsRaw("sessionStorage.cp"), directories.contextPath)

  def reload = Call("location.reload")

  def redirectTo(page: String, params: (String, String)*) = {
    val p = params.foldLeft("?") { (s, p) => s + p._1 + "=" + p._2 + "&" }
    Call("$.mobile.changePage", page + p, JsRaw("{reloadPage: true}"))
  }

  def redirectBack = Call("$.mobile.changePage", S.referer match {
    case Full(url) => url.substring(url.lastIndexOf('/') + 1)
    case _ => ""
  }, JsRaw("{reloadPage: true}"))

  def markVars(reqVars: TransientRequestVar[String]*): JsCmd = {
    reqVars.foldRight(Noop) { (rv, cmd) =>
      cmd & (if (rv.isEmpty || rv.is.isEmpty) {
        Call("$('#" + rv.getClass.getSimpleName.replace("$", "") + "').parent('div').addClass", Str("mandatory")).cmd
      } else Call("$('#" + rv.getClass.getSimpleName.replace("$", "") + "').parent('div').removeClass", Str("mandatory")).cmd)
    }
  }

  def markMandatory(prs: (String, String)*): JsCmd = {
    prs.foldRight(Noop) { (p, cmd) =>
      cmd & (if (p._1.isEmpty) {
        Call("$('#" + p._2 + "').parent('div').addClass", Str("mandatory")).cmd
      } else Call("$('#" + p._2 + "').parent('div').removeClass", Str("mandatory")).cmd)
    }
  }

  def bindId(reqVar: Object): ElemAttr = "id" -> reqVar.getClass.getSimpleName.replace("$", "")

  def checkVarsMandatory(reqVars: TransientRequestVar[String]*): Boolean = reqVars.forall(!_.is.isEmpty)

  def checkImage(reqVar: TransientRequestVar[Box[FileParamHolder]]): Boolean = reqVar.is.isEmpty || "image".equals(reqVar.is.mimeType.split("/").head)

  def checkValidDate(t: TransientRequestVar[String], p: String = dateFormatFull): Boolean = {
    Helpers.tryo(DateTimeFormat.forPattern(p).parseDateTime(t.is)) match {
      case Full(_) => true
      case _ => false
    }
  }

  def parseDate(t: TransientRequestVar[String], p: String = dateFormatFull) = DateTimeFormat.forPattern(p).parseDateTime(t.is)
  def printDateFull(t: DateTime, p: String = dateFormatFull) = DateTimeFormat.forPattern(p).print(t)
  def printDate(t: DateTime, p: String = dateFormat) = DateTimeFormat.forPattern(p).print(t)

  def strip(id: String) = id.replaceAll("[\\s\\.]", "-")

  implicit def BoxConversion[T](t: T): Box[T] = Box(t)
  implicit def ReverseSessionVarConversion[T](v: SessionVar[Box[T]]): T = v.is
  implicit def ReverseBoxConversion[T](t: Box[T]): T = if (t.isEmpty) Nil.asInstanceOf[T] else t.iterator.next()
  implicit def FullDateConversion(t: DateTime): String = DateTimeFormat.fullDate().print(t)
}
