package biz.shopboard.domain.model

import merchant.DraftDeal

case class CreateDealRequest(merchantid: String, campaignid: String, deal: DraftDeal)
