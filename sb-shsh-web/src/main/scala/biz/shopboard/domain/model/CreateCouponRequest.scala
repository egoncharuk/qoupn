package biz.shopboard.domain.model

import merchant.DraftCoupon

/**
 * Created with IntelliJ IDEA.
 * User: goncharuk
 * Date: 2/1/13
 * Time: 12:01 AM
 * To change this template use File | Settings | File Templates.
 */
case class CreateCouponRequest(merchantid: String, campaignid: String, coupon: DraftCoupon)
