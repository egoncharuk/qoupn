package biz.shopboard.domain.model

import merchant.DraftCampaign

/**
 * Created with IntelliJ IDEA.
 * User: goncharuk
 * Date: 1/30/13
 * Time: 11:49 PM
 * To change this template use File | Settings | File Templates.
 */
case class CreateCampaignRequest(merchantid: String, campaign: DraftCampaign)
