package biz.shopboard.rest

import java.util.{ Properties, UUID }
import biz.shopboard.infrastructure.FacadeProvided
import unfiltered.response._
import unfiltered.request._
import biz.shopboard.domain.model.user.{ CheckoutId, User, ShoppingLists }
import biz.shopboard.Constants
import java.io.InputStream
import biz.shopboard.domain.model.merchant._
import collection.mutable.ListBuffer
import util.control.Breaks._
import biz.shopboard.domain.model.PassInfo
import biz.shopboard.domain.model.merchant.Deals
import scalaz.Failure
import scala.Some
import scalaz.Success
import unfiltered.Cookie
import biz.shopboard.domain.model.merchant.Coupons
import org.apache.commons.codec.binary.Base64
import biz.shopboard.util.Imaging

object RestService extends unfiltered.filter.async.Plan with FacadeProvided with JsonHelper {

  def nextId = UUID.randomUUID().toString
  val SESSION_KEY = "x2gmx3m0t723mgx40t7823mtgxo"

  val props = {
    val props: Properties = new Properties()
    val inputStream: InputStream = getClass.getClassLoader.getResourceAsStream("common.properties")
    props.load(inputStream)
    props
  }
  val posUsername = props.getProperty("posterminal.username")
  val posPassword = props.getProperty("posterminal.password")

  def getCookie(userId: String): Cookie = {
    val sessionId: String = SessionStore.put(userId)
    Cookie(SESSION_KEY, sessionId)
  }

  def intent = {
    case req @ POST(Path(Seg("login" :: Nil))) => {
      val json = Body.string(req)
      val passInfo = mapper.readValue(json, classOf[PassInfo])

      val username: String = passInfo.username
      val password: String = passInfo.password
      userService.activeUserExists(username, password) match {
        case true => {
          val userid = userService.findUserByNamePwd(username, password).id
          val json = SetCookies(getCookie(userid)) ~> ResponseJson(AppResponse(ResponseStatus.OK, null, null))
          req.respond(json ~> Ok)
        }
        case false => {
          if (username == posUsername && password == posPassword) {
            val json = SetCookies(getCookie("POSTERMINALID")) ~> ResponseJson(AppResponse(ResponseStatus.OK, null, null))
            req.respond(json ~> Ok)
          } else {
            val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.INVALID_USER, null))
            req.respond(json ~> Ok)
          }
        }
      }
    }

    case req @ GET(Path(Seg("user" :: userId :: Nil))) => {
      userService.getUser(userId) match {
        case None => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.USER_NOT_FOUND, null))
          req.respond(json ~> Ok)
        }
        case Some(user) => {
          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, user))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("signup" :: Nil))) => {

      val jsonStr: String = Body.string(req)
      JsonValidator.validate(jsonStr, classOf[User]) match {
        case Success(_) => {
          val user: User = mapper.readValue(jsonStr, classOf[User])
          execute(userService.createUser(user)) match {
            case Success(inMemUser) => {
              val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Constants.signup_success))
              req.respond(json ~> Ok)
            }
            case Failure(_) => {
              val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.SIGNUP_FAIL, null))
              req.respond(json ~> Ok)
            }
          }
        }
        case Failure(failureStr: String) => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, new ErrorInformation(failureStr, "99"), null))
          req.respond(json ~> Ok)
        }
      }

    }

    case req @ GET(Path(Seg("alldeals" :: Nil))) => {
      val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Deals(merchantService.getDeals.toList collect { case c: ActiveDeal => c })))
      req.respond(json ~> Ok)
    }

    case req @ GET(Path(Seg("allcoupons" :: Nil))) => {
      val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Coupons(merchantService.getCoupons.toList collect { case c: ActiveCoupon => c })))
      req.respond(json ~> Ok)
    }

    case req @ GET(Path(Seg("profile" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val userId = SessionStore.get(sid).getOrElse("")
          userService.getUser(userId) match {
            case None => {
              val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
              req.respond(json ~> Ok)
            }
            case Some(user) => {
              val json = ResponseJson(AppResponse(ResponseStatus.OK, null, user.withoutPassword))
              req.respond(json ~> Ok)
            }
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("subscribe" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[SubscribeRequest])
          val userId = SessionStore.get(sid).getOrElse("")
          execute(userService.subscribeToMerchant(userId, requestData.merchantid)) match {
            case Success(_) => {
              val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Constants.subscribed_success))
              req.respond(json ~> Ok)
            }
            case Failure(f) => {
              val errorInfo: ErrorInformation = ErrorInformation("0", f.toString)
              val json = ResponseJson(AppResponse(ResponseStatus.ERROR, errorInfo, null))
              req.respond(json ~> Ok)
            }
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("unsubscribe" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[UnsubscribeRequest])
          val userId = SessionStore.get(sid).getOrElse("")
          execute(userService.unsubscribeFromMerchant(userId, requestData.merchantid)) match {
            case Success(_) => {
              val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Constants.unsubscribed_success))
              req.respond(json ~> Ok)
            }
            case Failure(f) => {
              val errorInfo: ErrorInformation = ErrorInformation("1", f.toString)
              val json = ResponseJson(AppResponse(ResponseStatus.ERROR, errorInfo, null))
              req.respond(json ~> Ok)
            }
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("redeem" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[RedeemCouponRequest])
          couponSubscriptionService.redeemCoupons(requestData.userid, requestData.merchantid, requestData.couponids)
          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Constants.redeemed_success))
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ GET(Path(Seg("logout" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          SessionStore.remove(sid)
          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Constants.logout_success))
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ GET(Path(Seg("shoppinglists" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val json = userService.getUser(SessionStore.get(sid).getOrElse("")) match {
            case Some(user) => {
              ResponseJson(AppResponse(ResponseStatus.OK, null,
                ShoppingLists(user.shoppingListsIds.map(userService.getShoppingList(_).toList).flatten)))
            }
            case _ => ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.USER_NOT_FOUND, null))
          }
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ GET(Path(Seg("subscribedcoupons" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val userId = SessionStore.get(sid).getOrElse("")
          val couponIds: List[String] = couponSubscriptionService.collectCoupons(userId).getOrElse(List())
          val coupons: ListBuffer[Deal] = ListBuffer()
          for (couponId <- couponIds) {
            coupons += merchantService.getCoupon(couponId).get
          }
          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Coupons(coupons.toList)))
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ GET(Path(Seg("subscribeddeals" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val userId = SessionStore.get(sid).getOrElse("")
          val dealIds: List[String] = dealSubscriptionService.collectDeals(userId).getOrElse(List())
          val deals: ListBuffer[Deal] = ListBuffer()
          for (dealId <- dealIds) {
            deals += merchantService.getDeal(dealId).get
          }
          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Deals(deals.toList)))
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("linkdealshoppinglist" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[DealShoppingListRequest])

          val userId = SessionStore.get(sid).getOrElse("")
          val shoppingListItemId: String = requestData.shoppinglistitemid
          val shoppingListId: String = requestData.shoppinglistid
          val dealId: String = requestData.dealid

          execute(userService.linkShoppingListAndDeal(userId, shoppingListId, dealId, shoppingListItemId)) match {
            case Failure(e) => req.respond(ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.DEAL_LINK_ERROR, null)) ~> Ok)
            case Success(_) => req.respond(ResponseJson(AppResponse(ResponseStatus.OK, null, null)) ~> Ok)
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("unlinkdealshoppinglist" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[DealShoppingListRequest])

          val userId = SessionStore.get(sid).getOrElse("")
          val shoppingListItemId: String = requestData.shoppinglistitemid
          val shoppingListId: String = requestData.shoppinglistid
          val dealId: String = requestData.dealid

          execute(userService.unlinkShoppingListAndDeal(userId, shoppingListId, dealId, shoppingListItemId)) match {
            case Failure(e) => req.respond(ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.DEAL_LINK_ERROR, null)) ~> Ok)
            case Success(_) => req.respond(ResponseJson(AppResponse(ResponseStatus.OK, null, null)) ~> Ok)
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("checkoutdeals" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[CheckoutDealsRequest])

          val checkoutId: String = requestData.checkoutid
          val merchantId: String = requestData.merchantid

          val dealIds: List[String] = checkoutService.collectDeals(checkoutId, merchantId).getOrElse(List())

          val deals: ListBuffer[Deal] = ListBuffer()
          for (dealId <- dealIds) {
            deals += merchantService.getDeal(dealId).get
          }
          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Deals(deals.toList)))
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("linkcouponshoppinglist" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[CouponShoppingListRequest])

          val userId = SessionStore.get(sid).getOrElse("")
          val shoppingListItemId: String = requestData.shoppinglistitemid
          val shoppingListId: String = requestData.shoppinglistid
          val couponId: String = requestData.couponid

          execute(userService.linkShoppingListAndCoupon(userId, shoppingListId, couponId, shoppingListItemId)) match {
            case Failure(e) => req.respond(ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.COUPON_LINK_ERROR, null)) ~> Ok)
            case Success(_) => req.respond(ResponseJson(AppResponse(ResponseStatus.OK, null, null)) ~> Ok)
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("unlinkcouponshoppinglist" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[CouponShoppingListRequest])

          val userId = SessionStore.get(sid).getOrElse("")
          val shoppingListItemId: String = requestData.shoppinglistitemid
          val shoppingListId: String = requestData.shoppinglistid
          val couponId: String = requestData.couponid

          execute(userService.unlinkShoppingListAndCoupon(userId, shoppingListId, couponId, shoppingListItemId)) match {
            case Failure(e) => req.respond(ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.COUPON_LINK_ERROR, null)) ~> Ok)
            case Success(_) => req.respond(ResponseJson(AppResponse(ResponseStatus.OK, null, null)) ~> Ok)
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("checkoutcoupons" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[CheckoutCouponsRequest])

          val checkoutId: String = requestData.checkoutid
          val merchantId: String = requestData.merchantid

          val couponIds: List[String] = checkoutService.collectCoupons(checkoutId, merchantId).getOrElse(List())

          val coupons: ListBuffer[Deal] = ListBuffer()
          for (couponId <- couponIds) {
            coupons += merchantService.getCoupon(couponId).get
          }
          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, Coupons(coupons.toList)))
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("checkoutshoppinglist" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val requestData = mapper.readValue(jsonBody, classOf[CheckoutShoppingListRequest])

          val userId = SessionStore.get(sid).getOrElse("")
          val checkoutId: String = checkoutService.checkoutShoppingList(userId, requestData.shoppinglistid).getOrElse("")

          val json = ResponseJson(AppResponse(ResponseStatus.OK, null, CheckoutId(checkoutId, Base64.encodeBase64String(Imaging.generate(checkoutId).toByteArray))))
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ POST(Path(Seg("shoppinglistchanges" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val jsonBody = Body.string(req)
          val shoppingListChanges = mapper.readValue(jsonBody, classOf[ShoppingListChanges])
          val userId = SessionStore.get(sid).getOrElse("")
          var json = ResponseJson(AppResponse(ResponseStatus.OK, null, null))

          breakable {
            for (change: ShoppingListChange <- shoppingListChanges.changes) {
              change.changetype match {
                case ChangeType.create => {
                  json = executeCreateShoppingList(userId, change, json)
                }
                case ChangeType.remove => {
                  json = executeRemoveShoppingList(userId, change, json)
                }
                case ChangeType.updateDetails => {
                  json = executeShoppingListUpdateDetails(userId, change, json)
                }
                case ChangeType.addItem => {
                  json = executeAddShoppingListItem(userId, change, json)
                }
                case ChangeType.updateItem => {
                  json = executeUpdateShoppingListItem(userId, change, json)
                }
                case ChangeType.removeItem => {
                  json = executeRemoveShoppingListItem(userId, change, json)
                }
              }
            }
          }
          req.respond(json ~> Ok)
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

    case req @ GET(Path(Seg("merchants" :: Nil))) => {
      val cookies = Cookies(req)
      cookies(SESSION_KEY) match {
        case Some(Cookie(_, sid, _, _, _, _, _, _)) => {
          val userId = SessionStore.get(sid).getOrElse("")
          userService.getUser(userId) match {
            case None => {
              val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
              req.respond(json ~> Ok)
            }
            case Some(user) => {
              val merchants: Iterable[Merchant] = merchantService.getMerchants
              val campaigns = merchantService.getCampaigns.toList collect { case c: ActiveCampaign => c }

              val json = ResponseJson(AppResponse(ResponseStatus.OK, null, MarchantAndCampaigns(merchants.toList, campaigns)))
              req.respond(json ~> Ok)
            }
          }
        }
        case _ => {
          val json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.NO_USER_RELOGIN, null))
          req.respond(json ~> Ok)
        }
      }
    }

  }

  def executeRemoveShoppingListItem(userId: String, change: ShoppingListChange, _json: RestService.ResponseJson): RestService.ResponseJson = {
    var json: RestService.ResponseJson = _json
    execute(userService.removeShoppingListItem(
      userId, change.id, Some(change.version), change.itemid)) match {
      case Failure(f) => {
        json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation("12", f.toString()), null))
        break()
      }
      case _ =>
    }
    json
  }

  def executeAddShoppingListItem(userId: String, change: ShoppingListChange, _json: RestService.ResponseJson): RestService.ResponseJson = {
    var json: RestService.ResponseJson = _json
    execute(userService.addShoppingListItem(
      userId, change.id, Some(change.version), change.item)) match {
      case Failure(f) => {
        json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation("10", f.toString()), null))
        break()
      }
      case _ =>
    }
    json
  }

  def executeShoppingListUpdateDetails(userId: String, change: ShoppingListChange, _json: RestService.ResponseJson): RestService.ResponseJson = {
    var json: RestService.ResponseJson = _json
    execute(userService.updateShoppingListDetails(
      userId, change.id, Some(change.version), change.details)) match {
      case Failure(f) => {
        json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation("9", f.toString()), null))
        break()
      }
      case _ =>
    }
    json
  }

  def executeRemoveShoppingList(userId: String, change: ShoppingListChange, _json: RestService.ResponseJson): RestService.ResponseJson = {
    var json: RestService.ResponseJson = _json
    execute(userService.removeShoppingList(
      userId, change.id, Some(change.version))) match {
      case Failure(f) => {
        json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation("8", f.toString()), null))
        break()
      }
      case _ =>
    }
    json
  }

  def executeCreateShoppingList(userId: String, change: ShoppingListChange, _json: RestService.ResponseJson): RestService.ResponseJson = {
    var json: RestService.ResponseJson = _json
    execute(userService.createShoppingList(
      userId, Some(change.version), change.id, change.details)) match {
      case Failure(f) => {
        json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation("7", f.toString()), null))
        break()
      }
      case _ =>
    }
    json
  }

  def executeUpdateShoppingListItem(userId: String, change: ShoppingListChange, _json: RestService.ResponseJson): RestService.ResponseJson = {
    var json: RestService.ResponseJson = _json
    execute(userService.updateShoppingListItem(
      userId, change.id, Some(change.version), change.item)) match {
      case Failure(f) => {
        json = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation("11", f.toString()), null))
        break()
      }
      case _ =>
    }
    json
  }

}
