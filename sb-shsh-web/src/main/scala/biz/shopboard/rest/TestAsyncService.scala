package biz.shopboard.rest

import unfiltered.request.{ Seg, Path => UFPath, GET }
import unfiltered.response.Ok

object TestAsyncService extends unfiltered.filter.async.Plan with JsonHelper {

  def intent = {
    case req @ GET(UFPath(Seg("test" :: userId :: Nil))) => {
      val json: ResponseJson = ResponseJson(AppResponse(ResponseStatus.ERROR, ErrorInformation.SIGNUP_FAIL, null))
      req.respond(json ~> Ok)
    }
  }

}

object App {
  def main(args: Array[String]) {
    val server = unfiltered.jetty.Http.local(8080)
    server.filter(TestAsyncService).start()
  }
}
