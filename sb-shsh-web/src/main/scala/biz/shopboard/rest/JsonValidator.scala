package biz.shopboard.rest

import com.github.fge.jsonschema.util.JsonLoader
import com.github.fge.jsonschema.cfg.ValidationConfiguration
import com.github.fge.jsonschema.SchemaVersion
import com.github.fge.jsonschema.main.JsonSchemaFactory
import scala.tools.nsc.plugins.Plugin.AnyClass
import scalaz.Validation
import scalaz.Validations

object JsonValidator extends JsonHelper with Validations {

  def validate(jsonStr: String, klass: AnyClass) : Validation[String, Any] = {
    val schema = mapper.generateJsonSchema(klass)
    val schemaJson = mapper.writeValueAsString(schema)

    val d = JsonLoader.fromString(jsonStr)
    val s = JsonLoader.fromString(schemaJson)
    /*
        println(schemaJson)
        println(jsonStr)
    */
    val cfg = ValidationConfiguration.newBuilder().setDefaultVersion(SchemaVersion.DRAFTV3).freeze()
    val factory = JsonSchemaFactory.newBuilder().setValidationConfiguration(cfg).freeze()
    val v = factory.getValidator

    val report = v.validate(s, d)
    if (!report.isSuccess) {
      failure(schemaJson)
    } else {
      success()
    }
  }

}
