package biz.shopboard.rest

import biz.shopboard.domain.model.user.{ ShoppingListItem, ShoppingListDetails }

object ChangeType {
  val create = "create"
  val updateDetails = "update_details"
  val remove = "remove"
  val addItem = "add_item"
  val updateItem = "update_item"
  val removeItem = "remove_item"
}

case class RedeemCouponRequest(userid: String, merchantid: String, couponids: List[String])

case class SubscribeRequest(merchantid: String)

case class UnsubscribeRequest(merchantid: String)

case class CheckoutCouponsRequest(checkoutid: String, merchantid: String)

case class CheckoutDealsRequest(checkoutid: String, merchantid: String)

case class DealShoppingListRequest(dealid: String, shoppinglistid: String, shoppinglistitemid: String)

case class CouponShoppingListRequest(couponid: String, shoppinglistid: String, shoppinglistitemid: String)

case class ShoppingListChange(changetype: String, id: String, version: Long,
  details: ShoppingListDetails, itemid: String, item: ShoppingListItem)

case class ShoppingListChanges(changes: List[ShoppingListChange])

case class CheckoutShoppingListRequest(shoppinglistid: String)

