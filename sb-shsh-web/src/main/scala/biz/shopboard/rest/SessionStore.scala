package biz.shopboard.rest

import java.security.SecureRandom

trait SessionStore[T, S] {
  def get(sid: S): Option[T]
  def put(data: T): S
  def remove(sid: S)
}

object SessionStore extends SessionStore[String, String] {
  private val storage = scala.collection.mutable.Map[String, String]()
  private val prng: SecureRandom = SecureRandom.getInstance("SHA1PRNG")

  def get(sid: String) = storage get sid

  def put(data: String) = {
    val sid = generateSid
    SessionStore.synchronized {
      storage += (sid -> data)
    }
    sid
  }

  def remove(sid: String) {
    SessionStore.synchronized {
      storage -= (sid)
    }
  }

  def dump = storage.toMap

  protected def generateSid = {
    //generate a random number
    val randomNum = new Integer(prng.nextInt()).toString

    val md = java.security.MessageDigest.getInstance("SHA-256")
    val digest: Array[Byte] = md.digest(randomNum.getBytes("UTF-8"))
    new sun.misc.BASE64Encoder().encode(digest)
  }
}
