package bootstrap.liftweb

import _root_.net.liftweb.http._
import biz.shopboard.infrastructure.ShopBoardContext
import biz.shopboard.domain.model.user.{ UserStatus, UserDetails, User }
import biz.shopboard.domain.model.common.AdminRole
import biz.shopboard.lift._
import net.liftweb.common.Full
import biz.shopboard.lift.{ Directories, UserGUID }
import net.liftweb.sitemap.{ Menu, Loc, SiteMap }
import net.liftweb.sitemap.Loc.{ If, Hidden }
import biz.shopboard.lift.snippet.{ ActivateUser, LinkItem, MarkItem }
import support.{ CheckoutIdResponse, PreviewIdResponse, Authenticated }
import bootstrap.JettyBoot
import provider.servlet.HTTPServletContext
import org.mortbay.jetty.webapp.WebAppContext
import net.liftweb.util.Props
import biz.shopboard.service.MailServiceImpl
import org.mortbay.jetty.handler.ContextHandler

/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 *
 * Execute mvn jetty:run-war to start Application on Jetty server
 */
class Boot extends Authenticated {

  def boot() {
    // define working directories
    val (realPath, contextPath) = LiftRules.context match {
      case c: HTTPServletContext => (c.ctx.getRealPath("/"), c.ctx.getContextPath)
      case _ => WebAppContext.getCurrentWebAppContext match {
        case c if c != null => {
          val ctx: ContextHandler#SContext = c.getServletContext
          (ctx.getRealPath("/"), ctx.getContextPath)
        }
        case _ => ("/", "")
      }
    }

    val savePath = if (realPath == null) "" else realPath
    val i: Int = savePath.lastIndexOf("/")
    val root = savePath.substring(0, i + 1)
    directories.set(Directories(root, savePath + "/", contextPath))

    //start application
    ShopBoardContext.mailService = new MailServiceImpl
    ShopBoardContext.start(contextPath)
    JettyBoot.start()

    // define authentication
    LiftRules.loggedInTest = Full(() => UserGUID.isDefined)
    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    // where to search snippet
    LiftRules.addToPackages("biz.shopboard.lift")

    //Enable HTML5 support
    LiftRules.htmlProperties.default.set((r: Req) => new Html5Properties(r.userAgent))

    //create secure sitemap
    //LiftRules.siteMapFailRedirectLocation = List("index.html")
    val authAll = If(() => isUser || isAdmin || isMerchant, () => RedirectResponse("/index.html"))
    val authUser = If(() => isUser || isAdmin, () => RedirectResponse("/index.html"))
    val authMerchant = If(() => isMerchant || isAdmin, () => RedirectResponse("/index.html"))
    val authAdmin = If(() => isAdmin, () => RedirectResponse("/index.html"))

    val entries =
      Menu(Loc("First", "index" :: Nil, "Qoupn", Hidden)) ::
        Menu(Loc("Login", "login" :: Nil, "Login", Hidden)) ::
        Menu(Loc("Signup", "signup" :: Nil, "Sign Up", Hidden)) ::
        Menu(Loc("Change password", "change-password" :: Nil, "Change password", Hidden)) ::
        Menu(Loc("Reset password", "reset-password" :: Nil, "Reset password", Hidden)) ::
        Menu(Loc("Search", "search" :: Nil, "Search", authUser)) ::
        Menu(Loc("Show ID", "show-id" :: Nil, "Show ID", authUser)) ::
        Menu(Loc("Checkout", "checkout" :: Nil, "Checkout", authUser)) ::
        Menu(Loc("Checklist", "checklist" :: Nil, "Checklist", authUser)) ::
        Menu(Loc("My Merchants", "my-merchants" :: Nil, "My Merchants", authUser)) ::
        Menu(Loc("Search Deals", "search-deals" :: Nil, "Search Deals", authUser)) ::
        Menu(Loc("Profile", "edit-profile" :: Nil, "Profile", authAll)) ::
        Menu(Loc("Add Shopping List", "add-shopping-list" :: Nil, "Add Shopping List", authUser)) ::
        Menu(Loc("Edit Shopping List", "edit-shopping-list" :: Nil, "Edit Shopping List", authUser)) ::
        Menu(Loc("Delete Shopping List", "delete-shopping-list" :: Nil, "Delete Shopping List", authUser)) ::
        Menu(Loc("Delete Shopping List Item", "delete-shopping-list-item" :: Nil, "Delete Shopping List Item", authUser)) ::
        Menu(Loc("Edit Shopping List Item", "edit-shopping-list-item" :: Nil, "Edit Shopping List Item", authUser)) ::
        Menu(Loc("Add Shopping List Item", "add-shopping-list-item" :: Nil, "Add Shopping List Item", authUser)) ::
        Menu(Loc("My Shopping Lists", "my-shopping-lists" :: Nil, "My Shopping Lists", authUser)) ::
        Menu(Loc("Shopping List Items", "shopping-list-items" :: Nil, "Shopping List Items", authUser)) ::
        Menu(Loc("Personal Deals", "personal-deals" :: Nil, "Personal Deals", authUser)) ::
        Menu(Loc("Personal Coupons", "personal-coupons" :: Nil, "Personal Coupons", authUser)) ::
        Menu(Loc("Add Active Deal", "add-active-deal" :: Nil, "Add Active Deal", authUser)) ::
        Menu(Loc("Add Active Coupon", "add-active-coupon" :: Nil, "Add Active Coupon", authUser)) ::
        Menu(Loc("Link Coupon", "link-coupon" :: Nil, "Link Coupon", authUser)) ::
        Menu(Loc("Link Deal", "link-deal" :: Nil, "Link Deal", authUser)) ::
        Menu(Loc("Campaigns", "campaigns" :: Nil, "Campaigns", authMerchant)) ::
        Menu(Loc("Deals", "deals" :: Nil, "Deals", authMerchant)) ::
        Menu(Loc("Product Info", "product-info" :: Nil, "Product Info", authMerchant)) ::
        Menu(Loc("Merchants", "merchants" :: Nil, "Merchants", authAdmin)) ::
        Menu(Loc("Add Merchant", "add-merchant" :: Nil, "Add Merchant", authAdmin)) ::
        Menu(Loc("Edit Merchant", "edit-merchant" :: Nil, "Edit Merchant", authAdmin)) ::
        Menu(Loc("Add Campaign", "add-campaign" :: Nil, "Add Campaign", authMerchant)) ::
        Menu(Loc("Edit Campaign", "edit-campaign" :: Nil, "Edit Campaign", authMerchant)) ::
        Menu(Loc("Delete Campaign", "delete-campaign" :: Nil, "Delete Campaign", authMerchant)) ::
        Menu(Loc("Activate Campaign", "activate-campaign" :: Nil, "Activate Campaign", authMerchant)) ::
        Menu(Loc("Deactivate Campaign", "deactivate-campaign" :: Nil, "Deactivate Campaign", authMerchant)) ::
        Menu(Loc("Add Deal", "add-deal" :: Nil, "Add Deal", authMerchant)) ::
        Menu(Loc("Edit Deal", "edit-deal" :: Nil, "Edit Deal", authMerchant)) ::
        Menu(Loc("Delete Deal", "delete-deal" :: Nil, "Delete Deal", authMerchant)) ::
        Menu(Loc("Add Coupon", "add-coupon" :: Nil, "Add Coupon", authMerchant)) ::
        Menu(Loc("Edit Coupon", "edit-coupon" :: Nil, "Edit Coupon", authMerchant)) ::
        Menu(Loc("Delete Coupon", "delete-coupon" :: Nil, "Delete Coupon", authMerchant)) ::
        Menu(Loc("Add Product Info", "add-product-info" :: Nil, "Add Product Info", authMerchant)) ::
        Menu(Loc("Delete Product Info", "delete-product-info" :: Nil, "Delete Product Info", authMerchant)) ::
        Nil
    LiftRules.setSiteMap(SiteMap(entries: _*))

    LiftRules.dispatch.append {
      case Req("logout" :: Nil, _, _) => logout _
      case Req("activate-user" :: Nil, _, GetRequest) => () => ActivateUser()
      case Req("preview-id" :: Nil, _, _) if isUser => () => PreviewIdResponse()
      case Req("checkout-id" :: Nil, _, _) if isUser => () => CheckoutIdResponse()
      case r @ Req("mark-item" :: Nil, _, PostRequest) if isUser => () => MarkItem(r)
      case r @ Req("link-item" :: Nil, _, PostRequest) if isUser => () => LinkItem(r)
    }

    //start application hooks
    createDefaultAdminUser()
    //stop application hooks
    LiftRules.unloadHooks.append(ShopBoardContext.stop)

    /************************ FILE UPLOAD *******************************/
    // In cases where we have an AJAX request for IE with an uploaded file, we
    // assume we served through an iframe (a fairly safe assumption) and serve
    // up the response with a content type of text/plain so that IE does not
    // attempt to save the response as a downloaded file.
    LiftRules.responseTransformers.append {
      resp =>
        (for (req <- S.request) yield {
          resp.toResponse match {
            case InMemoryResponse(data, headers, cookies, code) if !req.uploadedFiles.isEmpty &&
              /* req.isIE && */
              req.path.wholePath.head == LiftRules.ajaxPath =>
              val contentlessHeaders = headers.filterNot(_._1.toLowerCase == "content-type")
              InMemoryResponse(data, ("Content-Type", "text/plain; charset=utf-8") :: contentlessHeaders, cookies, code)
            case _ => resp
          }
        }) openOr resp
    }
    /********************************************************************/

    import javax.mail.Authenticator
    import net.liftweb.util.{ Mailer, Props }

    Mailer.authenticator = (Props.get("mail.user"), Props.get("mail.password")) match {
      case (Full(u), Full(p)) => Full(new Authenticator() {
        override def getPasswordAuthentication = new javax.mail.PasswordAuthentication(u, p)
      })
      case _ => net.liftweb.common.Empty //(new Exception("Username/password not supplied for Mailer."))
    }
  }

  def createDefaultAdminUser() {
    if ((Props.devMode || Props.testMode) && ShopBoardContext.userService.getAdmins.isEmpty) {
      val details: UserDetails = UserDetails(Props.get("admin.login").get,
        Props.get("admin.pass").get, "", "", Props.get("admin.email").get,
        status = UserStatus.ACTIVE)
      ShopBoardContext.userService.createUser(User.create(Props.get("admin.id").get, details, AdminRole).toOption.get)
    }
  }

}

