package bootstrap

import biz.shopboard.ConfigContext
import biz.shopboard.rest.RestService
import org.eclipse.jetty.util.thread.ExecutorThreadPool
import java.util.concurrent.TimeUnit
import org.slf4j.LoggerFactory

/**
 * Please note that for now Lift starts up Akka system in Boot class
 */
object JettyBoot {

  val log = LoggerFactory.getLogger(JettyBoot.getClass)

  val port = ConfigContext.getRestServicePort
  val host = "0.0.0.0"

  private val _unfiltered_server = unfiltered.jetty.Http.apply(port, host)
  private val _server = _unfiltered_server.underlying

  def start() {
    // Please change this setting very carefully
    val threadsCount = Option(System.getProperty("jetty.server.rest.threads")).getOrElse("10")
    // TODO: need to test what setup is best for our hosting server
    log.info("Starting up Rest JettyServer on %s : %s with threads count %s"  format (host, port.toString, threadsCount))
    _server.setThreadPool(new ExecutorThreadPool(threadsCount.toInt, threadsCount.toInt, 10, TimeUnit.SECONDS))
    _unfiltered_server.filter(RestService)
    _server.start()
  }

}
