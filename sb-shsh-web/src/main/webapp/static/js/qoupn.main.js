function validateEmail(elementValue) {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(elementValue);
}
function initMerchants() {
    $(document).on("pageshow", "#my-merchants-page", function() {
        $('#merchants').controlgroup('refresh', true);
    });
}
function initShoppingLists(pageId) {
    $(document).on("pageshow", "#" + pageId, function() {
        $('#shopping-lists').controlgroup('refresh', true);
    });
}
function initListView(pageId, id) {
    $(document).on("pageshow", "#" + pageId, function() {
        $('#' + id).listview({
            autodividers: true,
            autodividersSelector: function (li) {
                return $(li).find('h3').attr('merchant-id');
            }
        }).listview('refresh');
    });
}
function initSearch() {
    $(document).on("pageshow", "#search-page", function() {
        if (sessionStorage.q) {
            $('input[data-type="search"]').val(sessionStorage.q).change();
        }
        if (sessionStorage.paths && sessionStorage.paths != "[]") {
            $("#selected-merchants").empty();
            JSON.parse(sessionStorage.paths).forEach(function(path) {
                $("#selected-merchants").append('<img class=\"store-icons\" src=\"' + sessionStorage.cp + "/" + path + '\" />');
            });
        }
    });
    $(document).on("pageinit", "#search-page", function() {
        $( "#autocomplete" ).on("listviewbeforefilter", function (e, data) {
            var $ul = $( this ),
                $input = $( data.input ),
                value = $input.val(),
                html = "";
            $ul.html( "" );

            sessionStorage.q = value;
            if (value && value.length > 2) {
                $ul.html("<li><div class='ui-loader'><span class='ui-icon ui-icon-loading'></span></div></li>");
                $ul.listview("refresh");
                $.ajax({
                    url: sessionStorage.cp + "/search-deals",
                    data: { q: $input.val(), stores: sessionStorage.stores },
                    cache: false
                })
                    .then( function (response) {
                        html = $("<div>"+response+"</div>")
                        popups = html.find("#details-container").html()
                        items = html.find('ul').html()

                        $ul.html(items);
                        $("#details-container").html(popups)

                        $ul.listview("refresh");
                        $ul.trigger("updatelayout");
                        $("[data-role='popup']").popup();
                    });
            }
        });
    });
}

function initCalculations() {
    $(document).on("pageinit", "#add-deal-page", function() {
       initCalculationFields();
    });
    $(document).on("pageinit", "#add-coupon-page", function() {
       initCalculationFields();
    });
    $(document).on("pageinit", "#edit-deal-page", function() {
       initCalculationFields();
    });
    $(document).on("pageinit", "#edit-coupon-page", function() {
       initCalculationFields();
    });
}

function initCalculationFields() {
   $('#originalPrice,#discountedPrice,#discountPercent').blur(function() {
       calculate();
   });

   function calculate() {
       var originalPrice = getNumeric($('#originalPrice'));
       var discountedPrice = getNumeric($('#discountedPrice'));
       var discountPercent = getNumeric($('#discountPercent'));
       if (originalPrice != null && discountedPrice != null && discountPercent != null) {
           return;
       }
       if (originalPrice != null && discountedPrice != null ) {
           var num = (originalPrice - discountedPrice) / originalPrice * 100;
           $('#discountPercent').val(num.toFixed(2));
       } else if (discountedPrice != null && discountPercent != null ) {
           var num = (-100 * discountedPrice) / (discountPercent - 100);
           $('#originalPrice').val(num.toFixed(2));
       } else if (originalPrice != null && discountPercent != null ) {
           var num = originalPrice - ((originalPrice / 100) * discountPercent);
           $('#discountedPrice').val(num.toFixed(2));
       }
   }

   function getNumeric(input) {
       var val = input.val();
       if (val == null || !$.isNumeric(val)) {
           return null;
       }
       return val;
   }
}

function initItemsList() {
    $(document).on("pageinit", "#shopping-list-items-page", function() {
        $("li.withSwipe>div").on('swipe', function (event) {
            if ($(event.target).find("h3").hasClass('item-removed')) {
                $(event.target).find("h3").removeClass('item-removed');
                $(event.target).find("a.withId").removeClass('item-removed');
            } else {
                $(event.target).find("h3").addClass('item-removed');
                $(event.target).find("a.withId").addClass('item-removed');
            }
            $.ajax({
                url: sessionStorage.cp + "/mark-item",
                type: "POST",
                data: {
                    id: $(event.target).find("a.withId").attr("with-id"),
                    sid: $(event.target).find("a.withId").attr("with-sl-id")
                }
            });
        });
        $("li.withSwipe>div").on("tap", function (event) {
            $("#panel-" + $(event.target).find("a.withId").attr("with-id")).panel("open");
        });
        $("li.withLinking>div.ui-li").on("tap", function (event) {
            $.ajax({
                url: sessionStorage.cp + "/link-item",
                type: "POST",
                data: {
                    "item-id": $(event.target).find("a").attr("item-id"),
                    "merchant-id": $(event.target).find("a").attr("merchant-id"),
                    "shopping-list-id": $(event.target).find("a").attr("shopping-list-id"),
                    "is-coupon": $(event.target).find("a").attr("is-coupon")
                }
            });
        });
    });
}
function logout() {
    $.get(sessionStorage.cp + '/logout', {}, function (json) {
        sessionStorage.clear;
        $.mobile.alert(json, "title");
        $.mobile.changePage("index.html", {
            reloadPage: true }
        );
    });
}

function refreshPage() {
    $.mobile.changePage(
        window.location.href,
        {
            allowSamePageTransition : true,
            transition              : 'none',
            showLoadMsg             : false,
            reloadPage              : true
        }
    );
}

function initDefaults() {
    $.mobile.touchOverflowEnabled = true;
    $.mobile.defaultPageTransition = "none";
    $.event.special.swipe.durationThreshold = 3000;
    initListView("personal-coupons-page", "coupon-items");
    initListView("personal-deals-page", "deal-items");
    initShoppingLists('link-coupon-page');
    initShoppingLists('link-deal-page');
    initMerchants();
    initItemsList();
    initSearch();
    initCalculations();
}
