package org.eligosource.eventsourced.core

import biz.shopboard.journal.MongodbSnapshotState

case class SnapshotClass(cnt: Int) extends MongodbSnapshotState
