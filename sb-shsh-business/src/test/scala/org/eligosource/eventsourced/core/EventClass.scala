package org.eligosource.eventsourced.core

import biz.shopboard.journal.MongodbEvent

case class EventClass(s: String) extends MongodbEvent
