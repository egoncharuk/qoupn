package biz.shopboard

import java.util.concurrent.TimeUnit

import akka.actor._
import akka.util.Timeout
import scala.reflect.ClassTag

import journal.MongoDBCasbahJournalProps
import org.scalatest.matchers.MustMatchers

import org.eligosource.eventsourced.core.{ Journal, EventsourcingExtension }
import com.mongodb.casbah.Imports._
import org.scalatest.fixture.WordSpec

abstract class EventsourcingSpecWithMongodb[T <: EventsourcingMongodbFixture[_]: ClassTag] extends WordSpec with MustMatchers {
  type FixtureParam = T

  def createFixture =
    implicitly[ClassTag[T]].runtimeClass.newInstance().asInstanceOf[T]

  def withFixture(test: OneArgTest) {
    val fixture = createFixture
    try {
      test(fixture)
    } finally {
      fixture.shutdown()
    }
  }
}

trait EventsourcingMongodbFixture[A] {
  implicit val system = ActorSystem("test")
  implicit val timeout = new Timeout(5, TimeUnit.SECONDS)

  val port = biz.shopboard.journal.mongoDefaultPort
  val host = biz.shopboard.journal.mongoLocalHostName
  val postfix = ConfigContext.getDbNamePostfix
  val dbName = "es2" + postfix
  val collName = "event"
  val snapshotName = "snapshot"

  val client = MongoClient(host, port)
  val journal = Journal(MongoDBCasbahJournalProps(client, dbName, collName, snapshotName))

  val extension = EventsourcingExtension(system, journal)
  var deleteJournal = true

  def shutdown() {
    system.shutdown()
    system.awaitTermination()
    if (deleteJournal) {
      MongoClient(host, port)(dbName)(collName).dropCollection()
      MongoClient(host, port)(dbName)(snapshotName).dropCollection()
    }
  }
}
