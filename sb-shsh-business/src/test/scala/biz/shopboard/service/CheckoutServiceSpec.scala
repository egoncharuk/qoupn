package biz.shopboard.service

import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import scala.concurrent.duration._
import biz.shopboard.{ EventsourcingFixtureInMemory, EventsourcingSpecInMemory }
import biz.shopboard.service.CheckoutServiceSpec.FixtureInMemory
import biz.shopboard.domain.model.user._
import biz.shopboard.domain.model.merchant._
import biz.shopboard.domain.DomainValidation
import biz.shopboard.infrastructure.ValidationMessages
import org.joda.time.format.DateTimeFormat
import org.joda.time.DateTime
import akka.actor.Props
import org.eligosource.eventsourced.core._
import biz.shopboard.state.TotalEmitter
import concurrent.stm.Ref
import concurrent.Await
import biz.shopboard.domain.model.merchant.CampaignDetails
import scalaz.Failure
import scala.Some
import org.eligosource.eventsourced.core.DefaultChannelProps
import biz.shopboard.domain.model.user.UserDetails
import biz.shopboard.domain.model.user.ShoppingListDetails
import scalaz.Success
import biz.shopboard.domain.model.merchant.MerchantDetails
import biz.shopboard.domain.model.merchant.DealDetails
import biz.shopboard.domain.model.common.Address

@RunWith(classOf[JUnitRunner])
class CheckoutServiceSpec extends EventsourcingSpecInMemory[FixtureInMemory] with ValidationMessages {

  val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")

  val details1 = new UserDetails("login", "password", "name", "surname", "email@email.com")
  val details4 = new DealDetails("Tesco Deal", null, null, DateTime.parse("11.11.2011", formatter), DateTime.parse("11.12.2012", formatter))
  val details3 = new CampaignDetails("Tesco Xmas", "", DateTime.parse("11.11.2011", formatter), DateTime.parse("11.12.2012", formatter))
  val details2 = new MerchantDetails("Tesco", new Address)

  val user = new User("usr-id1", 0L, details1)
  val merchant = new Merchant("ptn-id1", 0L, "usr-id1", details2)
  val shoppingList = new ShoppingList("sid-id1", 0L, ShoppingListDetails("some name"))

  "A checkout service" when {
    "nothing yet been created" must {
      "return None when collecting coupons" in {
        fixture =>
          import fixture._

          checkoutService.collectCoupons(user.id, merchant.id) match {
            case Some(seq) => fail()
            case None =>
          }
      }
    }

    "another service was asked to create a user and subscribe it to created before merchant" must {
      "return None when Campaign + Coupon are created in the system and linking of ShoppingList and Coupon did not happened" in {
        fixture =>
          import fixture._

          createUser(userService)
          createMerchant(merchantService)
          createShoppingList(userService)

          Await.result(userService.subscribeToMerchant(user.id, merchant.id), 5.seconds)
          Await.result(merchantService.createCampaign(merchant.id, merchant.versionOption, "cmp-id1", details3), 5.seconds) match {
            case Success(campaign) => {
              Await.result(merchantService.createCoupon(merchant.id, campaign.id, campaign.versionOption, "cpn-id1", details4, 10), 5.seconds) match {
                case Success(coupon) => {
                  Await.result(merchantService.activateCampaign(merchant.id, campaign.id, Some(campaign.versionOption.get + 1)), 5.seconds)
                }
                case Failure(f) => {
                  f
                }
              }
            }
            case Failure(f) => {
              f
            }
          }

          val id: Option[String] = checkoutService.checkoutShoppingList(user.id, shoppingList.id)

          checkoutService.collectCoupons(id.get, merchant.id) match {
            case Some(ids) if !ids.isEmpty => fail()
            case _ =>
          }
      }
    }

    "another service was asked to create a user and subscribe it to created before merchant" must {
      "return collected coupon ids when Campaign + Coupon are created in the system and linking of ShoppingList and Coupon happened" in {
        fixture =>
          import fixture._

          createUser(userService)
          createMerchant(merchantService)
          createShoppingList(userService)

          Await.result(userService.subscribeToMerchant(user.id, merchant.id), 5.seconds)
          Await.result(merchantService.createCampaign(merchant.id, merchant.versionOption, "cmp-id1", details3), 5.seconds) match {
            case Success(campaign) => {
              Await.result(merchantService.createCoupon(merchant.id, campaign.id, campaign.versionOption, "cpn-id1", details4, 10), 5.seconds) match {
                case Success(coupon) => {
                  Await.result(merchantService.activateCampaign(merchant.id, campaign.id, Some(campaign.versionOption.get + 1)), 5.seconds)
                  Await.result(userService.linkShoppingListAndCoupon(user.id, shoppingList.id, coupon.id), 5.seconds)
                }
                case Failure(f) => {
                  f
                }
              }
            }
            case Failure(f) => {
              f
            }
          }

          val id: Option[String] = checkoutService.checkoutShoppingList(user.id, shoppingList.id)

          checkoutService.collectCoupons(id.get, merchant.id) match {
            case Some(ids) => ids must equal(List("cpn-id1"))
            case None => fail()
          }
      }
    }
  }

  def createUser(implicit service: UserService): DomainValidation[User] = {
    Await.result(service.createUser(user.id, user.details, user.roles), 5.seconds)
  }

  def createShoppingList(implicit service: UserService): DomainValidation[ShoppingList] = {
    Await.result(service.createShoppingList(user.id, user.versionOption, shoppingList.id, ShoppingListDetails("some name")), 5.seconds)
  }

  def createMerchant(implicit service: MerchantService): DomainValidation[Merchant] = {
    Await.result(service.createMerchant(merchant), 5.seconds)
  }

}

object CheckoutServiceSpec {

  class FixtureInMemory extends EventsourcingFixtureInMemory[Any] {

    val usersRef = Ref(Map.empty[String, User])
    val actionHashesRef = Ref(Map.empty[String, ActionHash])
    val merchantsRef = Ref(Map.empty[String, Merchant])
    val shoppingListsRef = Ref(Map.empty[String, ShoppingList])
    val shoppingListsToDealsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val shoppingListsToCouponsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val mailService = new MailServiceMock
    val mailerChannelId = 999

    val userProcessor = extension.processorOf(Props(new UserProcessor(usersRef, actionHashesRef, merchantsRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = 1
    }))
    val mailerActorRef = system.actorOf(Props(new MailerGateway(mailService, userProcessor) with Receiver with Confirm))
    extension.channelOf(ReliableChannelProps(mailerChannelId, mailerActorRef).withName("mailer"))
    val campaignsRef = Ref(Map.empty[String, Campaign])
    val couponsRef = Ref(Map.empty[String, Deal])
    val dealsRef = Ref(Map.empty[String, Deal])
    val merchantProcessor = extension.processorOf(Props(new MerchantProcessor(merchantsRef, campaignsRef, couponsRef, dealsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = 2
    }))
    val subscriptionProjection = extension.processorOf(Props(new CouponSubscriptionProjection(couponsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = 3
    }))
    val checkoutMapRef = Ref(Map.empty[String, (String, String, DateTime)])
    val checkoutProcessor = extension.processorOf(Props(new CheckoutProcessor(checkoutMapRef) with TotalEmitter with Confirm with Eventsourced {
      val id = 4
    }))

    extension.channelOf(DefaultChannelProps(1, subscriptionProjection))
    extension.channelOf(DefaultChannelProps(2, merchantProcessor))
    extension.recover()
    // wait for processor 1 to complete processing of replayed event messages
    // (ensures that recovery of externally visible state maintained by
    //  invoicesRef is completed when awaitProcessorCompletion returns)
    extension.awaitProcessing(Set(1, 2, 3, 4))

    val merchantService = new MerchantService(merchantsRef, campaignsRef, couponsRef, dealsRef, merchantProcessor)
    val userService = new UserService(usersRef, actionHashesRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef, userProcessor)
    val checkoutService = new CheckoutService(checkoutMapRef, shoppingListsToDealsRef, shoppingListsToCouponsRef, subscriptionProjection, null, checkoutProcessor)
    val subscriptionService = new CouponSubscriptionService(subscriptionProjection)
  }

}

