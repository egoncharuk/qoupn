package biz.shopboard.service

import scala.concurrent.duration._
import org.eligosource.eventsourced.core._
import biz.shopboard.domain.DomainValidation
import biz.shopboard.state.TotalEmitter
import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import akka.actor.Props
import concurrent.stm.Ref
import biz.shopboard.{ EventsourcingSpecWithMongodb, EventsourcingMongodbFixture }
import biz.shopboard.service.UserEventServiceSpec.FixtureByMongodb
import biz.shopboard.domain.model.user._
import biz.shopboard.domain.model.merchant.Merchant
import scala.Some
import biz.shopboard.domain.model.user.UserDetails
import biz.shopboard.domain.model.user.UserCreated
import biz.shopboard.domain.model.user.ShoppingListDetails
import biz.shopboard.domain.model.user.ShoppingListCreated
import scalaz.Success
import concurrent.Await

@RunWith(classOf[JUnitRunner])
class UserEventServiceSpec extends EventsourcingSpecWithMongodb[FixtureByMongodb] {

  val details1 = new UserDetails("login", "password", "name", "surname", "email@email.com")
  val shopListDetails1 = new ShoppingListDetails("list 1")

  val user = new User("usr-id1", 0L, details1.encryptPassword)
  val shopList = new ShoppingList("list-id1", 0L, shopListDetails1)

  "A user event service" when {
    "nothing yet been created" must {
      "return None when getting history" in { fixture =>
        import fixture._

        eventService.getHistory(user.id, "") match {
          case Some(seq) => fail()
          case None =>
        }
      }
    }

    "another service was asked to create a new shopping list" must {
      "consume and return all events which are produced by user & shopping list creation in that service" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service) must equal(Success(shopList))
        Thread.sleep(2000)

        eventService.getHistory(user.id, "") match {
          case None => fail()
          case Some(seq) => {
            seq(0) must equal(UserCreated(user.id, user.details))
            seq(1) must equal(ShoppingListCreated(user.id, shopList.id, shopListDetails1))
          }
        }
      }
    }

    "all events are stored in journal DB" must {
      "return all events which are restored by replay from journal DB" in { fixture =>
        import fixture._

        eventService.getHistory(user.id, "") match {
          case None => fail()
          case Some(seq) => {
            seq.size must be(2)
            seq(0) must equal(UserCreated(user.id, user.details))
            seq(1) must equal(ShoppingListCreated(user.id, shopList.id, shopListDetails1))
          }
        }
      }
    }

    "all events are stored in journal DB" must {
      "return all events which are restored by replay from journal DB staring with some seqNr" in { fixture =>
        import fixture._

        deleteJournal = true // for the last SUS we are deleting the journal

        eventService.getHistory(user.id, "", 5) match {
          case None => fail()
          case Some(seq) => {
            seq(0) must equal(UserCreated(user.id, user.details))
            seq(1) must equal(ShoppingListCreated(user.id, shopList.id, shopListDetails1))
          }
        }
        eventService.getHistory(user.id, "", 1) match {
          case None => fail()
          case Some(seq) => {
            seq(0) must equal(ShoppingListCreated(user.id, shopList.id, shopListDetails1))
          }
        }
      }
    }
  }

  "A user event service" when {
    "all events are stored in journal DB" must {
      "do a snapshot and return all events from UserEventService" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service) must equal(Success(shopList))
        Thread.sleep(2000)

        Await.result(extension.snapshot(Set(2)), 5.seconds)
        eventService.getHistory(user.id, "") match {
          case None => fail()
          case Some(seq) => {
            seq.size must be(2)
            seq(0) must equal(UserCreated(user.id, user.details))
            seq(1) must equal(ShoppingListCreated(user.id, shopList.id, shopListDetails1))
          }
        }
      }
    }

    "all events are stored in journal DB" must {
      "return all events from restored from the snapshot kept in MongoDB" in { fixture =>
        import fixture._

        deleteJournal = true // for the last SUS we are deleting the journal

        eventService.getHistory(user.id, "") match {
          case None => fail()
          case Some(seq) => {
            seq.size must be(2)
            seq(0) must equal(UserCreated(user.id, user.details))
            seq(1) must equal(ShoppingListCreated(user.id, shopList.id, shopListDetails1))
          }
        }
      }
    }
  }

  def createUser(implicit service: UserService): DomainValidation[User] = {
    Await.result(service.createUser(user.id, details1), 5.seconds)
  }

  def createShopList(implicit service: UserService, versionOpt: Option[Long] = shopList.versionOption): DomainValidation[ShoppingList] = {
    Await.result(service.createShoppingList(user.id, versionOpt, shopList.id, shopList.details), 5.seconds)
  }
}

object UserEventServiceSpec {

  class FixtureByMongodb extends EventsourcingMongodbFixture[Any] {
    val usersRef = Ref(Map.empty[String, User])
    val actionHashesRef = Ref(Map.empty[String, ActionHash])
    val merchantsRef = Ref(Map.empty[String, Merchant])
    val shoppingListsRef = Ref(Map.empty[String, ShoppingList])
    val shoppingListsToDealsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val shoppingListsToCouponsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val mailService = new MailServiceMock
    val mailerChannelId = 999

    val userProcessor = extension.processorOf(Props(
      new UserProcessor(usersRef, actionHashesRef, merchantsRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef) with TotalEmitter with Confirm with Eventsourced {
        val id = 1
      }))
    val mailerActorRef = system.actorOf(Props(new MailerGateway(mailService, userProcessor) with Receiver with Confirm))
    extension.channelOf(ReliableChannelProps(mailerChannelId, mailerActorRef).withName("mailer"))
    val userEventProjection = extension.processorOf(Props(new UserEventProjection with Receiver with Confirm with Eventsourced {
      val id = 2
    }))

    extension.channelOf(ReliableChannelProps(1, userEventProjection))

    extension.recover(extension.replayParams.allWithSnapshot)
    extension.awaitProcessing(Set(1, 2))

    val service = new UserService(usersRef, actionHashesRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef, userProcessor)
    val eventService = new UserEventService(userEventProjection)

    deleteJournal = false
  }

}

