package biz.shopboard.service

import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import scala.concurrent.duration._
import biz.shopboard.domain._
import model.merchant._
import biz.shopboard.domain.model.common.ProductInfo
import biz.shopboard.domain.model.common.Address
import scala.Some
import concurrent.stm.Ref
import akka.actor.Props
import org.eligosource.eventsourced.core.{ Confirm, DefaultChannelProps, Eventsourced, Receiver }
import biz.shopboard.{ EventsourcingFixtureInMemory, EventsourcingSpecInMemory }
import biz.shopboard.service.ProductSearchServiceSpec.FixtureInMemory
import biz.shopboard.state.TotalEmitter
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import concurrent.Await

@RunWith(classOf[JUnitRunner])
class ProductSearchServiceSpec extends EventsourcingSpecInMemory[FixtureInMemory] {

  val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")

  val details3 = DealDetails("Tesco Deal", null, null, DateTime.parse("11.11.2011", formatter), DateTime.parse("11.12.2012", formatter))
  val details2 = CampaignDetails("Tesco Xmas", "", DateTime.parse("11.11.2011", formatter), DateTime.parse("11.12.2012", formatter))
  val details1 = MerchantDetails("Tesco", new Address)
  val product = ProductInfo("Choco", "some description", "pastery")

  val merchant = Merchant("ptn-id1", 0L, "usr-id1", details1)
  val campaign = DraftCampaign("cpn-id1", 0L, details2)
  val deal = DraftDeal("dl-id1", 0L, details3)

  "A product search service" when {
    "asked to collect and cache product infos" must {
      "succeed by returning upon query cached info if deal product info is updated" in { fixture =>
        import fixture._

        createMerchant(merchantService)
        createCampaign(merchantService)
        createDeal(merchantService)
        updateDealProductInfo(merchantService, deal.version)

        merchantService.getDeal(deal.id).get.details.productInfo.head must equal(product)
        searchService.searchProducts("Choco-not-cached") must be(None)
        searchService.searchProducts() must equal(Some(List(product)))

        searchService.searchProducts("some other name") must equal(None)
        searchService.searchProducts(product.name) must equal(Some(List(product)))
        searchService.searchProducts(product.name.take(1)) must equal(Some(List(product)))
        searchService.searchProducts(product.name.take(2)) must equal(Some(List(product)))
        searchService.searchProducts(product.name.take(3)) must equal(Some(List(product)))
        searchService.searchProducts("", product.description) must equal(Some(List(product)))
        searchService.searchProducts("", "", product.category) must equal(Some(List(product)))
        searchService.searchProducts("", "", "", merchant.id) must equal(Some(List(product)))
        searchService.searchProducts(product.name, product.description, product.category, merchant.id) must equal(Some(List(product)))
        searchService.searchProducts(product.name, product.description, product.category, "some other id") must equal(None)
      }

      "succeed by returning upon query cached info if deal product info is updated and than removed and all information was correctly processed" in { fixture =>
        import fixture._

        createMerchant(merchantService)
        createCampaign(merchantService)
        createDeal(merchantService)
        updateDealProductInfo(merchantService, deal.version)

        merchantService.getDeal(deal.id).get.details.productInfo.head must equal(product)
        searchService.searchProducts("Choco-not-cached") must be(None)
        searchService.searchProducts() must equal(Some(List(product)))

        removeDealProductInfo(merchantService, deal.version + 1)
        searchService.searchProducts() must be(None)
      }
    }
  }

  def createMerchant(implicit service: MerchantService): DomainValidation[Merchant] = {
    Await.result(service.createMerchant(merchant), 5.seconds)
  }
  def createCampaign(implicit service: MerchantService, version: Option[Long] = merchant.versionOption): DomainValidation[Campaign] = {
    Await.result(service.createCampaign(merchant.id, version, campaign.id, details2), 5.seconds)
  }
  def createDeal(implicit service: MerchantService, version: Option[Long] = campaign.versionOption): DomainValidation[Deal] = {
    Await.result(service.createDeal(merchant.id, campaign.id, version, deal.id, details3), 5.seconds)
  }
  def updateDealProductInfo(implicit service: MerchantService, version: Long = 0L): DomainValidation[Deal] = {
    Await.result(service.updateDealProductInfo(merchant.id, deal.id, Some(version), List(product)), 5.seconds)
  }
  def removeDealProductInfo(implicit service: MerchantService, version: Long = 0L): DomainValidation[Deal] = {
    Await.result(service.removeDealProductInfo(merchant.id, deal.id, Some(version), product), 5.seconds)
  }
}

object ProductSearchServiceSpec {

  class FixtureInMemory extends EventsourcingFixtureInMemory[Any] {

    val merchantsRef = Ref(Map.empty[String, Merchant])
    val campaignsRef = Ref(Map.empty[String, Campaign])
    val couponsRef = Ref(Map.empty[String, Deal])
    val dealsRef = Ref(Map.empty[String, Deal])
    val merchantProcessor = extension.processorOf(Props(new MerchantProcessor(merchantsRef, campaignsRef, couponsRef, dealsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = 1
    }))
    val productSearchProjection = extension.processorOf(Props(new ProductSearchProjection with Receiver with Eventsourced {
      val id = 2
    }))

    extension.channelOf(DefaultChannelProps(1, productSearchProjection))

    extension.recover()
    extension.awaitProcessing(Set(1, 2))

    val merchantService = new MerchantService(merchantsRef, campaignsRef, couponsRef, dealsRef, merchantProcessor)
    val searchService = new ProductSearchService(productSearchProjection)
  }
}

