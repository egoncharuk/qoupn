package biz.shopboard.service

import akka.actor._
import org.eligosource.eventsourced.core._
import java.util.concurrent.CountDownLatch

trait CountDownLatchReceiver extends Actor {

  def getCdl() = new CountDownLatch(0)

  abstract override def receive = {
    case msg: Message => {
      try {
        super.receive(msg)
        getCdl().countDown()
      } catch {
        case e: Throwable => {
          msg.confirm(false);
          throw e
        }
      }
    }
    case msg => {
      super.receive(msg)
    }
  }
}
