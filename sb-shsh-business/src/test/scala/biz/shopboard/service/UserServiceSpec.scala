package biz.shopboard.service

import org.eligosource.eventsourced.core._
import concurrent.stm.Ref
import scala.concurrent.duration._
import akka.actor.Props
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import UserProcessor._
import biz.shopboard.state.TotalEmitter
import biz.shopboard.domain.{ VersionMessages, DomainValidation }
import biz.shopboard.{ EventsourcingFixtureInMemory, EventsourcingSpecInMemory }
import biz.shopboard.service.UserServiceSpec.FixtureInMemory
import biz.shopboard.domain.model.common.AdminRole
import biz.shopboard.domain.model.user._
import scalaz.Failure
import scala.Some
import scalaz.Success
import biz.shopboard.domain.model.merchant.Merchant
import concurrent.Await
import java.util.concurrent.{ CountDownLatch, LinkedBlockingQueue }
import java.util.concurrent.TimeUnit
import biz.shopboard.util.Hashing

@RunWith(classOf[JUnitRunner])
class UserServiceSpec extends EventsourcingSpecInMemory[FixtureInMemory] {

  val defaultVersion = 0L
  val details1 = new UserDetails("login", "password", "name", "surname", "email@email.com")
  val details2 = new UserDetails("login", "password", "name2", "surname2", "email2@email.com")
  val details3 = new UserDetails("login", "password", "name2", "surname2", "non-valid-email")

  val shopListDetails1 = new ShoppingListDetails("list 1")
  val shopListDetails2 = new ShoppingListDetails("list 1 updated")

  val user = new User("usr-id1", 0L, details1.encryptPassword)
  val admin = new User("usr-id1", 0L, details1.encryptPassword, List(AdminRole))
  val shopList = new ShoppingList("list-id1", 0L, shopListDetails1)
  val shopListItem1 = new ShoppingListItem("item-id1", "item 1")
  val shopListItem1Upd = new ShoppingListItem("item-id1", "item 1 - updated")
  val shopListItem2 = new ShoppingListItem("item-id2", "item 2")

  val message = VersionMessages.invalidVersionMessage format ("User", user.id, -1L, 0)
  val expectedVersionFailure = Failure(List(message))

  val userVersionFailure = Failure(List(VersionMessages.invalidVersionMessage format ("User", user.id, -1L, 0)))
  val userNotExistsFailure = Failure(List(userIdNotExistMsg format user.id))

  "A user service" when {
    "asked to create a new user" must {
      "return the created user & update in-memory storage & send email with activation link & store sent action hash" in { fixture =>
        import fixture._

        // Given
        userProcessorCdl = new CountDownLatch(2) // we expect 2 messages to be received by UserService

        // When
        createUser(service) must equal(Success(new User(user.id, user.version, user.details)))

        userProcessorCdl.await(3, TimeUnit.SECONDS) // wait while UserProcessor receives message with ActivationActionHash

        // Then
        service.getUser(user.id).get must equal(new User(user.id, user.version, user.details))
        mailService.getSentMailsCount must equal(1) // check email service was invoked to send email
        dequeue(userProcessorQueue)(m => { m.event must be(CreateUser(user.id, user.details, Nil)) })
        dequeue(userProcessorQueue)(m => {
          val userActivationEmailSentMsg: UserActivationEmailSent = m.event.asInstanceOf[UserActivationEmailSent]
          actionHashesRef.single.get.values.toList.contains(userActivationEmailSentMsg.actionHash) must be(true)
        })
      }
    }

    "asked to activate user with not existing action hash" must {
      "return error" in { fixture =>
        import fixture._

        val wrongActivationHash = "111222333444"
        activateUser(service, wrongActivationHash) must equal(Failure(List(s"Link $wrongActivationHash is invalid")))
      }
    }

    "asked to activate user with expired action hash" must {
      "return error & remove action hash" in { fixture =>
        import fixture._

        val expiredActionHash = new ActionHash(Hashing.generateActionHash(), ActionHashType.ACTIVATE_PASSWORD, "wrongUserId", (System.currentTimeMillis() - 1000 * 60 * 60 * 25))
        actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (expiredActionHash.hashKey -> expiredActionHash))

        activateUser(service, expiredActionHash.hashKey) must equal(Failure(List(actionLinkExpiredMsg)))
        actionHashesRef.single.get.keys.isEmpty must be(true)
      }
    }

    "asked to activate not existing user using existing action hash" must {
      "return error about not existing user" in { fixture =>
        import fixture._

        val wrongUserId: String = "wrongUserId"
        val actionHash = ActionHash.create(ActionHashType.ACTIVATE_PASSWORD, wrongUserId)
        actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (actionHash.hashKey -> actionHash))

        activateUser(service, actionHash.hashKey) must equal(Failure(List(s"User $wrongUserId does not exist")))
      }
    }

    "asked to activate user with RESET_PASSWORD action hash" must {
      "return error" in { fixture =>
        import fixture._

        usersRef.single.transform(usersMap => usersMap + (user.id -> user))
        val actionHash = ActionHash.create(ActionHashType.RESET_PASSWORD, user.id)
        actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (actionHash.hashKey -> actionHash))

        activateUser(service, actionHash.hashKey) must equal(Failure(List("Link is of wrong type")))
      }
    }

    "asked to activate existing not active user" must {
      "activate user, remove used action hash from hashes set" in { fixture =>
        import fixture._

        // Given
        val actionHash = ActionHash.create(ActionHashType.ACTIVATE_PASSWORD, user.id)
        usersRef.single.transform(usersMap => usersMap + (user.id -> user))
        actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (actionHash.hashKey -> actionHash))

        userProcessorCdl = new CountDownLatch(1)

        // When
        activateUser(service, actionHash.hashKey) must equal(Success(new User(user.id, user.version + 1, user.details.copy(status = UserStatus.ACTIVE))))

        userProcessorCdl.await(5, TimeUnit.SECONDS)

        // Then
        actionHashesRef.single.get.keys.isEmpty must be(true)
        dequeue(userProcessorQueue)(m => { m.event must be(ActivateUserPassword(actionHash.hashKey)) })
      }
    }

    "asked to reset password for existing user" must {
      "send reset password link to user email & store sent action hash code" in { fixture =>
        import fixture._

        // Given
        val updatedDetails: UserDetails = user.details.copy(status = UserStatus.ACTIVE)
        val activeUser = user.copy(details = updatedDetails, version = user.version + 1)
        usersRef.single.transform(usersMap => usersMap + (activeUser.id -> activeUser))

        userProcessorCdl = new CountDownLatch(2)

        // When
        sendUserPasswordChangeLink(service, user.details.email) must equal(Success(new User(user.id, user.version + 1, user.details.copy(status = UserStatus.ACTIVE))))

        userProcessorCdl.await(3, TimeUnit.SECONDS)

        // Then
        dequeue(userProcessorQueue)(m => { m.event must be(SendChangeUserPasswordLink(user.details.email)) })
        dequeue(userProcessorQueue)(m => {
          val userPasswordChangeEmailSentMsg: UserPasswordChangeEmailSent = m.event.asInstanceOf[UserPasswordChangeEmailSent]
          actionHashesRef.single.get.values.toList.contains(userPasswordChangeEmailSentMsg.actionHash) must be(true)
        })
      }
    }

    "asked to reset password for existing user with not-existing action hash" must {
      "return error" in { fixture =>
        import fixture._

        val wrongActivationHash = "111222333444"
        resetPassword(service, wrongActivationHash, "newPassword") must equal(Failure(List(s"Link $wrongActivationHash is invalid")))
      }
    }

    "asked to reset password with expired action hash" must {
      "return error & remove action hash" in { fixture =>
        import fixture._

        val expiredActionHash = new ActionHash(Hashing.generateActionHash(), ActionHashType.RESET_PASSWORD, "wrongUserId", (System.currentTimeMillis() - 1000 * 60 * 60 * 25))
        actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (expiredActionHash.hashKey -> expiredActionHash))

        resetPassword(service, expiredActionHash.hashKey, "newPassword") must equal(Failure(List(actionLinkExpiredMsg)))
        actionHashesRef.single.get.keys.isEmpty must be(true)
      }
    }

    "asked to reset password with ACTIVATION action hash" must {
      "return error" in { fixture =>
        import fixture._

        val wrongUserId: String = "wrongUserId"
        val actionHash = ActionHash.create(ActionHashType.ACTIVATE_PASSWORD, wrongUserId)
        actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (actionHash.hashKey -> actionHash))

        resetPassword(service, actionHash.hashKey, "newPassword") must equal(Failure(List("Link is of wrong type")))
      }
    }

    "asked to reset password for not-existing user with existing action hash" must {
      "return error" in { fixture =>
        import fixture._

        val wrongUserId: String = "wrongUserId"
        val actionHash = ActionHash.create(ActionHashType.RESET_PASSWORD, wrongUserId)
        actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (actionHash.hashKey -> actionHash))

        resetPassword(service, actionHash.hashKey, "newPassword") must equal(Failure(List(s"User $wrongUserId does not exist")))
      }
    }

    "asked to reset password for not-existing user" must {
      "return error" in { fixture =>
        import fixture._

        // Given
        val updatedDetails: UserDetails = user.details.copy(status = UserStatus.ACTIVE)
        val activeUser = user.copy(details = updatedDetails, version = user.version + 1)
        usersRef.single.transform(usersMap => usersMap + (activeUser.id -> activeUser))

        userProcessorCdl = new CountDownLatch(2)

        // When
        sendUserPasswordChangeLink(service, user.details.email) must equal(Success(new User(user.id, user.version + 1, user.details.copy(status = UserStatus.ACTIVE))))

        userProcessorCdl.await(3, TimeUnit.SECONDS)

        // Then
        dequeue(userProcessorQueue)(m => { m.event must be(SendChangeUserPasswordLink(user.details.email)) })
        dequeue(userProcessorQueue)(m => {
          val userPasswordChangeEmailSentMsg: UserPasswordChangeEmailSent = m.event.asInstanceOf[UserPasswordChangeEmailSent]
          actionHashesRef.single.get.values.toList.contains(userPasswordChangeEmailSentMsg.actionHash) must be(true)
        })
      }
    }

    "asked to create a new user with admin role" must {
      "return the created user & update in-memory storage" in { fixture =>
        import fixture._

        // Given
        userProcessorCdl = new CountDownLatch(2) // we expect 2 messages to be received by UserService

        // When
        createAdmin(service) must equal(Success(new User(admin.id, admin.version, admin.details, admin.roles)))

        userProcessorCdl.await(3, TimeUnit.SECONDS) // wait while UserProcessor receives message with ActivationActionHash

        // Then
        service.getUser(admin.id).get must equal(new User(admin.id, admin.version, admin.details, admin.roles))
        mailService.getSentMailsCount must equal(1) // check email service was invoked to send email
        dequeue(userProcessorQueue)(m => { m.event must be(CreateUser(admin.id, admin.details, admin.roles)) })
        dequeue(userProcessorQueue)(m => {
          val userActivationEmailSentMsg: UserActivationEmailSent = m.event.asInstanceOf[UserActivationEmailSent]
          actionHashesRef.single.get.values.toList.contains(userActivationEmailSentMsg.actionHash) must be(true)
        })
      }
    }

    "asked to create a new user with existing id" must {
      "fail & should not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createUser(service) must equal(Failure(List(userWithIdAlreadyExistMsg format (user.id))))
        service.getUsers.size must equal(1)
      }
    }

    "asked to create a new user with non-valid email" must {
      "fail & should not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service, details3) must equal(Failure(List(invalid_email)))
        service.getUsers.size must equal(0)
      }
    }

    "asked to update existing user details" must {
      "return the updated user & update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        updateUser(service, user.version) must equal(Success(new User(user.id, 1L, details2.encryptPassword)))
        service.getUsers.size must equal(1)
        service.getUser(user.id).get.details must equal(details2.encryptPassword)
      }

      "fail if user version is old & not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)

        // TODO: enable when needed
        //updateUser(service) must equal(userVersionFailure)

        service.getUsers.size must equal(1)
        service.getUser(user.id).get.version must equal(user.version)
      }
    }

    "asked to remove existing user" must {
      "delete user & update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        removeUser(service, user.version) must equal(Success(new User(user.id, user.version, user.details)))
        service.getUsers.size must equal(0)
      }
      "fail if version is old & not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)

        // TODO: enable when needed
        //updateUser(service) must equal(expectedVersionFailure)

        service.getUsers.size must equal(1)
        service.getUser(user.id).get.version must equal(user.version)
      }
    }

    "asked to remove non-existing user" must {
      "fail & should not update in-memory storage" in { fixture =>
        import fixture._

        removeUser(service, user.version) must equal(userNotExistsFailure)
        service.getUsers.size must equal(0)
      }
    }
  }

  "A user (shopping list) service" when {

    import ShoppingList._

    val defaultVersionFailure1 = Failure(List(VersionMessages.invalidVersionMessage format ("ShoppingList", shopList.id, -1L, 1)))
    val defaultVersionFailure = Failure(List(VersionMessages.invalidVersionMessage format ("ShoppingList", shopList.id, -1L, 0)))
    val shopListIdFailure = Failure(List(shoppingListWithIdNotExist format "wrong-shopList-id"))
    val shopListItemIdFailure = Failure(List(itemIdErrorMsg format shopListItem2.id))

    "asked to create a new shopping list" must {
      "create shopping list & update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service) must equal(Success(shopList))

        service.getShoppingLists.size must equal(1)
        service.getShoppingList(shopList.id) match {
          case None => fail
          case Some(createdList) => createdList must equal(shopList)
        }
      }
      "fail if user id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createShopList(service) must equal(userNotExistsFailure)

        service.getShoppingLists.size must equal(0)
      }
      "fail if user version is old & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)

        // TODO: enable when needed
        //createShopList(service, Some(-1L)) must equal(userVersionFailure)

        service.getShoppingLists.size must equal(0)
      }
    }
    "asked to update shopping list" must {
      "update shopping list & update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        updateShopList(service, shopList.version) must equal(Success(new ShoppingList("list-id1", 1L, shopListDetails2)))

        service.getShoppingLists.size must equal(1)
        service.getShoppingList(shopList.id) match {
          case None => fail
          case Some(updatedShopList) => updatedShopList.version must equal(1L)
        }
      }
      "fail if shopList id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        Await.result(service.updateShoppingListDetails(user.id, "wrong-shopList-id", shopList.versionOption, shopListDetails2), 5.seconds) must equal(shopListIdFailure)

        failIfdefaultVersionUpdated(service)
      }
      "fail if shopList version is old & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)

        // TODO: enable when needed
        //updateShopList(service, -1L) must equal(defaultVersionFailure)

        failIfdefaultVersionUpdated(service)
      }
    }
    "asked to remove shopping list" must {
      "remove shopping list & update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        removeShopList(service, shopList.version) must equal(Success(shopList))

        service.getShoppingLists.size must equal(0)
      }
      "fail if shopList id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        Await.result(service.removeShoppingList(user.id, "wrong-shopList-id", shopList.versionOption), 5.seconds) must equal(shopListIdFailure)

        failIfdefaultVersionUpdated(service)
      }
      "fail if shopList version is old & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)

        // TODO: enable when needed
        //removeShopList(service, -1L) must equal(defaultVersionFailure)

        failIfdefaultVersionUpdated(service)
      }
    }

    "asked to create a new shopList item" must {
      "create shopList item & update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service) must equal(shopList.addItem(shopListItem1))

        service.getShoppingList(shopList.id) match {
          case None => fail
          case Some(updatedShopList) => {
            updatedShopList.version must equal(1L)
            updatedShopList.items.size must equal(1L)
            updatedShopList.items.get(shopListItem1.id).get must equal(shopListItem1)
          }
        }

      }
      "fail if shopList id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service, shopListId = "wrong-shopList-id") must equal(shopListIdFailure)

        shopListIsNotUpdated(service)
      }
      "fail if shopList version is old & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)

        // TODO: enable when needed
        //addShopListItem(service, versionOpt = Some(-1)) must equal(defaultVersionFailure)

        shopListIsNotUpdated(service)
      }
    }

    "asked to update shopList item" must {
      "update shopList item & update in-memory storage" in { fixture =>
        import fixture._

        val expectedSuccess: DomainValidation[ShoppingList] = shopList.addItem(shopListItem1).toOption.get.updateItem(shopListItem1Upd)

        createUser(service)
        createShopList(service)
        addShopListItem(service)
        updateShopListItem(service, shopList.id, defaultVersion = 1L) must equal(expectedSuccess)

        service.getShoppingList(shopList.id) match {
          case None => fail
          case Some(updatedShopList) => {
            updatedShopList.version must equal(2L)
            updatedShopList.items.size must equal(1L)
            updatedShopList.items.head._2 must equal(shopListItem1Upd)
          }
        }
      }
      "fail if shopList id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service)
        updateShopListItem(service, "wrong-shopList-id", defaultVersion = 1L) must equal(shopListIdFailure)

        shopListItemIsNotUpdated(service)
      }
      "fail if shopList id is Ok, but shopListItem.id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service)
        updateShopListItem(service, shopList.id, defaultVersion = 1L, shopListItem2) must equal(shopListItemIdFailure)

        shopListItemIsNotUpdated(service)
      }
      "fail if shopList version is old & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service)

        // TODO: enable when needed
        //updateShopListItem(service, shopList.id, defaultVersion = -1L) must equal(defaultVersionFailure1)

        shopListItemIsNotUpdated(service)
      }
    }
    "asked to delete shopList item" must {
      "delete shopList item & update in-memory storage" in { fixture =>
        import fixture._

        val expectedSuccess: DomainValidation[ShoppingList] = shopList.addItem(shopListItem1).toOption.get.removeItem(shopListItem1.id)

        createUser(service)
        createShopList(service)
        addShopListItem(service)
        removeShopListItem(service, shopList.id, defaultVersion = 1L) must equal(expectedSuccess)

        service.getShoppingList(shopList.id) match {
          case None => fail
          case Some(updatedShopList) => {
            updatedShopList.version must equal(2L)
            updatedShopList.items.isEmpty must equal(true)
          }
        }
      }
      "fail if shopList id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service)
        removeShopListItem(service, "wrong-shopList-id", defaultVersion = 1L) must equal(shopListIdFailure)

        shopListItemIsNotUpdated(service)
      }
      "fail if shopList id is Ok, but shopListItem.id is wrong & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service)
        removeShopListItem(service, shopList.id, defaultVersion = 1L, shopListItem2.id) must equal(shopListItemIdFailure)

        shopListItemIsNotUpdated(service)
      }
      "fail if shopList version is old & do not update in-memory storage" in { fixture =>
        import fixture._

        createUser(service)
        createShopList(service)
        addShopListItem(service)

        // TODO: enable when needed
        //removeShopListItem(service, shopList.id, defaultVersion = -1L) must equal(defaultVersionFailure1)

        shopListItemIsNotUpdated(service)
      }
    }
  }

  def createUser(implicit service: UserService, details: UserDetails = details1): DomainValidation[User] = {
    Await.result(service.createUser(user.id, details), 5.seconds)
  }

  def activateUser(implicit service: UserService, hashKey: String): DomainValidation[User] = {
    Await.result(service.activateUser(hashKey), 5.seconds)
  }

  def resetPassword(implicit service: UserService, hashKey: String, newPassword: String): DomainValidation[User] = {
    Await.result(service.changeUserPassword(hashKey, newPassword), 5.seconds)
  }

  def sendUserPasswordChangeLink(implicit service: UserService, email: String = details1.email): DomainValidation[User] = {
    Await.result(service.sendChangeUserPasswordLink(email), 5.seconds)
  }

  def createAdmin(implicit service: UserService): DomainValidation[User] = {
    Await.result(service.createUser(admin.id, details1, admin.roles), 5.seconds)
  }

  def updateUser(implicit service: UserService, version: Long = 0L): DomainValidation[User] = {
    Await.result(service.updateUserDetails(user.id, Some(version), details2), 5.seconds)
  }

  def removeUser(implicit service: UserService, version: Long = 0L): DomainValidation[User] = {
    Await.result(service.removeUser(user.id, Some(version)), 5.seconds)
  }

  def createShopList(implicit service: UserService, versionOpt: Option[Long] = shopList.versionOption): DomainValidation[ShoppingList] = {
    Await.result(service.createShoppingList(user.id, versionOpt, shopList.id, shopList.details), 5.seconds)
  }

  def updateShopList(implicit service: UserService, version: Long = 0L): DomainValidation[ShoppingList] = {
    Await.result(service.updateShoppingListDetails(user.id, shopList.id, Some(version), shopListDetails2), 5.seconds)
  }

  def failIfdefaultVersionUpdated(implicit service: UserService) = {
    service.getShoppingList(shopList.id) match {
      case None => fail
      case Some(list) => list.version must equal(0L)
    }
  }

  def removeShopList(implicit service: UserService, version: Long = 0L): DomainValidation[ShoppingList] = {
    Await.result(service.removeShoppingList(user.id, shopList.id, Some(version)), 5.seconds)
  }

  def addShopListItem(implicit service: UserService, shopListId: String = shopList.id, versionOpt: Option[Long] = shopList.versionOption): DomainValidation[ShoppingList] = {
    Await.result(service.addShoppingListItem(user.id, shopListId, versionOpt, shopListItem1), 5.seconds)
  }

  def shopListIsNotUpdated(implicit service: UserService) = {
    service.getShoppingList(shopList.id) match {
      case None => fail
      case Some(updatedShopList) => {
        updatedShopList.version must equal(shopList.version)
        updatedShopList.items.size must equal(0L)
      }
    }
  }

  def shopListItemIsNotUpdated(implicit service: UserService) = {
    service.getShoppingList(shopList.id) match {
      case None => fail
      case Some(updatedShopList) => {
        updatedShopList.version must equal(1L)
        updatedShopList.items.size must equal(1L)
        updatedShopList.items.get(shopListItem1.id) match {
          case None => fail
          case Some(item) => item.name must equal(shopListItem1.name)
        }
      }
    }
  }

  def updateShopListItem(implicit service: UserService, shopListId: String = "wrong-shopList-id", defaultVersion: Long = 0L, shopListItem: ShoppingListItem = shopListItem1Upd) = {
    Await.result(service.updateShoppingListItem(user.id, shopListId, Some(defaultVersion), shopListItem), 5.seconds)
  }

  def removeShopListItem(implicit service: UserService, shopListId: String = "wrong-shopList-id", defaultVersion: Long = 0L, shopListItemId: String = shopListItem1.id) = {
    Await.result(service.removeShoppingListItem(user.id, shopListId, Some(defaultVersion), shopListItemId), 5.seconds)
  }
}

object UserServiceSpec {

  class FixtureInMemory extends EventsourcingFixtureInMemory[Any] {

    val usersRef = Ref(Map.empty[String, User])
    val actionHashesRef = Ref(Map.empty[String, ActionHash])
    val merchantsRef = Ref(Map.empty[String, Merchant])
    val shoppingListsRef = Ref(Map.empty[String, ShoppingList])
    val shoppingListsToDealsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val shoppingListsToCouponsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val mailerChannelId = 999; //mainProcessors.current
    val mailService = new MailServiceMock

    var userProcessorCdl = new CountDownLatch(0)
    var userProcessorQueue = new LinkedBlockingQueue[Message]

    val userProcessor = extension.processorOf(Props(
      new UserProcessor(usersRef, actionHashesRef, merchantsRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef) with TotalEmitter with Confirm with MsgQueue with CountDownLatchReceiver with Eventsourced {

        val id = 1

        override def getQueue() = userProcessorQueue
        override def getCdl(): CountDownLatch = userProcessorCdl
      }))

    val mailerActorRef = system.actorOf(Props(new MailerGateway(mailService, userProcessor) with Receiver with Confirm))
    extension.channelOf(ReliableChannelProps(mailerChannelId, mailerActorRef).withName("mailer"))

    extension.recover()
    // wait for processor 1 to complete processing of replayed event messages
    // (ensures that recovery of externally visible state maintained by
    //  invoicesRef is completed when awaitProcessorCompletion returns)
    extension.awaitProcessing(Set(1))

    val service = new UserService(usersRef, actionHashesRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef, userProcessor)

    def deq(queue: LinkedBlockingQueue[Message]): Message = {
      queue.poll(timeout.duration.toMillis, TimeUnit.MILLISECONDS)
    }

    def dequeue(queue: LinkedBlockingQueue[Message])(p: Message => Unit) = {
      p(deq(queue))
    }
  }

}
