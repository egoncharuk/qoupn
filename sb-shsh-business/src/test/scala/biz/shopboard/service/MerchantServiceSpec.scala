package biz.shopboard.service

import org.scalatest.junit.JUnitRunner
import org.junit.runner.RunWith
import scala.concurrent.duration._
import akka.actor.Props
import org.eligosource.eventsourced.core.{ Confirm, Eventsourced }
import biz.shopboard.state.TotalEmitter
import concurrent.stm.Ref
import MerchantProcessor._
import biz.shopboard.{ EventsourcingFixtureInMemory, EventsourcingSpecInMemory }
import biz.shopboard.service.MerchantServiceSpec.FixtureInMemory
import biz.shopboard.domain.model.merchant._
import biz.shopboard.domain.model.common.ProductInfo
import scalaz.Failure
import scala.Some
import scalaz.Success
import biz.shopboard.domain.model.common.Address
import biz.shopboard.domain.{ VersionMessages, DomainValidation }
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import concurrent.Await

@RunWith(classOf[JUnitRunner])
class MerchantServiceSpec extends EventsourcingSpecInMemory[FixtureInMemory] {

  val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")

  val details3 = DealDetails("Tesco Deal", null, null, DateTime.parse("11.11.2011", formatter), DateTime.parse("11.12.2012", formatter))
  val details2 = CampaignDetails("Tesco Xmas", "", DateTime.parse("11.11.2011", formatter), DateTime.parse("11.12.2012", formatter))
  val details1 = MerchantDetails("Tesco", new Address)
  val product = ProductInfo("Choco", "", "pastery")

  val merchant = Merchant("ptn-id1", 0L, "usr-id1", details1)
  val campaign = DraftCampaign("cpn-id1", 0L, details2)
  val deal = DraftDeal("dl-id1", 0L, details3)

  val merchantVersionFailure = Failure(List(VersionMessages.invalidVersionMessage format ("Merchant", merchant.id, -1L, 0)))
  val campaignVersionFailure = Failure(List(VersionMessages.invalidVersionMessage format ("DraftCampaign", campaign.id, -1L, 0)))
  val dealVersionFailure = Failure(List(VersionMessages.invalidVersionMessage format ("DraftDeal", deal.id, -1L, 0)))

  "A merchant service" when {
    "asked to create a new merchant" must {
      "return the created merchant & update in-memory storage" in { fixture =>
        import fixture._

        createMerchant(merchantService) must equal(Success(merchant))

        merchantService.getMerchants.size must equal(1)
        merchantService.getMerchant(merchant.id) match {
          case None => fail()
          case Some(createdMerchant) => createdMerchant must equal(merchant)
        }
      }
    }

    "asked to create a new merchant with existing id" must {
      "fail & should not update in-memory storage" in { fixture =>
        import fixture._

        createMerchant(merchantService)
        createMerchant(merchantService) must equal(Failure(List(merchantWithIdAlreadyExistMsg format (merchant.id))))
        merchantService.getMerchants.size must equal(1)
      }
    }

    "fail if merchant version is old & not update in-memory storage" in { fixture =>
      import fixture._

      createMerchant(merchantService)

      // TODO: enable when needed
      //updateMerchant(merchantService) must equal(merchantVersionFailure)

      merchantService.getMerchants.size must equal(1)
      merchantService.getMerchant(merchant.id).get.version must equal(merchant.version)
    }
  }

  "A campaign service" when {
    "asked to create a new campaign" must {
      "return the created campaign & update in-memory storage" in { fixture =>
        import fixture._

        createMerchant(merchantService)
        createCampaign(merchantService) must equal(Success(campaign))

        merchantService.getCampaigns.size must equal(1)
        merchantService.getCampaign(campaign.id) match {
          case None => fail()
          case Some(createdCampaign) => {
            createdCampaign must equal(campaign)
            merchantService.getMerchant(merchant.id).get.campaignIds must contain(createdCampaign.id)
          }
        }
      }
    }

    "asked to create a new campign with existing id" must {
      "fail & should not update in-memory storage" in { fixture =>
        import fixture._

        createMerchant(merchantService)
        createCampaign(merchantService)
        createCampaign(merchantService, Some(merchant.version + 1)) must equal(Failure(List(campaignIdAlreadyExistMsg format (campaign.id))))
        merchantService.getCampaigns.size must equal(1)
      }
    }

    "fail if campaign version is old & not update in-memory storage" in { fixture =>
      import fixture._

      createMerchant(merchantService)
      createCampaign(merchantService)

      // TODO: enable when needed
      //updateCampaign(merchantService) must equal(campaignVersionFailure)

      merchantService.getCampaigns.size must equal(1)
      merchantService.getCampaign(campaign.id).get.version must equal(campaign.version)
    }
  }

  "A deal service" when {
    "asked to create a new deal" must {
      "return the created deal & update in-memory storage" in { fixture =>
        import fixture._

        createMerchant(merchantService)
        createCampaign(merchantService)
        createDeal(merchantService) must equal(Success(deal))

        merchantService.getDeals.size must equal(1)
        merchantService.getDeal(deal.id) match {
          case None => fail()
          case Some(createdDeal) => {
            createdDeal must equal(deal)
            merchantService.getCampaign(campaign.id).get.asInstanceOf[DraftCampaign].dealIds must contain(createdDeal.id)
          }
        }
      }
    }

    "asked to create a new deal with existing id" must {
      "fail & should not update in-memory storage" in { fixture =>
        import fixture._

        createMerchant(merchantService)
        createCampaign(merchantService)
        createDeal(merchantService)
        createDeal(merchantService, Some(campaign.version + 1)) must equal(Failure(List(dealIdAlreadyExistMsg format (deal.id))))
        merchantService.getDeals.size must equal(1)
      }
    }

    "fail if deal version is old & not update in-memory storage" in { fixture =>
      import fixture._

      createMerchant(merchantService)
      createCampaign(merchantService)
      createDeal(merchantService)

      // TODO: enable when needed
      //updateDeal(merchantService) must equal(dealVersionFailure)

      merchantService.getDeals.size must equal(1)
      merchantService.getDeal(deal.id).get.version must equal(deal.version)
    }

    "succeed if deal version is OK & update in-memory storage with the new ProductInfo" in { fixture =>
      import fixture._

      createMerchant(merchantService)
      createCampaign(merchantService)
      createDeal(merchantService)
      updateDealProductInfo(merchantService, deal.version)
      merchantService.getDeal(deal.id).get.details.productInfo.head must equal(product)
    }

    "succeed if deal version is OK & update in-memory storage with the new ProductInfo #2" in { fixture =>
      import fixture._

      createMerchant(merchantService)
      createCampaign(merchantService)
      createDeal(merchantService)
      addDealProductInfo(merchantService, deal.version)
      merchantService.getDeal(deal.id).get.details.productInfo.head must equal(product)
    }
  }

  def createMerchant(implicit service: MerchantService): DomainValidation[Merchant] = {
    Await.result(service.createMerchant(merchant), 5.seconds)
  }
  def updateMerchant(implicit service: MerchantService, version: Long = 0L): DomainValidation[Merchant] = {
    Await.result(service.updateMerchantDetails(merchant.id, Some(version), details1), 5.seconds)
  }

  def createCampaign(implicit service: MerchantService, version: Option[Long] = merchant.versionOption): DomainValidation[Campaign] = {
    Await.result(service.createCampaign(merchant.id, version, campaign.id, details2), 5.seconds)
  }
  def updateCampaign(implicit service: MerchantService, version: Long = 0L): DomainValidation[Campaign] = {
    Await.result(service.updateCampaignDetails(merchant.id, campaign.id, Some(version), details2), 5.seconds)
  }

  def createDeal(implicit service: MerchantService, version: Option[Long] = campaign.versionOption): DomainValidation[Deal] = {
    Await.result(service.createDeal(merchant.id, campaign.id, version, deal.id, details3), 5.seconds)
  }
  def updateDeal(implicit service: MerchantService, version: Long = 0L): DomainValidation[Deal] = {
    Await.result(service.updateDealDetails(merchant.id, campaign.id, deal.id, Some(version), details3), 5.seconds)
  }
  def updateDealProductInfo(implicit service: MerchantService, version: Long = 0L): DomainValidation[Deal] = {
    Await.result(service.updateDealProductInfo(merchant.id, deal.id, Some(version), List(product)), 5.seconds)
  }
  def addDealProductInfo(implicit service: MerchantService, version: Long = 0L): DomainValidation[Deal] = {
    Await.result(service.addDealProductInfo(merchant.id, deal.id, Some(version), product), 5.seconds)
  }

}

object MerchantServiceSpec {

  class FixtureInMemory extends EventsourcingFixtureInMemory[Any] {

    val merchantsRef = Ref(Map.empty[String, Merchant])
    val campaignsRef = Ref(Map.empty[String, Campaign])
    val couponsRef = Ref(Map.empty[String, Deal])
    val dealsRef = Ref(Map.empty[String, Deal])
    val merchantProcessor = extension.processorOf(Props(new MerchantProcessor(merchantsRef, campaignsRef, couponsRef, dealsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = 1
    }))
    extension.recover()
    extension.awaitProcessing(Set(1))

    val merchantService = new MerchantService(merchantsRef, campaignsRef, couponsRef, dealsRef, merchantProcessor)
  }
}

