package biz.shopboard.service

import akka.actor.{ActorRef, Props, ActorSystem}
import akka.testkit.{TestProbe, TestKit, ImplicitSender}
import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import org.scalatest.BeforeAndAfterAll
import org.mockito.Mockito
import org.eligosource.eventsourced.core.{Message, Confirm, Receiver}

import biz.shopboard.domain._
import model.user.{ActionHashType, ActionHash, User, UserDetails}

class MailerGatewaySpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender
    with WordSpec with MustMatchers with BeforeAndAfterAll {

  val details1 = new UserDetails("login", "password", "name", "surname", "email@email.com")

  val user = new User("usr-id1", 0L, details1)

  def this() = this(ActorSystem("MailerGatewaySpec"))

  override def afterAll {
    system.shutdown()
  }

  val mockMailService: MailService = Mockito.mock(classOf[MailService])
  val userProcessorProbe: TestProbe = TestProbe.apply()
  val userProcessorMock: ActorRef = userProcessorProbe.ref

  "A MailerGateway actor" when {
    "asked to activate password" must {
      "call mailerService and return UserActivationEmailSent with appropriate actionHash" in {

        val activateActionHash: ActionHash = ActionHash.create(ActionHashType.ACTIVATE_PASSWORD, user.id)

        val mailerActorRef = system.actorOf(Props(
          new MailerGateway(mockMailService, userProcessorMock, "http://localhost/", "") with Receiver with Confirm {
            override def generateActionHash(id: String, actionHashType: ActionHashType) = activateActionHash
          }),
          "mailerActor1")

        // WHEN

        mailerActorRef ! UserActivationEmailRequested(user.id, user.details.firstName, user.details.lastName, user.details.email)

        // THEN

        userProcessorProbe expectMsg (Message(UserActivationEmailSent(activateActionHash)))
      }
    }

    "asked to reset password" must {
      "call mailerService and return UserPasswordChangeEmailSent with appropriate actionHash" in {
        val resetActionHash: ActionHash = ActionHash.create(ActionHashType.RESET_PASSWORD, user.id)

        val mailerActorRef = system.actorOf(Props(
          new MailerGateway(mockMailService, userProcessorMock, "http://localhost/", "") with Receiver with Confirm {
            override def generateActionHash(id: String, actionHashType: ActionHashType) = resetActionHash
          }),
          "mailerActor2")

        // WHEN

        mailerActorRef ! UserPasswordChangeEmailRequested(user.id, user.details.firstName, user.details.lastName, user.details.email)

        // THEN

        userProcessorProbe expectMsg (Message(UserPasswordChangeEmailSent(resetActionHash)))
      }
    }
  }

}
