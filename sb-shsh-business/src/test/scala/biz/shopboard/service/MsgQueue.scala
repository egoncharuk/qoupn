package biz.shopboard.service

import akka.actor._
import org.eligosource.eventsourced.core._
import java.util.concurrent.LinkedBlockingQueue

trait MsgQueue extends Actor {

  def getQueue() = new LinkedBlockingQueue[Message]

  abstract override def receive = {
    case msg: Message => {
      try {
        super.receive(msg)
        getQueue.add(msg)
        //        println(msg.toString)
      } catch {
        case e: Throwable => {
          msg.confirm(false);
          throw e
        }
      }
    }
    case msg => {
      super.receive(msg)
    }
  }
}
