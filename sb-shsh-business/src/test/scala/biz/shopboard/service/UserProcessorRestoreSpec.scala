package biz.shopboard.service

import biz.shopboard.{ EventsourcingSpecWithMongodb, EventsourcingMongodbFixture }
import biz.shopboard.service.UserProcessorRestoreSpec.Fixture
import biz.shopboard.domain.model.merchant.{ Deal, Campaign }
import org.joda.time.DateTime
import org.eligosource.eventsourced.core._
import concurrent.stm.Ref
import akka.actor.Props
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import biz.shopboard.state.TotalEmitter
import biz.shopboard.domain.model.user._
import biz.shopboard.domain.model.merchant.Merchant
import biz.shopboard.ConfigContext
import scala.concurrent.duration._
import biz.shopboard.domain._
import model.user.UserDetails
import concurrent.Await
import java.util.concurrent.{ LinkedBlockingQueue, TimeUnit, CountDownLatch }
import scalaz.Success
import biz.shopboard.util.Sequenator

@RunWith(classOf[JUnitRunner])
class UserProcessorRestoreSpec extends EventsourcingSpecWithMongodb[Fixture] {

  val details1 = new UserDetails("login", "password", "name", "surname", "email@email.com")
  val shopListDetails1 = new ShoppingListDetails("list 1")
  val user = new User("usr-id1", 0L, details1.encryptPassword)

  "User processor with created & activated user" when {
    "restarted" must {
      "restore state with created and active user" in { fixture =>
        import fixture._
        /////////////////////////////////////////////////////////////
        // Create User
        userProcessorCdl = new CountDownLatch(2) // we expect 2 messages to be received by UserService
        createUser(service) must equal(Success(new User(user.id, user.version, user.details)))
        userProcessorCdl.await(3, TimeUnit.SECONDS) // wait while UserProcessor receives message with ActivationActionHash

        dequeue(userProcessorQueue)(m => { m.event must be(CreateUser(user.id, user.details, Nil)) })
        dequeue(userProcessorQueue)(m => {
          val userActivationEmailSentMsg: UserActivationEmailSent = m.event.asInstanceOf[UserActivationEmailSent]
          actionHashesRef.single.get.values.toList.contains(userActivationEmailSentMsg.actionHash) must be(true)
        })

        /////////////////////////////////////////////////////////////
        // Activate User
        val actionHash: ActionHash = actionHashesRef.single.get.values.head

        userProcessorCdl = new CountDownLatch(1)
        activateUser(service, actionHash.hashKey) must equal(Success(new User(user.id, user.version + 1, user.details.copy(status = UserStatus.ACTIVE))))
        userProcessorCdl.await(5, TimeUnit.SECONDS)

        actionHashesRef.single.get.keys.isEmpty must be(true)
        dequeue(userProcessorQueue)(m => { m.event must be(ActivateUserPassword(actionHash.hashKey)) })

        /////////////////////////////////////////////////////////////
        // Clear models & restart to check recreated state (activated users)

        usersRef.single.transform(map => Map.empty[String, User])
        actionHashesRef.single.transform(map => Map.empty[String, ActionHash])
        userProcessorQueue = new LinkedBlockingQueue[Message]

        // replay all events /messages
        extension.recover()
        extension.awaitProcessing(mainProcessors.ids.toSet)

        dequeue(userProcessorQueue)(m => { m.event must be(CreateUser(user.id, user.details, Nil)) })
        dequeue(userProcessorQueue)(m => { m.event.asInstanceOf[UserActivationEmailSent] })
        dequeue(userProcessorQueue)(m => { m.event must be(ActivateUserPassword(actionHash.hashKey)) })
      }
    }
  }

  def createUser(implicit service: UserService, details: UserDetails = details1): DomainValidation[User] = {
    Await.result(service.createUser(user.id, details), 5.seconds)
  }

  def activateUser(implicit service: UserService, hashKey: String): DomainValidation[User] = {
    Await.result(service.activateUser(hashKey), 5.seconds)
  }
}

object UserProcessorRestoreSpec {

  class Fixture extends EventsourcingMongodbFixture[Any] {

    val mainProcessors: Sequenator = 1
    val channels: Sequenator = 1

    val mailService = new MailServiceMock

    val usersRef = Ref(Map.empty[String, User])
    val actionHashesRef = Ref(Map.empty[String, ActionHash])
    val merchantsRef = Ref(Map.empty[String, Merchant])
    val shoppingListsRef = Ref(Map.empty[String, ShoppingList])
    val shoppingListsToDealsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val shoppingListsToCouponsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val checkoutRef = Ref(Map.empty[String, (String, String, DateTime)])
    val campaignsRef = Ref(Map.empty[String, Campaign])
    val couponsRef = Ref(Map.empty[String, Deal])
    val dealsRef = Ref(Map.empty[String, Deal])

    var userProcessorCdl = new CountDownLatch(0)
    var userProcessorQueue = new LinkedBlockingQueue[Message]

    val userProcessor = extension.processorOf(Props(
      new UserProcessor(usersRef, actionHashesRef, merchantsRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef) with TotalEmitter with Confirm with MsgQueue with CountDownLatchReceiver with Eventsourced {

        val id = mainProcessors.current

        override def getQueue() = userProcessorQueue
        override def getCdl(): CountDownLatch = userProcessorCdl
      }))
    val mailerActorRef = system.actorOf(Props(new MailerGateway(mailService, userProcessor, ConfigContext.getEmailActionUrl) with Receiver with Confirm))
    extension.channelOf(ReliableChannelProps(channels.next, mailerActorRef).withName("mailer"))

    extension.recover(mainProcessors.ids.map(ReplayParams(_)), 5 minutes)
    extension.awaitProcessing(mainProcessors.ids.toSet)

    val service = new UserService(usersRef, actionHashesRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef, userProcessor)

    def deq(queue: LinkedBlockingQueue[Message]): Message = {
      queue.poll(timeout.duration.toMillis, TimeUnit.MILLISECONDS)
    }

    def dequeue(queue: LinkedBlockingQueue[Message])(p: Message => Unit) = {
      p(deq(queue))
    }

  }
}
