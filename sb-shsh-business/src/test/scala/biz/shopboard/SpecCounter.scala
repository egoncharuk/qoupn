package biz.shopboard

import biz.shopboard.util.Sequenator

object SpecCounter {
  private val seq = new Sequenator(0)
  def get = this.synchronized {
    seq.next
  }
}
