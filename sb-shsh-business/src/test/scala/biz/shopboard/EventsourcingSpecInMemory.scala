package biz.shopboard

import java.util.concurrent.{ LinkedBlockingQueue, TimeUnit }

import akka.actor._
import akka.util.Timeout
import scala.reflect.ClassTag

import org.scalatest.matchers.MustMatchers

import org.eligosource.eventsourced.core.{ Journal, EventsourcingExtension }
import org.scalatest.fixture
import org.eligosource.eventsourced.journal.inmem.InmemJournalProps

abstract class EventsourcingSpecInMemory[T <: EventsourcingFixtureInMemory[_]: ClassTag] extends fixture.WordSpec with MustMatchers {
  type FixtureParam = T

  def createFixture =
    implicitly[ClassTag[T]].runtimeClass.newInstance().asInstanceOf[T]

  def withFixture(test: OneArgTest) {
    val fixture = createFixture
    try {
      test(fixture)
    } finally {
      fixture.shutdown()
    }
  }
}

trait EventsourcingFixtureInMemory[A] {
  implicit val system = ActorSystem("test")
  implicit val timeout = new Timeout(5, TimeUnit.SECONDS)

  val journal = Journal(InmemJournalProps(Some("eventsourced-specttests")))
  val queue = new LinkedBlockingQueue[A]
  val extension = EventsourcingExtension(system, journal)

  def shutdown() {
    system.shutdown()
    system.awaitTermination()
  }
}
