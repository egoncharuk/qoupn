package biz.shopboard.domain.model.user

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import scalaz.{ Failure, Success }
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import biz.shopboard.domain.model.common.AdminRole
import biz.shopboard.domain.VersionMessages
import scala.collection.immutable.HashSet

@RunWith(classOf[JUnitRunner])
class UserSpec extends WordSpec with MustMatchers {

  val details1 = new UserDetails("login", "password", "name", "surname", "email@email.com")
  val details2 = new UserDetails("login", "password", "name2", "surname2", "email2@email.com")

  val user = new User("id", 0L, details1)
  val admin = new User("id", 0L, details1, List(AdminRole))

  "A User" should {

    "create new instance given parameters" in {
      val createdUser = User.create(user.id, user.details)
      createdUser must equal(Success(new User(user.id, user.version, user.details)))
    }

    "create new instance of admin given parameters" in {
      val createdUser = User.create(admin.id, admin.details, AdminRole)
      createdUser must equal(Success(new User(admin.id, admin.version, admin.details, admin.roles)))
    }

    // TODO: enable when needed
/*    "fail in case required version is not matching passed version" in {
      val failure: Failure[List[String], Nothing] = Failure(List(VersionMessages.invalidVersionMessage format ("User", "id", 1L, 0L)))
      user.requireVersion(Some(1L)) must equal(failure)
    }*/

    "succeed in case required version is matching passed version" in {
      user.requireVersion(Some(0L)) must equal(Success(user))
    }
  }

  "A user" should {
    "return None if version is -1L" in {
      val versionOption = new User(user.id, -1L, user.details).versionOption
      versionOption must be(None)
    }

    "return Some(4L) if version is 4L" in {
      val versionOption: Option[Long] = new User(user.id, 4L, user.details).versionOption
      versionOption must be(Some(4L))
    }

    "succeed when updating details" in {
      val validation = user.updateDetails(details2)
      validation must be(Success(new User(user.id, user.version + 1, details2)))
    }

    "succeed when shopping list is added and deleted" in {
      val shopListId = "shoplist-1"
      val validation = user.addShoppingList(shopListId)
      validation must be(Success(new User(user.id, user.version + 1, details1, List(), HashSet(), List(shopListId))))

      validation match {
        case Success(u) => {
          val validation2 = u.removeShoppingList(shopListId)
          validation2 must be(Success(new User(user.id, user.version + 2, details1)))
        }
        case Failure(f)  => fail()
      }
    }

    "fail when non-existing shopping list is deleted" in {
      val validation = new User(user.id, user.version, details1).removeShoppingList("uknown-Id")
      validation must be(Failure(List(user.shoppingListIdNotFoundMessage format ("uknown-Id"))))
    }
  }

}
