package biz.shopboard.domain.model.user

import org.scalatest.WordSpec
import org.scalatest.matchers.MustMatchers
import scalaz.{ Failure, Success }
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import biz.shopboard.domain.VersionMessages

@RunWith(classOf[JUnitRunner])
class ShoppingListSpec extends WordSpec with MustMatchers {

  val details1 = new ShoppingListDetails("name")
  val details2 = new ShoppingListDetails("name2")

  val shoppingList = new ShoppingList("id", version = 0L, details = details1)

  val shopListItem = new ShoppingListItem("item-id1", "description 1");

  "A ShoppingList" should {

    "create new instance given parameters" in {
      val createdShoppingList = ShoppingList.create(shoppingList.id, shoppingList.details)
      createdShoppingList must equal(Success(shoppingList))
    }

    // TODO: enable when needed
/*
    "fail in case required version is not matching passed version" in {
      val failure: Failure[List[String], Nothing] = Failure(List(VersionMessages.invalidVersionMessage format ("ShoppingList", "id", 1L, 0L)))
      shoppingList.requireVersion(Some(1L)) must equal(failure)
    }
*/

    "succeed in case required version is matching passed version" in {
      shoppingList.requireVersion(Some(0L)) must equal(Success(shoppingList))
    }
  }

  "A shoppingList" should {
    "return None if version is -1L" in {
      val versionOption = new ShoppingList(shoppingList.id, -1L, shoppingList.details).versionOption
      versionOption must be(None)
    }

    "return Some(4L) if version is 4L" in {
      val versionOption: Option[Long] = new ShoppingList(shoppingList.id, 4L, shoppingList.details).versionOption
      versionOption must be(Some(4L))
    }

    "succeed when updating details" in {
      val validation = shoppingList.updateDetails(details2)
      validation must be(Success(new ShoppingList(shoppingList.id, shoppingList.version + 1, details2)))
    }

    "succeed when item is added" in {
      val validation = shoppingList.addItem(shopListItem)
      validation must be(Success(new ShoppingList(shoppingList.id, shoppingList.version + 1, details1, Map(shopListItem.id -> shopListItem))))
    }

    "succeed when existing item is deleted" in {
      val validation = new ShoppingList(shoppingList.id, shoppingList.version, details1, Map(shopListItem.id -> shopListItem)).removeItem(shopListItem.id)
      validation must be(Success(new ShoppingList(shoppingList.id, shoppingList.version + 1, details1)))
    }

    "fail when non-existing item is deleted" in {
      val validation = new ShoppingList(shoppingList.id, shoppingList.version, details1, Map(shopListItem.id -> shopListItem)).removeItem("uknown-Id")
      validation must be(Failure(List(ShoppingList.itemIdErrorMsg format ("uknown-Id"))))
    }
  }
}
