package biz.shopboard

package object journal {
  val mongoLocalHostName = "localhost"
  val mongoDefaultPort = 27017
}
