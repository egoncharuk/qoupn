package biz.shopboard.journal

import org.scalatest.BeforeAndAfterEach
import com.mongodb.casbah.Imports._
import org.eligosource.eventsourced.core.PersistentReplaySpec
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class MongoDBCasbahReplaySpec extends PersistentReplaySpec with BeforeAndAfterEach {

  val dbName = "es2"
  val collName = "event"
  val snapshotName = "snapshot"

  def journalProps = MongoDBCasbahJournalProps(MongoClient(mongoLocalHostName, mongoDefaultPort), dbName, collName, snapshotName)

  override def afterEach() {
    MongoClient(mongoLocalHostName, mongoDefaultPort)(dbName)(collName).dropCollection()
    MongoClient(mongoLocalHostName, mongoDefaultPort)(dbName)(snapshotName).dropCollection()
  }
}
