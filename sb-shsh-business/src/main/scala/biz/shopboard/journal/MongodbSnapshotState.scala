package biz.shopboard.journal

import com.novus.salat.annotations.raw.Salat
import org.eligosource.eventsourced.core.{ Journal, Message, SnapshotMetadata, Snapshot }
import org.eligosource.eventsourced.core.Journal.{ WriteAck, WriteOutMsg, WriteInMsg }

@Salat
trait MongodbSnapshotState //we need to extend each case class snapshot explicitly from MongodbSnapshotState
@Salat
trait MongodbEvent //we need to extend each case class event explicitly from MongodbEvent

trait MongodbSnapshotFunctor {
  def map(state: MongodbSnapshotState): Option[MongodbSnapshot] = details map { d => new MongodbSnapshot(d._1, d._2, d._3, d._4, state) }
  protected[journal] lazy val details: Option[(Int, Long, Long, Int)] = None
}

case class MongodbSnapshotContainer(outer: Iterator[MongodbSnapshotState]) extends Iterator[Option[MongodbSnapshot]] with MongodbSnapshotFunctor {
  def toSnapshot: Option[Snapshot] = details map { d => Snapshot(d._1, d._2, d._3, this) }
  def hasNext = outer.hasNext
  def next() = map(outer.next())
}

case class Key(processorId: Int, channelId: Int, fromSequenceNr: Long, toSequenceNr: Long)
case class MongodbSnapshot(processorId: Int, sequenceNr: Long, timestamp: Long, snapshotNr: Int, state: MongodbSnapshotState) extends SnapshotMetadata
case class MongodbMessage(processorId: Int, sequenceNr: Long, initiatingChannelId: Int, confirmingChannelId: Int, event: Option[MongodbEvent], msg: Option[Message])

object MongodbSnapshotContainer {
  def apply(c: MongodbSnapshotContainer, s: Snapshot, sn: Int) = new MongodbSnapshotContainer(c.outer) {
    override protected[journal] lazy val details = Some((s.processorId, s.sequenceNr, s.timestamp, sn))
  }
  def apply(snapshots: BufferedIterator[MongodbSnapshot]) = new MongodbSnapshotContainer(Iterator.empty) {
    override protected[journal] lazy val details = if (hasNext) Some((snapshots.head.processorId, snapshots.head.sequenceNr, snapshots.head.timestamp, snapshots.head.snapshotNr)) else None
    override def hasNext = snapshots.hasNext
    override def next() = Some(snapshots.next())
  }
}

object MongodbSnapshot {
  def apply(snapshot: Snapshot)(implicit maxSnapshotNr: Int) = snapshot match {
    case Snapshot(p, sn, t, s: MongodbSnapshotState) => Some(new MongodbSnapshot(p, sn, t, maxSnapshotNr + 1, s))
    case s @ Snapshot(_, _, _, c: MongodbSnapshotContainer) => Some(MongodbSnapshotContainer(c, s, maxSnapshotNr + 1))
    case _ => None
  }
}

object MongodbMessage {
  def apply(cmd: WriteAck) = Some(List(new MongodbMessage(cmd.processorId, cmd.ackSequenceNr, 0, cmd.channelId, None, None)))
  def apply(cmd: WriteInMsg)(implicit sequenceNr: Long) = cmd.message.event match {
    case e: MongodbEvent => Some(List(new MongodbMessage(cmd.processorId, sequenceNr, 0, 0, Some(e), Some(clear(cmd.message)))))
    case _ => None
  }
  def apply(cmd: WriteOutMsg)(implicit sequenceNr: Long) = cmd.message.event match {
    case e: MongodbEvent => Some(new MongodbMessage(Int.MaxValue, sequenceNr, cmd.channelId, 0, Some(e), Some(clear(cmd.message))) :: (cmd.ackSequenceNr match {
      case Journal.SkipAck => Nil
      case _ => new MongodbMessage(cmd.ackProcessorId, cmd.ackSequenceNr, 0, cmd.channelId, None, None) :: Nil
    }))
    case _ => None
  }

  def clear(msg: Message) = Message(Some(0), msg.sequenceNr, msg.timestamp, msg.processorId, msg.acks, msg.ack)
}
