package biz.shopboard.service

import biz.shopboard.domain.model.merchant.{ ActivationEvent, Deal }
import biz.shopboard.domain.model.user.{ User, UserUnsubscribed, UserEvent }
import biz.shopboard.state.Snapshot

trait SubscriptionProjection extends SubscriptionProjectionType {
  def collect(id: String, user: Option[User] = None): Option[String] = user match {
    case Some(u) => if (currentState.getOrElse(u.id, Map()).flatMap(_._2).toList.contains(id)) Some(id) else None
    case None => None
  }
  def isApplicable(deal: Deal, user: Option[User] = None): Boolean = {
    deal.rules.isEmpty || (deal.rules.forall(_.isApplicable) && deal.rules.forall(user.isDefined && _.isApplicable(user.get)))
  }
  def getUser(event: UserEvent): Option[User] = {
    currentKey.get(event.asInstanceOf[ActivationEvent].merchantId).getOrElse(Map()).get(event.userId)
  }
  def add(state: StateMap, userId: String, merchantId: String, couponId: String): InnerStateMap = {
    val merchantsMap = state.getOrElse(userId, Map())
    merchantsMap + (merchantId -> (merchantsMap.getOrElse(merchantId, Set()) + couponId))
  }
  def project = {
    case (state, event: UserEvent) => state + (event.userId -> (collect(event.dealId, getUser(event)) match {
      case None => state.getOrElse(event.userId, Map())
      case Some(couponId) => add(state, event.userId, event.merchantId, couponId)
    }))
    case (state, event: ActivationEvent) => currentKey.get(event.merchantId) match {
      case Some(users) => state ++ users.map(user => user._1 -> (collect(event.dealId, Some(user._2)) match {
        case None => state.getOrElse(user._1, Map())
        case Some(couponId) => add(state, user._1, event.merchantId, couponId)
      }))
      case None => state
    }
  }

  def projectKey = {
    case (key, event) => key.get(event.merchantId) match {
      case Some(users) => key + (event.merchantId -> (users + (event.userId -> event.user)))
      case None => key + (event.merchantId -> Map(event.userId -> event.user))
    }
  }
  def unsubscribe(event: UserUnsubscribed) {
    snapshot.single transform { snapshot =>
      Snapshot(currentKey + (event.merchantId -> currentKey.getOrElse(event.merchantId, Map()).filterNot(_._1 == event.userId)),
        currentState + (event.userId -> currentState.getOrElse(event.userId, Map()).filterNot(_._1 == event.merchantId)))
    }
  }
}
