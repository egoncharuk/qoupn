package biz.shopboard.service

import biz.shopboard._
import domain.model
import model.merchant._
import infrastructure.ValidationMessages
import scala.concurrent.Await
import concurrent.stm._
import akka.actor._
import akka.pattern.ask
import biz.shopboard.domain.model.user._
import state.TotalEmitter
import org.eligosource.eventsourced.core.SnapshotRequest
import org.eligosource.eventsourced.core.Message
import scala.Some
import org.eligosource.eventsourced.core.SnapshotOffer

class DealSubscriptionService(projection: ActorRef)(implicit system: ActorSystem) {

  import system.dispatcher

  implicit val timeout = akka.util.Timeout(5000)

  def collectDeals(userId: String, merchant: Option[Merchant] = None): Option[List[String]] = {
    Await.result(projection ? Message(CheckDeals(userId, merchant.map(_.id))) map (_.asInstanceOf[Option[List[String]]]), duration)
  }
  def collectDeals(userId: String, merchantId: String): Option[List[String]] = {
    Await.result(projection ? Message(CheckDeals(userId, Some(merchantId))) map (_.asInstanceOf[Option[List[String]]]), duration)
  }
  def collectMerchants(userId: String): Option[Iterable[String]] = {
    Await.result(projection ? Message(CheckMerchants(userId)) map (_.asInstanceOf[Option[Iterable[String]]]), duration)
  }
  def collectSubscribeOnlyMerchants(userId: String): Option[Iterable[String]] = {
    Await.result(projection ? Message(CheckSubscribeOnlyMerchants(userId)) map (_.asInstanceOf[Option[Iterable[String]]]), duration)
  }
}

class DealSubscriptionProjection(dealsRef: Ref[Map[String, Deal]]) extends Actor with SubscriptionProjection with ValidationMessages {
  this: TotalEmitter =>

  def initialKey = Map.empty[String, Map[String, User]]
  def initialState = Map.empty[String, Map[String, Set[String]]]

  override def collect(dealId: String, user: Option[User] = None): Option[String] = super.collect(dealId, user) match {
    case id @ Some(_) => id
    case None => {
      dealsRef.single.transform(deals => deals.get(dealId) match {
        case Some(d: ActiveDeal) if isApplicable(d, user) => deals + (dealId -> d)
        case _ => deals
      })
      dealsRef.single.getWith(deals => deals.get(dealId) match {
        case Some(d: ActiveDeal) if isApplicable(d, user) => Some(d.id)
        case _ => None
      })
    }
  }

  override def receive = akka.event.LoggingReceive {
    case event: DealActivationEvent => updateState(event)
    case event: UserUnsubscribed => unsubscribe(event)
    case event: UserSubscribed => {
      updateKey(event)
      emitter sendEvent DistributeDeals(event.merchantId, event.userId)
    }
    case CheckDeals(userId, None) => {
      sender ! currentState.get(userId).collect {
        case pm: Map[String, Set[String]] => pm.flatMap(_._2).toList
      }
    }
    case CheckDeals(userId, Some(merchantId)) => {
      sender ! currentState.get(userId).collect {
        case pm: Map[String, Set[String]] => pm.getOrElse(merchantId, Set()).toList
      }
    }
    case CheckMerchants(userId) => {
      sender ! Some(currentState.getOrElse(userId, Map()).keys)
    }
    case CheckSubscribeOnlyMerchants(userId) => {
      sender ! Some(currentKey.filter(_._2.contains(userId)).keys)
    }

    case snapshotRequest @ SnapshotRequest(pid, snr, _) =>
      atomic { txn =>
        snapshotRequest.process(snapshot.get(txn).copy())
      }

    case SnapshotOffer(org.eligosource.eventsourced.core.Snapshot(_, snr, time,
      state: biz.shopboard.state.Snapshot[Map[String, Map[String, User]], Map[String, InnerStateMap]])) => {
      atomic { txn =>
        snapshot.single transform { snapshot => state }
      }
    }
  }
}

