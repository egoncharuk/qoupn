package biz.shopboard.service

import concurrent.stm._

import akka.actor._
import akka.pattern.ask

import scalaz.Scalaz._
import biz.shopboard.domain._
import biz.shopboard.domain.model.merchant.Merchant
import biz.shopboard.domain.model.user._
import biz.shopboard.domain.model.common._
import model.common.ApplicationRole
import model.user.ValidateActionHash
import org.eligosource.eventsourced.core._
import biz.shopboard.infrastructure.ValidationMessages
import biz.shopboard.state.TotalEmitter
import biz.shopboard.util.Hashing
import concurrent.Future

import scala.concurrent.ExecutionContext.Implicits.global
import scalaz.Scalaz
import akka.event.Logging
import biz.shopboard.domain.model.user.UpdateShoppingListItem
import biz.shopboard.domain.model.user.RemoveShoppingList
import biz.shopboard.domain.model.user.SendChangeUserPasswordLink
import biz.shopboard.domain.model.user.UnlinkShoppingListAndDeal
import biz.shopboard.domain.model.user.RemoveUser
import biz.shopboard.domain.model.user.ShoppingListDetails
import biz.shopboard.domain.model.user.ShoppingListCreated
import biz.shopboard.domain.model.user.CreateShoppingList
import biz.shopboard.domain.model.user.UserSubscribed
import biz.shopboard.domain.model.user.ChangeUserPassword
import biz.shopboard.domain.model.user.AddShoppingListItem
import biz.shopboard.domain.model.user.LinkShoppingListAndDeal
import biz.shopboard.domain.model.user.LinkShoppingListAndCoupon
import biz.shopboard.domain.model.user.ShoppingListItemUpdated
import biz.shopboard.domain.model.user.ShoppingListDetailsUpdated
import biz.shopboard.domain.model.user.UserRemoved
import biz.shopboard.domain.model.user.ShoppingListItemRemoved
import biz.shopboard.domain.model.user.UpdateShoppingListDetails
import biz.shopboard.domain.model.user.SubscribeUser
import biz.shopboard.domain.model.user.UserDetailsUpdated
import biz.shopboard.domain.model.user.ShoppingListAndDealUnlinked
import biz.shopboard.domain.model.user.ShoppingListItem
import biz.shopboard.domain.model.user.UserUnsubscribed
import biz.shopboard.domain.model.user.ActivateUserPassword
import biz.shopboard.domain.model.user.RemoveShoppingListItem
import biz.shopboard.domain.model.user.ShoppingListAndCouponLinked
import biz.shopboard.domain.model.user.UpdateUserDetails
import biz.shopboard.domain.model.user.UserDetails
import biz.shopboard.domain.model.user.UserCreated
import biz.shopboard.domain.model.user.CreateUser
import biz.shopboard.domain.model.user.UnsubscribeUser
import biz.shopboard.domain.model.user.ShoppingListAndDealLinked
import biz.shopboard.domain.model.user.UnlinkShoppingListAndCoupon
import biz.shopboard.domain.model.user.ShoppingListItemAdded
import biz.shopboard.domain.model.user.ShoppingListAndCouponUnlinked
import biz.shopboard.domain.model.user.ShoppingListRemoved
import biz.shopboard.journal.{ MongodbSnapshotContainer, MongodbSnapshotState }
import scalaz.Failure
import scala.Some
import org.eligosource.eventsourced.core.SnapshotOffer
import org.eligosource.eventsourced.core.Message
import scalaz.Success
import org.eligosource.eventsourced.patterns.reliable.requestreply.DestinationFailure
import org.eligosource.eventsourced.patterns.reliable.requestreply.DestinationNotResponding

class UserService(
                   usersRef: Ref[Map[String, User]],
                   actionHashesRef: Ref[Map[String, ActionHash]], // Map activation/psw.reset/etc.. hashKey:String->actionHash:ActionHash
                   shoppingListsRef: Ref[Map[String, ShoppingList]],
                   shoppingListsToDealsRef: Ref[Map[CompositeShoppingListID, List[String]]],
                   shoppingListsToCouponsRef: Ref[Map[CompositeShoppingListID, List[String]]],
                   userProcessor: ActorRef)(implicit system: ActorSystem) {

  //
  // Consistent reads
  //

  def getUsersMap = usersRef.single.get

  def getUser(userId: String): Option[User] = getUsersMap.get(userId)

  def getUsers: Iterable[User] = getUsersMap.values

  def getAdmins = getUsers filter (_.roles.contains(AdminRole))

  def getMerchants = getUsers filter (_.roles.contains(MerchantRole))

  def getShoppingListsMap = shoppingListsRef.single.get

  def getShoppingList(shoppingListId: String): Option[ShoppingList] = getShoppingListsMap.get(shoppingListId)

  def getShoppingLists: Iterable[ShoppingList] = getShoppingListsMap.values

  def getLinkedDeals(shoppingListId: String): Option[List[String]] = getLinkedDealsMap.get(shoppingListId)

  def getLinkedCoupons(shoppingListId: String): Option[List[String]] = getLinkedCouponsMap.get(shoppingListId)

  def getLinkedDealsMap = shoppingListsToDealsRef.single.get

  def getLinkedCouponsMap = shoppingListsToCouponsRef.single.get

  def getActionHashes = actionHashesRef.single.get

  //
  // Updates
  //

  implicit val timeout = akka.util.Timeout(5000)

  ///////////////////////////////////////////////////////////////////
  // User
  ///////////////////////////////////////////////////////////////////

  // User services - sending events & waiting for answer which is wrapped into Future
  def createUser(userId: String, userDetails: UserDetails, roles: List[ApplicationRole] = List()): Future[DomainValidation[User]] =
    userProcessor ? Message(CreateUser(userId.isEmpty ? Hashing.generateQuasiRandom | userId, userDetails.encryptPassword, roles)) map (_.asInstanceOf[DomainValidation[User]])

  def createUser(user: User): Future[DomainValidation[User]] =
    userProcessor ? Message(CreateUser(user.id, user.details.encryptPassword, user.roles)) map (_.asInstanceOf[DomainValidation[User]])

  def updateUserDetails(userId: String, expectedVersion: Option[Long], userDetails: UserDetails): Future[DomainValidation[User]] =
    userProcessor ? Message(UpdateUserDetails(userId, expectedVersion, userDetails.encryptPassword)) map (_.asInstanceOf[DomainValidation[User]])

  def removeUser(userId: String, expectedVersion: Option[Long]): Future[DomainValidation[User]] =
    userProcessor ? Message(RemoveUser(userId, expectedVersion)) map (_.asInstanceOf[DomainValidation[User]])

  def activeUserExists(userName: String, password: String): Boolean = filterActiveUsersByNamePwd(userName, password).nonEmpty

  def userNameExists(userName: String): Boolean = getUsersMap.filter(pair => pair._2.details.login.equals(userName)).nonEmpty

  def findUserByNamePwd(userName: String, password: String): User = filterUsersByNamePwd(userName, password).values.seq.head

  def findUserByEmail(email: String): User = getUsersMap.filter(pair => pair._2.details.email.equals(email)).values.seq.head

  private def filterUsersByNamePwd(userName: String, password: String): Map[String, User] =
    getUsersMap.filter(pair => pair._2.details.login.equals(userName)
      && pair._2.details.passwordMatches(password))

  private def filterActiveUsersByNamePwd(userName: String, password: String): Map[String, User] =
    getUsersMap.filter(pair => pair._2.details.login.equals(userName)
      && pair._2.details.passwordMatches(password)
      && pair._2.details.status.equals(UserStatus.ACTIVE))

  def sendChangeUserPasswordLink(email: String): Future[DomainValidation[User]] = {
    userProcessor ? Message(SendChangeUserPasswordLink(email)) map (_.asInstanceOf[DomainValidation[User]])
  }

  def activateUser(hashKey: String): Future[DomainValidation[User]] = {
    userProcessor ? Message(ActivateUserPassword(hashKey)) map (_.asInstanceOf[DomainValidation[User]])
  }

  def changeUserPassword(hashKey: String, newPassword: String): Future[DomainValidation[User]] = {
    userProcessor ? Message(ChangeUserPassword(hashKey, newPassword)) map (_.asInstanceOf[DomainValidation[User]])
  }

  def actionHashIsValid(hashKey: String, actionHashType: ActionHashType): Future[DomainValidation[ActionHash]] = {
    userProcessor ? Message(ValidateActionHash(hashKey, actionHashType)) map (_.asInstanceOf[DomainValidation[ActionHash]])
  }

  ///////////////////////////////////////////////////////////////////
  // Shopping List
  ///////////////////////////////////////////////////////////////////

  // Shopping list services - sending events & waiting for answer which is wrapped into Future
  def createShoppingList(userId: String, expectedVersion: Option[Long], shoppingListId: String, shoppingListDetails: ShoppingListDetails): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(CreateShoppingList(userId, expectedVersion, shoppingListId, shoppingListDetails)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  def updateShoppingListDetails(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListDetails: ShoppingListDetails): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(UpdateShoppingListDetails(userId, shoppingListId, expectedVersion, shoppingListDetails)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  def removeShoppingList(userId: String, shoppingListId: String, expectedVersion: Option[Long]): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(RemoveShoppingList(userId, shoppingListId, expectedVersion)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  // Shopping list item services - sending events & waiting for answer which is wrapped into Future
  def addShoppingListItem(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListItem: ShoppingListItem): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(AddShoppingListItem(userId, shoppingListId, expectedVersion, shoppingListItem)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  def updateShoppingListItem(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListItem: ShoppingListItem): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(UpdateShoppingListItem(userId, shoppingListId, expectedVersion, shoppingListItem)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  def removeShoppingListItem(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListItemId: String): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(RemoveShoppingListItem(userId, shoppingListId, expectedVersion, shoppingListItemId)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  ///////////////////////////////////////////////////////////////////
  // Integration with Merchant
  ///////////////////////////////////////////////////////////////////
  def subscribeToMerchant(userId: String, merchantId: String): Future[DomainValidation[User]] =
    userProcessor ? Message(SubscribeUser(userId, merchantId)) map (_.asInstanceOf[DomainValidation[User]])

  def unsubscribeFromMerchant(userId: String, merchantId: String): Future[DomainValidation[User]] =
    userProcessor ? Message(UnsubscribeUser(userId, merchantId)) map (_.asInstanceOf[DomainValidation[User]])

  def linkShoppingListAndDeal(userId: String, shoppingListId: String, dealId: String, shoppingListItemId: Option[String] = None): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(LinkShoppingListAndDeal(userId, (shoppingListId, shoppingListItemId), dealId)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  def unlinkShoppingListAndDeal(userId: String, shoppingListId: String, dealId: String, shoppingListItemId: Option[String] = None): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(UnlinkShoppingListAndDeal(userId, (shoppingListId, shoppingListItemId), dealId)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  def linkShoppingListAndCoupon(userId: String, shoppingListId: String, couponId: String, shoppingListItemId: Option[String] = None): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(LinkShoppingListAndCoupon(userId, (shoppingListId, shoppingListItemId), couponId)) map (_.asInstanceOf[DomainValidation[ShoppingList]])

  def unlinkShoppingListAndCoupon(userId: String, shoppingListId: String, couponId: String, shoppingListItemId: Option[String] = None): Future[DomainValidation[ShoppingList]] =
    userProcessor ? Message(UnlinkShoppingListAndCoupon(userId, (shoppingListId, shoppingListItemId), couponId)) map (_.asInstanceOf[DomainValidation[ShoppingList]])
}

// -------------------------------------------------------------------------------------------------------------
//  UserProcessor is single writer to UsersRef & ShoppingListsRef, so
// we can have reads and writes in separate transactions. But this should be fixed as 1 of 2 transactions
// rollback will give inconsistent state.
// -------------------------------------------------------------------------------------------------------------

object UserProcessor extends ValidationMessages

case class UserMapState(id: String, user: Option[User]) extends MongodbSnapshotState
case class MerchantMapState(id: String, merchant: Option[Merchant]) extends MongodbSnapshotState
case class ActionHashMapState(id: String, hash: Option[ActionHash]) extends MongodbSnapshotState
case class ShoppingListMapState(id: String, list: Option[ShoppingList]) extends MongodbSnapshotState
case class ShoppingListToDealsMapState(id: CompositeShoppingListID, linked: Option[List[String]]) extends MongodbSnapshotState
case class ShoppingListToCouponsMapState(id: CompositeShoppingListID, linked: Option[List[String]]) extends MongodbSnapshotState

class UserProcessor(usersRef: Ref[Map[String, User]],
                    actionHashesRef: Ref[Map[String, ActionHash]],
                    merchantsRef: Ref[Map[String, Merchant]],
                    shoppingListsRef: Ref[Map[String, ShoppingList]],
                    shoppingListsToDealsRef: Ref[Map[CompositeShoppingListID, List[String]]],
                    shoppingListsToCouponsRef: Ref[Map[CompositeShoppingListID, List[String]]]) extends Actor with ActorLogging {

  this: TotalEmitter with ActorLogging =>

  val dl = context.system.deadLetters

  override def preStart = {
    log.info("Starting actor UserProcessor. Instance hashcode=" + this.hashCode)
  }

  override def postStop = {
    log.info("Stopping actor UserProcessor. Instance hashcode=" + this.hashCode)
  }

  import UserProcessor._

  override def receive = {
    //User
    case CreateUser(userId, userDetails, roles) =>
      processUser(createUser(userId, userDetails, roles)) {
        user =>
          emitter sendEvent UserCreated(userId, userDetails)
      }
    case UpdateUserDetails(userId, expectedVersion, userDetails) =>
      processUser(updateUserDetails(userId, expectedVersion, userDetails)) {
        user =>
          emitter sendEvent UserDetailsUpdated(userId, userDetails)
      }
    case RemoveUser(userId, expectedVersion) =>
      processUserRemoval(removeUser(userId, expectedVersion)) {
        user =>
          emitter sendEvent UserRemoved(userId)
      }
    case SubscribeUser(userId, merchantId) =>
      processUser(userAndMerchantExists(userId, merchantId)) {
        user => {  //sanity check
          user.addMerchant(merchantId) match {
            case Success(updatedUser) =>
              updateUsers(updatedUser)
              emitter sendEvent UserSubscribed(userId, merchantId, updatedUser)
            case Failure(e) => DomainError(e).fail
          }
        }
      }
    case UnsubscribeUser(userId, merchantId) =>
      processUser(userAndMerchantExists(userId, merchantId)) {
        user => { //sanity check
          user.removeMerchant(merchantId) match {
            case Success(updatedUser) =>
              updateUsers(updatedUser)
              emitter sendEvent UserUnsubscribed(userId, merchantId)
            case Failure(e) => DomainError(e).fail
          }
        }
      }

    case ActivateUserPassword(hashKey) => {
      validateActionHash(hashKey, ActionHashType.ACTIVATE_PASSWORD) match {
        case error @ Failure(_) => {
          log.error(" ---> : Invalid hashKey = %s" format (hashKey))
          sender ! error
        }
        case Success(actionHash) => {

          val dayInMillis: Int = 1000 * 3600 * 24
          if (message.timestamp - dayInMillis < actionHash.timestamp) {
            userExists(actionHash.userId) match {
              case error @ Failure(_) => {
                log.error(" ---> : cannot find user by id! actionHash.merchantId = %s" format (actionHash.userId))
                sender ! error
              }
              case Success(user) => {
                val updatedDetails: UserDetails = user.details.copy(status = UserStatus.ACTIVE)
                processUser(updateUserDetails(user.id, Option(user.version), updatedDetails)) {
                  user =>
                    emitter sendEvent UserDetailsUpdated(user.id, updatedDetails)
                }
                removeActionHash(hashKey)
                log.debug(" ---> : activated user / removed action hash. updatedDetails = %s" format(updatedDetails))
                sender ! message.posConfirmationMessage
              }
            }
          } else {
            removeActionHash(hashKey)
            log.debug(" ---> : action hash expired! / removed action hash...")
            sender ! DomainError(actionLinkExpiredMsg).fail
          }
        }
      }
    }

    case ValidateActionHash(hashKey, actionHashType) => {
      validateActionHash(hashKey, actionHashType) match {
        case error @ Failure(_) => sender ! error
        case Success(actionHash) => sender ! Success(actionHash)
      }
    }

    case ChangeUserPassword(hashKey, newPassword) => {

      log.debug("ChangeUserPassword by hashKey=" + hashKey)

      validateActionHash(hashKey, ActionHashType.RESET_PASSWORD) match {
        case error @ Failure(_) => sender ! error
        case Success(actionHash) => {
          val dayInMillis: Int = 1000 * 3600 * 24
          if (message.timestamp - dayInMillis < actionHash.timestamp) {
            userExists(actionHash.userId) match {
              case error @ Failure(_) => sender ! error
              case Success(user) => {
                val updatedDetails: UserDetails = user.details.copy(password = newPassword)
                processUser(updateUserDetails(user.id, Option(user.version), updatedDetails)) {
                  user =>
                    emitter sendEvent UserDetailsUpdated(user.id, updatedDetails)
                }
                removeActionHash(hashKey)
                sender ! message.posConfirmationMessage
              }
            }
          } else {
            removeActionHash(hashKey)
            sender ! DomainError(actionLinkExpiredMsg).fail
          }
        }
      }
    }

    case UserActivationEmailSent(actionHash) => {
      addActionHash(actionHash)
    }

    case UserActivationEmailSendingFailed(actionHash) => {
      confirm(false)
    }

    case SendChangeUserPasswordLink(email) =>
      getUserByEmail(email) match {
        case Success(user) => {
          emitter("mailer") sendEvent UserPasswordChangeEmailRequested(user.id, user.details.firstName, user.details.lastName, user.details.email)
          sender ! Success(user)
        }
        case Failure(emailFail) => sender ! Failure(emailFail)
      }

    case UserPasswordChangeEmailSent(actionHash) => {
      addActionHash(actionHash)
    }

    case UserPasswordChangeEmailSendingFailed(actionHash) => {
      confirm(false)
    }

    case DestinationNotResponding(channelId, failureCount, request) => {
      log.warning("Destination of channel {} does not respond (failure count = {}). Negatively confirm message receipt.", channelId, failureCount)
      confirm(false) // retry (or escalate)
    }

    case DestinationFailure(channelId, failureCount, UserActivationEmailRequested(id, name, surname, email), throwable) => {
      if (failureCount > 2) {
        confirm(true)
      } else {
        log.warning("Destination of channel {} returned a failure (failure count = {}). Negatively confirm message receipt.", channelId, failureCount)
        confirm(false) // retry
      }
    }

    case DestinationFailure(channelId, failureCount, UserPasswordChangeEmailRequested(id, name, surname, email), throwable) => {
      if (failureCount > 2) {
        confirm(true)
      } else {
        log.warning("Destination of channel {} returned a failure (failure count = {}). Negatively confirm message receipt.", channelId, failureCount)
        confirm(false) // retry
      }
    }

    //    case DeliveryStopped(channelId) if (channelId == mailerChannelId) => {
    //      val delay = FiniteDuration(5, "seconds")
    //      log.warning("Channel {} stopped delivery. Reactivation in {}.", channelId, delay)
    //      context.system.scheduler.scheduleOnce(delay, mailerChannel, Deliver)(context.dispatcher)
    //    }

    //Shopping list
    case CreateShoppingList(userId, expectedVersion, shoppingListId, shoppingListDetails) =>

      def shoppingListNotExists(shoppingListId: String): DomainValidation[ShoppingList] = readShoppingLists.get(shoppingListId) match {
        case Some(shoppingList) => DomainError(shopListIdAlreadyExistMsg format shoppingListId).fail
        case None => createShoppingList(shoppingListId, shoppingListDetails)
      }

      val userValidation = userExists(userId) match {
        case Success(user) => updateUser(userId, expectedVersion) {
          user => user.addShoppingList(shoppingListId)
        }
        case error @ Failure(_) => error
      }
      val shoppingListValidation = shoppingListNotExists(shoppingListId)

      userValidation match {
        case Success(user) => shoppingListValidation match {
          case Success(shoppingList) => {
            updateUsersAndShoppingLists(user, shoppingList)
            // now call onSuccess
            emitter sendEvent ShoppingListCreated(userId, shoppingListId, shoppingListDetails)
          }
          case Failure(_) => Unit
        }
        case Failure(_) => Unit
      }

      (userValidation, shoppingListValidation) match {
        case (Success(userValidation), Success(shoppingListValidation)) => {
          sender ! Success(shoppingListValidation)
        }
        case (Success(_), Failure(listFail)) => {
          sender ! Failure(listFail)
        }
        case (Failure(userFail), Success(_)) => {
          sender ! Failure(userFail)
        }
        case (Failure(userFail), Failure(listFail)) => {
          sender ! DomainError(userFail.toString :: listFail).fail
        }
      }

    case UpdateShoppingListDetails(userId, shoppingListId, expectedVersion, shoppingListDetails) =>
      processShoppingList(updateShoppingListDetails(shoppingListId, expectedVersion, shoppingListDetails)) {
        shoppingList =>
          emitter sendEvent ShoppingListDetailsUpdated(userId, shoppingListId, shoppingListDetails)
      }
    case RemoveShoppingList(userId, shoppingListId, expectedVersion) =>
      processRemoval(removeShoppingList(userId, shoppingListId, expectedVersion)) {
        shoppingList =>
          emitter sendEvent ShoppingListRemoved(userId, shoppingListId)
      }
    case AddShoppingListItem(userId, shoppingListId, expectedVersion, shoppingListItem) =>
      processShoppingList(addShoppingListItem(shoppingListId, expectedVersion, shoppingListItem)) {
        shoppingList =>
          emitter sendEvent ShoppingListItemAdded(userId, shoppingListId, shoppingListItem)
      }
    case UpdateShoppingListItem(userId, shoppingListId, expectedVersion, shoppingListItem) =>
      processShoppingList(updateShoppingListItem(shoppingListId, expectedVersion, shoppingListItem)) {
        shoppingList =>
          emitter sendEvent ShoppingListItemUpdated(userId, shoppingListId, shoppingListItem)
      }
    case RemoveShoppingListItem(userId, shoppingListId, expectedVersion, shoppingListItemId) =>
      processShoppingList(removeShoppingListItem(shoppingListId, expectedVersion, shoppingListItemId)) {
        shoppingList =>
          emitter sendEvent ShoppingListItemRemoved(userId, shoppingListId, shoppingListItemId)
      }

    case LinkShoppingListAndDeal(userId, shoppingListId, dealId) => {
      linkTogether(ShoppingListAndDealLinked(userId, shoppingListId, dealId), shoppingListsToDealsRef) {
        link
      }
    }
    case UnlinkShoppingListAndDeal(userId, shoppingListId, dealId) => {
      linkTogether(ShoppingListAndDealUnlinked(userId, shoppingListId, dealId), shoppingListsToDealsRef) {
        unlink
      }
    }
    case LinkShoppingListAndCoupon(userId, shoppingListId, dealId) => {
      linkTogether(ShoppingListAndCouponLinked(userId, shoppingListId, dealId), shoppingListsToCouponsRef) {
        link
      }
    }
    case UnlinkShoppingListAndCoupon(userId, shoppingListId, dealId) => {
      linkTogether(ShoppingListAndCouponUnlinked(userId, shoppingListId, dealId), shoppingListsToCouponsRef) {
        unlink
      }
    }

    case snapshotRequest @ SnapshotRequest(pid, snr, _) =>
      atomic { txn =>
        val currentUsers: Map[String, User] = usersRef.get(txn)
        val currentMerchants: Map[String, Merchant] = merchantsRef.get(txn)
        val currentHashes: Map[String, ActionHash] = actionHashesRef.get(txn)
        val currentShoppingLists: Map[String, ShoppingList] = shoppingListsRef.get(txn)
        val currentShoppingListsToDeals: Map[CompositeShoppingListID, List[String]] = shoppingListsToDealsRef.get(txn)
        val currentShoppingListsToCoupons: Map[CompositeShoppingListID, List[String]] = shoppingListsToCouponsRef.get(txn)

        snapshotRequest.process(MongodbSnapshotContainer(outer = new Iterator[MongodbSnapshotState] {
          private val ukeys = currentUsers.keys.toIterator
          private val hkeys = currentHashes.keys.toIterator
          private val mkeys = currentMerchants.keys.toIterator
          private val slkeys = currentShoppingLists.keys.toIterator
          private val sldkeys = currentShoppingListsToDeals.keys.toIterator
          private val slckeys = currentShoppingListsToCoupons.keys.toIterator

          def next() = if (ukeys.hasNext) { val k = ukeys.next(); UserMapState(k, currentUsers.get(k)) }
          else if (hkeys.hasNext) { val k = hkeys.next(); ActionHashMapState(k, currentHashes.get(k)) }
          else if (mkeys.hasNext) { val k = mkeys.next(); MerchantMapState(k, currentMerchants.get(k)) }
          else if (slkeys.hasNext) { val k = slkeys.next(); ShoppingListMapState(k, currentShoppingLists.get(k)) }
          else if (sldkeys.hasNext) { val k = sldkeys.next(); ShoppingListToDealsMapState(k, currentShoppingListsToDeals.get(k)) }
          else { val k = slckeys.next(); ShoppingListToCouponsMapState(k, currentShoppingListsToCoupons.get(k)) }

          def hasNext = ukeys.hasNext || hkeys.hasNext || mkeys.hasNext || slkeys.hasNext || sldkeys.hasNext || slckeys.hasNext
        }))
      }

    case SnapshotOffer(org.eligosource.eventsourced.core.Snapshot(_, snr, time, container: MongodbSnapshotContainer)) => {
      atomic { txn =>
        container.foreach {
          _ match {
            case None => // do nothing here
            case Some(s) => s.state match {
              case UserMapState(id, Some(state)) => usersRef.single transform { map => map + (id -> state) }
              case MerchantMapState(id, Some(state)) => merchantsRef.single transform { map => map + (id -> state) }
              case ActionHashMapState(id, Some(state)) => actionHashesRef.single transform { map => map + (id -> state) }
              case ShoppingListMapState(id, Some(state)) => shoppingListsRef.single transform { map => map + (id -> state) }
              case ShoppingListToDealsMapState(id, Some(state)) => shoppingListsToDealsRef.single transform { map => map + (id -> state) }
              case ShoppingListToCouponsMapState(id, Some(state)) => shoppingListsToCouponsRef.single transform { map => map + (id -> state) }
              case _ =>
            }
          }
        }
      }
    }

    case msg: org.eligosource.eventsourced.core.Message => {
      println("[UserService] event = '%s'" format msg.event)
      // confirm receipt of event message from channel
      //msg.confirm()
    }
  }

  ///////////////////////////////////////////////////////////////////
  // Essential core validations
  ///////////////////////////////////////////////////////////////////

  def userExists(userId: String): DomainValidation[User] =
    readUsers.get(userId) match {
      case Some(user) => {
        log.debug(" ---> :  found user by id=%s" format (userId))
        user.success
      }
      case None => {
        log.warning(" ---> :  did not found user by id=%s" format (userId))
        DomainError(userIdNotExistMsg format userId).fail
      }
    }

  def merchantExists(merchantId: String): DomainValidation[Merchant] =
    readMerchants.get(merchantId) match {
      case Some(merchant) => {
        log.debug(" ---> :  found merchant by id=%s" format (merchantId))
        merchant.success
      }
      case None => {
        log.warning(" ---> :  did not found merchant by id=%s" format (merchantId))
        DomainError(merchantIdNotExistMsg format merchantId).fail
      }
    }

  def userDetailsAreValid(details: UserDetails): DomainValidation[UserDetails] =
    emailIsValid(details.email).flatMap(_ => details.success)

  def emailIsValid(email: String): DomainValidation[String] =
    emailRegex findFirstIn email match {
      case None => {
        log.warning(" ---> :  cannot find email=%s" format (email))
        DomainError(invalid_email).fail
      }
      case Some(mail) => {
        log.debug(" ---> :  found email=%s" format (email))
        email.success
      }
    }

  def emailIsValid(user: User): DomainValidation[User] =
    emailIsValid(user.details.email) match {
      case Success(mail) => user.success
      case _ => DomainError(invalid_email).fail // TODO refactor this
    }

  def userAndMerchantExists(userId: String, merchantId: String): DomainValidation[User] =
    for {
      user <- userExists(userId)
      merchant <- merchantExists(merchantId)
    } yield user

  def getUserByEmail(email: String): DomainValidation[User] = {

    def findUserByEmail(email: String): DomainValidation[User] = {
      val filteredUsers = readUsers.filter(pair => pair._2.details.email.equals(email))
      if (filteredUsers.isEmpty) {
        log.warning(" ---> :  user not found by email=%s" format (email))
        DomainError(userNotFoundByEmail).fail
      } else {
        log.debug(" ---> :  found user by email=%s" format (email))
        filteredUsers.values.seq.head.success
      }
    }

    for {
      validEmail <- emailIsValid(email)
      user <- findUserByEmail(validEmail)
    } yield user
  }

  def validateActionHash(hashKey: String, expectedActionHashType: ActionHashType): DomainValidation[ActionHash] = {
    for {
      existingActionHash <- actionHashExists(hashKey)
      validType <- validActionHashType(existingActionHash, expectedActionHashType)
    //validActionHash <- timeStampValid(existingActionHash)
    } yield validType
  }

  def timeStampValid(actionHash: ActionHash): DomainValidation[ActionHash] = {
    val dayInMillis: Int = 1000 * 3600 * 24
    if (System.currentTimeMillis() - dayInMillis < actionHash.timestamp) {
      actionHash.success
    } else {
      DomainError(actionLinkExpiredMsg).fail
    }
  }

  def validActionHashType(actionHash: ActionHash, expectedActionHashType: ActionHashType): DomainValidation[ActionHash] = {
    if (expectedActionHashType == actionHash.actionType) {
      log.debug(" ---> :  actionHash=%s type matching expectedType=%s" format (actionHash, expectedActionHashType))
      return Scalaz.success(actionHash)
    } else {
      log.warning(" ---> :  actionHash=%s type does not matches expectedType=%s" format (actionHash, expectedActionHashType))
      DomainError(actionLinkWrongType).fail
    }
  }

  def actionHashExists(hashKey: String): DomainValidation[ActionHash] =
    readActionHashes.get(hashKey) match {
      case Some(actionHash) => {
        log.debug(" ---> : exists hashKey = %s" format hashKey)
        Scalaz.success(actionHash)
      }
      case None => {
        log.warning(" ---> : not exists hashKey = %s" format hashKey)
        DomainError(actionHashKeyNotExistMsg format hashKey).fail
      }
    }

  def removeActionHash(hashKey: String): DomainValidation[ActionHash] =
    readActionHashes.get(hashKey) match {
      case None => {
        log.warning(" ---> : did not found hashKey = %s" format hashKey)
        DomainError(actionHashKeyNotExistMsg format hashKey).fail
      }
      case Some(actionHash) => {
        actionHashesRef.single.transform(actionHashes => actionHashes - hashKey)
        log.debug(" ---> : removed hashKey = %s" format hashKey)
        Scalaz.success(actionHash)
      }
    }

  /**
   * For passed in ''domainValidation[ShoppingList]''
   * 1) Add/Update shopping list contained in the shoppingLists map with newly passed one
   * 2) run onSuccess function
   * 3) send validation result to sender actor
   *
   * Used for any operation that involves shopping list version update :
   * any type of create/update related to Shopping list and
   * any operationd (create/update/delete) related to Shopping List Item
   *
   * @param validation
   * @param onSuccess
   * @return
   */
  def processShoppingList(validation: DomainValidation[ShoppingList])(onSuccess: ShoppingList => Unit) = {
    validation.foreach {
      shoppingList =>
        updateShoppingLists(shoppingList)
        onSuccess(shoppingList)
    }
    sender ! validation
  }

  def processUser(validation: DomainValidation[User])(onSuccess: User => Unit) {
    validation.foreach {
      user =>
        updateUsers(user)
        onSuccess(user)
    }
    sender ! validation
  }

  /**
   * For passed in ''domainValidation[ShoppingList]''
   * 1) Remove shopping list contained in the shoppingLists map.
   * 2) run onSuccess
   * 3) send validation result to sender actor
   *
   * Used only for deletion of whole shopping list as shopping list item removal
   * is regular update.
   *
   * @param validation
   * @param onSuccess
   * @return
   */
  def processRemoval(validation: DomainValidation[ShoppingList])(onSuccess: ShoppingList => Unit) = {
    // TODO: refactor ? (duplication of processUser()
    validation.foreach {
      shoppingList =>
        removeShoppingList(shoppingList)
        onSuccess(shoppingList)
    }
    sender ! validation
  }

  def processUserRemoval(validation: DomainValidation[User])(onSuccess: User => Unit) {
    // TODO: refactor ? (duplication of processUser()
    validation.foreach {
      user =>
        removeUser(user)
        onSuccess(user)
    }
    sender ! validation
  }

  def userIdIsNotUsed(userId: String): DomainValidation[String] =
    readUsers.get(userId) match {
      case Some(user) => DomainError(userWithIdAlreadyExistMsg format userId).fail
      case None => userId.success
    }

  def requestActivationEmail(actionHashType: ActionHashType, user: User): DomainValidation[Boolean] = {
    emitter("mailer") sendEvent UserActivationEmailRequested(user.id, user.details.firstName, user.details.lastName, user.details.email)
    true.success
  }

  // User
  def createUser(userId: String, details: UserDetails, roles: List[ApplicationRole]): DomainValidation[User] = {
    for {
      userId <- userIdIsNotUsed(userId)
      details <- userDetailsAreValid(details)
      user <- User.create(userId, details, roles)
      actionHash <- if (user.isInactive) requestActivationEmail(ActionHashType.ACTIVATE_PASSWORD, user) else true.success
    } yield user
  }

  def updateUserDetails(userId: String, expectedVersion: Option[Long], details: UserDetails): DomainValidation[User] =
    updateUser(userId, expectedVersion) {
      user => user.updateDetails(details).flatMap(emailIsValid(_))
    }

  def removeUser(userId: String, expectedVersion: Option[Long]): DomainValidation[User] =
    readUsers.get(userId) match {
      case None => DomainError(userIdNotExistMsg format userId).fail
      case Some(user) => user.requireVersion(expectedVersion)
    }

  def updateUser[B <: User](userId: String, expectedVersion: Option[Long])(f: User => DomainValidation[B]): DomainValidation[B] =
    readUsers.get(userId) match {
      case None => DomainError(userIdNotExist format userId).fail
      case Some(user) => for {
        current <- user.requireVersion(expectedVersion)
        updated <- f(user)
      } yield updated
    }

  // Shopping list
  def createShoppingListForUser(userId: String, expectedVersion: Option[Long], shoppingListId: String, details: ShoppingListDetails): (DomainValidation[User], DomainValidation[ShoppingList]) = {
    readUsers.get(userId) match {
      case Some(user) => (updateUser(userId, expectedVersion) {
        user => user.addShoppingList(shoppingListId)
      }, createShoppingList(shoppingListId, details))
      case None => (DomainError(userIdNotExist format userId).fail, DomainError(userIdNotExist format userId).fail) // TODO refactor this
    }
  }

  def createShoppingList(shoppingListId: String, details: ShoppingListDetails): DomainValidation[ShoppingList] = {
    readShoppingLists.get(shoppingListId) match {
      case Some(shoppingList) => DomainError(shoppingListWithIdAlreadyExist format shoppingListId).fail
      case None => ShoppingList.create(shoppingListId, details)
    }
  }

  def updateShoppingListDetails(shoppingListId: String, expectedVersion: Option[Long], details: ShoppingListDetails): DomainValidation[ShoppingList] =
    updateShoppingListImpl(shoppingListId, expectedVersion) {
      shoppingList => shoppingList.updateDetails(details)
    }

  /**
   * Method should return result of validation - whether shopping list is eligable for deletion or not -> version check.
   *
   * @param shoppingListId
   * @param expectedVersion
   * @return
   */
  def removeShoppingList(userId: String, shoppingListId: String, expectedVersion: Option[Long]): DomainValidation[ShoppingList] =
    readShoppingLists.get(shoppingListId) match {
      case None => DomainError(shoppingListWithIdNotExist format shoppingListId).fail
      case Some(shoppingList) => shoppingList.requireVersion(expectedVersion) match {
        case s @ Success(_) => readUsers.get(userId) match {
          case Some(user) => user.removeShoppingList(shoppingListId) match {
            case Success(u) =>
              updateUsers(u); s
            case Failure(e) => DomainError(e).fail
          }
          case None => DomainError(userIdNotExist format userId).fail
        }
        case f @ Failure(_) => f
      }
    }

  def addShoppingListItem(shoppingListId: String, expectedVersion: Option[Long], shoppingListItem: ShoppingListItem): DomainValidation[ShoppingList] =
    updateShoppingListImpl(shoppingListId, expectedVersion) {
      shoppingList => shoppingList.addItem(shoppingListItem)
    }

  def updateShoppingListItem(shoppingListId: String, expectedVersion: Option[Long], shoppingListItem: ShoppingListItem): DomainValidation[ShoppingList] =
    updateShoppingListImpl(shoppingListId, expectedVersion) {
      shoppingList => shoppingList.updateItem(shoppingListItem)
    }

  def removeShoppingListItem(shoppingListId: String, expectedVersion: Option[Long], shoppingListItemId: String): DomainValidation[ShoppingList] =
    updateShoppingListImpl(shoppingListId, expectedVersion) {
      shoppingList => shoppingList.removeItem(shoppingListItemId)
    }

  def updateShoppingListImpl[B <: ShoppingList](shoppingListId: String, expectedVersion: Option[Long])(f: ShoppingList => DomainValidation[B]): DomainValidation[B] =
    updateShoppingList(shoppingListId, expectedVersion) {
      shoppingList =>
        shoppingList match {
          case shoppingList: ShoppingList => f(shoppingList)
        }
    }

  def updateShoppingList[B <: ShoppingList](shoppingListId: String, expectedVersion: Option[Long])(f: ShoppingList => DomainValidation[B]): DomainValidation[B] =
    readShoppingLists.get(shoppingListId) match {
      case None => DomainError(shoppingListWithIdNotExist format shoppingListId).fail
      case Some(shoppingList) => for {
        current <- shoppingList.requireVersion(expectedVersion)
        updated <- f(shoppingList)
      } yield updated
    }

  def linkTogether(event: LinkingEvent, ref: Ref[Map[CompositeShoppingListID, List[String]]])(f: (LinkingEvent, Map[CompositeShoppingListID, List[String]]) => (CompositeShoppingListID, List[String])) {
    val id: CompositeShoppingListID = event.shoppingListId
    readShoppingLists.get(id.asID) match {
      case None => sender ! DomainError(shoppingListWithIdNotExist format id.asID).fail
      case Some(shoppingList) if id.asItem.forall(shoppingList.items.get(_).isDefined) => {
        ref.single.transform(map => map + f(event, map))
        emitter sendEvent event
        sender ! shoppingList.success
      }
      case _ => sender ! DomainError(shoppingListDoesNotHaveItem format(id.asID, id.asItem)).fail
    }
  }

  def link(e: LinkingEvent, m: Map[CompositeShoppingListID, List[String]]): (CompositeShoppingListID, List[String]) = e.shoppingListId -> (e.dealId :: m.getOrElse(e.shoppingListId, List()).filterNot(_ == e.dealId))

  def unlink(e: LinkingEvent, m: Map[CompositeShoppingListID, List[String]]): (CompositeShoppingListID, List[String]) = e.shoppingListId -> m.getOrElse(e.shoppingListId, List()).filterNot(_ == e.dealId)

  private def addActionHash(actionHash: ActionHash) =
    actionHashesRef.single.transform(actionHashesMap => actionHashesMap + (actionHash.hashKey -> actionHash))

  private def updateUsers(user: User) =
    usersRef.single.transform(users => users + (user.id -> user))

  private def removeUser(user: User) = // TODO: Needs to be refactored (merge with updateShoppingLists)
    usersRef.single.transform(users => users - user.id)

  private def readUsers = usersRef.single.get

  private def readActionHashes = actionHashesRef.single.get

  private def readMerchants = merchantsRef.single.get

  private def updateUsersAndShoppingLists(user: User, shoppingList: ShoppingList) = {
    //    atomic {
    val userOpt = usersRef.single.get.get(user.id)
    val updatedUser = userOpt.map(_.addShoppingList(shoppingList.id)).get
    updatedUser match {
      case Success(newUser) => usersRef.single.transform(users => users + (user.id -> newUser))
      case Failure(_) => Nil
    }
    shoppingListsRef.single.transform(shoppingLists => shoppingLists + (shoppingList.id -> shoppingList))
    //    }
  }

  private def updateShoppingLists(shoppingList: ShoppingList) =
    shoppingListsRef.single.transform(shoppingLists => shoppingLists + (shoppingList.id -> shoppingList))

  private def removeShoppingList(shoppingList: ShoppingList) = // TODO: Needs to be refactored (merge with updateShoppingLists)
    shoppingListsRef.single.transform(shoppingLists => shoppingLists - shoppingList.id)

  private def readShoppingLists = shoppingListsRef.single.get

  def killProcessor() {
  }

}
