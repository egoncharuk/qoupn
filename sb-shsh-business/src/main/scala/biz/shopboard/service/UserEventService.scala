package biz.shopboard.service

import biz.shopboard.state.InMemoryProjection
import org.eligosource.eventsourced.core._
import biz.shopboard.domain.model.user._
import akka.pattern.ask
import akka.actor._
import concurrent.Await
import concurrent.stm._
import biz.shopboard._
import biz.shopboard.journal.{ MongodbSnapshotState, MongodbSnapshotContainer }
import domain.model.user.GetUserEventHistory
import domain.model.user.GetUserEventHistoryBySeqNr
import state.Snapshot
import org.eligosource.eventsourced.core.Message
import scala.Some
import org.eligosource.eventsourced.core.SnapshotOffer

class UserEventService(projection: ActorRef)(implicit system: ActorSystem) {

  import system.dispatcher

  implicit val timeout = akka.util.Timeout(5000)

  def getHistory(userId: String, deviceId: String): Option[List[UserEvent]] = {
    Await.result(projection ? Message(GetUserEventHistory(userId, deviceId)) map (_.asInstanceOf[Option[List[UserEvent]]]), duration)
  }

  def getHistory(userId: String, deviceId: String, lastSeqNr: Int): Option[List[UserEvent]] = {
    Await.result(projection ? Message(GetUserEventHistoryBySeqNr(userId, deviceId, lastSeqNr)) map (_.asInstanceOf[Option[List[UserEvent]]]), duration)
  }

}

case class Events(feed: List[UserEvent] = List())
case class UserEventProjectionSnapshot(key: String, count: Option[Int], state: Option[Events]) extends MongodbSnapshotState {
  def toKey = (key, count.getOrElse(0))
  def toState = (key, state.getOrElse(Events()))
}

class UserEventProjection extends Actor with InMemoryProjection[Map[String, Int], UserEvent, Map[String, Events], UserEvent] {
  def initialState = Map.empty[String, Events]
  def initialKey = Map.empty[String, Int]

  def project = {
    case (state, event) => state.get(event.userId) match {
      case Some(events) => state + (event.userId -> events.copy(feed = events.feed :+ event))
      case None => state + (event.userId -> Events(List(event)))
    }
  }
  def projectKey = {
    case (key, event) => key.get(event.userId) match {
      case Some(seqNr) => key + (event.userId -> (seqNr + 1))
      case None => key + (event.userId -> 1)
    }
  }
  override def receive = akka.event.LoggingReceive {
    case GetUserEventHistoryBySeqNr(userId, _, seqNr) => {
      currentKey.get(userId) match {
        case None => sender ! None
        case Some(someNr) => {
          val events = currentState.get(userId) match {
            case Some(e) => e.feed
            case _ => List()
          }
          sender ! Some(if (seqNr > someNr) events else events.drop(seqNr))
        }
      }
    }
    case GetUserEventHistory(userId, _) => sender ! (currentState.get(userId) match {
      case Some(events) => Some(events.feed)
      case _ => None
    })
    case msg: UserEvent => update(msg)

    case snapshotRequest @ SnapshotRequest(pid, snr, _) =>
      atomic { txn =>
        val current: Snapshot[Map[String, Int], Map[String, Events]] = snapshot.get(txn)
        snapshotRequest.process(MongodbSnapshotContainer(outer = new Iterator[MongodbSnapshotState] {
          private val keys = current.key.keys.toIterator

          def hasNext = keys.hasNext
          def next() = { val k = keys.next(); UserEventProjectionSnapshot(k, current.key.get(k), current.state.get(k)) }
        }))
      }

    case SnapshotOffer(org.eligosource.eventsourced.core.Snapshot(_, snr, time, container: MongodbSnapshotContainer)) => {
      atomic { txn =>
        container.foreach { image =>
          snapshot.single transform { snapshot =>
            val key = snapshot.key ++ image.toList.map(s => s.state.asInstanceOf[UserEventProjectionSnapshot].toKey)
            val state = snapshot.state ++ image.toList.map(s => s.state.asInstanceOf[UserEventProjectionSnapshot].toState)
            snapshot.copy(key = key, state = state)
          }
        }
      }
    }
  }
}
