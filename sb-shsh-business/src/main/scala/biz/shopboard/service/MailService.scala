package biz.shopboard.service

import net.liftweb.util.Mailer
import Mailer._

class MailServiceImpl extends MailService {

  def sendMail(fromText: String, subjectText: String, toText: String, bodyText: String) {
    Mailer.sendMail(
      From(fromText),
      Subject(subjectText),
      To(toText),
      PlainMailBodyType(bodyText))
  }

}

trait MailService {
  def sendMail(fromText: String, subjectText: String, toText: String, bodyText: String)
}

class MailServiceMock extends MailService {

  var mailsSent: Int = 0

  def resetMock() = {
    mailsSent = 0
  }

  def sendMail(fromText: String, subjectText: String, toText: String, bodyText: String) = {
    mailsSent += 1
  }

  def getSentMailsCount = { // reset is used for tests
    val value = mailsSent
    resetMock
    value
  }

}
