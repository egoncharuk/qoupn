package biz.shopboard.service

import akka.actor._
import biz.shopboard.journal.MongodbEvent
import biz.shopboard.domain._
import model.user.{ ActionHashType, ActionHash }
import org.eligosource.eventsourced.core.{ Message, Receiver }
import akka.event.Logging

case class UserActivationEmailRequested(id: String, name: String, surname: String, email: String) extends MongodbEvent
case class UserPasswordChangeEmailRequested(id: String, name: String, surname: String, email: String) extends MongodbEvent

// Commands
trait MailCommand extends Command

case class UserActivationEmailSent(actionHash: ActionHash) extends MongodbEvent
case class UserActivationEmailSendingFailed(actionHash: ActionHash) extends MongodbEvent

case class UserPasswordChangeEmailSent(actionHash: ActionHash) extends MongodbEvent
case class UserPasswordChangeEmailSendingFailed(actionHash: ActionHash) extends MongodbEvent

/**
 * Mailer gateway actor that sends emails.
 */
class MailerGateway(mailService: MailService, userProcessor: ActorRef, emailActionUrl: String = "localhost", contextPath: String = "") extends Actor with ActorLogging { this: Receiver with ActorLogging =>

  override def receive = akka.event.LoggingReceive {
    case UserActivationEmailRequested(id, name, surname, email) => {
      val actionHash = sendActivationMail(id, name, surname, email)
      userProcessor ! Message(UserActivationEmailSent(actionHash))
    }

    case UserPasswordChangeEmailRequested(id, name, surname, email) => {
      val actionHash = sendChangePasswordMail(id, name, surname, email)
      userProcessor ! Message(UserPasswordChangeEmailSent(actionHash))
    }
  }

  def generateActionHash(id: String, actionHashType: ActionHashType) = ActionHash.create(actionHashType, id)

  def sendActivationMail(id: String, name: String, surname: String, email: String): ActionHash = {
    val actionHash: ActionHash = generateActionHash(id, ActionHashType.ACTIVATE_PASSWORD)

    val eol = System.getProperty("line.separator")
    val link: String = StringBuilder.newBuilder.append("http://").append(emailActionUrl).append(contextPath).append("/activate-user?key=")
      .append(actionHash.hashKey).toString()

    val fromText = "admin@shopboard.biz"
    val subjectText = "Account activation @ Qoupn"
    val toText = email
    val bodyText: String = StringBuilder.newBuilder.append(
      "Dear ").append(name).append(",")
      .append(eol).append("Please click on activation link below to activate your account ")
      .append(eol).append("%s").append(eol).append(" Best regards, ")
      .append(eol).append(" Qoupn Team").toString format link
    log.debug("Sending an email from=%s, subject=%s, to=%s, body=%s" format (fromText, subjectText, toText, bodyText))
    mailService.sendMail(fromText, subjectText, toText, bodyText)

    actionHash
  }

  def sendChangePasswordMail(id: String, name: String, surname: String, email: String): ActionHash = {
    val actionHash: ActionHash = generateActionHash(id, ActionHashType.RESET_PASSWORD)

    val eol = System.getProperty("line.separator")
    val link: String = StringBuilder.newBuilder.append("http://").append(emailActionUrl).append(contextPath).append("/change-password?key=")
      .append(actionHash.hashKey).toString()

    val fromText = "admin@shopboard.biz"
    val subjectText = "Password change @ Qoupn"
    val toText = email
    val bodyText: String = StringBuilder.newBuilder.append(
      "Please click on link below to change your password ")
      .append(eol).append("%s").append(eol).append(" Best regards, ")
      .append(eol).append(" Qoupn Team").toString format link
    log.debug("Sending an email from=%s, subject=%s, to=%s, body=%s" format(fromText, subjectText, toText, bodyText))
    mailService.sendMail(fromText, subjectText, toText, bodyText)

    actionHash
  }
}
