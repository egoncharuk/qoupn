package biz.shopboard.service

import biz.shopboard._
import biz.shopboard.util.Hashing

import scala.concurrent.Await
import concurrent.stm.Ref

import akka.actor._
import akka.pattern.ask

import biz.shopboard.domain.model.user._
import biz.shopboard.state.TotalEmitter
import biz.shopboard.domain.model.user.CheckDeals
import org.eligosource.eventsourced.core.{ Snapshot, SnapshotOffer, SnapshotRequest, Message }
import scala.Some
import org.joda.time.DateTime

class CheckoutService(checkoutMapRef: Ref[Map[String, (String, String, DateTime)]],
    shoppingListsToDealsRef: Ref[Map[CompositeShoppingListID, List[String]]],
    shoppingListsToCouponsRef: Ref[Map[CompositeShoppingListID, List[String]]],
    couponSubscriptionProjection: ActorRef,
    dealsSubscriptionProjection: ActorRef,
    checkoutProcessor: ActorRef)(implicit system: ActorSystem) {

  import system.dispatcher

  implicit val timeout = akka.util.Timeout(5000)

  def checkoutShoppingList(userId: String, shoppingListId: String): Option[String] = {
    Await.result(checkoutProcessor ? Message(CheckoutShoppingList(userId, shoppingListId)) map (_.asInstanceOf[Option[String]]), duration)
  }

  def collectCoupons(checkoutId: String, merchantId: String): Option[List[String]] = {
    val linking: Map[CompositeShoppingListID, List[String]] = shoppingListsToCouponsRef.single.get
    val checkout: Option[(String, String, DateTime)] = checkoutMapRef.single.get.get(checkoutId)
    checkout collect {
      case c if c._3.isAfterNow =>
        linking.get(c._2).filterNot(_.isEmpty).map(l => Await.result(couponSubscriptionProjection ?
          Message(CheckCoupons(c._1, Some(merchantId))) map (_.asInstanceOf[Option[List[String]]]), duration).toList.flatten.filter(l.contains(_))).toList.distinct.flatten
    }
  }

  def collectDeals(checkoutId: String, merchantId: String): Option[List[String]] = {
    val linking: Map[CompositeShoppingListID, List[String]] = shoppingListsToDealsRef.single.get
    val checkout: Option[(String, String, DateTime)] = checkoutMapRef.single.get.get(checkoutId)
    checkout collect {
      case c if c._3.isAfterNow =>
        linking.get(c._2).filterNot(_.isEmpty).map(l => Await.result(dealsSubscriptionProjection ?
          Message(CheckDeals(c._1, Some(merchantId))) map (_.asInstanceOf[Option[List[String]]]), duration).toList.flatten.filter(l.contains(_))).toList.distinct.flatten
    }
  }

}

case class CheckoutProcessorState(checkoutMap: Map[String, (String, String, DateTime)])

class CheckoutProcessor(checkoutMapRef: Ref[Map[String, (String, String, DateTime)]]) extends Actor {
  this: TotalEmitter =>
  val checkoutExpirationMinutes: Int = 30

  import scala.concurrent.stm._

  override def receive = akka.event.LoggingReceive {
    case CheckoutShoppingList(userId, shoppingListId) => {
      val id: String = Hashing.generateQuasiRandom
      checkoutMapRef.single.transform(checkoutMap => checkoutMap + (id -> (userId, shoppingListId, new DateTime().plusMinutes(checkoutExpirationMinutes))))
      emitter sendEvent ShoppingListCheckedOut(userId, shoppingListId)
      sender ! Some(id)
    }

    case snapshotRequest @ SnapshotRequest(pid, snr, _) => {
      atomic { txn =>
        val state = CheckoutProcessorState(checkoutMapRef.get(txn))
        snapshotRequest.process(state)
      }
    }

    case SnapshotOffer(Snapshot(_, snr, time, state: CheckoutProcessorState)) => {
      atomic { txn =>
        checkoutMapRef.set(state.checkoutMap)(txn)
      }
    }
  }
}

