package biz.shopboard.service

import scala.concurrent.stm._

import akka.actor._
import akka.pattern.ask

import scala.concurrent.Future
import biz.shopboard.domain._
import model.common.ProductInfo
import model.merchant._
import biz.shopboard.domain._
import biz.shopboard.state.TotalEmitter
import biz.shopboard.infrastructure.ValidationMessages
import model.merchant.ActivateCampaign
import model.merchant.ActiveCampaign
import model.merchant.AddCoupon
import model.merchant.AddCouponProductInfo
import model.merchant.AddDeal
import model.merchant.AddDealProductInfo
import model.merchant.CampaignActivated
import model.merchant.CampaignCreated
import model.merchant.CampaignDetails
import model.merchant.CampaignDetailsUpdated
import model.merchant.CampaignRemoved
import model.merchant.CouponActivated
import model.merchant.CouponAdded
import model.merchant.CouponCreated
import model.merchant.CouponDetails
import model.merchant.CouponDetailsUpdated
import model.merchant.CouponDistributed
import model.merchant.CouponProductInfoUpdated
import model.merchant.CouponRemoved
import model.merchant.CouponsDistributed
import model.merchant.CreateCampaign
import model.merchant.CreateCoupon
import model.merchant.CreateDeal
import model.merchant.CreateMerchant
import model.merchant.DealActivated
import model.merchant.DealAdded
import model.merchant.DealCreated
import model.merchant.DealDetails
import model.merchant.DealDetailsUpdated
import model.merchant.DealDistributed
import model.merchant.DealProductInfoUpdated
import model.merchant.DealRemoved
import model.merchant.DealsDistributed
import model.merchant.DistributeCoupon
import model.merchant.DistributeCoupons
import model.merchant.DistributeDeal
import model.merchant.DistributeDeals
import model.merchant.MerchantCreated
import model.merchant.MerchantDetails
import model.merchant.MerchantDetailsUpdated
import model.merchant.MerchantRemoved
import model.merchant.RemoveCampaign
import model.merchant.RemoveCoupon
import model.merchant.RemoveCouponProductInfo
import model.merchant.RemoveDeal
import model.merchant.RemoveDealProductInfo
import model.merchant.RemoveMerchant
import model.merchant.UpdateCampaignDetails
import model.merchant.UpdateCouponDetails
import model.merchant.UpdateCouponProductInfo
import model.merchant.UpdateDealDetails
import model.merchant.UpdateDealProductInfo
import model.merchant.UpdateMerchantDetails
import scalaz._
import Scalaz._
import org.eligosource.eventsourced.core.SnapshotRequest

import scala.concurrent.ExecutionContext.Implicits.global
import scalaz.Failure
import scala.Some
import org.eligosource.eventsourced.core.Snapshot
import org.eligosource.eventsourced.core.SnapshotOffer
import org.eligosource.eventsourced.core.Message
import scalaz.Success

class MerchantService(
    merchantsRef: Ref[Map[String, Merchant]],
    campaignsRef: Ref[Map[String, Campaign]],
    couponsRef: Ref[Map[String, Deal]],
    dealsRef: Ref[Map[String, Deal]],
    merchantProcessor: ActorRef)(implicit system: ActorSystem) {

  //
  // Consistent reads
  //

  def getMerchantsMap = merchantsRef.single.get
  def getMerchant(merchantId: String): Option[Merchant] = getMerchantsMap.get(merchantId)
  def getMerchants: Iterable[Merchant] = getMerchantsMap.values

  def getCampaignsMap = campaignsRef.single.get
  def getCampaign(campaignId: String): Option[Campaign] = getCampaignsMap.get(campaignId)
  def getCampaigns: Iterable[Campaign] = getCampaignsMap.values

  def getDealsMap = dealsRef.single.get
  def getDeal(dealId: String): Option[Deal] = getDealsMap.get(dealId)
  def getDeals: Iterable[Deal] = getDealsMap.values

  def getCouponsMap = couponsRef.single.get
  def getCoupon(couponId: String): Option[Deal] = getCouponsMap.get(couponId)
  def getCoupons: Iterable[Deal] = getCouponsMap.values

  def getAllProductInfo(merchantId: String): Iterable[ProductInfo] = {
    //    getMerchant(merchantId).toList.flatMap(p => p.campaignIds.flatMap(id => getCampaign(id).toList.flatMap(c =>
    //      c.dealIds.flatMap(id2 => getDeal(id2).toList.flatMap(d => d.details.productInfo)) :::
    //        c.couponIds.flatMap(id2 => getCoupon(id2).toList.flatMap(d => d.details.productInfo)))))
    for {
      merchant <- getMerchant(merchantId).toList
      campaignId <- merchant.campaignIds
      campaign <- getCampaign(campaignId).toList

      dealId <- campaign.dealIds
      deal <- getDeal(dealId).toList
      productInfoD <- deal.details.productInfo

      couponId <- campaign.couponIds
      coupon <- getCoupon(couponId).toList
      productInfoC <- coupon.details.productInfo

      iterable <- List(productInfoD) ::: List(productInfoC)
    } yield iterable
  }

  def searchDeals(data: String, info: Option[List[ProductInfo]] = None, merchantIds: Option[List[String]] = None): Iterable[(Merchant, Deal)] = {
    //    getMerchants.flatMap(p => p.campaignIds.flatMap(id => getCampaign(id).toList.flatMap(c =>
    //      c.dealIds.flatMap(id2 => getDeal(id2).collect({ case d: ActiveDeal => d }).toList).filter(search(data, info)).map((p, _)))))
    for {
      merchant <- getMerchants
      campaignId <- merchant.campaignIds if merchantIds.isEmpty || merchantIds.exists(ids => ids.isEmpty || ids.contains(merchant.id))
      campaign <- getCampaign(campaignId).toList
      dealId <- campaign.dealIds
      deals <- getDeal(dealId).collect({ case d: ActiveDeal => d }).toList.filter(search(data, info)).map((merchant, _))
    } yield deals
  }

  def searchCoupons(data: String, info: Option[List[ProductInfo]] = None, merchantIds: Option[List[String]] = None): Iterable[(Merchant, Deal)] = {
    //    getMerchants.flatMap(p => p.campaignIds.flatMap(id => getCampaign(id).toList.flatMap(c =>
    //      c.couponIds.flatMap(id2 => getCoupon(id2).collect({ case c: ActiveCoupon => c }).toList).filter(search(data, info)).map((p, _)))))
    for {
      merchant <- getMerchants
      campaignId <- merchant.campaignIds if merchantIds.isEmpty || merchantIds.exists(ids => ids.isEmpty || ids.contains(merchant.id))
      campaign <- getCampaign(campaignId).toList
      couponId <- campaign.couponIds
      coupons <- getCoupon(couponId).collect({ case d: ActiveCoupon => d }).toList.filter(search(data, info)).map((merchant, _))
    } yield coupons
  }

  private def search(data: String, info: Option[List[ProductInfo]]): (Deal) => Boolean = { d =>
    d.details.name.toLowerCase.contains(data.toLowerCase) ||
      d.details.description.toLowerCase.contains(data.toLowerCase) ||
      info.exists(!_.intersect(d.details.productInfo).isEmpty)
  }

  //
  // Updates
  //

  implicit val timeout = akka.util.Timeout(5000)

  ///////////////////////////////////////////////////////////////////
  // Merchant
  ///////////////////////////////////////////////////////////////////

  // Merchant services - sending events & waiting for answer which is wrapped into Future
  def createMerchant(userId: String, merchantId: String, merchantDetails: MerchantDetails): Future[DomainValidation[Merchant]] =
    merchantProcessor ? Message(CreateMerchant(userId, merchantId, merchantDetails)) map (_.asInstanceOf[DomainValidation[Merchant]])

  def createMerchant(merchant: Merchant): Future[DomainValidation[Merchant]] =
    merchantProcessor ? Message(CreateMerchant(merchant.adminId, merchant.id, merchant.details)) map (_.asInstanceOf[DomainValidation[Merchant]])

  def updateMerchantDetails(merchantId: String, expectedVersion: Option[Long], merchantDetails: MerchantDetails): Future[DomainValidation[Merchant]] =
    merchantProcessor ? Message(UpdateMerchantDetails(merchantId, expectedVersion, merchantDetails)) map (_.asInstanceOf[DomainValidation[Merchant]])

  def removeMerchant(merchantId: String, expectedVersion: Option[Long]): Future[DomainValidation[Merchant]] =
    merchantProcessor ? Message(RemoveMerchant(merchantId, expectedVersion)) map (_.asInstanceOf[DomainValidation[Merchant]])

  def merchantExists(merchantName: String): Boolean = filterMerchantsBy(merchantName).nonEmpty

  def findMerchantBy(merchantName: String): Option[Merchant] = filterMerchantsBy(merchantName) match {
    case head :: tail => Some(head)
    case _ => None
  }

  def findMerchantByAdmin(adminId: String): Option[Merchant] = filterMerchantsByAdmin(adminId) match {
    case head :: tail => Some(head)
    case _ => None
  }

  private def filterMerchantsBy(merchantName: String) = getMerchants.filter(_.details.name.equals(merchantName))
  private def filterMerchantsByAdmin(adminId: String) = getMerchants.filter(_.adminId.equals(adminId))

  ///////////////////////////////////////////////////////////////////
  // Campaign
  ///////////////////////////////////////////////////////////////////

  // Campaign services - sending events & waiting for answer which is wrapped into Future
  def createCampaign(merchantId: String, expectedVersion: Option[Long], campaignId: String, campaignDetails: CampaignDetails): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(CreateCampaign(merchantId, expectedVersion, campaignId, campaignDetails)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def updateCampaignDetails(merchantId: String, campaignId: String, expectedVersion: Option[Long], campaignDetails: CampaignDetails): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(UpdateCampaignDetails(merchantId, campaignId, expectedVersion, campaignDetails)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def removeCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(RemoveCampaign(merchantId, campaignId, expectedVersion)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def activateCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(ActivateCampaign(merchantId, campaignId, expectedVersion)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def deactivateCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(DeactivateCampaign(merchantId, campaignId, expectedVersion)) map (_.asInstanceOf[DomainValidation[Campaign]])

  ///////////////////////////////////////////////////////////////////
  // Deal & Coupon
  ///////////////////////////////////////////////////////////////////

  // Deal services - sending events & waiting for answer which is wrapped into Future
  def createDeal(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String, dealDetails: DealDetails): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(CreateDeal(merchantId, campaignId, expectedVersion, dealId, dealDetails)) map (_.asInstanceOf[DomainValidation[Deal]])

  def addDeal(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(AddDeal(merchantId, campaignId, expectedVersion, dealId)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def removeDeal(merchantId: String, campaignId: String, campaignVersion: Option[Long], dealId: String, dealVersion: Option[Long]): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(RemoveDeal(merchantId, campaignId, campaignVersion, dealId, dealVersion)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def updateDealDetails(merchantId: String, campaignId: String, dealId: String, expectedVersion: Option[Long], dealDetails: DealDetails): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(UpdateDealDetails(merchantId, campaignId, dealId, expectedVersion, dealDetails)) map (_.asInstanceOf[DomainValidation[Deal]])

  def updateDealProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: List[ProductInfo]): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(UpdateDealProductInfo(merchantId, dealId, expectedVersion, productInfo)) map (_.asInstanceOf[DomainValidation[Deal]])

  def addDealProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(AddDealProductInfo(merchantId, dealId, expectedVersion, productInfo)) map (_.asInstanceOf[DomainValidation[Deal]])

  def removeDealProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(RemoveDealProductInfo(merchantId, dealId, expectedVersion, productInfo)) map (_.asInstanceOf[DomainValidation[Deal]])

  // Coupon services - sending events & waiting for answer which is wrapped into Future
  def updateCouponProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: List[ProductInfo]): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(UpdateCouponProductInfo(merchantId, dealId, expectedVersion, productInfo)) map (_.asInstanceOf[DomainValidation[Deal]])

  def addCouponProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(AddCouponProductInfo(merchantId, dealId, expectedVersion, productInfo)) map (_.asInstanceOf[DomainValidation[Deal]])

  def removeCouponProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(RemoveCouponProductInfo(merchantId, dealId, expectedVersion, productInfo)) map (_.asInstanceOf[DomainValidation[Deal]])

  def addCoupon(merchantId: String, campaignId: String, expectedVersion: Option[Long], couponId: String): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(AddCoupon(merchantId, campaignId, expectedVersion, couponId)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def removeCoupon(merchantId: String, campaignId: String, campaignVersion: Option[Long], couponId: String, couponVersion: Option[Long]): Future[DomainValidation[Campaign]] =
    merchantProcessor ? Message(RemoveCoupon(merchantId, campaignId, campaignVersion, couponId, couponVersion)) map (_.asInstanceOf[DomainValidation[Campaign]])

  def updateCouponDetails(merchantId: String, campaignId: String, dealId: String, expectedVersion: Option[Long], couponDetails: CouponDetails): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(UpdateCouponDetails(merchantId, campaignId, dealId, expectedVersion, couponDetails)) map (_.asInstanceOf[DomainValidation[Deal]])

  def createCoupon(merchantId: String, campaignId: String, expectedVersion: Option[Long], couponId: String, dealDetails: DealDetails, count: Long): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(CreateCoupon(merchantId, campaignId, expectedVersion, couponId, dealDetails, count)) map (_.asInstanceOf[DomainValidation[Deal]])

  def distributeDeal(merchantId: String, userId: String, dealId: String): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(DistributeDeal(merchantId, userId, dealId)) map (_.asInstanceOf[DomainValidation[Deal]])

  def distributeCoupon(merchantId: String, userId: String, couponId: String): Future[DomainValidation[Deal]] =
    merchantProcessor ? Message(DistributeCoupon(merchantId, userId, couponId)) map (_.asInstanceOf[DomainValidation[Deal]])

}

// -------------------------------------------------------------------------------------------------------------
//  MerchantProcessor is single writer to MerchantsRef & CampaignsRef, so we can have reads and writes in separate transactions
// -------------------------------------------------------------------------------------------------------------

object MerchantProcessor extends ValidationMessages

case class MerchantProcessorState(merchantsMap: Map[String, Merchant],
  campaignsMap: Map[String, Campaign],
  couponsMap: Map[String, Deal],
  dealsMap: Map[String, Deal])

class MerchantProcessor(
    merchantsRef: Ref[Map[String, Merchant]],
    campaignsRef: Ref[Map[String, Campaign]],
    couponsRef: Ref[Map[String, Deal]],
    dealsRef: Ref[Map[String, Deal]]) extends Actor with ActorLogging { this: TotalEmitter with ActorLogging =>

  import MerchantProcessor._
  import biz.shopboard.domain._

  override def preStart = {
    log.info("Starting actor MerchantProcessor. Instance hashcode=" + this.hashCode)
  }

  override def postStop = {
    log.info("Stopping actor MerchantProcessor. Instance hashcode=" + this.hashCode)
  }

  override def receive = akka.event.LoggingReceive {
    //Merchant
    case CreateMerchant(userId, merchantId, merchantDetails) =>
      processMerchant(createMerchant(userId, merchantId, merchantDetails)) { merchant =>
        emitter sendEvent MerchantCreated(userId, merchantId, merchantDetails)
      }
    case UpdateMerchantDetails(merchantId, expectedVersion, merchantDetails) =>
      processMerchant(updateMerchantDetails(merchantId, expectedVersion, merchantDetails)) { merchant =>
        emitter sendEvent MerchantDetailsUpdated(merchantId, merchantDetails)
      }
    case RemoveMerchant(merchantId, expectedVersion) =>
      processMerchantRemoval(removeMerchant(merchantId, expectedVersion)) { merchant =>
        emitter sendEvent MerchantRemoved(merchantId)
      }
    case DistributeDeals(merchantId, userId) =>
      distributeDeals(merchantId, userId).foreach {
        _ => emitter sendEvent DealsDistributed(merchantId, userId)
      }
    case DistributeCoupons(merchantId, userId) =>
      distributeCoupons(merchantId, userId).foreach {
        _ => emitter sendEvent CouponsDistributed(merchantId, userId)
      }
    //Campaign
    case CreateCampaign(merchantId, expectedVersion, campaignId, campaignDetails) =>

      def merchantExists(merchantId: String): DomainValidation[Merchant] =
        readMerchants.get(merchantId) match {
          case Some(merchant) => updateMerchant(merchantId, expectedVersion) { merchant => merchant.addCampaign(campaignId) }
          case None => DomainError(merchantIdNotExistMsg format merchantId).fail
        }
      def campaignNotExists(campaignId: String): DomainValidation[Campaign] = readCampaigns.get(campaignId) match {
        case Some(campaign) => DomainError(campaignIdAlreadyExistMsg format campaignId).fail
        case None => createCampaign(campaignId, campaignDetails)
      }

      val merchantValidation = merchantExists(merchantId)
      val campaignValidation = campaignNotExists(campaignId)

      merchantValidation match {
        case Success(merchant) => campaignValidation match {
          case Success(campaign) => {
            updateMerchantsAndCampaigns(merchant, campaign)
            // now call onSuccess
            emitter sendEvent CampaignCreated(merchantId, campaignId, campaignDetails)
          }
          case Failure(_) => Unit
        }
        case Failure(_) => Unit
      }

      (merchantValidation, campaignValidation) match {
        case (Success(merchant), Success(campaign)) => { sender ! Success(campaign) }
        case (Success(_), Failure(listFail)) => { sender ! Failure(listFail) }
        case (Failure(merchantFail), Success(_)) => { sender ! Failure(merchantFail) }
        case (Failure(merchantFail), Failure(listFail)) => { sender ! DomainError(merchantFail.toString :: listFail).fail }
      }

    case UpdateCampaignDetails(merchantId, campaignId, expectedVersion, campaignDetails) =>
      processCampaign(updateCampaignDetails(campaignId, expectedVersion, campaignDetails)) { campaign =>
        emitter sendEvent CampaignDetailsUpdated(merchantId, campaignId, campaignDetails)
      }
    case RemoveCampaign(merchantId, campaignId, expectedVersion) =>
      processCampaignRemoval(removeCampaign(merchantId, campaignId, expectedVersion)) { campaign =>
        emitter sendEvent CampaignRemoved(merchantId, campaignId)
      }
    case ActivateCampaign(merchantId, campaignId, expectedVersion) =>
      processCampaign(activateCampaign(merchantId, campaignId, expectedVersion)) { campaign =>
        campaign match {
          case ActiveCampaign(_, false) => emitter sendEvent CampaignActivated(merchantId, campaignId)
          case ActiveCampaign(_, true) => emitter sendEvent CampaignReactivated(merchantId, campaignId)
        }
      }
    case DeactivateCampaign(merchantId, campaignId, expectedVersion) =>
      processCampaign(deactivateCampaign(merchantId, campaignId, expectedVersion)) { campaign =>
        emitter sendEvent CampaignDeactivated(merchantId, campaignId)
      }
    case AddDeal(merchantId, campaignId, expectedVersion, dealId) =>
      addDeal(merchantId, campaignId, expectedVersion, dealId)
    case RemoveDeal(merchantId, campaignId, campaignVersion, dealId, dealVersion) =>
      processCampaign(removeDeal(campaignId, campaignVersion, dealId, dealVersion)) { campaign =>
        emitter sendEvent DealRemoved(merchantId, campaignId, dealId)
      }
    case UpdateDealDetails(merchantId, campaignId, dealId, expectedVersion, dealDetails) =>
      processDeal(updateDeal(Some(campaignId), dealId, expectedVersion) { d => d.updateDetails(dealDetails) }) { deal =>
        emitter sendEvent DealDetailsUpdated(merchantId, dealId, dealDetails)
      }
    case UpdateDealProductInfo(merchantId, dealId, expectedVersion, productInfo) =>
      val previousInfo = getPreviousProductInfo(readDeals, dealId)
      processDeal(updateDeal(None, dealId, expectedVersion)(updateDetails(d => d.updateProductInfo(productInfo)))) { deal =>
        emitter sendEvent DealProductInfoUpdated(merchantId, dealId, previousInfo, deal.details.productInfo)
      }
    case AddDealProductInfo(merchantId, dealId, expectedVersion, productInfo) =>
      val previousInfo = getPreviousProductInfo(readDeals, dealId)
      processDeal(updateDeal(None, dealId, expectedVersion)(updateDetails(d => d.addProductInfo(productInfo)))) { deal =>
        emitter sendEvent DealProductInfoUpdated(merchantId, dealId, previousInfo, deal.details.productInfo)
      }
    case RemoveDealProductInfo(merchantId, dealId, expectedVersion, productInfo) =>
      val previousInfo = getPreviousProductInfo(readDeals, dealId)
      processDeal(updateDeal(None, dealId, expectedVersion)(updateDetails(d => d.removeProductInfo(productInfo)))) { deal =>
        emitter sendEvent DealProductInfoUpdated(merchantId, dealId, previousInfo, deal.details.productInfo)
      }
    case AddCoupon(merchantId, campaignId, expectedVersion, couponId) =>
      addCoupon(merchantId, campaignId, expectedVersion, couponId)
    case RemoveCoupon(merchantId, campaignId, campaignVersion, couponId, couponVersion) =>
      processCampaign(removeCoupon(campaignId, campaignVersion, couponId, couponVersion)) { campaign =>
        emitter sendEvent CouponRemoved(merchantId, campaignId, couponId)
      }
    case UpdateCouponDetails(merchantId, campaignId, dealId, expectedVersion, couponDetails) =>
      processCoupon(updateCoupon(Some(campaignId), dealId, expectedVersion)(c => c.asInstanceOf[DraftCoupon].updateCouponDetails(couponDetails))) { deal =>
        emitter sendEvent CouponDetailsUpdated(merchantId, dealId, couponDetails)
      }
    case UpdateCouponProductInfo(merchantId, dealId, expectedVersion, productInfo) =>
      val previousInfo = getPreviousProductInfo(readCoupons, dealId)
      processCoupon(updateCoupon(None, dealId, expectedVersion)(updateDetails(d => d.updateProductInfo(productInfo)))) { deal =>
        emitter sendEvent CouponProductInfoUpdated(merchantId, dealId, previousInfo, deal.details.productInfo)
      }
    case AddCouponProductInfo(merchantId, dealId, expectedVersion, productInfo) =>
      val previousInfo = getPreviousProductInfo(readCoupons, dealId)
      processCoupon(updateCoupon(None, dealId, expectedVersion)(updateDetails(d => d.addProductInfo(productInfo)))) { deal =>
        emitter sendEvent CouponProductInfoUpdated(merchantId, dealId, previousInfo, deal.details.productInfo)
      }
    case RemoveCouponProductInfo(merchantId, dealId, expectedVersion, productInfo) =>
      val previousInfo = getPreviousProductInfo(readCoupons, dealId)
      processCoupon(updateCoupon(None, dealId, expectedVersion)(updateDetails(d => d.removeProductInfo(productInfo)))) { deal =>
        emitter sendEvent CouponProductInfoUpdated(merchantId, dealId, previousInfo, deal.details.productInfo)
      }
    case CreateDeal(merchantId, campaignId, expectedVersion, dealId, dealDetails) =>
      processDeal(createDeal(merchantId, campaignId, expectedVersion, dealId, dealDetails)) { deal =>
        emitter sendEvent DealCreated(merchantId, campaignId, dealId, dealDetails)
      }
    case CreateCoupon(merchantId, campaignId, expectedVersion, couponId, dealDetails, count) =>
      processCoupon(createCoupon(merchantId, campaignId, expectedVersion, couponId, dealDetails, count)) { deal =>
        emitter sendEvent CouponCreated(merchantId, campaignId, couponId, dealDetails, count)
      }
    case DistributeDeal(merchantId, userId, dealId) => sender ! distributeDeal(merchantId, userId, dealId)
    case DistributeCoupon(merchantId, userId, couponId) => sender ! distributeCoupon(merchantId, userId, couponId)

    case snapshotRequest @ SnapshotRequest(pid, snr, _) => {
      atomic { txn =>
        val state = MerchantProcessorState(merchantsRef.get(txn), campaignsRef.get(txn), couponsRef.get(txn), dealsRef.get(txn))
        snapshotRequest.process(state)
      }
    }

    case SnapshotOffer(Snapshot(_, snr, time, state: MerchantProcessorState)) => {
      atomic { txn =>
        merchantsRef.set(state.merchantsMap)(txn)
        campaignsRef.set(state.campaignsMap)(txn)
        couponsRef.set(state.couponsMap)(txn)
        dealsRef.set(state.dealsMap)(txn)
      }
    }
  }

  ///////////////////////////////////////////////////////////////////
  // Validations
  ///////////////////////////////////////////////////////////////////

  def logValidation(validation: DomainValidation[_]) = validation match {
    case Failure(e) => e.foreach(log.error(_))
    case _ =>
  }

  def merchantExists(merchant: Merchant): DomainValidation[Merchant] = {
    readMerchants.values.filter(_.adminId.equals(merchant.adminId)) match {
      case head :: tail => DomainError(merchantWithAdminIdAlreadyExistMsg format merchant.adminId).fail
      case _ => merchant.success
    }
  }

  def isPeriodValid(campaign: Campaign): DomainValidation[Campaign] = {
    campaign.details.valid_from.compareTo(campaign.details.valid_to) match {
      case 1 => DomainError(invalid_date_range).fail
      case _ => isPeriodValidWrtDeals(campaign) && isPeriodValidWrtDeals(campaign, checkCoupons = true)
    }
  }

  def isPeriodValidWrtDeals(campaign: Campaign, checkCoupons: Boolean = false): DomainValidation[Campaign] = {
    val ids = checkCoupons ? campaign.couponIds | campaign.dealIds
    val inner = checkCoupons ? readCoupons | readDeals
    ids forall (inner.get(_) match {
      case Some(deal) => deal.details.validTo.compareTo(campaign.details.valid_to) <= 0 &&
        campaign.details.valid_from.compareTo(deal.details.validFrom) <= 0
      case None => true
    }) match {
      case false => DomainError(checkCoupons ? invalid_date_range_wrt_coupons | invalid_date_range_wrt_deals).fail
      case true => campaign.success
    }
  }

  def isPeriodValid(deal: Deal): DomainValidation[Deal] = {
    deal.details.validFrom.compareTo(deal.details.validTo) match {
      case 1 => DomainError(invalid_date_range).fail
      case _ => deal.success
    }
  }

  def isPeriodValid(deal: Deal, campaignId: Option[String]): DomainValidation[Deal] = {
    if (campaignId.isEmpty) return deal.success
    readCampaigns.get(campaignId.get) match {
      case Some(campaign) => {
        campaign.details.valid_from.compareTo(deal.details.validFrom) match {
          case 1 => DomainError(date_range_out_of_campaign_date_range).fail
          case _ => deal.details.validTo.compareTo(campaign.details.valid_to) match {
            case 1 => DomainError(date_range_out_of_campaign_date_range).fail
            case _ => deal.success
          }
        }
      }
      case _ => DomainError(campaignWithIdNotExist format campaignId).fail
    }
  }

  /**
   * For passed in ''domainValidation[Campaign]''
   * 1) Add/Update campaign contained in the campaigns map with newly passed one
   * 2) run onSuccess function
   * 3) send validation result to sender actor
   *
   * Used for any operation that involves campaign version update :
   * any type of create/update related to Campaign and
   * any operations (create/update/delete) related to Campaign's Deal
   *
   * @param validation
   * @param onSuccess
   * @return
   */
  def processCampaign(validation: DomainValidation[Campaign], sendReplay: Boolean = true)(onSuccess: Campaign => Unit) {
    validation.foreach { campaign =>
      updateCampaigns(campaign)
      onSuccess(campaign)
    }
    logValidation(validation)
    if (sendReplay) sender ! validation
  }

  def processMerchant(validation: DomainValidation[Merchant], sendReplay: Boolean = true)(onSuccess: Merchant => Unit) {
    validation.foreach { merchant =>
      updateMerchants(merchant)
      onSuccess(merchant)
    }
    logValidation(validation)
    if (sendReplay) sender ! validation
  }

  def processDeal(validation: DomainValidation[Deal], sendReplay: Boolean = true)(onSuccess: Deal => Unit) {
    validation.foreach { deal =>
      updateDeals(deal)
      onSuccess(deal)
    }
    logValidation(validation)
    if (sendReplay) sender ! validation
  }

  def processCoupon(validation: DomainValidation[Deal], sendReplay: Boolean = true)(onSuccess: Deal => Unit) {
    validation.foreach { coupon =>
      updateCoupons(coupon)
      onSuccess(coupon)
    }
    logValidation(validation)
    if (sendReplay) sender ! validation
  }

  /**
   * For passed in ''domainValidation[Campaign]''
   * 1) Remove Campaign contained in the campaigns map.
   * 2) run onSuccess
   * 3) send validation result to sender actor
   *
   * Used only for deletion of whole campaign list as shopping list item removal
   * is regular update.
   *
   * @param validation
   * @param onSuccess
   * @return
   */
  def processCampaignRemoval(validation: DomainValidation[Campaign])(onSuccess: Campaign => Unit) {
    validation.foreach { campaign =>
      removeCampaign(campaign)
      onSuccess(campaign)
    }
    logValidation(validation)
    sender ! validation
  }

  def processMerchantRemoval(validation: DomainValidation[Merchant])(onSuccess: Merchant => Unit) {
    validation.foreach { merchant =>
      removeMerchant(merchant)
      onSuccess(merchant)
    }
    logValidation(validation)
    sender ! validation
  }

  // Merchant
  def createMerchant(userId: String, merchantId: String, details: MerchantDetails): DomainValidation[Merchant] = {
    readMerchants.get(merchantId) match {
      case Some(merchant) => DomainError(merchantWithIdAlreadyExistMsg format merchantId).fail
      case None => {
        val validation = Merchant.create(merchantId, userId, details).flatMap(merchantExists(_))
        if (validation.isSuccess) details.write()
        validation
      }
    }
  }

  def updateMerchantDetails(merchantId: String, expectedVersion: Option[Long], details: MerchantDetails): DomainValidation[Merchant] =
    updateMerchant(merchantId, expectedVersion) { merchant => merchant.updateDetails(details) }

  def removeMerchant(merchantId: String, expectedVersion: Option[Long]): DomainValidation[Merchant] =
    readMerchants.get(merchantId) match {
      case None => DomainError(merchantIdNotExistMsg format merchantId).fail
      case Some(merchant) => merchant.requireVersion(expectedVersion)
    }

  def updateMerchant[B <: Merchant](merchantId: String, expectedVersion: Option[Long])(f: Merchant => DomainValidation[B]): DomainValidation[B] =
    readMerchants.get(merchantId) match {
      case None => DomainError(merchantIdNotExits format merchantId).fail
      case Some(merchant) => for {
        current <- merchant.requireVersion(expectedVersion)
        updated <- f(merchant)
      } yield updated
    }

  def createCampaign(campaignId: String, details: CampaignDetails): DomainValidation[Campaign] = {
    readCampaigns.get(campaignId) match {
      case Some(campaign) => DomainError(campaignWithIdAlreadyExist format campaignId).fail
      case None => DraftCampaign.create(campaignId, details).flatMap(isPeriodValid(_))
    }
  }

  def updateCampaignDetails(campaignId: String, expectedVersion: Option[Long], details: CampaignDetails): DomainValidation[Campaign] =
    updateCampaignImpl(campaignId, expectedVersion) { campaign =>
      campaign match {
        case c: DraftCampaign => c.updateDetails(details).flatMap(isPeriodValid(_))
        case c: InactiveCampaign => c.asDraft.updateDetails(details) match {
          case Success(draft) => InactiveCampaign.create(draft).flatMap(isPeriodValid(_))
          case f @ Failure(_) => f
        }
        case _ => campaign.success
      }
    }

  def distribute(merchantId: String)(f: (Merchant, Campaign) => DomainValidation[Merchant]): DomainValidation[Merchant] = readMerchants.get(merchantId) match {
    case Some(merchant) => merchant.campaignIds.foldRight[DomainValidation[Merchant]](merchant.success)((cid, validation) => validation && (readCampaigns.get(cid) match {
      case Some(campaign) => f(merchant, campaign)
      case None => DomainError(campaignWithIdNotExist format cid).fail
    }))
    case None => DomainError(merchantIdNotExistMsg format merchantId).fail
  }

  def distributeDeal(merchantId: String, userId: String, dealId: String) = { //TODO: add merchant & user validation
    readDeals.get(dealId) match {
      case Some(deal: ActiveDeal) =>
        emitter sendEvent DealDistributed(merchantId, "", deal.id, userId); deal.success
      case Some(_) => DomainError(cannotDistributeNonActiveDeal format dealId).fail
      case None => DomainError(dealWithIdNotExist format dealId).fail
    }
  }

  def distributeCoupon(merchantId: String, userId: String, couponId: String) = { //TODO: add merchant & user validation
    readCoupons.get(couponId) match {
      case Some(coupon: ActiveCoupon) =>
        emitter sendEvent CouponDistributed(merchantId, "", coupon.id, userId); coupon.success
      case Some(_) => DomainError(cannotDistributeNonActiveCoupon format couponId).fail
      case None => DomainError(couponWithIdNotExist format couponId).fail
    }
  }

  def distributeDeals(merchantId: String, userId: String) = { //TODO: add user validation
    distribute(merchantId) { (merchant, campaign) =>
      campaign.dealIds.foldRight[DomainValidation[Merchant]](merchant.success)((did, validation) => validation && (readDeals.get(did) match {
        case Some(deal: ActiveDeal) =>
          emitter sendEvent DealDistributed(merchantId, campaign.id, did, userId); merchant.success
        case None => DomainError(dealWithIdNotExist format did).fail
        case Some(_) => merchant.success
      }))
    }
  }

  def distributeCoupons(merchantId: String, userId: String) = { //TODO: add user validation
    distribute(merchantId) { (merchant, campaign) =>
      campaign.couponIds.foldRight[DomainValidation[Merchant]](merchant.success)((did, validation) => validation && (readCoupons.get(did) match {
        case Some(deal: ActiveCoupon) =>
          emitter sendEvent CouponDistributed(merchantId, campaign.id, did, userId); merchant.success
        case None => DomainError(couponWithIdNotExist format did).fail
        case Some(_) => merchant.success
      }))
    }
  }

  def deactivateCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]): DomainValidation[Campaign] =
    updateCampaignImpl(campaignId, expectedVersion) { campaign =>
      campaign match {
        case c: ActiveCampaign => {
          //TODO: notify sender if deal/coupon is not found in Ref
          c.dealIds map { id =>
            processDeal(readDeals.get(id) match {
              case None => DomainError(dealWithIdNotExist format id).fail
              case Some(deal: ActiveDeal) => InactiveDeal.create(deal)
              case Some(_) => DomainError(cannotDeactivateNonActiveDeal format id).fail
            }, sendReplay = false) { deal =>
              emitter sendEvent DealDeactivated(merchantId, campaignId, id)
            }
          }
          c.couponIds map { id =>
            processCoupon(readCoupons.get(id) match {
              case None => DomainError(dealWithIdNotExist format id).fail
              case Some(coupon: ActiveCoupon) => InactiveCoupon.create(coupon)
              case Some(_) => DomainError(cannotDeactivateNonActiveCoupon format id).fail
            }, sendReplay = false) { coupon =>
              emitter sendEvent CouponDeactivated(merchantId, campaignId, id)
            }
          }
          InactiveCampaign.create(c)
        }
        case c: Campaign => DomainError(cannotDeactivateNonActiveCampaign format campaignId).fail
      }
    }

  def activateCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]): DomainValidation[Campaign] =
    updateCampaignImpl(campaignId, expectedVersion) { campaign =>
      campaign match {
        case c: DraftCampaign => {
          //TODO: notify sender if deal/coupon is not found in Ref
          c.dealIds map { id =>
            processDeal(readDeals.get(id) match {
              case None => DomainError(dealWithIdNotExist format id).fail
              case Some(deal: DraftDeal) => ActiveDeal.create(deal)
              case Some(_) => DomainError(cannotActivateNonDraftDeal format id).fail
            }, sendReplay = false) { deal =>
              emitter sendEvent DealActivated(merchantId, campaignId, id)
            }
          }
          c.couponIds map { id =>
            processCoupon(readCoupons.get(id) match {
              case None => DomainError(dealWithIdNotExist format id).fail
              case Some(coupon: DraftCoupon) => ActiveCoupon.create(coupon)
              case Some(_) => DomainError(cannotActivateNonDraftCoupon format id).fail
            }, sendReplay = false) { coupon =>
              emitter sendEvent CouponActivated(merchantId, campaignId, id)
            }
          }
          ActiveCampaign.create(c)
        }
        case c: InactiveCampaign => {
          //TODO: notify sender if deal/coupon is not found in Ref
          c.dealIds map { id =>
            processDeal(readDeals.get(id) match {
              case None => DomainError(dealWithIdNotExist format id).fail
              case Some(deal: InactiveDeal) => ActiveDeal.create(deal.asDraft)
              case Some(_) => DomainError(cannotActivateNonInactiveDeal format id).fail
            }, sendReplay = false) { deal =>
              emitter sendEvent DealReactivated(merchantId, campaignId, id)
            }
          }
          c.couponIds map { id =>
            processCoupon(readCoupons.get(id) match {
              case None => DomainError(dealWithIdNotExist format id).fail
              case Some(coupon: InactiveCoupon) => ActiveCoupon.create(coupon.asDraft)
              case Some(_) => DomainError(cannotActivateNonInactiveCoupon format id).fail
            }, sendReplay = false) { coupon =>
              emitter sendEvent CouponReactivated(merchantId, campaignId, id)
            }
          }
          ActiveCampaign.create(c)
        }
        case c: Campaign => DomainError(cannotActivateActiveCampaign format campaignId).fail
      }
    }

  /**
   * Method should return result of validation - whether campaign is eligable for deletion or not -> version check.
   *
   * @param campaignId
   * @param expectedVersion
   * @return
   */
  def removeCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]): DomainValidation[Campaign] =
    readCampaigns.get(campaignId) match {
      case None => DomainError(campaignWithIdNotExist format campaignId).fail
      case Some(campaign) => readMerchants.get(merchantId) match {
        case None => DomainError(merchantIdNotExistMsg format merchantId).fail
        case Some(merchant) => campaign.requireVersion(expectedVersion) match {
          case s @ Success(_) => merchant.removeCampaign(campaignId) match {
            case Failure(msg) => DomainError(msg).fail
            case Success(m) => updateMerchants(m); s
          }
          case f @ Failure(_) => f
        }
      }
    }

  def addDeal(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String, sendReplay: Boolean = true) {
    processCampaign(addDeal(campaignId, expectedVersion, dealId), sendReplay) { campaign =>
      emitter sendEvent DealAdded(merchantId, campaignId, dealId)
    }
  }

  def addDeal(campaignId: String, expectedVersion: Option[Long], dealId: String): DomainValidation[Campaign] =
    updateCampaignImpl(campaignId, expectedVersion) { campaign =>
      campaign match {
        case c: DraftCampaign => c.addDeal(dealId)
        case _ => DomainError(cannotUpdateNonDraftCampaign).fail
      }
    }

  def removeDeal(campaignId: String, campaignVersion: Option[Long], dealId: String, dealVersion: Option[Long]): DomainValidation[Campaign] =
    updateCampaignImpl(campaignId, campaignVersion) { campaign =>
      campaign match {
        case c: DraftCampaign => readDeals.get(dealId) match {
          case Some(deal) => deal.requireVersion(dealVersion) match {
            case Success(_) =>
              removeDeal(deal); c.removeDeal(dealId)
            case Failure(msg) => DomainError(msg).fail
          }
          case None => DomainError(dealWithIdNotExist format dealId).fail
        }
        case _ => DomainError(cannotUpdateNonDraftCampaign).fail
      }
    }

  def addCoupon(merchantId: String, campaignId: String, expectedVersion: Option[Long], couponId: String, sendReplay: Boolean = true) {
    processCampaign(addCoupon(campaignId, expectedVersion, couponId), sendReplay) { campaign =>
      emitter sendEvent CouponAdded(merchantId, campaignId, couponId)
    }
  }

  def addCoupon(campaignId: String, expectedVersion: Option[Long], dealId: String): DomainValidation[Campaign] =
    updateCampaignImpl(campaignId, expectedVersion) { campaign =>
      campaign match {
        case c: DraftCampaign => c.addCoupon(dealId)
        case _ => DomainError(cannotUpdateNonDraftCampaign).fail
      }
    }

  def removeCoupon(campaignId: String, campaignVersion: Option[Long], couponId: String, couponVersion: Option[Long]): DomainValidation[Campaign] =
    updateCampaignImpl(campaignId, campaignVersion) { campaign =>
      campaign match {
        case c: DraftCampaign => readCoupons.get(couponId) match {
          case Some(coupon) => coupon.requireVersion(couponVersion) match {
            case Success(_) =>
              removeCoupon(coupon); c.removeCoupon(couponId)
            case Failure(msg) => DomainError(msg).fail
          }
          case None => DomainError(dealWithIdNotExist format couponId).fail
        }
        case _ => DomainError(cannotUpdateNonDraftCampaign).fail
      }
    }

  def updateCampaignImpl[B <: Campaign](campaignId: String, expectedVersion: Option[Long])(f: Campaign => DomainValidation[B]): DomainValidation[B] =
    updateCampaign(campaignId, expectedVersion) { campaign =>
      campaign match {
        case campaign: Campaign => f(campaign)
      }
    }

  def updateCampaign[B <: Campaign](campaignId: String, expectedVersion: Option[Long])(f: Campaign => DomainValidation[B]): DomainValidation[B] =
    readCampaigns.get(campaignId) match {
      case None => DomainError(campaignWithIdNotExist format campaignId).fail
      case Some(campaign) => for {
        current <- campaign.requireVersion(expectedVersion)
        updated <- f(campaign)
      } yield updated
    }

  //Deals & Coupons
  def createDeal(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String, dealDetails: DealDetails): DomainValidation[Deal] = {
    readDeals.get(dealId) match {
      case Some(deal) => DomainError(dealIdAlreadyExistMsg format dealId).fail
      case None => {
        val validation = pipelineDealValidation(Some(campaignId), DraftDeal.create(dealId, dealDetails))
        if (validation.isSuccess) addDeal(merchantId, campaignId, expectedVersion, dealId, sendReplay = false)
        if (validation.isSuccess) dealDetails.write()
        validation
      }
    }
  }

  def createCoupon(merchantId: String, campaignId: String, expectedVersion: Option[Long], couponId: String, dealDetails: DealDetails, count: Long): DomainValidation[Deal] = {
    readCoupons.get(couponId) match {
      case Some(coupon) => DomainError(couponIdAlreadyExistMsg format couponId).fail
      case None => {
        val validation = pipelineDealValidation(Some(campaignId), DraftCoupon.create(couponId, dealDetails, count))
        if (validation.isSuccess) addCoupon(merchantId, campaignId, expectedVersion, couponId, sendReplay = false)
        if (validation.isSuccess) dealDetails.write()
        validation
      }
    }
  }

  def updateDetails(sub: DealDetails => DomainValidation[DealDetails]): DealDetailed => DomainValidation[Deal] = { deal =>
    sub(deal.details) match {
      case Success(d) => deal.updateDetails(d)
      case Failure(e) => DomainError(e).fail
    }
  }

  def updateDeal(campaignId: Option[String], dealId: String, expectedVersion: Option[Long])(f: DealDetailed => DomainValidation[Deal]): DomainValidation[Deal] =
    updateDealImpl(readDeals, dealId, expectedVersion) { deal =>
      deal match {
        case d: DraftDeal => pipelineDealValidation(campaignId, f(d))
        case d: InactiveDeal => pipelineDealValidation(campaignId, f(d.asDraft)) match {
          case Success(draft: DraftDeal) => InactiveDeal.create(draft)
          case Success(_) => DomainError(cannotUpdateNonDraftDeal format dealId).fail
          case f @ Failure(_) => f
        }
        case _ => DomainError(cannotUpdateNonDraftDeal format dealId).fail
      }
    }

  def updateCoupon(campaignId: Option[String], dealId: String, expectedVersion: Option[Long])(f: DealDetailed => DomainValidation[Deal]): DomainValidation[Deal] =
    updateDealImpl(readCoupons, dealId, expectedVersion) { deal =>
      deal match {
        case c: DraftCoupon => pipelineDealValidation(campaignId, f(c))
        case c: InactiveCoupon => pipelineDealValidation(campaignId, f(c.asDraft)) match {
          case Success(draft: DraftCoupon) => InactiveCoupon.create(draft)
          case Success(_) => DomainError(cannotUpdateNonDraftDeal format dealId).fail
          case f @ Failure(_) => f
        }
        case _ => DomainError(cannotUpdateNonDraftDeal format dealId).fail
      }
    }

  def updateDealImpl[B <: Deal](deals: Map[String, Deal], dealId: String, expectedVersion: Option[Long])(f: Deal => DomainValidation[B]): DomainValidation[B] =
    updateDeal(deals, dealId, expectedVersion) { deal =>
      deal match {
        case deal: Deal => f(deal)
      }
    }

  def updateDeal[B <: Deal](deals: Map[String, Deal], dealId: String, expectedVersion: Option[Long])(f: Deal => DomainValidation[B]): DomainValidation[B] =
    deals.get(dealId) match {
      case None => DomainError(dealWithIdNotExist format dealId).fail
      case Some(deal) => for {
        current <- deal.requireVersion(expectedVersion)
        updated <- f(deal)
      } yield updated
    }

  def pipelineDealValidation(campaignId: Option[String], validation: DomainValidation[Deal]): DomainValidation[Deal] = {
    validation.flatMap(isPeriodValid(_)).flatMap(isPeriodValid(_, campaignId))
  }

  private def updateMerchants(merchant: Merchant) {
    merchantsRef.single.transform(merchants => merchants + (merchant.id -> merchant))
  }

  private def removeMerchant(merchant: Merchant) {
    merchant.campaignIds foreach { id: String => removeCampaign(readCampaigns.get(id).get) }
    merchantsRef.single.transform(merchants => merchants - merchant.id)
  }

  private def readMerchants = merchantsRef.single.get

  private def updateMerchantsAndCampaigns(merchant: Merchant, campaign: Campaign) {
    //    atomic {
    val merchantOpt = merchantsRef.single.get.get(merchant.id)
    val updatedMerchant = merchantOpt.map(_.addCampaign(campaign.id)).get
    updatedMerchant match {
      case Success(newMerchant) => merchantsRef.single.transform(merchants => merchants + (merchant.id -> newMerchant))
      case Failure(_) => return
    }
    campaignsRef.single.transform(campaigns => campaigns + (campaign.id -> campaign))
    //    }
  }

  private def updateCampaigns(campaign: Campaign) {
    campaignsRef.single.transform(campaigns => campaigns + (campaign.id -> campaign))
  }

  private def removeCampaign(campaign: Campaign) {
    campaignsRef.single.transform(campaigns => campaigns - campaign.id)
    campaign.dealIds foreach { id: String => dealsRef.single.transform(deals => deals - id) }
    campaign.couponIds foreach { id: String => couponsRef.single.transform(deals => deals - id) }
  }

  private def readCampaigns = campaignsRef.single.get

  private def readCoupons = couponsRef.single.get

  private def readDeals = dealsRef.single.get

  private def updateDeals(deal: Deal) {
    dealsRef.single.transform(deals => deals + (deal.id -> deal))
  }

  private def updateCoupons(coupon: Deal) {
    couponsRef.single.transform(coupons => coupons + (coupon.id -> coupon))
  }

  private def removeDeal(deal: Deal) {
    dealsRef.single.transform(deals => deals - deal.id)
  }

  private def removeCoupon(coupon: Deal) {
    couponsRef.single.transform(coupons => coupons - coupon.id)
  }

  private def getPreviousProductInfo(deals: Map[String, Deal], dealId: String) = deals.get(dealId) match {
    case Some(deal) => deal.details.productInfo
    case None => List()
  }

}
