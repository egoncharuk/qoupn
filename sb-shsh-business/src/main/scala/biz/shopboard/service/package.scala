package biz.shopboard

import domain.model.merchant.ActivationEvent
import domain.model.user.{ User, UserSubscribed }
import state.InMemoryProjection
import util.MakeIfTrue

package object service {
  implicit def autoMakeIfTrue(b: => Boolean) = new MakeIfTrue(b)
  def make(s: String): List[String] = s.toLowerCase.foldLeft[List[String]](List("")) { (l, s) => l.head + s :: l }

  type InnerStateMap = Map[String, Set[String]]
  type KeyMap = Map[String, Map[String, User]]
  type StateMap = Map[String, InnerStateMap]
  type SubscriptionProjectionType = InMemoryProjection[KeyMap, UserSubscribed, StateMap, ActivationEvent]
}
