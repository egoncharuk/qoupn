package biz.shopboard.service

import biz.shopboard.domain._

import scalaz._
import Scalaz._
import biz.shopboard.state.Snapshot
import scalaz.Failure
import scalaz.Success
import biz.shopboard._
import domain.{ DomainError, AggregateError }
import model.merchant._
import infrastructure.ValidationMessages
import scala.concurrent.Await
import scala.concurrent.stm.Ref
import akka.actor._
import akka.pattern.ask
import biz.shopboard.domain.model.user._
import biz.shopboard.state.TotalEmitter
import org.eligosource.eventsourced.core.{ SnapshotOffer, SnapshotRequest, Message }
import scala.Some

class CouponSubscriptionService(projection: ActorRef)(implicit system: ActorSystem) {

  import system.dispatcher

  implicit val timeout = akka.util.Timeout(5000)

  def collectCoupons(userId: String, merchant: Option[Merchant] = None): Option[List[String]] = {
    Await.result(projection ? Message(CheckCoupons(userId, merchant.map(_.id))) map (_.asInstanceOf[Option[List[String]]]), duration)
  }
  def collectCoupons(userId: String, merchantId: String): Option[List[String]] = {
    Await.result(projection ? Message(CheckCoupons(userId, Some(merchantId))) map (_.asInstanceOf[Option[List[String]]]), duration)
  }
  def collectMerchants(userId: String): Option[Iterable[String]] = {
    Await.result(projection ? Message(CheckMerchants(userId)) map (_.asInstanceOf[Option[Iterable[String]]]), duration)
  }
  def collectSubscribeOnlyMerchants(userId: String): Option[Iterable[String]] = {
    Await.result(projection ? Message(CheckSubscribeOnlyMerchants(userId)) map (_.asInstanceOf[Option[Iterable[String]]]), duration)
  }
  def redeemCoupons(userId: String, merchantId: String, couponIds: List[String]): DomainValidation[String] = {
    Await.result(projection ? Message(RedeemCoupons(userId, merchantId, couponIds)) map (_.asInstanceOf[DomainValidation[String]]), duration)
  }
}

class CouponSubscriptionProjection(couponsRef: Ref[Map[String, Deal]]) extends Actor with SubscriptionProjection with ValidationMessages {
  this: TotalEmitter =>

  def initialKey = Map.empty[String, Map[String, User]]
  def initialState = Map.empty[String, Map[String, Set[String]]]

  import scala.concurrent.stm._

  override def collect(couponId: String, user: Option[User] = None): Option[String] = super.collect(couponId, user) match {
    case id @ Some(_) => id
    case None => {
      couponsRef.single.transform(coupons => coupons.get(couponId) match {
        case Some(c: ActiveCoupon) if isApplicable(c, user) => c.countDown match {
          case Failure(AggregateError(msg, None)) =>
            sender ! DomainError(msg); coupons
          case Failure(AggregateError(msg, Some(coupon))) =>
            sender ! DomainError(msg); coupons + (couponId -> coupon)
          case Success(coupon) => coupons + (couponId -> coupon)
        }
        case _ => coupons
      })
      couponsRef.single.getWith(coupons => coupons.get(couponId) match {
        case Some(c: ActiveCoupon) if isApplicable(c, user) => Some(c.id)
        case _ => None
      })
    }
  }

  def redeemCoupons(userId: String, merchantId: String, couponIds: List[String]) {
    snapshot.single transform { snapshot =>
      Snapshot(currentKey, currentState.get(userId) match {
        case None =>
          sender ! DomainError(userIdNotExist format userId).fail; currentState
        case Some(merchants) => merchants.get(merchantId) match {
          case None =>
            sender ! DomainError(merchantIdNotExits format merchantId).fail; currentState
          case Some(coupons) => sender ! couponsAreRedeemedSuccessfully.success; currentState + (userId -> (merchants + (merchantId -> coupons.filterNot(couponIds.contains(_)))))
        }
      })
    }
  }

  override def receive = akka.event.LoggingReceive {
    case event: CouponActivationEvent => updateState(event)
    case event: UserUnsubscribed => unsubscribe(event)
    case event: UserSubscribed => {
      updateKey(event)
      emitter sendEvent DistributeCoupons(event.merchantId, event.userId)
    }
    case CheckCoupons(userId, None) => {
      sender ! currentState.get(userId).collect {
        case pm: Map[String, Set[String]] => pm.flatMap(_._2).toList
      }
    }
    case CheckCoupons(userId, Some(merchantId)) => {
      sender ! currentState.get(userId).collect {
        case pm: Map[String, Set[String]] => pm.getOrElse(merchantId, Set()).toList
      }
    }
    case CheckMerchants(userId) => {
      sender ! Some(currentState.getOrElse(userId, Map()).keys)
    }
    case CheckSubscribeOnlyMerchants(userId) => {
      sender ! Some(currentKey.filter(_._2.contains(userId)).keys)
    }
    case RedeemCoupons(userId, merchantId, couponIds) => {
      redeemCoupons(userId, merchantId, couponIds)
    }

    case snapshotRequest @ SnapshotRequest(pid, snr, _) =>
      atomic { txn =>
        snapshotRequest.process(snapshot.get(txn).copy())
      }

    case SnapshotOffer(org.eligosource.eventsourced.core.Snapshot(_, snr, time,
      state: biz.shopboard.state.Snapshot[Map[String, Map[String, User]], Map[String, InnerStateMap]])) => {
      atomic { txn =>
        snapshot.single transform { snapshot => state }
      }
    }
  }
}

