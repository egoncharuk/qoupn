package biz.shopboard.service

import biz.shopboard.state.InMemoryProjection
import biz.shopboard.domain.model.merchant._
import org.eligosource.eventsourced.core.SnapshotRequest
import biz.shopboard._
import akka.pattern.ask
import akka.actor._
import concurrent.Await
import concurrent.stm._
import domain.model.common.ProductInfo
import domain.model.merchant.SearchProducts
import org.eligosource.eventsourced.core.Message
import org.eligosource.eventsourced.core.SnapshotOffer

class ProductSearchService(projection: ActorRef)(implicit system: ActorSystem) {

  import system.dispatcher

  implicit val timeout = akka.util.Timeout(5000)

  def searchProducts(name: String = "", desc: String = "", category: String = "", merchantId: String = ""): Option[List[ProductInfo]] = {
    Await.result(projection ? Message(SearchProducts(merchantId, name, desc, category)) map (_.asInstanceOf[Option[List[ProductInfo]]]), duration)
  }
}

case class CompositeKey(merchantId: String, name: String, desc: String, category: String)

object CompositeKey {
  def apply(cmd: SearchProducts): CompositeKey = CompositeKey(cmd.merchantId, cmd.name.toLowerCase, cmd.desc.toLowerCase, cmd.category.toLowerCase)
  def apply(merchantId: String, info: ProductInfo): List[CompositeKey] = {
    List(merchantId, "").flatMap(s0 => make(info.name).flatMap(s1 => make(info.description).flatMap(s2 => make(info.category).map(s3 => CompositeKey(s0, s1, s2, s3)))))
  }
}

class ProductSearchProjection extends Actor with InMemoryProjection[Map[(ProductInfo, String), Long], ProductInfoEvent, Map[CompositeKey, List[ProductInfo]], ProductInfoEvent] {
  def initialKey = Map.empty[(ProductInfo, String), Long]
  def initialState = Map.empty[CompositeKey, List[ProductInfo]]

  def projectKey = {
    case (key, event) => {
      val k0 = event.currentInfo.foldRight(key) { (i, k) => event.previousInfo.contains(i) ? k | k + ((i, event.merchantId) -> (k.getOrElse((i, event.merchantId), 0L) + 1L)) }
      event.previousInfo.foldRight(k0) { (i, k) => event.currentInfo.contains(i) ? k | k + ((i, event.merchantId) -> (k.getOrElse((i, event.merchantId), 0L) - 1L)) }
    }
  }

  def project = {
    case (state, event) => {
      val removable = currentKey.filter(p => p._1._2 == event.merchantId && p._2 == 0).keys.flatMap(p => CompositeKey(p._2, p._1))
      event.currentInfo.foldRight(state.filterNot(p => removable.exists(p._1 == _))) { (i, s) => s ++ CompositeKey(event.merchantId, i).map(ck => ck -> (i :: s.getOrElse(ck, List()))) }
    }
  }

  def receive = akka.event.LoggingReceive {
    case c: SearchProducts => sender ! currentState.get(CompositeKey(c))
    case msg: ProductInfoEvent => update(msg)

    case snapshotRequest @ SnapshotRequest(pid, snr, _) =>
      atomic { txn =>
        snapshotRequest.process(snapshot.get(txn).copy())
      }

    case SnapshotOffer(org.eligosource.eventsourced.core.Snapshot(_, snr, time,
      state: biz.shopboard.state.Snapshot[Map[(ProductInfo, String), Long], Map[CompositeKey, List[ProductInfo]]])) => {
      atomic { txn =>
        snapshot.single transform { snapshot => state }
      }
    }
  }
}
