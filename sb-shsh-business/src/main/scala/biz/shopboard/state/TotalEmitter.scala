package biz.shopboard.state

import org.eligosource.eventsourced.core._
import akka.actor.{ ActorContext, ActorSystem, ActorRef }
import org.eligosource.eventsourced.core.Message

trait TotalEmitter extends Emitter {
  override def emitter = new TotalMessageEmitterFactory(channels, namedChannels, message)(context.system)

  abstract override def receive = {
    case msg => {
      super.receive(msg)
    }
  }
}

class TotalMessageEmitterFactory(override val channels: Map[Int, ActorRef], namedChannels: Map[String, ActorRef], override val message: Message)(implicit system: ActorSystem) extends MessageEmitterFactory(channels, namedChannels, message) {

  def send(f: Message => Message)(implicit sender: ActorRef = null) = {
    channels foreach { el => el._2 ! f(message) }
  }

  def forward(f: Message => Message)(implicit context: ActorContext) = {
    channels foreach { el => el._2 forward f(message) }
  }

  def sendEvent(event: Any)(implicit sender: ActorRef = null) = {
    channels foreach { el => el._2 ! message.copy(event = event) }
  }

  def forwardEvent(event: Any)(implicit context: ActorContext) = {
    channels foreach { el => el._2 forward message.copy(event = event) }
  }
}

