package biz.shopboard.state

import concurrent.stm.Ref

trait Projection[S, A] {
  def initialState: S
  def currentState: S

  def project: PartialFunction[(S, A), S]
}

trait WithKeyProjection[K, B] {
  def initialKey: K
  def currentKey: K

  def projectKey: PartialFunction[(K, B), K]
}

case class Snapshot[K, S](key: K, state: S)

trait InMemoryProjection[K, B, S, A] extends Projection[S, A] with WithKeyProjection[K, B] {
  protected lazy val snapshot = Ref(Snapshot(initialKey, initialState))

  def currentKey: K = snapshot.single.get.key
  def currentState: S = snapshot.single.get.state

  def update(msg: Any) {
    updateKey(msg.asInstanceOf[B])
    updateState(msg.asInstanceOf[A])
  }

  def updateKey(msg: B) {
    snapshot.single transform {
      snapshot => Snapshot(projectKey(currentKey, msg), currentState)
    }
  }
  def updateState(msg: A) {
    snapshot.single transform {
      snapshot => Snapshot(currentKey, project(currentState, msg))
    }
  }

}
