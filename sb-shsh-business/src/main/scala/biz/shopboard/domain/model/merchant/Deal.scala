package biz.shopboard.domain.model.merchant

import biz.shopboard.domain._
import model.common.{FileWritable, ApplicabilityRule, DiscountInfo, ProductInfo}
import model.user.{ UserCommand, UserEvent }
import org.apache.commons.io.FileUtils
import org.joda.time.DateTime
import scalaz._
import Scalaz._
import java.io.File
import org.apache.commons.codec.binary.Base64
import scala.Some
import com.fasterxml.jackson.annotation.{JsonProperty, JsonIgnore}
import biz.shopboard.journal.MongodbEvent

case class Deals(deals: List[Deal])
case class Coupons(coupons: List[Deal])

case class DraftDeals(deals: List[DraftDeal])
case class ActiveDeals(deals: List[ActiveDeal])

case class DraftCoupons(coupons: List[DraftCoupon])
case class ActiveCoupons(coupons: List[ActiveCoupon])

object DraftDeal {
  def create(id: String, details: DealDetails): DomainValidation[DraftDeal] = DraftDeal(id, 0L, details).success
}

object DraftCoupon {
  def create(id: String, details: DealDetails, count: Long): DomainValidation[DraftCoupon] = DraftCoupon(id, 0L, CouponDetails(details, count)).success
}

object ActiveDeal {
  def create(deal: DraftDeal): DomainValidation[ActiveDeal] = ActiveDeal(deal).success
}

object ActiveCoupon {
  @JsonIgnore
  val allCouponsAreUsed = "Coupon %s: the number of used coupons is exceeded."
  def create(coupon: DraftCoupon): DomainValidation[ActiveCoupon] = ActiveCoupon(coupon, coupon.version, coupon.couponDetails.count).success
}

object InactiveDeal {
  def create(deal: ActiveDeal): DomainValidation[InactiveDeal] = InactiveDeal(deal).success
  def create(deal: DraftDeal): DomainValidation[InactiveDeal] = InactiveDeal(ActiveDeal(deal)).success
}

object InactiveCoupon {
  def create(coupon: ActiveCoupon): DomainValidation[InactiveCoupon] = InactiveCoupon(coupon).success
  def create(coupon: DraftCoupon): DomainValidation[InactiveCoupon] = InactiveCoupon(ActiveCoupon(coupon, coupon.version, coupon.couponDetails.count)).success
}

sealed trait Deal extends Aggregate with VersionRequired[Deal] {
  def rules: List[ApplicabilityRule]
  def details: DealDetails
}

sealed trait DealDetailed extends Deal {
  def updateDetails(newDetails: DealDetails): DomainValidation[Deal]
}

sealed trait DealWrapper extends Deal {
  def id = deal.id
  def rules = deal.rules
  def version = deal.version
  def details = deal.details
  def deal: Deal
}

case class ActiveDeal(deal: DraftDeal) extends DealWrapper
case class InactiveDeal(deal: ActiveDeal) extends DealWrapper {
  def asDraft = deal.deal
}

case class DraftDeal(@JsonProperty("id") id: String,
                     @JsonProperty("version") version: Long = -1,
                     @JsonProperty("details") details: DealDetails,
                     @JsonProperty("rules") rules: List[ApplicabilityRule] = List()) extends DealDetailed {
  def updateDetails(newDetails: DealDetails) = { newDetails.rewrite(Some(details)); copy(version = version + 1, details = newDetails).success }
}

case class ActiveCoupon(deal: DraftCoupon, override val version: Long, count: Long) extends DealWrapper {
  def countUp = copy(version = version + 1, count = count + 1).success
  def countDown: AggregateValidation[InactiveCoupon, ActiveCoupon] = {
    if (count > 0) copy(version = version + 1, count = count - 1).success
    else AggregateError[InactiveCoupon](ActiveCoupon.allCouponsAreUsed format id, InactiveCoupon(copy(version = version + 1))).fail
  }
}
case class InactiveCoupon(@JsonIgnore deal: ActiveCoupon) extends DealWrapper {
  def asDraft = deal.deal
}

case class DraftCoupon(@JsonProperty("id") id: String,
                       @JsonProperty("version") version: Long = -1,
                       @JsonProperty("couponDetails") couponDetails: CouponDetails,
                       @JsonProperty("rules") rules: List[ApplicabilityRule] = List()) extends Deal with DealDetailed {
  @JsonIgnore
  val ruleNotFoundMessage = "rule with details = %s is not found in the list of rules"

  def details = couponDetails.innerDetails
  def addRule(rule: ApplicabilityRule) = copy(version = version + 1, rules = rules :+ rule).success
  def removeRule(rule: ApplicabilityRule) =
    if (!rules.contains(rule)) DomainError(ruleNotFoundMessage format (rule.toString)).fail
    else copy(version = version + 1, rules = rules.filterNot(_ == rule)).success

  def updateCount(newCount: Long) = copy(version = version + 1, couponDetails = CouponDetails(couponDetails.innerDetails, newCount)).success
  def updateDetails(newDetails: DealDetails) = {
    newDetails.rewrite(Some(couponDetails.innerDetails))
    copy(version = version + 1, couponDetails = CouponDetails(newDetails, couponDetails.count)).success
  }
  def updateCouponDetails(newDetails: CouponDetails) = {
    newDetails.innerDetails.rewrite(Some(couponDetails.innerDetails))
    copy(version = version + 1, couponDetails = newDetails).success
  }
}

case class CouponDetails(innerDetails: DealDetails, count: Long = 0L)
case class DealDetails(name: String, description: String, discount: DiscountInfo, validFrom: DateTime,
                       validTo: DateTime, productInfo: List[ProductInfo] = List(),
                       filePath: String = null, fileBase: String = null) extends FileWritable {

  def updateProductInfo(newProductInfo: List[ProductInfo]) = copy(productInfo = newProductInfo).success
  def addProductInfo(productInfoToAdd: ProductInfo) = copy(productInfo = productInfo :+ productInfoToAdd).success
  def removeProductInfo(productInfoToRemove: ProductInfo) = copy(productInfo = productInfo.filterNot(_ == productInfoToRemove)).success
}

sealed trait ActivationEvent extends DealEvent
sealed trait DealActivationEvent extends ActivationEvent
sealed trait CouponActivationEvent extends ActivationEvent

sealed trait DealEvent extends MerchantEvent {
  def dealId: String
}
sealed trait ProductInfoEvent extends DealEvent {
  def previousInfo: List[ProductInfo]
  def currentInfo: List[ProductInfo]
}
sealed trait ProductInfoCreationEvent extends ProductInfoEvent {
  def dealDetails: DealDetails
  def previousInfo: List[ProductInfo] = List()
  def currentInfo: List[ProductInfo] = dealDetails.productInfo
}

// Events
case class DealCreated(merchantId: String, campaignId: String, dealId: String, dealDetails: DealDetails) extends MerchantEvent with ProductInfoCreationEvent with MongodbEvent
case class DealActivated(merchantId: String, campaignId: String, dealId: String) extends DealActivationEvent with MongodbEvent
case class DealReactivated(merchantId: String, campaignId: String, dealId: String) extends DealActivationEvent with MongodbEvent
case class DealDeactivated(merchantId: String, campaignId: String, dealId: String) extends DealActivationEvent with MongodbEvent
case class DealDistributed(merchantId: String, campaignId: String, dealId: String, userId: String) extends DealActivationEvent with UserEvent with MongodbEvent
case class DealDetailsUpdated(merchantId: String, dealId: String, dealDetails: DealDetails) extends MerchantEvent with MongodbEvent
case class CouponCreated(merchantId: String, campaignId: String, dealId: String, dealDetails: DealDetails, count: Long) extends MerchantEvent with ProductInfoCreationEvent with MongodbEvent
case class CouponActivated(merchantId: String, campaignId: String, dealId: String) extends CouponActivationEvent with MongodbEvent
case class CouponReactivated(merchantId: String, campaignId: String, dealId: String) extends CouponActivationEvent with MongodbEvent
case class CouponDeactivated(merchantId: String, campaignId: String, dealId: String) extends CouponActivationEvent with MongodbEvent
case class CouponDistributed(merchantId: String, campaignId: String, dealId: String, userId: String) extends CouponActivationEvent with UserEvent with MongodbEvent
case class CouponDetailsUpdated(merchantId: String, dealId: String, couponDetails: CouponDetails) extends MerchantEvent with MongodbEvent
case class DealProductInfoUpdated(merchantId: String, dealId: String, previousInfo: List[ProductInfo], currentInfo: List[ProductInfo]) extends ProductInfoEvent with MongodbEvent
case class CouponProductInfoUpdated(merchantId: String, dealId: String, previousInfo: List[ProductInfo], currentInfo: List[ProductInfo]) extends ProductInfoEvent with MongodbEvent
case class DealsDistributed(merchantId: String, userId: String) extends MerchantEvent with UserEvent with MongodbEvent
case class CouponsDistributed(merchantId: String, userId: String) extends MerchantEvent with UserEvent with MongodbEvent

// Commands
case class CreateDeal(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String, dealDetails: DealDetails) extends MerchantCommand with MongodbEvent
case class UpdateDealDetails(merchantId: String, campaignId: String, dealId: String, expectedVersion: Option[Long], dealDetails: DealDetails) extends MerchantCommand with MongodbEvent
case class CreateCoupon(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String, dealDetails: DealDetails, count: Long) extends MerchantCommand with MongodbEvent
case class UpdateCouponDetails(merchantId: String, campaignId: String, dealId: String, expectedVersion: Option[Long], couponDetails: CouponDetails) extends MerchantCommand with MongodbEvent
case class UpdateDealProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: List[ProductInfo]) extends MerchantCommand with MongodbEvent
case class AddDealProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo) extends MerchantCommand with MongodbEvent
case class RemoveDealProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo) extends MerchantCommand with MongodbEvent
case class UpdateCouponProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: List[ProductInfo]) extends MerchantCommand with MongodbEvent
case class AddCouponProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo) extends MerchantCommand with MongodbEvent
case class RemoveCouponProductInfo(merchantId: String, dealId: String, expectedVersion: Option[Long], productInfo: ProductInfo) extends MerchantCommand with MongodbEvent
case class DistributeCoupon(merchantId: String, userId: String, couponId: String) extends MerchantCommand with UserCommand with MongodbEvent
case class DistributeDeal(merchantId: String, userId: String, dealId: String) extends MerchantCommand with UserCommand with MongodbEvent
case class DistributeCoupons(merchantId: String, userId: String) extends MerchantCommand with UserCommand with MongodbEvent
case class DistributeDeals(merchantId: String, userId: String) extends MerchantCommand with UserCommand with MongodbEvent
