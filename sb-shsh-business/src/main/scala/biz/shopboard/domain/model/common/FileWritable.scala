package biz.shopboard.domain.model.common

import org.apache.commons.io.FileUtils
import org.apache.commons.codec.binary.Base64
import java.io.File
import com.fasterxml.jackson.annotation.JsonIgnore

trait FileWritable {
  def filePath: String
  def fileBase: String
  @JsonIgnore
  def isWritten = filePath != null && !filePath.isEmpty && fileBase != null && !fileBase.isEmpty

  def rewrite(writable: Option[FileWritable]) {
    delete(writable); write()
  }

  def write() {
    if (isWritten) FileUtils.writeByteArrayToFile(new File(filePath), Base64.decodeBase64(fileBase))
  }

  def delete(writable: Option[FileWritable]) {
    writable.foreach(d => if (d.filePath != null && !d.filePath.isEmpty) FileUtils.forceDelete(new File(d.filePath)))
  }
}
