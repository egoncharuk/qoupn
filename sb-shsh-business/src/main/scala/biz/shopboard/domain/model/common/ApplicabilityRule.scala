package biz.shopboard.domain.model.common

trait ApplicabilityProvider

trait ApplicabilityRule {
  def isApplicable: Boolean = true
  def isSatisfied: Boolean = true
  def isApplicable(provider: ApplicabilityProvider): Boolean = true
  def isSatisfied(provider: ApplicabilityProvider): Boolean = true
}
