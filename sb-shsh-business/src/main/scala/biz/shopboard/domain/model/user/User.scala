package biz.shopboard.domain.model.user

import scalaz._
import Scalaz._
import scala._
import scala.Predef._
import biz.shopboard.domain._
import model.common._
import com.fasterxml.jackson.annotation.{JsonProperty, JsonIgnore}
import model.common.Address
import model.common.ApplicationRole
import model.common.ProductInfo
import model.merchant.MerchantEvent
import biz.shopboard.journal.MongodbEvent
import biz.shopboard.util.{Crypto, Hashing}
import scala.collection.immutable.HashSet

case class Users(users: List[User])

case class ActionHash(hashKey: String, actionType: ActionHashType, userId: String, timestamp: Long)

object ActionHash {
  def create(actionType: ActionHashType, userId: String) = {
    val hashKey: String = Hashing.generateActionHash()
    ActionHash(hashKey, actionType, userId, System.currentTimeMillis()) // TODO: create global utility for getting TimeZoneless timestamp
  }
}

case class ActionHashType(actionType: String)

object ActionHashType {
  val ACTIVATE_PASSWORD = ActionHashType("Activate Password")
  val RESET_PASSWORD = ActionHashType("Reset Password")
  val VALUES = Seq(ACTIVATE_PASSWORD, RESET_PASSWORD)
}

case class UserStatus(status: String)

object UserStatus {
  val ACTIVE = UserStatus("Active")
  val INACTIVE = UserStatus("Inactive") //Activation pending
  val VALUES = Seq(ACTIVE, INACTIVE)
}

object User {
  //DO NOT PUT ANY REAL BUSINESS VALIDATION HERE
  def create(id: String, details: UserDetails): DomainValidation[User] =
    User(id, 0L, details).success

  def create(id: String, details: UserDetails, roles: List[ApplicationRole]): DomainValidation[User] =
    User(id, 0L, details, roles).success

  def create(id: String, details: UserDetails, role: ApplicationRole): DomainValidation[User] =
    User(id, 0L, details, List(role)).success
}

case class User(@JsonProperty("id") id: String,
                @JsonProperty("version") version: Long = -1L,
                @JsonProperty("details") details: UserDetails,
                @JsonProperty("roles") roles: List[ApplicationRole] = List(),
                @JsonProperty("subscribedMerchantIds") subscribedMerchantIds: Set[String] = HashSet(),
                @JsonProperty("shoppingListsIds") shoppingListsIds: List[String] = List()) extends Aggregate with VersionRequired[User] with ApplicabilityProvider {

  @JsonIgnore
  val shoppingListIdNotFoundMessage = "shopping list with id = %s is not found in the list of items"
  @JsonIgnore
  val roleNotFoundMessage = "application role = %s is not found in the list of roles"

  def addMerchant(merchandId: String): DomainValidation[User] =
    copy(version = version + 1, subscribedMerchantIds = subscribedMerchantIds + merchandId).success

  def removeMerchant(merchandId: String): DomainValidation[User] =
    copy(version = version + 1, subscribedMerchantIds = subscribedMerchantIds.filterNot(_ ==  merchandId)).success

  def updateDetails(newDetails: UserDetails): DomainValidation[User] =
    copy(version = version + 1, details = newDetails).success

  def addRole(role: ApplicationRole): DomainValidation[User] =
    copy(version = version + 1, roles = roles :+ role).success

  def removeRole(role: ApplicationRole): DomainValidation[User] =
    if (!roles.contains(role)) DomainError(roleNotFoundMessage format (role)).fail
    else copy(version = version + 1, roles = roles.filterNot(_ == role)).success

  def addShoppingList(shoppingListId: String): DomainValidation[User] =
    copy(version = version + 1, shoppingListsIds = shoppingListsIds :+ shoppingListId).success

  def removeShoppingList(shoppingListId: String): DomainValidation[User] =
    if (!shoppingListsIds.contains(shoppingListId)) DomainError(shoppingListIdNotFoundMessage format (shoppingListId)).fail
    else copy(version = version + 1, shoppingListsIds = shoppingListsIds.filterNot(_ == shoppingListId)).success

  def encryptPassword = copy(details = details.encryptPassword)
  def withoutPassword = copy(details = details.withoutPassword)

  @JsonIgnore
  def isActive = UserStatus.ACTIVE.equals(this.details.status)
  @JsonIgnore
  def isInactive = UserStatus.INACTIVE.equals(this.details.status)
}

case class UserDetails(login: String, password: String, firstName: String, lastName: String, email: String,
  age: Int = 0, sex: Sex = Sex.MALE, address: Address = Address(), interestedIn: List[ProductInfo] = List(),
  status: UserStatus = UserStatus.INACTIVE) {
  def passwordMatches(pass: String) = Crypto.encryptAES(pass).equals(password) || password.equals(pass) //TODO: remove the last line upon transition
  def encryptPassword = copy(password = Crypto.encryptAES(password))
  def withoutPassword = copy(password = "")
}

// Events
trait UserEvent extends Event {
  def userId: String
}

trait SubscriptionEvent extends UserEvent with MerchantEvent
case class UserCreated(userId: String, userDetails: UserDetails) extends UserEvent with MongodbEvent
case class UserDetailsUpdated(userId: String, userDetails: UserDetails) extends UserEvent with MongodbEvent
case class UserUnsubscribed(userId: String, merchantId: String) extends SubscriptionEvent with MongodbEvent
case class UserSubscribed(userId: String, merchantId: String, user: User) extends SubscriptionEvent with MongodbEvent
case class RedeemCoupons(userId: String, merchantId: String, couponIds: List[String]) extends UserEvent with MongodbEvent
case class CheckCoupons(userId: String, merchantId: Option[String] = None) extends UserEvent with MongodbEvent
case class CheckDeals(userId: String, merchantId: Option[String] = None) extends UserEvent with MongodbEvent
case class CheckSubscribeOnlyMerchants(userId: String) extends UserEvent with MongodbEvent
case class CheckMerchants(userId: String) extends UserEvent with MongodbEvent
case class UserRemoved(userId: String) extends UserEvent with MongodbEvent

case class UserActivationLinkSent(email: String) extends MongodbEvent
case class UserActivated(hashKey: String) extends MongodbEvent
case class UserPasswordChangeLinkSent(email: String) extends MongodbEvent
case class UserPasswordChanged(userId: String, newUserPassword: String) extends MongodbEvent

// Commands
trait UserCommand extends Command {
  def userId: String
}

case class CreateUser(userId: String, userDetails: UserDetails, roles: List[ApplicationRole] = List()) extends UserCommand with MongodbEvent
case class UpdateUserDetails(userId: String, expectedVersion: Option[Long], userDetails: UserDetails) extends UserCommand with MongodbEvent
case class SubscribeUser(userId: String, merchantId: String) extends UserCommand with MongodbEvent
case class UnsubscribeUser(userId: String, merchantId: String) extends UserCommand with MongodbEvent
case class RemoveUser(userId: String, expectedVersion: Option[Long]) extends UserCommand with MongodbEvent
case class GetUserEventHistory(userId: String, deviceId: String) extends UserCommand with MongodbEvent
case class GetUserEventHistoryBySeqNr(userId: String, deviceId: String, seqNr: Int) extends UserCommand with MongodbEvent
case class ActivateUser(userId: String, userDetails: UserDetails, roles: List[ApplicationRole] = List()) extends UserCommand with MongodbEvent

case class ActivateUserPassword(hashKey: String) extends MongodbEvent
case class ChangeUserPassword(userId: String, newPassword: String) extends MongodbEvent
case class ValidateActionHash(hashKey: String, actionHashType: ActionHashType) extends MongodbEvent
case class SendChangeUserPasswordLink(email: String) extends MongodbEvent
