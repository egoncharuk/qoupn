package biz.shopboard.domain.model.common

case class ApplicationRole(name: String, desc: String)

object AdminRole extends ApplicationRole("Admin", "administrator of the system")
object MerchantRole extends ApplicationRole("Merchant", "system merchant")
object UserRole extends ApplicationRole("User", "system user")
