package biz.shopboard.domain.model.common

import biz.shopboard.I18nContext

case class DiscountInfo(originalPrice: Double = 0.0, discountPercent: Double = 0.0, discountedPrice: Double = 0.0) {
  override def toString = discountedPrice.toString + I18nContext.getCurrencyTitle + " ( -" + discountPercent.toString + "%)"
  def saved = originalPrice - discountedPrice
}
