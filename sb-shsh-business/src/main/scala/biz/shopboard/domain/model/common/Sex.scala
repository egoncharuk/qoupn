package biz.shopboard.domain.model.common

case class Sex(name: String)

object Sex {
  val MALE = Sex("Male")
  val FEMALE = Sex("Female")
  val SEXES = Seq(MALE, FEMALE)
}