package biz.shopboard.domain.model.merchant

import biz.shopboard.domain._
import org.joda.time.DateTime
import scalaz._
import Scalaz._
import biz.shopboard.journal.MongodbEvent
import com.fasterxml.jackson.annotation.{JsonProperty, JsonIgnore}

case class Campaigns(campaigns: List[Campaign])
case class ActiveCampaigns(campaigns: List[ActiveCampaign])

object DraftCampaign {
  def create(id: String, details: CampaignDetails): DomainValidation[DraftCampaign] = DraftCampaign(id, 0L, details).success
}

object ActiveCampaign {
  def create(campaign: DraftCampaign): DomainValidation[ActiveCampaign] = ActiveCampaign(campaign).success
  def create(campaign: InactiveCampaign): DomainValidation[ActiveCampaign] = ActiveCampaign(campaign.asDraft, reactive = true).success
}

object InactiveCampaign {
  def create(campaign: DraftCampaign): DomainValidation[InactiveCampaign] = InactiveCampaign(ActiveCampaign(campaign)).success
  def create(campaign: ActiveCampaign): DomainValidation[InactiveCampaign] = InactiveCampaign(campaign).success
}

sealed trait Campaign extends Aggregate with VersionRequired[Campaign] {
  def dealIds: List[String]
  def couponIds: List[String]
  def details: CampaignDetails
}

sealed trait CampaignWrapper extends Campaign {
  def id = campaign.id
  def version = campaign.version
  def details = campaign.details
  def dealIds = campaign.dealIds
  def couponIds = campaign.couponIds
  def campaign: Campaign
}

case class ActiveCampaign(campaign: DraftCampaign, reactive: Boolean = false) extends CampaignWrapper
case class InactiveCampaign(campaign: ActiveCampaign) extends CampaignWrapper {
  def asDraft = campaign.campaign
}

case class DraftCampaign(@JsonProperty("id") id: String,
                         @JsonProperty("version") version: Long = -1,
                         @JsonProperty("details") details: CampaignDetails,
                         @JsonProperty("dealIds") dealIds: List[String] = Nil,
                         @JsonProperty("couponIds") couponIds: List[String] = Nil) extends Campaign {

  @JsonIgnore
  val dealIdNotFoundMessage = "deal with id = %s is not found in the list of deals"
  @JsonIgnore
  val couponIdNotFoundMessage = "coupon with id = %s is not found in the list of coupons"

  def updateDetails(newDetails: CampaignDetails): DomainValidation[DraftCampaign] =
    copy(version = version + 1, details = newDetails).success

  def addDeal(dealId: String): DomainValidation[DraftCampaign] =
    copy(version = version + 1, dealIds = dealIds :+ dealId).success

  def removeDeal(dealId: String): DomainValidation[DraftCampaign] =
    if (!dealIds.contains(dealId)) DomainError(dealIdNotFoundMessage format (dealId)).fail
    else copy(version = version + 1, dealIds = dealIds.filterNot(_ == dealId)).success

  def addCoupon(couponId: String): DomainValidation[DraftCampaign] =
    copy(version = version + 1, couponIds = couponIds :+ couponId).success

  def removeCoupon(couponId: String): DomainValidation[DraftCampaign] =
    if (!couponIds.contains(couponId)) DomainError(couponIdNotFoundMessage format (couponId)).fail
    else copy(version = version + 1, couponIds = couponIds.filterNot(_ == couponId)).success
}

case class CampaignDetails(name: String, description: String, valid_from: DateTime, valid_to: DateTime)

// Events
case class CampaignCreated(merchantId: String, campaignId: String, campaignDetails: CampaignDetails) extends MerchantEvent with MongodbEvent
case class CampaignDetailsUpdated(merchantId: String, campaignId: String, campaignDetails: CampaignDetails) extends MerchantEvent with MongodbEvent
case class CampaignRemoved(merchantId: String, campaignId: String) extends MerchantEvent with MongodbEvent
case class CampaignActivated(merchantId: String, campaignId: String) extends MerchantEvent with MongodbEvent
case class CampaignReactivated(merchantId: String, campaignId: String) extends MerchantEvent with MongodbEvent
case class CampaignDeactivated(merchantId: String, campaignId: String) extends MerchantEvent with MongodbEvent

case class DealAdded(merchantId: String, campaignId: String, dealId: String) extends MerchantEvent with MongodbEvent
case class DealRemoved(merchantId: String, campaignId: String, dealId: String) extends MerchantEvent with MongodbEvent
case class CouponAdded(merchantId: String, campaignId: String, dealId: String) extends MerchantEvent with MongodbEvent
case class CouponRemoved(merchantId: String, campaignId: String, dealId: String) extends MerchantEvent with MongodbEvent

// Commands
case class CreateCampaign(merchantId: String, expectedVersion: Option[Long], campaignId: String, campaignDetails: CampaignDetails) extends MerchantCommand with MongodbEvent
case class UpdateCampaignDetails(merchantId: String, campaignId: String, expectedVersion: Option[Long], campaignDetails: CampaignDetails) extends MerchantCommand with MongodbEvent
case class RemoveCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]) extends MerchantCommand with MongodbEvent
case class ActivateCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]) extends MerchantCommand with MongodbEvent
case class DeactivateCampaign(merchantId: String, campaignId: String, expectedVersion: Option[Long]) extends MerchantCommand with MongodbEvent

case class AddDeal(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String) extends MerchantCommand with MongodbEvent
case class RemoveDeal(merchantId: String, campaignId: String, campaignVersion: Option[Long], dealId: String, dealVersion: Option[Long]) extends MerchantCommand with MongodbEvent
case class AddCoupon(merchantId: String, campaignId: String, expectedVersion: Option[Long], dealId: String) extends MerchantCommand with MongodbEvent
case class RemoveCoupon(merchantId: String, campaignId: String, campaignVersion: Option[Long], dealId: String, dealVersion: Option[Long]) extends MerchantCommand with MongodbEvent
