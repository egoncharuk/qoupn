package biz.shopboard.domain.model.merchant

import scalaz._
import Scalaz._
import scala._
import scala.Predef._
import biz.shopboard.domain._
import model.common.{FileWritable, Address}
import biz.shopboard.journal.MongodbEvent
import com.fasterxml.jackson.annotation.{JsonProperty, JsonIgnore}

case class MarchantAndCampaigns(merchants: List[Merchant], campaigns: List[ActiveCampaign])

object Merchant {

  def create(id: String, adminId: String, details: MerchantDetails): DomainValidation[Merchant] =
    Merchant(id, 0L, adminId, details).success
}

case class Merchant(@JsonProperty("id") id: String,
                    @JsonProperty("version") version: Long = -1,
                    @JsonProperty("adminId") adminId: String,
                    @JsonProperty("details") details: MerchantDetails,
                    @JsonProperty("campaignIds") campaignIds: List[String] = Nil)
  extends Aggregate with VersionRequired[Merchant] {

  @JsonIgnore
  val campaignIdNotFoundMessage = "campaign with id = %s is not found in the list of merchant's campaigns"

  def updateAdmin(newAdminId: String): DomainValidation[Merchant] =
    copy(version = version + 1, adminId = newAdminId).success

  def updateDetails(newDetails: MerchantDetails): DomainValidation[Merchant] = {
    newDetails.rewrite(Some(details)); copy(version = version + 1, details = newDetails).success
  }

  def addCampaign(campaignId: String): DomainValidation[Merchant] =
    copy(version = version + 1, campaignIds = campaignIds :+ campaignId).success

  def removeCampaign(campaignId: String): DomainValidation[Merchant] =
    if (!campaignIds.contains(campaignId)) DomainError(campaignIdNotFoundMessage format (campaignId)).fail
    else copy(version = version + 1, campaignIds = campaignIds.filterNot(_ == campaignId)).success

}

case class MerchantDetails(name: String, address: Address, filePath: String = null, fileBase: String = null) extends FileWritable
// Events
trait MerchantEvent extends Event {
  def merchantId: String
}

case class MerchantCreated(userId: String, merchantId: String, merchantDetails: MerchantDetails) extends MerchantEvent with MongodbEvent
case class MerchantDetailsUpdated(merchantId: String, merchantDetails: MerchantDetails) extends MerchantEvent with MongodbEvent
case class MerchantAdminUpdated(merchantId: String, merchantAdminId: String) extends MerchantEvent with MongodbEvent
case class MerchantRemoved(merchantId: String) extends MerchantEvent with MongodbEvent

// Commands
trait MerchantCommand extends Command {
  def merchantId: String
}

case class CreateMerchant(userId: String, merchantId: String, merchantDetails: MerchantDetails) extends MerchantCommand with MongodbEvent
case class UpdateMerchantDetails(merchantId: String, expectedVersion: Option[Long], merchantDetails: MerchantDetails) extends MerchantCommand with MongodbEvent
case class UpdateMerchantAdmin(merchantId: String, expectedVersion: Option[Long], merchantAdminId: String) extends MerchantCommand with MongodbEvent
case class RemoveMerchant(merchantId: String, expectedVersion: Option[Long]) extends MerchantCommand with MongodbEvent
case class SearchProducts(merchantId: String, name: String, desc: String, category: String) extends MerchantCommand with MongodbEvent
