package biz.shopboard.domain.model.common

import biz.shopboard.domain._

case class Address(street: String = "", town: String = "", state: String = "", country: String = "") {
  override def toString = append(street) + append(town) + append(state) + country
}