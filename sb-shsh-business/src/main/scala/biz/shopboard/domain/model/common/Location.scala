package biz.shopboard.domain.model.common

import biz.shopboard.domain.model.merchant.{ Deal, Campaign }

case class Location(address: Address, description: String = "", name: String = "", campaign: Campaign, deal: Deal)
