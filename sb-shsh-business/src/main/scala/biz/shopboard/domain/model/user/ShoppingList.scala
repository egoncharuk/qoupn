/*
 * Copyright 2012 Eligotech BV.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package biz.shopboard.domain.model.user

import scalaz._
import Scalaz._
import scala._
import scala.Predef._
import biz.shopboard.domain._
import biz.shopboard.journal.MongodbEvent
import com.fasterxml.jackson.annotation.{JsonProperty, JsonIgnore}

case class ShoppingLists(shoppinglists: List[ShoppingList])
case class CheckoutId(checkoutid: String, barcodebase: String)

object ShoppingList {

  val invalidVersionMessage = "shoppingList %s: expected version %s doesn't match current version %s"
  val itemIdErrorMsg = "item with id = %s not found in them list of items"

  def create(id: String, details: ShoppingListDetails): DomainValidation[ShoppingList] =
    ShoppingList(id, 0L, details, Map[String, ShoppingListItem]()).success

}

case class ShoppingList(@JsonProperty("id") id: String,
                        @JsonProperty("version") version: Long = -1,
                        @JsonProperty("details") details: ShoppingListDetails,
                        @JsonProperty("items") items: Map[String, ShoppingListItem] = Map[String, ShoppingListItem]())
  extends Aggregate with VersionRequired[ShoppingList] {

  import ShoppingList._

  def updateDetails(newDetails: ShoppingListDetails): DomainValidation[ShoppingList] =
    copy(version = version + 1, details = newDetails).success

  def addItem(item: ShoppingListItem): DomainValidation[ShoppingList] =
    copy(version = version + 1, items = items + (item.id -> item)).success

  def updateItem(item: ShoppingListItem): DomainValidation[ShoppingList] =
    if (!items.contains(item.id)) DomainError(itemIdErrorMsg format (item.id)).fail
    else copy(version = version + 1, items = items + (item.id -> item)).success

  def removeItem(itemId: String): DomainValidation[ShoppingList] =
    if (!items.contains(itemId)) DomainError(itemIdErrorMsg format (itemId)).fail
    else copy(version = version + 1, items = items.filterKeys(key => !(key equals itemId))).success

  def itemExists(query: String) = items.foldLeft(false) { (res, p) =>
    val name = p._2.name.toLowerCase
    val desc = p._2.desc.toLowerCase
    val ql = query.toLowerCase.split("\\s\\t\\n")
    res || ql.foldLeft(false) { (res2, i) => res2 || name.contains(i) || desc.contains(i) }
  }

  @JsonIgnore
  def isDone = items.forall(_._2.done)
}

case class ShoppingListDetails(name: String = "", tags: List[String] = List[String]())

case class ShoppingListItem(id: String, name: String = "", desc: String = "", quantity: Int = 0, price: Double = 0.0, done: Boolean = false)

// Utility ID objects
case class CompositeShoppingListID(shoppingListId: String, shoppingListItemId: Option[String] = None) {
  def asItem = shoppingListItemId
  def asID = shoppingListId
}
object CompositeShoppingListID {
  implicit def apply(id: String) = new CompositeShoppingListID(id)
  implicit def apply(id: Pair[String, Option[String]]) = new CompositeShoppingListID(id._1, id._2)
}

// Events
case class ShoppingListCreated(userId: String, shoppingListId: String, shoppingListDetails: ShoppingListDetails) extends UserEvent with MongodbEvent
case class ShoppingListDetailsUpdated(userId: String, shoppingListId: String, shoppingListDetails: ShoppingListDetails) extends UserEvent with MongodbEvent
case class ShoppingListRemoved(userId: String, shoppingListId: String) extends UserEvent with MongodbEvent

case class ShoppingListItemAdded(userId: String, shoppingListId: String, item: ShoppingListItem) extends UserEvent with MongodbEvent
case class ShoppingListItemUpdated(userId: String, shoppingListId: String, item: ShoppingListItem) extends UserEvent with MongodbEvent
case class ShoppingListItemRemoved(userId: String, shoppingListId: String, itemId: String) extends UserEvent with MongodbEvent

case class ShoppingListCheckedOut(userId: String, shoppingListId: String) extends UserEvent with MongodbEvent

sealed trait LinkingEvent extends UserEvent {
  def shoppingListId: CompositeShoppingListID
  def dealId: String
}

case class ShoppingListAndCouponLinked(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends LinkingEvent with MongodbEvent
case class ShoppingListAndCouponUnlinked(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends LinkingEvent with MongodbEvent
case class ShoppingListAndDealLinked(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends LinkingEvent with MongodbEvent
case class ShoppingListAndDealUnlinked(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends LinkingEvent with MongodbEvent

// Commands
case class CreateShoppingList(userId: String, expectedVersion: Option[Long], shoppingListId: String, shoppingListDetails: ShoppingListDetails) extends UserCommand with MongodbEvent
case class UpdateShoppingListDetails(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListDetails: ShoppingListDetails) extends UserCommand with MongodbEvent
case class RemoveShoppingList(userId: String, shoppingListId: String, expectedVersion: Option[Long]) extends UserCommand with MongodbEvent
//case class SendShoppingList(userId: String, expectedVersion: Option[Long], target: String); // Target - User | email | SMS ?

case class AddShoppingListItem(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListItem: ShoppingListItem) extends UserCommand with MongodbEvent
case class UpdateShoppingListItem(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListItem: ShoppingListItem) extends UserCommand with MongodbEvent
case class RemoveShoppingListItem(userId: String, shoppingListId: String, expectedVersion: Option[Long], shoppingListItemId: String) extends UserCommand with MongodbEvent

case class LinkShoppingListAndCoupon(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends UserCommand with MongodbEvent
case class UnlinkShoppingListAndCoupon(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends UserCommand with MongodbEvent
case class LinkShoppingListAndDeal(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends UserCommand with MongodbEvent
case class UnlinkShoppingListAndDeal(userId: String, shoppingListId: CompositeShoppingListID, dealId: String) extends UserCommand with MongodbEvent

case class CheckoutShoppingList(userId: String, shoppingListId: String) extends UserCommand with MongodbEvent
case class CheckShoppingListCoupons(shoppingListId: String) extends MongodbEvent
