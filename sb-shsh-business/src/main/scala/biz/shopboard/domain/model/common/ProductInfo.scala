package biz.shopboard.domain.model.common

case class ProductInfo(name: String, description: String, category: String)
