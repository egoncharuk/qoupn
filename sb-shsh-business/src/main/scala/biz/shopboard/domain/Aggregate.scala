package biz.shopboard.domain

import com.fasterxml.jackson.annotation.JsonIgnore

trait Aggregate {
  @JsonIgnore
  def id: String
  @JsonIgnore
  def version: Long
}
