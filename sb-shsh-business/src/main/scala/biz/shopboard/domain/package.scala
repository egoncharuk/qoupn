package biz.shopboard

import scalaz._
import Scalaz._
import scalaz.{ Failure, Success, Validation }

package object domain {

  type AggregateValidation[E <: Aggregate, A] = Validation[AggregateError[E], A]
  type DomainError = List[String]
  type DomainValidation[A] = Validation[DomainError, A]

  object DomainError {
    def apply(error: String): DomainError = List(error)
    def apply(errors: List[String]): DomainError = errors
  }

  case class AggregateError[E <: Aggregate](msg: String, agg: Option[E])

  object AggregateError {
    def apply[E <: Aggregate](msg: String): AggregateError[E] = AggregateError[E](msg, None)
    def apply[E <: Aggregate](msg: String, agg: E): AggregateError[E] = AggregateError[E](msg, Some(agg))
  }

  class DV[A](validation: DomainValidation[A]) {
    def &&(other: DomainValidation[A]): DomainValidation[A] = (this.validation, other) match {
      case (Success(a1), Success(a2)) => a2.success
      case (Success(a1), Failure(a2)) => a2.fail
      case (Failure(a1), Success(a2)) => a1.fail
      case (Failure(a1), Failure(a2)) => DomainError(a1 ::: a2).fail
    }
  }

  def append(value: String) = if (!value.isEmpty) value + ", " else ""
  val emailRegex = """(?i)\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b""".r

  implicit def toDV[A](validation: DomainValidation[A]) = new DV(validation)
}
