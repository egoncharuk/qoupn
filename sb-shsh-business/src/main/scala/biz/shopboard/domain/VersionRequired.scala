package biz.shopboard.domain

import scalaz._
import Scalaz._
import java.util.logging.Level
import java.util.logging.Logger
import com.fasterxml.jackson.annotation.JsonIgnore

object VersionMessages {
  val invalidVersionMessage = "%s instance with id %s was updated by other client. Expected version %s doesn't match current version %s."
}

trait VersionRequired[T <: Aggregate] { this: Aggregate =>

  @JsonIgnore
  val LOGGER: Logger = Logger.getLogger("VersionRequired")

  def versionOption = if (version == -1L) None else Some(version)

  def invalidVersion(className: String, id: String, expected: Long, current: Long) =
    LOGGER.log(Level.WARNING, VersionMessages.invalidVersionMessage format (className, id, expected, current))

  def requireVersion(expectedVersion: Option[Long]): DomainValidation[T] = expectedVersion match {
    case Some(expected) =>
      this.version == expected match {
        case true => success(this).asInstanceOf[DomainValidation[T]]
        case false => {
          invalidVersion(this.getClass.getSimpleName, this.id, expected, this.version)
          success(this).asInstanceOf[DomainValidation[T]]
        }
      }
    case None => success(this).asInstanceOf[DomainValidation[T]]
  }
}
