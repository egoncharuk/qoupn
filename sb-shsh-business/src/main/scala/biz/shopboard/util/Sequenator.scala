package biz.shopboard.util

class Sequenator(start: Int) {
  var ids = List(start)

  def current: Int = ids.head
  def next: Int = { ids = (ids.head + 1) :: ids; ids.head }
}

object Sequenator {
  implicit def apply(num: Int) = new Sequenator(num)
}

