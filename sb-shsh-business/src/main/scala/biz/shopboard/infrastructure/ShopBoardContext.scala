package biz.shopboard.infrastructure

import biz.shopboard.service._
import akka.actor.{ Props, ActorSystem }
import com.mongodb.casbah.Imports._
import org.eligosource.eventsourced.core._
import concurrent.stm.Ref
import akka.util.Timeout
import biz.shopboard.domain.model.user.{CompositeShoppingListID, ActionHash, ShoppingList, User}
import biz.shopboard.domain.model.merchant.{ Deal, Campaign, Merchant }
import biz.shopboard.state.TotalEmitter
import biz.shopboard.util.Sequenator
import org.joda.time.DateTime
import scala.concurrent.duration._
import biz.shopboard.ConfigContext
import biz.shopboard.journal.MongoDBCasbahJournalProps

object ShopBoardContext {

  var mailService: MailService = _

  var userService: UserService = _
  var checkoutService: CheckoutService = _
  var merchantService: MerchantService = _
  var userEventService: UserEventService = _
  var productSearchService: ProductSearchService = _
  var dealSubscriptionService: DealSubscriptionService = _
  var couponSubscriptionService: CouponSubscriptionService = _

  private implicit var system: ActorSystem = _

  def start(contextPath: String = "") {

    /////////////////////////////////////////////////////////////////
    // Akka + DB journaling for Eventsourced

    implicit val timeout = Timeout(10000)
    system = ActorSystem("eventsourced")

    val postfix = ConfigContext.getDbNamePostfix
    val journal = Journal(MongoDBCasbahJournalProps(MongoClient(), "eventsourced" + postfix, "events", "snapshots"))
    val extension = EventsourcingExtension(system, journal)

    /////////////////////////////////////////////////////////////////
    // Data

    val usersRef = Ref(Map.empty[String, User])
    val actionHashesRef = Ref(Map.empty[String, ActionHash])
    val merchantsRef = Ref(Map.empty[String, Merchant])
    val shoppingListsRef = Ref(Map.empty[String, ShoppingList])
    val shoppingListsToDealsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val shoppingListsToCouponsRef = Ref(Map.empty[CompositeShoppingListID, List[String]])
    val checkoutRef = Ref(Map.empty[String, (String, String, DateTime)])
    val campaignsRef = Ref(Map.empty[String, Campaign])
    val couponsRef = Ref(Map.empty[String, Deal])
    val dealsRef = Ref(Map.empty[String, Deal])

    /////////////////////////////////////////////////////////////////
    // Main Processors

    val mainProcessors: Sequenator = 1
    val channels: Sequenator = 1

    val userProcessor = extension.processorOf(Props(new UserProcessor(usersRef, actionHashesRef, merchantsRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = mainProcessors.current
    }), Some("userProcessor"))
    /*
      UserProcessor emits:
        emitter sendEvent UserCreated(userId, userDetails)                    ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent UserDetailsUpdated(userId, userDetails)             ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent UserRemoved(userId)                                 ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent UserSubscribed(userId, merchantId, user)
          ---> CouponSubscriptionService / DealSubscriptionService both are listening and after resending next 2 events
            --> emitter sendEvent DistributeCoupons(event.merchantId, event.userId) --> listened by MerchantService
            --> emitter sendEvent DistributeDeals(event.merchantId, event.userId)   --> listened by MerchantService
        emitter sendEvent UserUnsubscribed(userId, merchantId)
          ---> CouponSubscriptionService / DealSubscriptionService both are listening, but do nothing afterwards, they just change inner state.
        emitter sendEvent ShoppingListCreated(userId, shoppingListId, shoppingListDetails)          ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent ShoppingListDetailsUpdated(userId, shoppingListId, shoppingListDetails)   ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent ShoppingListRemoved(userId, shoppingListId)                               ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent ShoppingListItemAdded(userId, shoppingListId, shoppingListItem)           ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent ShoppingListItemUpdated(userId, shoppingListId, shoppingListItem)         ---> no one is listening for this specifically ( but this is TotalEmmiter )
        emitter sendEvent ShoppingListItemRemoved(userId, shoppingListId, shoppingListItemId)       ---> no one is listening for this specifically ( but this is TotalEmmiter )

        as well there are 4 linking events emited, and as usually no one is interested in them (this is TotalEmmiter - so all channels will get them)
          ShoppingListAndCouponLinked
          ShoppingListAndCouponUnlinked
          ShoppingListAndDealLinked
          ShoppingListAndDealUnlinked
     */
    val mailerActorRef = system.actorOf(Props(new MailerGateway(mailService, userProcessor, ConfigContext.getEmailActionUrl, contextPath) with Receiver with Confirm),
      "mailerActor")

    val merchantProcessor = extension.processorOf(Props(new MerchantProcessor(merchantsRef, campaignsRef, couponsRef, dealsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = mainProcessors.next
    }), Some("merchantProcessor"))
    /*
    MerchantProcessor emits
      (NEXT ONE Is originated from: [[[dealSubscriptionProjection]]]--->DistributeDeals)
      emitter sendEvent DealDistributed(merchantId, campaign.id, did, userId);
        and right after all Deals are distributed:
        emitter sendEvent DealsDistributed -(to confirm that all went well)   ---> no one is listening for this specifically ( but this is TotalEmmiter )
      emitter sendEvent CouponDistributed(merchantId, campaign.id, did, userId);
        and right after all Coupons are distributed:
        emitter sendEvent CouponsDistributed -(to confirm that all went well) ---> no one is listening for this specifically ( but this is TotalEmmiter )
      emitter sendEvent MerchantCreated                                       ---> no one is listening for this specifically ( but this is TotalEmmiter )
      emitter sendEvent MerchantDetailsUpdated(merchantId, merchantDetails)   ---> no one is listening for this specifically ( but this is TotalEmmiter )
      emitter sendEvent MerchantRemoved(merchantId)                           ---> no one is listening for this specifically ( but this is TotalEmmiter )
     */

    val checkoutProcessor = extension.processorOf(Props(new CheckoutProcessor(checkoutRef) with TotalEmitter with Confirm with Eventsourced {
      val id = mainProcessors.next
    }), Some("checkoutProcessor"))
    /*
      CheckoutProcessor emits:
        emitter sendEvent ShoppingListCheckedOut(userId, shoppingListId)      ---> no one is listening for this specifically ( but this is TotalEmmiter )
     */

    // Secondary Processors

    val dependantProcessors: Sequenator = mainProcessors.next

    val productSearchProjection = extension.processorOf(Props(new ProductSearchProjection with Receiver with Confirm with Eventsourced {
      val id = dependantProcessors.current
    }), Some("productSearchProcessor"))
    /*
    productSearchProjection -> does not emit anything
     */

    val userEventProjection = extension.processorOf(Props(new UserEventProjection with Receiver with Confirm with Eventsourced {
      val id = dependantProcessors.next
    }), Some("userEventProjection"))
    /*
    does not emmits, but just answers calee with all events
     */

    val dealSubscriptionProjection = extension.processorOf(Props(new DealSubscriptionProjection(dealsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = dependantProcessors.next
    }), Some("dealSubscriptionProjection"))
    /*
    dealSubscriptionProjection emmits
      emitter sendEvent DistributeDeals(event.merchantId, event.userId) ----> MerchantService
    */
    val couponSubscriptionProjection = extension.processorOf(Props(new CouponSubscriptionProjection(couponsRef) with TotalEmitter with Confirm with Eventsourced {
      val id = dependantProcessors.next
    }), Some("couponSubscriptionProjection"))
    /*
    couponSubscriptionProjection emmits
      emitter sendEvent DistributeCoupons(event.merchantId, event.userId) ----> MerchantService
    */

    /////////////////////////////////////////////////////////////////
    // Channels setup

    extension.channelOf(ReliableChannelProps(channels.current, userEventProjection))
    extension.channelOf(ReliableChannelProps(channels.next, dealSubscriptionProjection))
    extension.channelOf(ReliableChannelProps(channels.next, couponSubscriptionProjection))
    extension.channelOf(ReliableChannelProps(channels.next, productSearchProjection))
    extension.channelOf(ReliableChannelProps(channels.next, merchantProcessor))
    extension.channelOf(ReliableChannelProps(channels.next, mailerActorRef).withName("mailer"))

    /////////////////////////////////////////////////////////////////
    // Recovery

    extension.recover(mainProcessors.ids.map(ReplayParams(_)), 5 minutes)
    extension.awaitProcessing(mainProcessors.ids.toSet) // wait for processors to complete replay

    extension.recover(dependantProcessors.ids.map(ReplayParams(_)), 5 minutes)
    extension.awaitProcessing(dependantProcessors.ids.toSet) // wait for processors to complete replay

    /////////////////////////////////////////////////////////////////
    // Services creation

    checkoutService = new CheckoutService(checkoutRef, shoppingListsToDealsRef, shoppingListsToCouponsRef,
      couponSubscriptionProjection, dealSubscriptionProjection, checkoutProcessor)
    merchantService = new MerchantService(merchantsRef, campaignsRef, couponsRef, dealsRef, merchantProcessor)
    userService = new UserService(usersRef, actionHashesRef, shoppingListsRef, shoppingListsToDealsRef, shoppingListsToCouponsRef, userProcessor)
    productSearchService = new ProductSearchService(productSearchProjection)
    dealSubscriptionService = new DealSubscriptionService(dealSubscriptionProjection)
    couponSubscriptionService = new CouponSubscriptionService(couponSubscriptionProjection)
    userEventService = new UserEventService(userEventProjection)
  }

  def cleanup() {
    MongoConnection("localhost")("eventsourced" + ConfigContext.getDbNamePostfix).dropDatabase()
  }

  def stop() {
    system.shutdown()
    system.awaitTermination()
  }

  def isTerminated: Boolean = system.isTerminated
}
