package biz.shopboard.infrastructure

import biz.shopboard.domain._
import biz.shopboard.service._
import scala.concurrent.Await
import scala.concurrent.Future
import java.util.concurrent.TimeoutException
import biz.shopboard._
import scalaz._
import Scalaz._

trait FacadeProvided extends ValidationMessages {

  def userService: UserService = ShopBoardContext.userService
  def checkoutService: CheckoutService = ShopBoardContext.checkoutService
  def merchantService: MerchantService = ShopBoardContext.merchantService
  def userEventService: UserEventService = ShopBoardContext.userEventService
  def productSearchService: ProductSearchService = ShopBoardContext.productSearchService
  def dealSubscriptionService: DealSubscriptionService = ShopBoardContext.dealSubscriptionService
  def couponSubscriptionService: CouponSubscriptionService = ShopBoardContext.couponSubscriptionService

  def execute[T](f: Future[DomainValidation[T]]): DomainValidation[T] = {
    try { Await.result(f, duration) } catch { case te: TimeoutException => DomainError(general_system_fail).fail }
  }

}
