package biz.shopboard.infrastructure

trait ValidationMessages {
  // --------------------------------------------------------------------------------
  //  General messages
  // --------------------------------------------------------------------------------
  val general_system_fail = "Please retry after 5 seconds!"

  // --------------------------------------------------------------------------------
  //  MerchantProcessor & Services related messages
  // --------------------------------------------------------------------------------
  val merchantIdNotExistMsg = "Merchant %s does not exist"
  val campaignIdAlreadyExistMsg = "Compaign %s already exists"
  val dealIdAlreadyExistMsg = "Deal %s already exists"
  val couponIdAlreadyExistMsg = "Coupon %s already exists"
  val merchantWithIdAlreadyExistMsg = "Merchant with id = %s already exists"
  val merchantWithAdminIdAlreadyExistMsg = "Merchant with admin id = %s already exists"
  val merchantIdNotExits = "Merchant %s does not exist"
  val campaignWithIdAlreadyExist = "Campaign %s already exists"
  val campaignWithIdNotExist = "Campaign %s does not exist"
  val dealWithIdNotExist = "Deal %s does not exist"
  val couponWithIdNotExist = "Coupon %s does not exist"
  val invalid_date_range = "Invalid date range!"
  val invalid_date_range_wrt_deals = "Invalid date range with respect to linked deals!"
  val invalid_date_range_wrt_coupons = "Invalid date range with respect to linked coupons!"
  val cannotDeactivateNonActiveCampaign = "Non-Active Campaign %s cannot be deactivated!"
  val cannotDeactivateNonActiveDeal = "Non-Active Deal %s cannot be deactivated!"
  val cannotDeactivateNonActiveCoupon = "Non-Active Coupon %s cannot be deactivated!"
  val cannotActivateActiveCampaign = "Active Campaign %s cannot be activated twice!"
  val cannotActivateNonDraftDeal = "Non-Draft Deal %s cannot be activated!"
  val cannotActivateNonDraftCoupon = "Non-Draft Coupon %s cannot be activated!"
  val cannotActivateNonInactiveDeal = "Non-Inactive Deal %s cannot be activated!"
  val cannotActivateNonInactiveCoupon = "Non-Inactive Coupon %s cannot be activated!"
  val cannotUpdateNonDraftCampaign = "Non-Draft Campaign %s cannot be updated"
  val cannotUpdateNonDraftDeal = "Non-Draft Deal/Coupon %s cannot be updated"
  val cannotDistributeNonActiveDeal = "Non-Active Deal %s cannot be distributed"
  val cannotDistributeNonActiveCoupon = "Non-Active Coupon %s cannot be distributed"
  val date_range_out_of_campaign_date_range = "Date range is out of campaign date range!"

  // --------------------------------------------------------------------------------
  //  UserProcessor & Services related messages
  // --------------------------------------------------------------------------------
  val userIdNotExistMsg = "User %s does not exist"
  val actionHashKeyNotExistMsg = "Link %s is invalid"
  val actionLinkExpiredMsg = "Link expired"
  val actionLinkWrongType = "Link is of wrong type"
  val shopListIdAlreadyExistMsg = "Shopping list %s already exists"
  val userWithIdAlreadyExistMsg = "User with id = %s already exists"
  val userCreateErrorMsg = "Error creating user with id = %s"
  val userIdNotExist = "User %s does not exist"
  val shoppingListWithIdAlreadyExist = "ShoppingList %s already exists"
  val shoppingListWithIdNotExist = "ShoppingList %s does not exist"
  val shoppingListDoesNotHaveItem = "ShoppingList %s does not have item %s"
  val invalid_email = "Email is invalid!"
  val userNotFoundByEmail = "No user found by provided email!"

  // --------------------------------------------------------------------------------
  //  CouponSubscriptionService related messages
  // --------------------------------------------------------------------------------
  val couponsAreRedeemedSuccessfully = "Coupons are redeemed successfully!"
}
